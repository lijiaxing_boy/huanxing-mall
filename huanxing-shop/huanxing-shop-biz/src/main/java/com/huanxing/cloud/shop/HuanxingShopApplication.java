package com.huanxing.cloud.shop;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 商城店铺启动类
 *
 * @author lijx
 * @since 2022/2/26 16:38
 */
@EnableDubbo
@SpringBootApplication
public class HuanxingShopApplication {

	public static void main(String[] args) {
		SpringApplication.run(HuanxingShopApplication.class, args);
	}

}
