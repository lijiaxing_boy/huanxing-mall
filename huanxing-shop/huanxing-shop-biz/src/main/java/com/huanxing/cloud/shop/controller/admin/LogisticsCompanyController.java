package com.huanxing.cloud.shop.controller.admin;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.common.log.annotation.SysLog;
import com.huanxing.cloud.shop.api.entity.LogisticsCompany;
import com.huanxing.cloud.shop.service.ILogisticsCompanyService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 物流公司
 *
 * @author lijx
 * @date 2023/1/10
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/logisticscompany")
@Tag(description = "logisticscompany", name = "物流公司")
public class LogisticsCompanyController {

	private final ILogisticsCompanyService logisticsCompanyService;

	@Operation(summary = "物流公司列表")
	@SaCheckPermission("mall:logisticscompany:page")
	@GetMapping("/page")
	public Result<IPage<LogisticsCompany>> page(Page page, LogisticsCompany logisticsCompany) {
		IPage<LogisticsCompany> iPage = logisticsCompanyService.page(page, Wrappers.query(logisticsCompany));
		return Result.success(iPage);
	}

	@Operation(summary = "查询全部物流公司")
	@SaCheckPermission("mall:logisticscompany:get")
	@GetMapping("/list")
	public Result<List<LogisticsCompany>> getList(LogisticsCompany logisticsCompany) {
		return Result.success(logisticsCompanyService.list(Wrappers.query(logisticsCompany)));
	}

	@Operation(summary = "物流公司查询")
	@SaCheckPermission("mall:logisticscompany:get")
	@GetMapping("/{id}")
	public Result<LogisticsCompany> getById(@PathVariable("id") String id) {
		return Result.success(logisticsCompanyService.getById(id));
	}

	@SysLog("新增物流公司")
	@Operation(summary = "物流公司新增")
	@SaCheckPermission("mall:logisticscompany:add")
	@PostMapping
	public Result<Boolean> add(@RequestBody LogisticsCompany logisticsCompany) {
		return Result.success(logisticsCompanyService.save(logisticsCompany));
	}

	@SysLog("修改物流公司")
	@Operation(summary = "物流公司修改")
	@SaCheckPermission("mall:logisticscompany:edit")
	@PutMapping
	public Result<Boolean> edit(@RequestBody LogisticsCompany logisticsCompany) {
		return Result.success(logisticsCompanyService.updateById(logisticsCompany));
	}

	@SysLog("删除物流公司")
	@Operation(summary = "物流公司删除")
	@SaCheckPermission("mall:logisticscompany:del")
	@DeleteMapping("/{id}")
	public Result<Boolean> del(@PathVariable("id") String id) {
		return Result.success(logisticsCompanyService.removeById(id));
	}

}
