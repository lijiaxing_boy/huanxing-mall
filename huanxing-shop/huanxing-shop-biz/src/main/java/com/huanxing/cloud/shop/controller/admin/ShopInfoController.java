package com.huanxing.cloud.shop.controller.admin;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.common.log.annotation.SysLog;
import com.huanxing.cloud.shop.api.entity.ShopInfo;
import com.huanxing.cloud.shop.service.IShopInfoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 店铺
 *
 * @author lijx
 * @date 2024/05/07 13:07
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/shopinfo")
@Tag(description = "shopinfo", name = "店铺")
public class ShopInfoController {

	private final IShopInfoService shopInfoService;

	@Operation(summary = "店铺列表")
	@SaCheckPermission("shop:shopinfo:page")
	@GetMapping("/page")
	public Result<IPage<ShopInfo>> page(Page page, ShopInfo shopInfo) {
		IPage<ShopInfo> iPage = shopInfoService.page(page, Wrappers.query(shopInfo));
		return Result.success(iPage);
	}

	@Operation(summary = "店铺查询")
	@SaCheckPermission("shop:shopinfo:get")
	@GetMapping("/{id}")
	public Result<ShopInfo> getById(@PathVariable String id) {
		return Result.success(shopInfoService.getById(id));
	}

	@SysLog("新增店铺")
	@Operation(summary = "店铺新增")
	@SaCheckPermission("shop:shopinfo:add")
	@PostMapping
	public Result<Boolean> add(@RequestBody ShopInfo shopInfo) {
		return Result.success(shopInfoService.save(shopInfo));
	}

	@SysLog("修改店铺")
	@Operation(summary = "店铺修改")
	@SaCheckPermission("shop:shopinfo:edit")
	@PutMapping
	public Result<Boolean> edit(@RequestBody ShopInfo shopInfo) {
		return Result.success(shopInfoService.updateById(shopInfo));
	}

	@SysLog("删除店铺")
	@Operation(summary = "店铺删除")
	@SaCheckPermission("shop:shopinfo:del")
	@DeleteMapping("/{id}")
	public Result<Boolean> del(@PathVariable String id) {
		return Result.success(shopInfoService.removeById(id));
	}

	@Operation(summary = "全部店铺")
	@SaCheckPermission("shop:shopinfo:get")
	@GetMapping("/list")
	public Result<List<ShopInfo>> list(ShopInfo shopInfo) {
		return Result.success(shopInfoService.list(Wrappers.query(shopInfo)));
	}

}
