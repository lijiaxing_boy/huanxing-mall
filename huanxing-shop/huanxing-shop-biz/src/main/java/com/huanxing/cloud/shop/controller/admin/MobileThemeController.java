package com.huanxing.cloud.shop.controller.admin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.common.log.annotation.SysLog;
import com.huanxing.cloud.shop.api.entity.MobileTheme;
import com.huanxing.cloud.shop.service.IMobileThemeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 商城移动端主题
 *
 * @author lijx
 * @since 2022/3/17 10:55
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/mobiletheme")
@Tag(description = "mobiletheme", name = "商城移动端主题")
public class MobileThemeController {

	private final IMobileThemeService mobileThemeService;

	@Operation(summary = "商城移动端主题分页列表")
	@GetMapping("/page")
	public Result<IPage<MobileTheme>> page(Page page, MobileTheme mobileTheme) {
		IPage<MobileTheme> iPage = mobileThemeService.page(page, Wrappers.lambdaQuery(mobileTheme));
		return Result.success(iPage);
	}

	@Operation(summary = "查询全部商城移动端主题")
	@GetMapping("/list")
	public Result<List<MobileTheme>> list(MobileTheme mobileTheme) {
		return Result.success(mobileThemeService.list(Wrappers.lambdaQuery(mobileTheme)));
	}

	@Operation(summary = "商城移动端主题修改")
	@SysLog("修改商城移动端主题")
	@PutMapping
	public Result<Boolean> edit(@RequestBody MobileTheme mobileTheme) {
		return Result.success(mobileThemeService.updateById(mobileTheme));
	}

}
