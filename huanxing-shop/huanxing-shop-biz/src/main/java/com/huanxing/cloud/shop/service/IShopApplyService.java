package com.huanxing.cloud.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.shop.api.dto.ShopApplyVerifyDTO;
import com.huanxing.cloud.shop.api.entity.ShopApply;
import com.huanxing.cloud.shop.api.vo.ShopApplyVO;

/**
 * 店铺入驻
 *
 * @author lijx
 * @date 2024/05/07 13:07
 */
public interface IShopApplyService extends IService<ShopApply> {

	/**
	 * 审核
	 * @param shopApplyVerifyDTO 申请审核
	 * @return boolean
	 */
	boolean verify(ShopApplyVerifyDTO shopApplyVerifyDTO);

	/**
	 * 申请
	 * @param shopApply
	 * @return
	 */
	boolean saveApply(ShopApply shopApply);

	ShopApplyVO getShopApply();

}
