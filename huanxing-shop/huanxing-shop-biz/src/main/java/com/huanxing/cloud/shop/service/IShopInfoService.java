package com.huanxing.cloud.shop.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.shop.api.dto.ShopInfoDTO;
import com.huanxing.cloud.shop.api.entity.ShopInfo;
import com.huanxing.cloud.shop.api.vo.ShopInfoVO;

import java.util.List;

/**
 * 店铺
 *
 * @author lijx
 * @date 2024/05/07 13:07
 */
public interface IShopInfoService extends IService<ShopInfo> {

	/**
	 * 分页查询
	 * @param page
	 * @param shopInfoDTO
	 * @return
	 */
	IPage<ShopInfoVO> getPage(Page page, ShopInfoDTO shopInfoDTO);

	/**
	 * 根据ids查询店铺信息
	 * @param ids
	 * @return
	 */
	List<ShopInfoVO> getShopByIds(List<String> ids);

}
