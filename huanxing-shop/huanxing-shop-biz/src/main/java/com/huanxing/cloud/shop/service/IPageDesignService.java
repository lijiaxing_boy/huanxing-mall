package com.huanxing.cloud.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.shop.api.entity.PageDesign;

/**
 * 页面设计
 *
 * @author lijx
 * @date 2022/12/07
 */
public interface IPageDesignService extends IService<PageDesign> {

	/**
	 * @param request 页面设计
	 * @return
	 */
	PageDesign getHomePage(PageDesign request);

	/**
	 * 修改页面设计
	 * @param pageDesign 页面设计
	 * @return
	 */
	boolean updatePageDesignById(PageDesign pageDesign);

}
