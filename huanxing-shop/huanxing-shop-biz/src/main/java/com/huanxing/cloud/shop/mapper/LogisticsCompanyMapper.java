package com.huanxing.cloud.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanxing.cloud.shop.api.entity.LogisticsCompany;
import org.apache.ibatis.annotations.Mapper;

/**
 * 物流公司
 *
 * @author lijx
 * @since 2023/1/10
 */
@Mapper
public interface LogisticsCompanyMapper extends BaseMapper<LogisticsCompany> {

}
