package com.huanxing.cloud.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.shop.api.entity.LogisticsCompany;

/**
 * 物流公司
 *
 * @author lijx
 * @date 2023/1/10
 */
public interface ILogisticsCompanyService extends IService<LogisticsCompany> {

}
