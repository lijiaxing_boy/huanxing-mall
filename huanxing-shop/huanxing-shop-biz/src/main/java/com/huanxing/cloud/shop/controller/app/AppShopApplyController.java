package com.huanxing.cloud.shop.controller.app;

import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.shop.api.entity.ShopApply;
import com.huanxing.cloud.shop.api.vo.ShopApplyVO;
import com.huanxing.cloud.shop.service.IShopApplyService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * 店铺入驻
 *
 * @author lijx
 * @date 2024/05/07 13:07
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/app/apply")
@Tag(description = "app-apply", name = "店铺入驻-API")
public class AppShopApplyController {

	private final IShopApplyService shopApplyService;

	@PostMapping
	public Result<Boolean> save(@RequestBody @Valid ShopApply shopApply) {
		return Result.success(shopApplyService.saveApply(shopApply));
	}

	@GetMapping
	public Result<ShopApplyVO> getShopApply() {
		return Result.success(shopApplyService.getShopApply());
	}

}
