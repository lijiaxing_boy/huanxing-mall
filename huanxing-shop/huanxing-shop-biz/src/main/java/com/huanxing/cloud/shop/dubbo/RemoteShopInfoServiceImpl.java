package com.huanxing.cloud.shop.dubbo;

import com.huanxing.cloud.shop.api.remote.RemoteShopInfoService;
import com.huanxing.cloud.shop.api.vo.ShopInfoVO;
import com.huanxing.cloud.shop.service.IShopInfoService;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lijia
 * @description
 * @date 2024/11/23
 */
@Service
@DubboService
@RequiredArgsConstructor
public class RemoteShopInfoServiceImpl implements RemoteShopInfoService {

	private final IShopInfoService shopInfoService;

	@Override
	public List<ShopInfoVO> getShopByIds(List<String> shopIds) {
		return shopInfoService.getShopByIds(shopIds);
	}

}
