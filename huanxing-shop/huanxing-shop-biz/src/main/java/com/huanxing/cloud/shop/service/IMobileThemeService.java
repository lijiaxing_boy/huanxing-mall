package com.huanxing.cloud.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.shop.api.entity.MobileTheme;

/**
 * 商城移动端主题
 *
 * @author lijx
 * @since 2022/3/17 11:02
 */
public interface IMobileThemeService extends IService<MobileTheme> {

	/**
	 * 获取移动端主题
	 * @return
	 */
	MobileTheme getMobileTheme(String tenantId);

}
