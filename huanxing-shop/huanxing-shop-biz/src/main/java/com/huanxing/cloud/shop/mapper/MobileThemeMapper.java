package com.huanxing.cloud.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanxing.cloud.shop.api.entity.MobileTheme;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商城移动端主题
 *
 * @author lijx
 * @since 2022/3/17 10:58
 */
@Mapper
public interface MobileThemeMapper extends BaseMapper<MobileTheme> {

}
