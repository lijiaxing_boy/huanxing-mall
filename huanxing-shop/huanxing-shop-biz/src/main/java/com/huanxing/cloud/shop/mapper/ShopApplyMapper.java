package com.huanxing.cloud.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanxing.cloud.shop.api.entity.ShopApply;
import org.apache.ibatis.annotations.Mapper;

/**
 * 店铺入驻
 *
 * @author lijx
 * @date 2024/05/07 13:07
 */
@Mapper
public interface ShopApplyMapper extends BaseMapper<ShopApply> {

}
