package com.huanxing.cloud.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanxing.cloud.shop.api.entity.PageDesign;
import org.apache.ibatis.annotations.Mapper;

/**
 * 页面设计
 *
 * @author lijx
 * @date 2022/12/07
 */
@Mapper
public interface PageDesignMapper extends BaseMapper<PageDesign> {

}
