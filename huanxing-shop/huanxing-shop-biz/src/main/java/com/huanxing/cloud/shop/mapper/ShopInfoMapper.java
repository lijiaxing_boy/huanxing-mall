package com.huanxing.cloud.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huanxing.cloud.shop.api.dto.ShopInfoDTO;
import com.huanxing.cloud.shop.api.entity.ShopInfo;
import com.huanxing.cloud.shop.api.vo.ShopInfoVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 店铺
 *
 * @author lijx
 * @date 2024/05/07 13:07
 */
@Mapper
public interface ShopInfoMapper extends BaseMapper<ShopInfo> {

	/**
	 * 分页查询
	 * @param page
	 * @param shopInfoDTO
	 * @return
	 */
	IPage<ShopInfoVO> selectShopPage(Page page, @Param("query") ShopInfoDTO shopInfoDTO);

}
