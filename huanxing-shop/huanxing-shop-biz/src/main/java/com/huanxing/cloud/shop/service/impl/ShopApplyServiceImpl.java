package com.huanxing.cloud.shop.service.impl;

import cn.dev33.satoken.secure.SaSecureUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huanxing.cloud.common.core.constant.CommonConstants;
import com.huanxing.cloud.common.security.util.SecurityUtils;
import com.huanxing.cloud.shop.api.constants.ShopConstants;
import com.huanxing.cloud.shop.api.dto.ShopApplyVerifyDTO;
import com.huanxing.cloud.shop.api.entity.ShopApply;
import com.huanxing.cloud.shop.api.entity.ShopInfo;
import com.huanxing.cloud.shop.api.vo.ShopApplyVO;
import com.huanxing.cloud.shop.mapper.ShopApplyMapper;
import com.huanxing.cloud.shop.mapper.ShopInfoMapper;
import com.huanxing.cloud.shop.service.IShopApplyService;
import com.huanxing.cloud.upms.api.dto.SysUserReqDTO;
import com.huanxing.cloud.upms.api.remote.RemoteSysUserService;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

/**
 * 店铺入驻
 *
 * @author lijx
 * @date 2024/05/07 13:07
 */
@Service
@RequiredArgsConstructor
public class ShopApplyServiceImpl extends ServiceImpl<ShopApplyMapper, ShopApply> implements IShopApplyService {

	@DubboReference
	private final RemoteSysUserService remoteSysUserService;

	private final ShopInfoMapper shopInfoMapper;

	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean verify(ShopApplyVerifyDTO request) {
		ShopApply shopApply = this.getById(request.getId());
		if (Objects.isNull(shopApply)) {
			return Boolean.FALSE;
		}
		if (!shopApply.getVerifyStatus().equals(ShopConstants.VERIFY_STATUS_0)) {
			return Boolean.FALSE;
		}
		if (request.getVerifyStatus().equals(ShopConstants.VERIFY_STATUS_2)) {
			// 创建店铺
			ShopInfo shopInfo = new ShopInfo();
			BeanUtils.copyProperties(shopApply, shopInfo);
			shopInfo.setType(ShopConstants.SHOP_TYPE_2);
			shopInfo.setStatus(CommonConstants.NORMAL_STATUS);
			shopInfoMapper.insert(shopInfo);
			// 创建账号
			SysUserReqDTO sysUserReqDTO = new SysUserReqDTO();
			sysUserReqDTO.setShopId(shopInfo.getShopId());
			sysUserReqDTO.setPassword(shopApply.getPassword());
			sysUserReqDTO.setPhone(shopApply.getTelephone());
			sysUserReqDTO.setUsername(shopApply.getUsername());
			sysUserReqDTO.setType(CommonConstants.YES);
			remoteSysUserService.createUser(sysUserReqDTO);
		}
		shopApply.setVerifyStatus(request.getVerifyStatus());
		shopApply.setVerifyDesc(request.getVerifyDesc());
		return this.updateById(shopApply);
	}

	@Override
	public boolean saveApply(ShopApply shopApply) {
		String userId = SecurityUtils.getUser().getUserId();
		long count = this.count(Wrappers.<ShopApply>lambdaQuery().eq(ShopApply::getApplyUserId, userId));
		if (count > 0) {
			throw new RuntimeException("您已提交过申请，请勿重复申请");
		}
		shopApply.setPassword(SaSecureUtil.md5(shopApply.getPassword()));
		shopApply.setVerifyStatus(ShopConstants.VERIFY_STATUS_0);
		shopApply.setApplyUserId(userId);
		return this.save(shopApply);
	}

	@Override
	public ShopApplyVO getShopApply() {
		String userId = SecurityUtils.getUser().getUserId();
		ShopApply shopApply = this.getOne(Wrappers.<ShopApply>lambdaQuery().eq(ShopApply::getApplyUserId, userId));
		if (Objects.isNull(shopApply)) {
			return null;
		}
		ShopApplyVO shopApplyVO = new ShopApplyVO();
		BeanUtils.copyProperties(shopApply, shopApplyVO);
		return shopApplyVO;
	}

}
