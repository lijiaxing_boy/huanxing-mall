package com.huanxing.cloud.shop.controller.app;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.shop.api.dto.ShopInfoDTO;
import com.huanxing.cloud.shop.api.entity.ShopInfo;
import com.huanxing.cloud.shop.api.vo.ShopInfoVO;
import com.huanxing.cloud.shop.service.IShopInfoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 店铺
 *
 * @author lijx
 * @date 2024/05/07 13:07
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/app/shopinfo")
@Tag(description = "app-shopinfo", name = "店铺-API")
public class AppShopInfoController {

	private final IShopInfoService shopInfoService;

	@Operation(summary = "店铺列表")
	@GetMapping("/page")
	public Result<IPage<ShopInfoVO>> page(Page page, ShopInfoDTO shopInfoDTO) {
		IPage<ShopInfoVO> iPage = shopInfoService.getPage(page, shopInfoDTO);
		return Result.success(iPage);
	}

	@Operation(summary = "店铺查询")
	@GetMapping("/{id}")
	public Result<ShopInfo> getById(@PathVariable String id) {
		return Result.success(shopInfoService.getById(id));
	}

}
