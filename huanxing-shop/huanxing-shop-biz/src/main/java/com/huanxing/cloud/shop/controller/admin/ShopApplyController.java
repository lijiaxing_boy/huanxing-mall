package com.huanxing.cloud.shop.controller.admin;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.common.log.annotation.SysLog;
import com.huanxing.cloud.shop.api.constants.ShopConstants;
import com.huanxing.cloud.shop.api.dto.ShopApplyVerifyDTO;
import com.huanxing.cloud.shop.api.entity.ShopApply;
import com.huanxing.cloud.shop.service.IShopApplyService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * 店铺入驻
 *
 * @author lijx
 * @date 2024/05/07 13:07
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/apply")
@Tag(description = "apply", name = "店铺入驻")
public class ShopApplyController {

	private final IShopApplyService shopApplyService;

	@Operation(summary = "店铺入驻列表")
	@SaCheckPermission("shop:shopapply:page")
	@GetMapping("/page")
	public Result<IPage<ShopApply>> page(Page page, ShopApply shopApply) {
		IPage<ShopApply> iPage = shopApplyService.page(page, Wrappers.query(shopApply));
		return Result.success(iPage);
	}

	@Operation(summary = "店铺入驻查询")
	@SaCheckPermission("shop:shopapply:get")
	@GetMapping("/{id}")
	public Result<ShopApply> getById(@PathVariable String id) {
		return Result.success(shopApplyService.getById(id));
	}

	@SysLog("新增店铺入驻单")
	@Operation(summary = "店铺入驻单新增")
	@SaCheckPermission("shop:shopapply:add")
	@PostMapping
	public Result<Boolean> add(@RequestBody ShopApply shopApply) {
		shopApply.setVerifyStatus(ShopConstants.VERIFY_STATUS_0);
		return Result.success(shopApplyService.save(shopApply));
	}

	@SysLog("修改店铺入驻单")
	@Operation(summary = "店铺入驻单修改")
	@SaCheckPermission("shop:shopapply:edit")
	@PutMapping
	public Result<Boolean> edit(@RequestBody ShopApply shopApply) {
		return Result.success(shopApplyService.updateById(shopApply));
	}

	@SysLog("删除店铺入驻单")
	@Operation(summary = "店铺入驻单删除")
	@SaCheckPermission("shop:shopapply:del")
	@DeleteMapping("/{id}")
	public Result<Boolean> del(@PathVariable String id) {
		return Result.success(shopApplyService.removeById(id));
	}

	@SysLog("审核店铺入驻单")
	@Operation(summary = "店铺入驻单审核")
	@SaCheckPermission("shop:shopapply:verify")
	@PostMapping("/verify")
	public Result<Boolean> verify(@RequestBody ShopApplyVerifyDTO shopApplyVerifyDTO) {
		return Result.success(shopApplyService.verify(shopApplyVerifyDTO));
	}

}
