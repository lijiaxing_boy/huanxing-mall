package com.huanxing.cloud.shop.api.remote;

import com.huanxing.cloud.shop.api.vo.ShopInfoVO;

import java.util.List;

/**
 * @author lijx
 */
public interface RemoteShopInfoService {

	/**
	 * 根据shopId获取店铺信息
	 * @param shopIds 店铺id
	 * @return 店铺列表
	 */
	List<ShopInfoVO> getShopByIds(List<String> shopIds);

}
