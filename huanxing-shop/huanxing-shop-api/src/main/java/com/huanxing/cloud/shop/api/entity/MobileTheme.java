package com.huanxing.cloud.shop.api.entity;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.huanxing.cloud.common.myabtis.handler.JsonObjTypeHandler;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.ibatis.type.JdbcType;

import java.time.LocalDateTime;

/**
 * 商城移动端主题
 *
 * @author lijx
 * @since 2022/3/17 10:52
 */
@Data
@Schema(description = "商城移动端主题")
@EqualsAndHashCode(callSuper = true)
@TableName(value = "mobile_theme")
public class MobileTheme extends Model<MobileTheme> {

	@Schema(description = "PK")
	@TableId(type = IdType.ASSIGN_ID)
	private String id;

	@Schema(description = "主题名称")
	private String name;

	@Schema(description = "主颜色")
	private String themeColor;

	@Schema(description = "配色")
	private String matchColor;

	@Schema(description = "底色")
	private String bottomColor;

	@Schema(description = "使用：0.否；1.是")
	private String isUse;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建人")
	private String createBy;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改人")
	private String updateBy;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建时间")
	private LocalDateTime createTime;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改时间")
	private LocalDateTime updateTime;

	@TableLogic
	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "逻辑删除：0.显示；1.隐藏；")
	private String delFlag;

	@Schema(description = "底部导航颜色")
	private String tabbarColor;

	@Schema(description = "底部导航选中颜色")
	private String tabbarSelectedColor;

	@Schema(description = "tabBar上边框的颜色， 仅支持 black/white")
	private String tabbarBorderStyle;

	@Schema(description = "tabBar背景色，HexColor")
	private String tabbarBgColor;

	@Schema(description = "tabBar明细")
	@TableField(typeHandler = JsonObjTypeHandler.class, jdbcType = JdbcType.VARCHAR)
	private JSONObject tabbarItem;

	@Schema(description = "租户id")
	private String tenantId;

}
