package com.huanxing.cloud.shop.api.vo;

import com.huanxing.cloud.product.api.entity.GoodsSpu;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Accessors(chain = true)
public class ShopInfoVO implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;

	@Schema(description = "PK")
	private String shopId;

	@Schema(description = "店铺名称")
	private String name;

	@Schema(description = "联系电话")
	private String telephone;

	@Schema(description = "店铺logo")
	private String logoUrl;

	@Schema(description = "状态：0.正常；1.已关闭；")
	private String status;

	@Schema(description = "详细地址")
	private String detailAddress;

	@Schema(description = "创建时间")
	private LocalDateTime createTime;

	@Schema(description = "店铺类型：1.自营；2.三方入驻；")
	private String type;

	@Schema(description = "商品信息")
	private List<GoodsSpu> goodsSpuList;

}
