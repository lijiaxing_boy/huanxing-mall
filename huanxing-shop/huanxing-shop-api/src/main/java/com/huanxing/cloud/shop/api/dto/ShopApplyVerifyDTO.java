package com.huanxing.cloud.shop.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 店铺入驻
 *
 * @author lijx
 * @since 2023/6/28 10:20
 */
@Data
public class ShopApplyVerifyDTO {

	@Schema(description = "PK")
	private String id;

	@Schema(description = "审核状态：0.审核中；1.审核驳回；2.审核通过；")
	private String verifyStatus;

	@Schema(description = "审核描述")
	private String verifyDesc;

}
