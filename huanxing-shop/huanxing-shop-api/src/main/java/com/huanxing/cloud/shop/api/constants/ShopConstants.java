package com.huanxing.cloud.shop.api.constants;

/**
 * @author MAX
 * @description
 * @date 2024/05/07 14:32
 */
public interface ShopConstants {

	// 审核状态：0.审核中；1.审核驳回；2.审核通过；
	String VERIFY_STATUS_0 = "0";

	String VERIFY_STATUS_1 = "1";

	String VERIFY_STATUS_2 = "2";

	// 店铺类型：1.自营；2.三方入驻；
	String SHOP_TYPE_1 = "1";

	String SHOP_TYPE_2 = "2";

}
