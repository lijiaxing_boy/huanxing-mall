package com.huanxing.cloud.shop.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 店铺自提点
 *
 * @author lijx
 * @since 2023/6/28 10:20
 */
@Data
@Schema(description = "店铺自提点")
@EqualsAndHashCode(callSuper = true)
@TableName(value = "shop_self_fetch")
public class ShopSelfFetch extends Model<ShopSelfFetch> {

	@Schema(description = "PK")
	@TableId(type = IdType.ASSIGN_ID)
	private String id;

	@Schema(description = "店铺id")
	private String shopId;

	@Schema(description = "自提点名称")
	private String name;

	@Schema(description = "联系电话")
	private String telephone;

	@Schema(description = "省编码")
	private String provinceCode;

	@Schema(description = "省名称")
	private String provinceName;

	@Schema(description = "市编码")
	private String cityCode;

	@Schema(description = "市名称")
	private String cityName;

	@Schema(description = "区编码")
	private String districtCode;

	@Schema(description = "区名称")
	private String districtName;

	@Schema(description = "维度")
	private String latitude;

	@Schema(description = "经度")
	private String longitude;

	@Schema(description = "详细地址")
	private String address;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建人")
	private String createBy;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改人")
	private String updateBy;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建时间")
	private LocalDateTime createTime;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改时间")
	private LocalDateTime updateTime;

	@TableLogic
	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "逻辑删除：0.显示；1.隐藏；")
	private String delFlag;

	@Schema(description = "租户id")
	private String tenantId;

	@Schema(description = "自提点照片")
	private String photoUrl;

	@Schema(description = "状态：0.正常；1.关闭；")
	private String status;

}
