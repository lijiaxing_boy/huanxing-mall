package com.huanxing.cloud.shop.api.entity;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.huanxing.cloud.common.myabtis.handler.JsonObjTypeHandler;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.JdbcType;

import java.time.LocalDateTime;

/**
 * 页面设计
 *
 * @author lijx
 * @date 2022/12/07
 */
@Data
@Schema(description = "页面设计")
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName(value = "page_design")
public class PageDesign extends Model<PageDesign> {

	@Schema(description = "PK")
	@TableId(type = IdType.ASSIGN_ID)
	private String id;

	@Schema(description = "页面名称")
	private String pageName;

	@Schema(description = "页面内容")
	@TableField(typeHandler = JsonObjTypeHandler.class, jdbcType = JdbcType.VARCHAR)
	private JSONObject pageContent;

	@Schema(description = "状态：0.正常；1.停用；")
	private String status;

	@Schema(description = "首页页面：0.否；1.是；")
	private String homeStatus;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建人")
	private String createBy;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改人")
	private String updateBy;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建时间")
	private LocalDateTime createTime;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改时间")
	private LocalDateTime updateTime;

	@TableLogic
	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "逻辑删除：0.显示；1.隐藏；")
	private String delFlag;

	@Schema(description = "租户id")
	private String tenantId;

	@Schema(description = "店铺ID")
	private String shopId;

}
