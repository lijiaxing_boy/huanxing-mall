package com.huanxing.cloud.shop.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 店铺入驻
 *
 * @author lijx
 * @since 2023/6/28 10:20
 */
@Data
@Schema(description = "店铺入驻")
@EqualsAndHashCode(callSuper = true)
@TableName(value = "shop_apply")
public class ShopApply extends Model<ShopApply> {

	@Schema(description = "PK")
	@TableId(type = IdType.ASSIGN_ID)
	private String id;

	@Schema(description = "店铺名称")
	@NotEmpty(message = "店铺名称不能为空")
	private String name;

	@Schema(description = "联系电话")
	@NotEmpty(message = "联系电话不能为空")
	private String telephone;

	@Schema(description = "店铺logo")
	private String logoUrl;

	@Schema(description = "审核状态：0.审核中；1.审核驳回；2.审核通过；")
	private String verifyStatus;

	@Schema(description = "审核描述")
	private String verifyDesc;

	@Schema(description = "详细地址")
	@NotEmpty(message = "详细地址不能为空")
	private String detailAddress;

	@Schema(description = "用户名")
	@NotEmpty(message = "用户名不能为空")
	private String username;

	@Schema(description = "密码")
	@NotEmpty(message = "密码不能为空")
	private String password;

	@Schema(description = "店铺ID")
	private String shopId;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建人")
	private String createBy;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改人")
	private String updateBy;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建时间")
	private LocalDateTime createTime;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改时间")
	private LocalDateTime updateTime;

	@TableLogic
	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "逻辑删除：0.显示；1.隐藏；")
	private String delFlag;

	@Schema(description = "租户id")
	private String tenantId;

	@Schema(description = "申请人ID")
	private String applyUserId;

}
