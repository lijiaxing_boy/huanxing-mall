package com.huanxing.cloud.shop.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class ShopInfoDTO {

	@Schema(description = "店铺名称")
	private String name;

	@Schema(description = "状态：0.正常；1.已关闭；")
	private String status;

}
