package com.huanxing.cloud.shop.api.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class ShopApplyVO implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;

	@Schema(description = "PK")
	private String id;

	@Schema(description = "店铺名称")
	@NotEmpty(message = "店铺名称不能为空")
	private String name;

	@Schema(description = "联系电话")
	@NotEmpty(message = "联系电话不能为空")
	private String telephone;

	@Schema(description = "店铺logo")
	private String logoUrl;

	@Schema(description = "审核状态：0.审核中；1.审核驳回；2.审核通过；")
	private String verifyStatus;

	@Schema(description = "审核描述")
	private String verifyDesc;

	@Schema(description = "详细地址")
	@NotEmpty(message = "详细地址不能为空")
	private String detailAddress;

	@Schema(description = "用户名")
	@NotEmpty(message = "用户名不能为空")
	private String username;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建人")
	private String createBy;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建时间")
	private LocalDateTime createTime;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改时间")
	private LocalDateTime updateTime;

	@Schema(description = "申请人ID")
	private String applyUserId;

}
