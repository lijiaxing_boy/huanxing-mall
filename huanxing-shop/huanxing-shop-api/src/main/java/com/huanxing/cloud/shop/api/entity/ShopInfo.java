package com.huanxing.cloud.shop.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 店铺信息
 *
 * @author lijx
 * @since 2023/6/28 10:20
 */
@Data
@Schema(description = "店铺信息")
@EqualsAndHashCode(callSuper = true)
@TableName(value = "shop_info")
public class ShopInfo extends Model<ShopInfo> {

	@Schema(description = "PK")
	@TableId(type = IdType.ASSIGN_ID)
	private String shopId;

	@Schema(description = "店铺名称")
	private String name;

	@Schema(description = "联系电话")
	private String telephone;

	@Schema(description = "店铺logo")
	private String logoUrl;

	@Schema(description = "状态：0.正常；1.停用；")
	private String status;

	@Schema(description = "详细地址")
	private String detailAddress;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建人")
	private String createBy;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改人")
	private String updateBy;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建时间")
	private LocalDateTime createTime;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改时间")
	private LocalDateTime updateTime;

	@TableLogic
	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "逻辑删除：0.显示；1.隐藏；")
	private String delFlag;

	@Schema(description = "租户id")
	private String tenantId;

	@Schema(description = "店铺类型：1.自营；2.三方入驻；")
	private String type;

}
