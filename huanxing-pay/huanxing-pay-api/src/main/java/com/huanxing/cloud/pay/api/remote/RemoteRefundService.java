package com.huanxing.cloud.pay.api.remote;

import com.huanxing.cloud.pay.api.dto.CreateRefundsReqDTO;

/**
 * @author lijx
 */
public interface RemoteRefundService {

	Object refunds(CreateRefundsReqDTO createRefundsReqDTO);

}
