package com.huanxing.cloud.pay.api.remote;

import com.huanxing.cloud.pay.api.dto.CreateOrderReqDTO;

/**
 * @author lijx
 */
public interface RemotePayService {

	Object createOrder(CreateOrderReqDTO createOrderReqDTO);

}
