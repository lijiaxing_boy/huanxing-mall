package com.huanxing.cloud.pay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.pay.api.entity.PayRefundOrder;

public interface IPayRefundOrderService extends IService<PayRefundOrder> {

}
