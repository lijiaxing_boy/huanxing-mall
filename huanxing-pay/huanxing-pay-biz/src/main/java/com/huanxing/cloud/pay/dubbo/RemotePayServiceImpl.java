package com.huanxing.cloud.pay.dubbo;

import com.huanxing.cloud.pay.api.dto.CreateOrderReqDTO;
import com.huanxing.cloud.pay.api.remote.RemotePayService;
import com.huanxing.cloud.pay.handler.PayOrderHandler;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author lijia
 * @description
 * @date 2024/11/23
 */
@Service
@DubboService
@RequiredArgsConstructor
public class RemotePayServiceImpl implements RemotePayService {

	private final Map<String, PayOrderHandler> payOrderHandlerMap;

	@Override
	public Object createOrder(CreateOrderReqDTO createOrderReqDTO) {
		return payOrderHandlerMap.get(createOrderReqDTO.getTradeType()).pay(createOrderReqDTO);
	}

}
