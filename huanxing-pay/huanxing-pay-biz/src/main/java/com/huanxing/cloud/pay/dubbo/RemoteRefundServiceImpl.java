package com.huanxing.cloud.pay.dubbo;

import com.huanxing.cloud.pay.api.dto.CreateRefundsReqDTO;
import com.huanxing.cloud.pay.api.remote.RemoteRefundService;
import com.huanxing.cloud.pay.handler.PayRefundOrderHandler;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author lijia
 * @description
 * @date 2024/11/23
 */
@Service
@DubboService
@RequiredArgsConstructor
public class RemoteRefundServiceImpl implements RemoteRefundService {

	private final Map<String, PayRefundOrderHandler> payRefundOrderHandlerMap;

	@Override
	public Object refunds(CreateRefundsReqDTO createRefundsReqDTO) {
		return payRefundOrderHandlerMap.get(createRefundsReqDTO.getRefundType()).refund(createRefundsReqDTO);
	}

}
