package com.huanxing.cloud.pay.handler;

import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.pay.api.dto.CreateOrderReqDTO;
import com.huanxing.cloud.pay.api.entity.PayTradeOrder;

public interface PayOrderHandler {

	/**
	 * 创建订单
	 * @param createOrderReqDTO 预支付参数
	 * @return obj
	 */
	PayTradeOrder createOrder(CreateOrderReqDTO createOrderReqDTO);

	/**
	 * 调起渠道支付
	 * @param createOrderReqDTO 预支付参数
	 * @return obj
	 */
	Object pay(CreateOrderReqDTO createOrderReqDTO);

}
