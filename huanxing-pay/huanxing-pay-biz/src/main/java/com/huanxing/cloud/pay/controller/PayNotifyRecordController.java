package com.huanxing.cloud.pay.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.binarywang.wxpay.bean.notify.WxPayNotifyV3Response;
import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.common.myabtis.tenant.HxTenantContextHolder;
import com.huanxing.cloud.pay.api.constants.PayConstants;
import com.huanxing.cloud.pay.api.entity.PayNotifyRecord;
import com.huanxing.cloud.pay.service.IPayNotifyRecordService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/notify")
@RequiredArgsConstructor
@Tag(description = "notify", name = "支付回调")
public class PayNotifyRecordController {

	private final IPayNotifyRecordService payNotifyRecordService;

	@Operation(summary = "微信支付回调通知")
	@PostMapping("/wx/{tenantId}")
	public String notifyOrderWx(@PathVariable("tenantId") String tenantId, @RequestBody String notifyData) {
		if (!StringUtils.hasText(tenantId)) {
			return WxPayNotifyV3Response.fail("无效的tenantId");
		}
		HxTenantContextHolder.setTenantId(tenantId);
		return payNotifyRecordService.wxPayNotify(tenantId, notifyData);
	}

	@Operation(summary = "支付宝支付回调")
	@PostMapping("/alipay/{tenantId}")
	public String notifyOrderAli(@PathVariable("tenantId") String tenantId, HttpServletRequest request) {
		if (!StringUtils.hasText(tenantId)) {
			return PayConstants.ALIPAY_FAIL;
		}
		HxTenantContextHolder.setTenantId(tenantId);
		return payNotifyRecordService.aliPayNotify(tenantId, request);
	}

	@Operation(summary = "微信退款回调通知")
	@PostMapping("/refund/wx/{tenantId}")
	public String notifyRefundOrderWx(@PathVariable("tenantId") String tenantId, @RequestBody String notifyData) {
		if (!StringUtils.hasText(tenantId)) {
			return WxPayNotifyV3Response.fail("无效的tenantId");
		}
		HxTenantContextHolder.setTenantId(tenantId);
		return payNotifyRecordService.wxPayRefundNotify(tenantId, notifyData);
	}

	@Operation(summary = "通过id查询")
	@SaCheckPermission("pay:paynotifyrecord:get")
	@GetMapping("/{id}")
	public Result getById(@PathVariable("id") String id) {
		return Result.success(payNotifyRecordService.getById(id));
	}

	@Operation(summary = "分页查询")
	@SaCheckPermission("pay:paynotifyrecord:page")
	@GetMapping("/page")
	public Result getPage(Page page, PayNotifyRecord payNotifyRecord) {
		return Result.success(payNotifyRecordService.page(page, Wrappers.query(payNotifyRecord)));
	}

}
