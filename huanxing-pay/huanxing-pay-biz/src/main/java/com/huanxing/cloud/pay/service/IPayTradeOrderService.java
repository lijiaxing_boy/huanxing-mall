package com.huanxing.cloud.pay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.pay.api.entity.PayTradeOrder;

/**
 * 支付订单
 *
 * @author lijx
 * @date 2022/6/16
 */
public interface IPayTradeOrderService extends IService<PayTradeOrder> {

}
