package com.huanxing.cloud.pay.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huanxing.cloud.pay.mapper.PayRefundOrderMapper;
import com.huanxing.cloud.pay.service.IPayRefundOrderService;
import com.huanxing.cloud.pay.api.entity.PayRefundOrder;
import org.springframework.stereotype.Service;

@Service
public class PayRefundOrderServiceImpl extends ServiceImpl<PayRefundOrderMapper, PayRefundOrder>
		implements IPayRefundOrderService {

}
