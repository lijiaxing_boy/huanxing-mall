package com.huanxing.cloud.pay.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huanxing.cloud.pay.mapper.PayTradeOrderMapper;
import com.huanxing.cloud.pay.service.IPayTradeOrderService;
import com.huanxing.cloud.pay.api.entity.PayTradeOrder;
import org.springframework.stereotype.Service;

/**
 * 支付订单
 *
 * @author lijx
 * @date 2022/6/16
 */
@Service
public class PayTradeOrderServiceImpl extends ServiceImpl<PayTradeOrderMapper, PayTradeOrder>
		implements IPayTradeOrderService {

}
