package com.huanxing.cloud.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanxing.cloud.pay.api.entity.PayNotifyRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * 支付订单
 *
 * @author lijx
 * @date 2022/6/16
 */
@Mapper
public interface PayNotifyRecordMapper extends BaseMapper<PayNotifyRecord> {

}
