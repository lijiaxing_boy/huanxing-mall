package com.huanxing.cloud.pay.config;

import com.alipay.api.AlipayClient;
import com.alipay.api.CertAlipayRequest;
import com.alipay.api.DefaultAlipayClient;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.huanxing.cloud.pay.service.IPayConfigService;
import com.huanxing.cloud.pay.api.constants.PayConstants;
import com.huanxing.cloud.pay.api.entity.PayConfig;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;

/**
 * 支付宝配置
 *
 * @author lijx
 * @since 2022/3/14 15:24
 */
@Configuration
@AllArgsConstructor
@ConditionalOnClass(AliPayConfiguration.class)
public class AliPayConfiguration {

	private static IPayConfigService payConfigService;

	@Autowired
	public AliPayConfiguration(IPayConfigService payConfigService) {
		AliPayConfiguration.payConfigService = payConfigService;
	}

	/**
	 * 通用sdk
	 * @return
	 * @throws Exception
	 */
	public static AlipayClient getAlipayClient() throws Exception {
		PayConfig payConfig = payConfigService
			.getOne(Wrappers.<PayConfig>query().lambda().eq(PayConfig::getType, PayConstants.PAY_TYPE_2));
		if (null == payConfig) {
			throw new IllegalArgumentException("当前租户没有配置支付宝");
		}

		CertAlipayRequest certAlipayRequest = new CertAlipayRequest();
		// 支付宝网关（固定）。
		certAlipayRequest.setServerUrl("https://openapi.alipay.com/gateway.do");
		// APPID 即创建应用后生成。
		certAlipayRequest.setAppId(payConfig.getAppId());
		// 开发者私钥，由开发者自己生成。
		certAlipayRequest.setPrivateKey(payConfig.getMchKey());
		// 参数返回格式，只支持 JSON（固定）。
		certAlipayRequest.setFormat("json");
		// 编码集，支持 GBK/UTF-8。
		certAlipayRequest.setCharset("utf-8");
		// 生成签名字符串所使用的签名算法类型，目前支持 RSA2 算法。
		certAlipayRequest.setSignType("RSA2");
		// 应用公钥证书文件本地路径。
		certAlipayRequest.setCertPath(payConfig.getPrivateCertPath());
		// 支付宝公钥证书文件本地路径。
		certAlipayRequest.setAlipayPublicCertPath(payConfig.getPrivateKeyPath());
		// 支付宝根证书文件本地路径。
		certAlipayRequest.setRootCertPath(payConfig.getKeyPath());
		certAlipayRequest.setEncryptor("0vcoqyTeSz26tW6wiwltfw==");
		certAlipayRequest.setEncryptType("AES");
		return new DefaultAlipayClient(certAlipayRequest);
	}

}
