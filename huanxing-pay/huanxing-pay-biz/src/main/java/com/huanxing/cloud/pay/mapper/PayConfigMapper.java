package com.huanxing.cloud.pay.mapper;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanxing.cloud.pay.api.entity.PayConfig;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 支付配置
 *
 * @author lijx
 * @date 2022/6/16
 */
@Mapper
public interface PayConfigMapper extends BaseMapper<PayConfig> {

	/**
	 * 通过appid查询支付配置
	 * @param appId
	 * @return
	 */
	@InterceptorIgnore(tenantLine = "true")
	PayConfig selectByAppId(@Param("appId") String appId);

}
