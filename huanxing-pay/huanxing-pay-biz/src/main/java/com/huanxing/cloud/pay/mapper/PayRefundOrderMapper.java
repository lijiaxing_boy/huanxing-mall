package com.huanxing.cloud.pay.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanxing.cloud.pay.api.entity.PayRefundOrder;
import org.apache.ibatis.annotations.Mapper;

/**
 * 退款订单
 *
 * @author lijx
 * @date 2022/6/16
 */
@Mapper
public interface PayRefundOrderMapper extends BaseMapper<PayRefundOrder> {

}
