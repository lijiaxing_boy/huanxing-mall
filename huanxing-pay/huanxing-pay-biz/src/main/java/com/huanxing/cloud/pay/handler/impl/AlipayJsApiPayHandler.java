package com.huanxing.cloud.pay.handler.impl;

import cn.hutool.json.JSONObject;
import com.alipay.api.AlipayClient;
import com.alipay.api.request.AlipayTradeCreateRequest;
import com.alipay.api.response.AlipayTradeCreateResponse;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.huanxing.cloud.common.core.constant.CommonConstants;
import com.huanxing.cloud.common.myabtis.tenant.HxTenantContextHolder;
import com.huanxing.cloud.common.security.handler.HxBusinessException;
import com.huanxing.cloud.pay.api.dto.CreateOrderReqDTO;
import com.huanxing.cloud.pay.api.entity.PayTradeOrder;
import com.huanxing.cloud.pay.api.enums.PayTradeTypeEnum;
import com.huanxing.cloud.pay.config.AliPayConfiguration;
import com.huanxing.cloud.pay.handler.AbstractPayOrderHandler;
import com.huanxing.cloud.pay.service.IPayTradeOrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service("ALI_JSAPI_PAY")
@RequiredArgsConstructor
public class AlipayJsApiPayHandler extends AbstractPayOrderHandler {

	private final IPayTradeOrderService payTradeOrderService;

	@Override
	public Object doPay(PayTradeOrder payTradeOrder) {
		try {
			AlipayClient alipayClient = AliPayConfiguration.getAlipayClient();
			JSONObject bizContent = new JSONObject();
			/****** 必传参数 ******/
			bizContent.set("out_trade_no", payTradeOrder.getOutTradeNo());
			bizContent.set("total_amount", payTradeOrder.getAmount());
			bizContent.set("subject", payTradeOrder.getDescription());
			AlipayTradeCreateRequest alipayTradeCreateRequest = new AlipayTradeCreateRequest();
			alipayTradeCreateRequest
				.setNotifyUrl(payTradeOrder.getNotifyUrl() + "/pay/notify/ali/" + HxTenantContextHolder.getTenantId());
			bizContent.set("buyer_id", payTradeOrder.getOpenId());
			alipayTradeCreateRequest.setBizContent(bizContent.toString());
			AlipayTradeCreateResponse alipayTradeCreateResponse = alipayClient
				.certificateExecute(alipayTradeCreateRequest);
			if (!alipayTradeCreateResponse.isSuccess()) {
				throw new HxBusinessException(
						alipayTradeCreateResponse.getMsg() + alipayTradeCreateResponse.getSubMsg());
			}
			return alipayTradeCreateResponse;
		}
		catch (Exception e) {
			throw new HxBusinessException(e.getMessage());
		}
	}

	@Override
	public PayTradeOrder createOrder(CreateOrderReqDTO createOrderReqDTO) {
		// 先查询
		PayTradeOrder payTradeOrder = payTradeOrderService.getOne(Wrappers.<PayTradeOrder>lambdaQuery()
			.eq(PayTradeOrder::getOutTradeNo, createOrderReqDTO.getOutTradeNo()));
		if (null != payTradeOrder) {
			return payTradeOrder;
		}
		payTradeOrder = new PayTradeOrder();
		payTradeOrder.setPayStatus(CommonConstants.NO);
		payTradeOrder.setDescription(createOrderReqDTO.getSubject());
		payTradeOrder.setOpenId(createOrderReqDTO.getBuyerId());
		payTradeOrder.setOutTradeNo(createOrderReqDTO.getOutTradeNo());
		payTradeOrder.setTradeType(PayTradeTypeEnum.ALI_JSAPI_PAY.getName());
		payTradeOrder.setAmount(new BigDecimal(createOrderReqDTO.getTotalAmount()));
		payTradeOrder.setExtra(createOrderReqDTO.getExtra());
		payTradeOrderService.save(payTradeOrder);
		return payTradeOrder;
	}

}
