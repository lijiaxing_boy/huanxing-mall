package com.huanxing.cloud.pay.config;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.binarywang.wxpay.config.WxPayConfig;
import com.github.binarywang.wxpay.service.WxPayService;
import com.github.binarywang.wxpay.service.impl.WxPayServiceImpl;
import com.huanxing.cloud.pay.api.constants.PayConstants;
import com.huanxing.cloud.pay.api.entity.PayConfig;
import com.huanxing.cloud.pay.service.IPayConfigService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;

/**
 * 微信支付自动配置
 *
 * @author lijx
 * @since 2022/3/14 15:24
 */
@Configuration
@AllArgsConstructor
@ConditionalOnClass(WxPayService.class)
public class WxPayConfiguration {

	private static IPayConfigService payConfigService;

	@Autowired
	public WxPayConfiguration(IPayConfigService payConfigService) {
		WxPayConfiguration.payConfigService = payConfigService;
	}

	/**
	 * 构造微信支付服务对象.
	 * @return 微信支付service
	 */
	public static WxPayService wxPayService() {
		PayConfig payConfig = payConfigService
			.getOne(Wrappers.<PayConfig>lambdaQuery().eq(PayConfig::getType, PayConstants.PAY_TYPE_1));
		if (null == payConfig) {
			throw new IllegalArgumentException("未配置微信支付");
		}
		final WxPayServiceImpl wxPayService = new WxPayServiceImpl();
		WxPayConfig wxPayConfig = new WxPayConfig();
		wxPayConfig.setAppId(payConfig.getAppId());
		wxPayConfig.setMchId(payConfig.getMchId());
		wxPayConfig.setMchKey(payConfig.getMchKey());
		wxPayConfig.setKeyPath(payConfig.getKeyPath());
		// 以下是apiv3
		wxPayConfig.setPrivateKeyPath(payConfig.getPrivateKeyPath());
		wxPayConfig.setPrivateCertPath(payConfig.getPrivateCertPath());
		wxPayConfig.setCertSerialNo(payConfig.getCertSerialNo());
		wxPayConfig.setApiV3Key(payConfig.getApiv3Key());
		wxPayService.setConfig(wxPayConfig);
		return wxPayService;
	}

}
