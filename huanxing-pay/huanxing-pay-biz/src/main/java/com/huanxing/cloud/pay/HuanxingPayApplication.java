package com.huanxing.cloud.pay;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 支付模块启动类
 *
 * @author lijx
 * @since 2022/2/26 16:38
 */
@EnableDubbo
@SpringBootApplication
public class HuanxingPayApplication {

	public static void main(String[] args) {
		SpringApplication.run(HuanxingPayApplication.class, args);
	}

}
