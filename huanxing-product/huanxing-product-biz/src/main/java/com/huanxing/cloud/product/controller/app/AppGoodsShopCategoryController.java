package com.huanxing.cloud.product.controller.app;

import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.product.api.entity.GoodsShopCategory;
import com.huanxing.cloud.product.service.IGoodsShopCategoryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 店铺商品类目
 *
 * @author lijx
 * @since 2022/2/26 16:30
 */
@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/app/goodsshopcategory")
@Tag(description = "app-goodsshopcategory", name = "店铺商品类目-API")
public class AppGoodsShopCategoryController {

	private final IGoodsShopCategoryService goodsShopCategoryService;

	@Operation(summary = "树结构店铺商品类目列表")
	@GetMapping("/tree")
	public Result tree(GoodsShopCategory goodsShopCategory) {
		return Result.success(goodsShopCategoryService.getGoodsCategoryTreeList(goodsShopCategory));
	}

}
