package com.huanxing.cloud.product;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 商城商品启动类
 *
 * @author lijx
 * @since 2022/2/26 16:38
 */
@EnableDubbo
@SpringBootApplication
public class HuanxingProductApplication {

	public static void main(String[] args) {
		SpringApplication.run(HuanxingProductApplication.class, args);
	}

}
