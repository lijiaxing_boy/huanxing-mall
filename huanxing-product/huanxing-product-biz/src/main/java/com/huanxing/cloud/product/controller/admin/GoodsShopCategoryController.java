package com.huanxing.cloud.product.controller.admin;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.common.log.annotation.SysLog;
import com.huanxing.cloud.product.api.entity.GoodsShopCategory;
import com.huanxing.cloud.product.service.IGoodsShopCategoryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * 店铺商品类目
 *
 * @author lijx
 * @since 2022/2/26 16:30
 */
@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/goodsshopcategory")
@Tag(description = "goodsshopcategory", name = "店铺商品类目")
public class GoodsShopCategoryController {

	private final IGoodsShopCategoryService goodsShopCategoryService;

	@Operation(summary = "店铺商品类目列表")
	@SaCheckPermission("product:goodsshopcategory:page")
	@GetMapping("/page")
	public Result<IPage<GoodsShopCategory>> page(Page page, GoodsShopCategory goodsShopCategory) {
		return Result.success(goodsShopCategoryService.page(page, Wrappers.query(goodsShopCategory)));
	}

	@Operation(summary = "店铺商品类目查询")
	@SaCheckPermission("product:goodsshopcategory:get")
	@GetMapping("/list")
	public Result list(GoodsShopCategory goodsShopCategory) {
		return Result.success(goodsShopCategoryService.list(Wrappers.query(goodsShopCategory)));
	}

	@Operation(summary = "树结构店铺商品类目列表")
	@SaCheckPermission("product:goodsshopcategory:get")
	@GetMapping("/tree")
	public Result tree(GoodsShopCategory goodsShopCategory) {
		return Result.success(goodsShopCategoryService.getGoodsCategoryTreeList(goodsShopCategory));
	}

	@Operation(summary = "通过id查询店铺商品类目")
	@SaCheckPermission("product:goodsshopcategory:get")
	@GetMapping("/{id}")
	public Result getById(@PathVariable String id) {
		return Result.success(goodsShopCategoryService.getById(id));
	}

	@SysLog("新增店铺商品类目")
	@Operation(summary = "店铺商品类目新增")
	@SaCheckPermission("product:goodsshopcategory:add")
	@PostMapping
	public Result add(@RequestBody GoodsShopCategory goodsShopCategory) {
		return Result.success(goodsShopCategoryService.save(goodsShopCategory));
	}

	@SysLog("修改店铺商品类目")
	@Operation(summary = "店铺商品类目修改")
	@SaCheckPermission("product:goodsshopcategory:edit")
	@PutMapping
	public Result edit(@RequestBody GoodsShopCategory goodsShopCategory) {
		if (goodsShopCategory.getId().equals(goodsShopCategory.getParentId())) {
			return Result.fail("不能将本级设为父类");
		}
		return Result.success(goodsShopCategoryService.updateById(goodsShopCategory));
	}

	@SysLog("删除店铺商品类目")
	@Operation(summary = "店铺商品类目删除")
	@SaCheckPermission("product:goodsshopcategory:del")
	@DeleteMapping("/{id}")
	public Result del(@PathVariable String id) {
		long count = goodsShopCategoryService
			.count(Wrappers.<GoodsShopCategory>lambdaQuery().eq(GoodsShopCategory::getParentId, id));
		if (count > 0) {
			return Result.fail("存在下级类目，请先删除下级类目");
		}
		return Result.success(goodsShopCategoryService.removeById(id));
	}

}
