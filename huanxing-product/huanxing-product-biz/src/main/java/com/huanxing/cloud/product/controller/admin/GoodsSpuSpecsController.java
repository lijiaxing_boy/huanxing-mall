package com.huanxing.cloud.product.controller.admin;

import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.product.api.entity.GoodsSpuSpecs;
import com.huanxing.cloud.product.service.IGoodsSpuSpecsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 商品sku关联规格值
 *
 * @author lijx
 * @since 2022/3/1 10:13
 */
@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/goodsspuspecs")
@Tag(description = "goodsspuspecs", name = "商品规格")
public class GoodsSpuSpecsController {

	private final IGoodsSpuSpecsService goodsSpuSpecsService;

	@Operation(summary = "商品spu规格列表")
	@GetMapping("/list")
	public Result list(GoodsSpuSpecs goodsSpuSpecs) {
		return Result.success(goodsSpuSpecsService.getList(goodsSpuSpecs));
	}

}
