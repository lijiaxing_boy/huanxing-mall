package com.huanxing.cloud.product.dubbo;

import com.huanxing.cloud.product.api.remote.RemoteGoodsAppraiseService;
import com.huanxing.cloud.product.api.entity.GoodsAppraise;
import com.huanxing.cloud.product.service.IGoodsAppraiseService;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lijia
 * @description
 * @date 2024/11/22
 */
@Service
@DubboService
@RequiredArgsConstructor
public class RemoteGoodsAppraiseServiceImpl implements RemoteGoodsAppraiseService {

	private final IGoodsAppraiseService goodsAppraiseService;

	@Override
	public boolean addGoodsAppraise(List<GoodsAppraise> goodsAppraiseList) {
		return goodsAppraiseService.add(goodsAppraiseList);
	}

}
