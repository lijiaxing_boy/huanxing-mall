package com.huanxing.cloud.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huanxing.cloud.product.api.entity.GoodsAppraise;
import com.huanxing.cloud.product.mapper.GoodsAppraiseMapper;
import com.huanxing.cloud.product.service.IGoodsAppraiseService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 商品评价
 *
 * @author lijx
 * @since 2022/3/7 14:18
 */
@Service
public class GoodsAppraiseServiceImpl extends ServiceImpl<GoodsAppraiseMapper, GoodsAppraise>
		implements IGoodsAppraiseService {

	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean add(List<GoodsAppraise> listGoodsAppraise) {
		return this.saveBatch(listGoodsAppraise);
	}

}
