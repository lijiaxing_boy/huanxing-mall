package com.huanxing.cloud.product.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huanxing.cloud.product.api.entity.GoodsCollect;
import com.huanxing.cloud.product.api.entity.GoodsSpu;
import com.huanxing.cloud.product.api.vo.GoodsCollectVO;
import com.huanxing.cloud.product.mapper.GoodsCollectMapper;
import com.huanxing.cloud.product.mapper.GoodsSpuMapper;
import com.huanxing.cloud.product.service.IGoodsCollectService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class GoodsCollectServiceImpl extends ServiceImpl<GoodsCollectMapper, GoodsCollect>
		implements IGoodsCollectService {

	private final GoodsSpuMapper goodsSpuMapper;

	@Override
	public IPage<GoodsCollectVO> getPage(Page page, GoodsCollect userCollect) {
		return baseMapper.selectCollectPage(page, userCollect);
	}

	@Override
	public GoodsCollect saveCollect(GoodsCollect userCollect) {
		// 查询商品信息
		GoodsSpu goodsSpu = goodsSpuMapper.selectById(userCollect.getSpuId());
		if (Objects.nonNull(goodsSpu)) {
			userCollect.setSalesPrice(goodsSpu.getLowPrice());
		}
		baseMapper.insert(userCollect);
		return userCollect;
	}

}
