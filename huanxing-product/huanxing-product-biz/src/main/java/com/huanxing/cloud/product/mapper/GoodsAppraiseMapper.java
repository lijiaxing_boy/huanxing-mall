package com.huanxing.cloud.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanxing.cloud.product.api.entity.GoodsAppraise;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品评价
 *
 * @author lijx
 * @since 2022/3/10 17:09
 */
@Mapper
public interface GoodsAppraiseMapper extends BaseMapper<GoodsAppraise> {

}
