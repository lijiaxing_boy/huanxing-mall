package com.huanxing.cloud.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huanxing.cloud.product.api.entity.FreightTemplate;
import com.huanxing.cloud.product.mapper.FreightTemplateMapper;
import com.huanxing.cloud.product.service.IFreightTemplateService;
import org.springframework.stereotype.Service;

/**
 * 运费模板
 *
 * @author lijx
 * @since 2022/2/26 16:37
 */
@Service
public class FreightTemplateServiceImpl extends ServiceImpl<FreightTemplateMapper, FreightTemplate>
		implements IFreightTemplateService {

}
