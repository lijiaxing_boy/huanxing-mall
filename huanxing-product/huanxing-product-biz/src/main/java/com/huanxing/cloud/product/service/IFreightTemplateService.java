package com.huanxing.cloud.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.product.api.entity.FreightTemplate;

/**
 * 运费模板
 *
 * @author lijx
 * @since 2022/2/26 16:36
 */
public interface IFreightTemplateService extends IService<FreightTemplate> {

}
