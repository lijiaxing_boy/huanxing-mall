package com.huanxing.cloud.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huanxing.cloud.product.mapper.GoodsSpecsValueMapper;
import com.huanxing.cloud.product.service.IGoodsSpecsValueService;
import com.huanxing.cloud.product.api.entity.GoodsSpecsValue;
import org.springframework.stereotype.Service;

/**
 * 商品规格值
 *
 * @author lijx
 * @since 2022/2/26 16:37
 */
@Service
public class GoodsSpecsValueServiceImpl extends ServiceImpl<GoodsSpecsValueMapper, GoodsSpecsValue>
		implements IGoodsSpecsValueService {

}
