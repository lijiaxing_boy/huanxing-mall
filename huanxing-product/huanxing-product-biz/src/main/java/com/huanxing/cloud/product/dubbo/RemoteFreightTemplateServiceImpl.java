package com.huanxing.cloud.product.dubbo;

import com.huanxing.cloud.product.api.entity.FreightTemplate;
import com.huanxing.cloud.product.api.remote.RemoteFreightTemplateService;
import com.huanxing.cloud.product.service.IFreightTemplateService;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;

/**
 * @author lijia
 * @description
 * @date 2024/11/22
 */
@Service
@DubboService
@RequiredArgsConstructor
public class RemoteFreightTemplateServiceImpl implements RemoteFreightTemplateService {

	private final IFreightTemplateService freightTemplateService;

	@Override
	public FreightTemplate getById(String id) {
		return freightTemplateService.getById(id);
	}

}
