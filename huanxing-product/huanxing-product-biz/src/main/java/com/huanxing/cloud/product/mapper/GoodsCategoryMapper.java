package com.huanxing.cloud.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanxing.cloud.product.api.entity.GoodsCategory;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品类目
 *
 * @author lijx
 * @since 2022/2/26 16:32
 */
@Mapper
public interface GoodsCategoryMapper extends BaseMapper<GoodsCategory> {

}
