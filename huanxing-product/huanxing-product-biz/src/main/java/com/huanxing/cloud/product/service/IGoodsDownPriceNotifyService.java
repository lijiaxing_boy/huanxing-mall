package com.huanxing.cloud.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.product.api.entity.GoodsDownPriceNotify;

public interface IGoodsDownPriceNotifyService extends IService<GoodsDownPriceNotify> {

}
