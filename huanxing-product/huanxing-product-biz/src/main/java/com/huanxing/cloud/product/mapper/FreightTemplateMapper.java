package com.huanxing.cloud.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanxing.cloud.product.api.entity.FreightTemplate;
import org.apache.ibatis.annotations.Mapper;

/**
 * 运费模板
 *
 * @author lijx
 * @since 2022/2/26 16:32
 */
@Mapper
public interface FreightTemplateMapper extends BaseMapper<FreightTemplate> {

}
