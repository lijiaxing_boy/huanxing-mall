package com.huanxing.cloud.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanxing.cloud.product.api.entity.GoodsSpecsValue;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品规格值
 *
 * @author lijx
 * @since 2022/2/26 16:32
 */
@Mapper
public interface GoodsSpecsValueMapper extends BaseMapper<GoodsSpecsValue> {

}
