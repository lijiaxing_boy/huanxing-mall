package com.huanxing.cloud.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huanxing.cloud.product.mapper.GoodsSkuSpecsValueMapper;
import com.huanxing.cloud.product.service.IGoodsSkuSpecsValueService;
import com.huanxing.cloud.product.api.entity.GoodsSkuSpecsValue;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商品sku关联规格值
 *
 * @author lijx
 * @since 2022/2/26 16:37
 */
@Service
public class GoodsSkuSpecsValueServiceImpl extends ServiceImpl<GoodsSkuSpecsValueMapper, GoodsSkuSpecsValue>
		implements IGoodsSkuSpecsValueService {

	@Override
	public List<GoodsSkuSpecsValue> getBySkuId(String skuId) {
		return baseMapper.selectBySkuId(skuId);
	}

}
