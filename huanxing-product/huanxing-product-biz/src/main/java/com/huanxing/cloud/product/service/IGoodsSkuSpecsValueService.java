package com.huanxing.cloud.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.product.api.entity.GoodsSkuSpecsValue;

import java.util.List;

/**
 * 商品sku关联规格值
 *
 * @author lijx
 * @since 2022/2/26 16:32
 */
public interface IGoodsSkuSpecsValueService extends IService<GoodsSkuSpecsValue> {

	List<GoodsSkuSpecsValue> getBySkuId(String skuId);

}
