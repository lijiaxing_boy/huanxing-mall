package com.huanxing.cloud.product.controller.admin;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.common.log.annotation.SysLog;
import com.huanxing.cloud.product.api.entity.FreightTemplate;
import com.huanxing.cloud.product.service.IFreightTemplateService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * 运费模板
 *
 * @author lijx
 * @since 2022/2/23 13:11
 */
@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/freighttemplate")
@Tag(description = "freighttemplate", name = "运费模板")
public class FreightTemplateController {

	private final IFreightTemplateService freightTemplateService;

	@Operation(summary = "运费模板列表")
	@SaCheckPermission("product:freighttemplate:page")
	@GetMapping("/page")
	public Result<IPage<FreightTemplate>> page(Page page, FreightTemplate freightTemplate) {
		return Result.success(freightTemplateService.page(page, Wrappers.query(freightTemplate)));
	}

	@Operation(summary = "运费模板查询")
	@SaCheckPermission("product:freighttemplate:get")
	@GetMapping("/list")
	public Result<?> list(FreightTemplate freightTemplate) {
		return Result.success(freightTemplateService.list(Wrappers.query(freightTemplate)
			.lambda()
			.select(FreightTemplate::getId, FreightTemplate::getName, FreightTemplate::getShopId)));
	}

	@Operation(summary = "通过id查询运费模板")
	@SaCheckPermission("product:freighttemplate:get")
	@GetMapping("/{id}")
	public Result<?> getById(@PathVariable String id) {
		return Result.success(freightTemplateService.getById(id));
	}

	@SysLog("新增运费模板")
	@Operation(summary = "运费模板新增")
	@SaCheckPermission("product:freighttemplate:add")
	@PostMapping
	public Result<?> add(@RequestBody FreightTemplate freightTemplate) {
		return Result.success(freightTemplateService.save(freightTemplate));
	}

	@SysLog("修改运费模板")
	@Operation(summary = "运费模板修改")
	@SaCheckPermission("product:freighttemplate:edit")
	@PutMapping
	public Result<?> edit(@RequestBody FreightTemplate freightTemplate) {
		return Result.success(freightTemplateService.updateById(freightTemplate));
	}

	@SysLog("删除运费模板")
	@Operation(summary = "运费模板删除")
	@SaCheckPermission("product:freighttemplate:del")
	@DeleteMapping("/{id}")
	public Result<?> del(@PathVariable String id) {
		return Result.success(freightTemplateService.removeById(id));
	}

}
