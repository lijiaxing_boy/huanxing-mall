package com.huanxing.cloud.product.dubbo;

import com.huanxing.cloud.product.api.dto.GoodsSpuSalesVolumeReqDTO;
import com.huanxing.cloud.product.api.entity.GoodsSpu;
import com.huanxing.cloud.product.api.remote.RemoteGoodsSpuService;
import com.huanxing.cloud.product.service.IGoodsSpuService;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lijia
 * @description
 * @date 2024/11/22
 */
@Service
@DubboService
@RequiredArgsConstructor
public class RemoteGoodsSpuServiceImpl implements RemoteGoodsSpuService {

	private final IGoodsSpuService goodsSpuService;

	@Override
	public boolean updateSalesVolume(List<GoodsSpuSalesVolumeReqDTO> list) {
		list.forEach(v -> goodsSpuService.updateSalesVolume(v.getSpuId(), v.getSalesVolume()));
		return true;
	}

	@Override
	public GoodsSpu getBySpuId(String id) {
		return goodsSpuService.getApiSpuById(id);
	}

	@Override
	public List<GoodsSpu> getBySpuIds(List<String> ids) {
		return goodsSpuService.listByIds(ids);
	}

	@Override
	public List<GoodsSpu> getSpuListByShopIds(List<String> ids) {
		return goodsSpuService.listByShopIds(ids);
	}

}
