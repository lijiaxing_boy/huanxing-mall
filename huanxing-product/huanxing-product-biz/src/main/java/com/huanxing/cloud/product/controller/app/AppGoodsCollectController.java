package com.huanxing.cloud.product.controller.app;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.common.security.util.SecurityUtils;
import com.huanxing.cloud.product.api.entity.GoodsCollect;
import com.huanxing.cloud.product.service.IGoodsCollectService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/app/collect")
@Tag(description = "collect", name = "用户收藏-API")
public class AppGoodsCollectController {

	private final IGoodsCollectService userCollectService;

	@Operation(summary = "用户收藏列表")
	@GetMapping("/page")
	public Result page(Page page, GoodsCollect userCollect) {
		userCollect.setUserId(SecurityUtils.getUser().getUserId());
		return Result.success(userCollectService.getPage(page, userCollect));
	}

	@Operation(summary = "新增用户收藏")
	@PostMapping
	public Result save(@RequestBody GoodsCollect userCollect) {
		userCollect.setUserId(SecurityUtils.getUser().getUserId());
		return Result.success(userCollectService.saveCollect(userCollect));
	}

	@Operation(summary = "用户取消收藏")
	@DeleteMapping("/{id}")
	public Result delete(@PathVariable("id") String id) {
		return Result.success(userCollectService.removeById(id));
	}

}
