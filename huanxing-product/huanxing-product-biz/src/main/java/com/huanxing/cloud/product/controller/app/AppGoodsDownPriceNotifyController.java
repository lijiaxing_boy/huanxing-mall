package com.huanxing.cloud.product.controller.app;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huanxing.cloud.common.core.constant.CommonConstants;
import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.common.security.util.SecurityUtils;
import com.huanxing.cloud.product.api.entity.GoodsDownPriceNotify;
import com.huanxing.cloud.product.service.IGoodsDownPriceNotifyService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/app/goodsdownpricenotify")
@Tag(description = "app-goodsdownpricenotify", name = "商品降价通知-API")
public class AppGoodsDownPriceNotifyController {

	private final IGoodsDownPriceNotifyService goodsDownPriceNotifyService;

	@Operation(summary = "降价通知列表")
	@GetMapping("/page")
	public Result page(Page page, GoodsDownPriceNotify goodsDownPriceNotify) {
		goodsDownPriceNotify.setUserId(SecurityUtils.getUser().getUserId());
		return Result.success(goodsDownPriceNotifyService.page(page, Wrappers.query(goodsDownPriceNotify)));
	}

	@Operation(summary = "新增商品降价通知")
	@PostMapping
	public Result save(@RequestBody GoodsDownPriceNotify goodsDownPriceNotify) {
		goodsDownPriceNotify.setUserId(SecurityUtils.getUser().getUserId());
		goodsDownPriceNotify.setStatus(CommonConstants.NO);
		return Result.success(goodsDownPriceNotifyService.save(goodsDownPriceNotify));
	}

}
