package com.huanxing.cloud.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanxing.cloud.product.api.entity.GoodsDownPriceNotify;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GoodsDownPriceNotifyMapper extends BaseMapper<GoodsDownPriceNotify> {

}
