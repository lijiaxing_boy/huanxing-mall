package com.huanxing.cloud.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.product.api.entity.GoodsSpecs;

/**
 * 商品规格
 *
 * @author lijx
 * @since 2022/2/26 16:32
 */
public interface IGoodsSpecsService extends IService<GoodsSpecs> {

}
