package com.huanxing.cloud.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.product.api.entity.GoodsAppraise;

import java.util.List;

/**
 * 商品评价
 *
 * @author lijx
 * @since 2022/2/26 16:36
 */
public interface IGoodsAppraiseService extends IService<GoodsAppraise> {

	/**
	 * 用户发表评论
	 * @param listGoodsAppraise
	 * @return
	 */
	boolean add(List<GoodsAppraise> listGoodsAppraise);

}
