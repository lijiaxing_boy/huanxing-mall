package com.huanxing.cloud.product.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.product.api.entity.GoodsSpu;

import java.util.List;

/**
 * 商品spu
 *
 * @author lijx
 * @since 2022/2/22 15:03
 */
public interface IGoodsSpuService extends IService<GoodsSpu> {

	/**
	 * 分页查询商品
	 * @param page page
	 * @param goodsSpu goodsSpu
	 * @return IPage<GoodsSpu>
	 */
	IPage<GoodsSpu> adminPage(Page page, GoodsSpu goodsSpu);

	/**
	 * 分页查询商品库列表
	 * @param page page
	 * @param goodsSpu goodsSpu
	 * @return IPage<GoodsSpu>
	 */
	IPage<GoodsSpu> warehousePage(Page page, GoodsSpu goodsSpu);

	/**
	 * 新增商品
	 *
	 * @author lijx
	 * @date 2022/6/16
	 * @param goodsSpu
	 * @return: boolean
	 */
	boolean saveGoods(GoodsSpu goodsSpu);

	/**
	 * 通过ID查询商品
	 *
	 * @author lijx
	 * @date 2022/6/16
	 * @param id
	 * @return: com.huanxing.cloud.mall.common.entity.GoodsSpu
	 */
	GoodsSpu getSpuById(String id);

	/**
	 * 修改商品
	 *
	 * @author lijx
	 * @date 2022/6/16
	 * @param goodsSpu
	 * @return: boolean
	 */
	boolean updateGoods(GoodsSpu goodsSpu);

	/**
	 * 分页查询商品
	 * @param page page
	 * @param goodsSpu goodsSpu
	 * @return IPage<GoodsSpu>
	 */
	IPage<GoodsSpu> apiPage(Page page, GoodsSpu goodsSpu);

	/**
	 * 商品详情
	 *
	 * @author lijx
	 * @date 2022/6/6
	 * @param id
	 * @return: com.huanxing.cloud.mall.common.entity.GoodsSpu
	 */
	GoodsSpu getApiSpuById(String id);

	/**
	 * 更新销量
	 * @param spuId
	 * @param buyQuantity
	 * @return
	 */
	boolean updateSalesVolume(String spuId, Integer buyQuantity);

	List<GoodsSpu> listByShopIds(List<String> ids);

}
