package com.huanxing.cloud.product.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanxing.cloud.product.api.entity.GoodsShopCategory;
import org.apache.ibatis.annotations.Mapper;

/**
 * 店铺商品类目
 *
 * @author lijx
 * @since 2022/2/26 16:32
 */
@Mapper
public interface GoodsShopCategoryMapper extends BaseMapper<GoodsShopCategory> {

}
