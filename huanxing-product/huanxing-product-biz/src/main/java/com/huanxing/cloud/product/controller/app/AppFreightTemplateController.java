package com.huanxing.cloud.product.controller.app;

import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.product.api.entity.FreightTemplate;
import com.huanxing.cloud.product.service.IFreightTemplateService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/app/freighttemplate")
@Tag(description = "freighttemplate", name = "运费模板")
public class AppFreightTemplateController {

	private final IFreightTemplateService freightTemplateService;

	@GetMapping("/{id}")
	public Result<FreightTemplate> getById(@PathVariable("id") String id) {
		return Result.success(freightTemplateService.getById(id));
	}

}
