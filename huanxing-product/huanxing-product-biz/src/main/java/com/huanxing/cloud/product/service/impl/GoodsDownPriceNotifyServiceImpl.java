package com.huanxing.cloud.product.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huanxing.cloud.product.api.entity.GoodsDownPriceNotify;
import com.huanxing.cloud.product.mapper.GoodsDownPriceNotifyMapper;
import com.huanxing.cloud.product.service.IGoodsDownPriceNotifyService;
import org.springframework.stereotype.Service;

@Service
public class GoodsDownPriceNotifyServiceImpl extends ServiceImpl<GoodsDownPriceNotifyMapper, GoodsDownPriceNotify>
		implements IGoodsDownPriceNotifyService {

}
