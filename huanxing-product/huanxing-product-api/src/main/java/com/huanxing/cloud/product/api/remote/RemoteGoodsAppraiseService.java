package com.huanxing.cloud.product.api.remote;

import com.huanxing.cloud.product.api.entity.GoodsAppraise;

import java.util.List;

/**
 * @author lijx
 */
public interface RemoteGoodsAppraiseService {

	/**
	 * this is addGoodsAppraise method
	 * @param goodsAppraiseList
	 * @return
	 */
	boolean addGoodsAppraise(List<GoodsAppraise> goodsAppraiseList);

}
