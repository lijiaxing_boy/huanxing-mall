package com.huanxing.cloud.product.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * 商品审核DTO
 *
 * @author lijx
 * @date 2024/12/23
 */
@Data
@Schema(description = "商品审核DTO")
public class GoodsSpuVerifyDTO implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;

	@NotEmpty(message = "审核状态不能为空")
	private String verifyStatus;

	private String verifyDesc;

	@NotNull(message = "商品不能为空")
	private List<String> spuIds;

}
