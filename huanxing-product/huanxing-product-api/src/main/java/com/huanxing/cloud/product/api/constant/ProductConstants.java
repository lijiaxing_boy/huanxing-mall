package com.huanxing.cloud.product.api.constant;

/**
 * 商品常量
 *
 * @author lijx
 * @date 2024/12/23
 */
public interface ProductConstants {

	/**
	 * 商品审核状态 0.待审核 1.审核通过 2.审核失败
	 */
	String VERIFY_STATUS_WAIT = "0";

	String VERIFY_STATUS_PASS = "1";

	String VERIFY_STATUS_FAIL = "2";

}
