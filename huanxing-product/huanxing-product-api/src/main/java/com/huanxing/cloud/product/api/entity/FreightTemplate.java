package com.huanxing.cloud.product.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 运费模板
 *
 * @author lijx
 * @since 2022/2/23 13:09
 */
@Data
@Schema(description = "运费模板")
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName(value = "freight_template")
public class FreightTemplate extends Model<FreightTemplate> {

	@Schema(description = "PK")
	@TableId(type = IdType.ASSIGN_ID)
	private String id;

	@Schema(description = "运费模板名称")
	private String name;

	@Schema(description = "发货时间")
	private String sendTime;

	@Schema(description = "发货地址")
	private String address;

	@Schema(description = "是否包邮：0.否；1.是；")
	private String isInclPostage;

	@Schema(description = "是否指定条件包邮：0.否；1.是；")
	private String isInclPostageByIf;

	@Schema(description = "计价方式：1.按件数；2.按重量；3.按体积；")
	private String pricingType;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建人")
	private String createBy;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改人")
	private String updateBy;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建时间")
	private LocalDateTime createTime;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改时间")
	private LocalDateTime updateTime;

	@TableLogic
	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "逻辑删除：0.显示；1.隐藏；")
	private String delFlag;

	@Schema(description = "首件（个） 首重（kg）")
	private BigDecimal firstNum;

	@Schema(description = "续件（个） 续重（kg）")
	private BigDecimal continueNum;

	@Schema(description = "首费（元）")
	private BigDecimal firstFreight;

	@Schema(description = "续费（元）")
	private BigDecimal continueFreight;

	@Schema(description = "满包邮（元）")
	private BigDecimal fullAmount;

	@Schema(description = "租户id")
	private String tenantId;

	@Schema(description = "店铺ID")
	private String shopId;

	@Schema(description = "店铺名称")
	private String shopName;

}
