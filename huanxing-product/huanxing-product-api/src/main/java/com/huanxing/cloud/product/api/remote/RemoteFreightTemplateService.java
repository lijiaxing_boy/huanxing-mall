package com.huanxing.cloud.product.api.remote;

import com.huanxing.cloud.product.api.entity.FreightTemplate;

/**
 * @author lijx
 */
public interface RemoteFreightTemplateService {

	FreightTemplate getById(String id);

}
