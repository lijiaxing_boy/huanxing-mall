package com.huanxing.cloud.product.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.huanxing.cloud.common.myabtis.handler.ListStringTypeHandler;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 商品spu
 *
 * @author lijx
 * @since 2022/2/22 14:28
 */
@Data
@Schema(description = "商品spu")
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName(value = "goods_spu", autoResultMap = true)
public class GoodsSpu extends Model<GoodsSpu> {

	@Schema(description = "PK")
	@TableId(type = IdType.ASSIGN_ID)
	private String id;

	@Schema(description = "商品名称")
	private String name;

	@Schema(description = "子标题")
	private String subTitle;

	@Schema(description = "商品图地址")
	@TableField(typeHandler = ListStringTypeHandler.class)
	private List<String> spuUrls;

	@Schema(description = "状态：0.下架；1.上架")
	private String status;

	@Schema(description = "销量")
	private Integer salesVolume;

	@Schema(description = "运费模板主键")
	private String freightTemplateId;

	@Schema(description = "发货地点id")
	private String placeShipmentId;

	@Schema(description = "一级类目主键")
	private String categoryFirstId;

	@Schema(description = "二级类目主键")
	private String categorySecondId;

	@Schema(description = "描述")
	private String description;

	@Schema(description = "多规格：0.否；1.是")
	private String enableSpecs;

	@Schema(description = "最低价（元）")
	private BigDecimal lowPrice;

	@Schema(description = "最高价（元）")
	private BigDecimal highPrice;

	@Schema(description = "租户id")
	private String tenantId;

	@Schema(description = "店铺ID")
	private String shopId;

	@Schema(description = "店铺名称")
	private String shopName;

	@Schema(description = "店铺类型")
	private String shopType;

	@Schema(description = "店铺一级类目ID")
	private String shopCategoryFirstId;

	@Schema(description = "店铺二级类目ID")
	private String shopCategorySecondId;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建人")
	private String createBy;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改人")
	private String updateBy;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建时间")
	private LocalDateTime createTime;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改时间")
	private LocalDateTime updateTime;

	@TableLogic
	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "逻辑删除：0.显示；1.隐藏；")
	private String delFlag;

	@Schema(description = "审核状态")
	private String verifyStatus;

	@Schema(description = "审核描述")
	private String verifyDesc;

	@Schema(description = "商品类目名称")
	@TableField(exist = false)
	private String categoryName;

	@Schema(description = "运费模版")
	@TableField(exist = false)
	private FreightTemplate freightTemplate;

	@Schema(description = "商品sku")
	@TableField(exist = false)
	private List<GoodsSku> goodsSkus;

	@Schema(description = "商品规格")
	@TableField(exist = false)
	private List<GoodsSpuSpecs> goodsSpuSpecs;

	@TableField(exist = false)
	private String collectId;

}
