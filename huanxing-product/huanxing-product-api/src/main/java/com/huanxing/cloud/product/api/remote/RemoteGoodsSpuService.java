package com.huanxing.cloud.product.api.remote;

import com.huanxing.cloud.product.api.dto.GoodsSpuSalesVolumeReqDTO;
import com.huanxing.cloud.product.api.entity.GoodsSpu;

import java.util.List;

/**
 * @author lijx
 */
public interface RemoteGoodsSpuService {

	boolean updateSalesVolume(List<GoodsSpuSalesVolumeReqDTO> list);

	GoodsSpu getBySpuId(String id);

	List<GoodsSpu> getBySpuIds(List<String> ids);

	List<GoodsSpu> getSpuListByShopIds(List<String> ids);

}
