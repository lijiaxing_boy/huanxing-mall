package com.huanxing.cloud.product.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * 商品sku关联规格值
 *
 * @author lijx
 * @since 2022/2/22 14:28
 */
@Data
@Schema(description = "商品sku关联规格值")
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName(value = "goods_sku_specs_value", autoResultMap = true)
public class GoodsSkuSpecsValue extends Model<GoodsSkuSpecsValue> {

	@Schema(description = "PK")
	@TableId(type = IdType.ASSIGN_ID)
	private String id;

	@Schema(description = "skuId")
	private String skuId;

	@Schema(description = "spuId")
	private String spuId;

	@Schema(description = "规格值主键")
	private String specsValueId;

	@Schema(description = "排序字段")
	private Integer sort;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建人")
	private String createBy;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改人")
	private String updateBy;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建时间")
	private LocalDateTime createTime;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改时间")
	private LocalDateTime updateTime;

	@TableLogic
	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "逻辑删除：0.显示；1.隐藏；")
	private String delFlag;

	@Schema(description = "图片")
	private String picUrl;

	@Schema(description = "租户id")
	private String tenantId;

	@Schema(description = "规格值名")
	@TableField(exist = false)
	private String specsValueName;

	@Schema(description = "规格Id")
	@TableField(exist = false)
	private String specsId;

}
