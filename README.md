<p align="center">
	<img height="150" width="150" alt="logo" src="https://huanxing.oss-cn-beijing.aliyuncs.com/about-huanxing/7c37ddaa15648e4ae0d15ac81d5a401.png">
</p>
<h1 align="center" style="margin: 20px 0 20px 0px; font-weight: bold;">环兴商城</h1>
<h2 align="center">给孩子点个 Star 吧  <img height="60" width="60" alt="logo" src="https://huanxing.oss-cn-beijing.aliyuncs.com/about-huanxing/d411def865196500df9c4cdc63f40cc.png"></h2>
<p align="center">
	<a href="https://gitee.com/lijiaxing_boy/huanxing-mall/stargazers"><img src="https://gitee.com/lijiaxing_boy/huanxing-mall/badge/star.svg?theme=dark"></a>
	<a href="https://gitee.com/lijiaxing_boy/huanxing-mall"><img src="https://img.shields.io/badge/version-v1.0.0-blue.svg"></a>
	<a href="https://gitee.com/lijiaxing_boy/huanxing-mall/blob/master/LICENSE"><img src="https://img.shields.io/github/license/mashape/apistatus.svg"></a>
</p>

#### 项目介绍

环兴商城基于 Spring Cloud 微服务商城系统，前后端分离，前端模板（[vue-next-admin](https://gitee.com/lyt-top/vue-next-admin)）基于 vue3.x 、typescript、vite、element-plus 移动端采用 uni-app、vk-ui 一套代码，可发布到 iOS、Android、以及各种小程序。核心功能包括（sku、运费模板、素材库、快递物流、DIY 页面设计）

#### 文档地址：https://www.ihuanxing.cn
#### 前端模板（[vue-next-admin]文档地址: http://vue-next-admin-doc.ihuanxing.cn

#### 商城后台演示地址

[商城后台演示地址](https://mall.ihuanxing.cn)：https://mall.ihuanxing.cn

- 商城租户：账号/密码 admin/123456
- 店员：账号/密码 huawei/123456

#### 商城H5演示地址

[商城后台演示地址](https://demo.ihuanxing.cn)：https://demo.ihuanxing.cn

- 商城用户：账号/密码 17615123397/123456

#### 商户端H5演示地址

[商城商户端演示地址](https://shop.ihuanxing.cn)：https://shop.ihuanxing.cn

- 商城租户：账号/密码 admin/123456
- 店员：账号/密码 huawei/123456

#### 交流群 

<p align="center">
	<img height="500" width="330" alt="logo" src="https://minio.ihuanxing.cn/huanxing/file/5627fd85-8eb6-4955-b498-f93057c121e1.jpg">
</p>

#### 商城演示  演示账号/密码： 17615123397/123456

| 商城 H5    | <img src="https://minio.ihuanxing.cn/huanxing/file/36e339c8-151d-46c4-b20c-7b24e36fdd1d.png" height="300" width="300" >                                     |
| ---        | ---                                                                                                                              |
| 商家版 H5    | <img src="https://minio.ihuanxing.cn/huanxing/file/4e13911f-f290-4bd0-afc4-c7f349b7c7eb.png" height="300" width="300" >                                     |
| ---        | ---                                                                                                                              |


#### 核心依赖

| 依赖                                                         | 版本         |
|------------------------------------------------------------|------------|
| Spring Boot                                                | 3.3.0      |
| Spring Cloud                                               | 2023.0.1   |
| Spring Cloud Alibaba                                       | 2022.0.1.0 |
| Sa-Token                                                   | 1.38.0     |
| Mybatis-Plus                                               | 3.5.6      |
| Hutool                                                     | 5.8.28     |
| Vue                                                        | 3.2.47     |
| Vk-Ui                                                      | 1.5.2      |
| Element-Plus                                               | 2.3.3      |
| [vue-next-admin](https://gitee.com/lyt-top/vue-next-admin) | 2.4.33     |

#### 系统模块
```
huanxing
├─huanxing-auth     # 授权服务[5227]                 
├─huanxing-common    # 系统公共模块 
│  ├─huanxing-common-core   #公共核心包
│  ├─huanxing-common-feign  #feign扩展
│  ├─huanxing-common-job    #xxl-job 封装
│  ├─huanxing-common-log    #日志模块
│  ├─huanxing-common-logistics  #快递模块
│  ├─huanxing-common-mybatis    #mybatis 扩展封装
│  ├─huanxing-common-nacos  #nacos 扩展封装
│  ├─huanxing-common-security   #安全模块
│  ├─huanxing-common-sms    #短信模块
│  └─huanxing-common-storage    #存储模块
├─huanxing-gateway  #网关服务[9999]
├─huanxing-miniapp  #小程序模块
│  ├─huanxing-miniapp-api  #小程序公共api模块
│  └─huanxing-miniapp-biz  #小程序业务处理模块
├─huanxing-order  #订单模块
│  ├─huanxing-order-api  #订单公共api模块
│  └─huanxing-order-biz  #订单业务处理模块
├─huanxing-pay  #支付模块
│  ├─huanxing-pay-api  #支付公共api模块
│  └─huanxing-pay-biz  #支付业务处理模块
├─huanxing-product  #商品模块
│  ├─huanxing-product-api  #商品公共api模块
│  └─huanxing-product-biz  #商品业务处理模块
├─huanxing-promotion  #营销模块
│  ├─huanxing-promotion-api  #营销公共api模块
│  └─huanxing-promotion-biz  #营销业务处理模块
├─huanxing-shop  #店铺模块
│  ├─huanxing-shop-api  #店铺公共api模块
│  └─huanxing-shop-biz  #店铺业务处理模块
├─huanxing-upms  #用户权限管理模块
│  ├─huanxing-upms-api  #用户权限管理系统公共api模块
│  └─huanxing-upms-biz  #用户权限管理系统业务处理模块
├─huanxing-user  #商城用户模块
│  ├─huanxing-user-api  #商城用户公共api模块
│  └─huanxing-user-biz  #商城用户业务处理模块
├─huanxing-visual  #系统图形化管理模块
   ├─huanxing-monitor  #服务监控[7001]
   └─huanxing-xxl-job-admin  #xxl-job 定时任务管理端[7002]
```

#### 项目链接

- java 后台：https://gitee.com/lijiaxing_boy/huanxing-mall
- vue 前端：https://gitee.com/lijiaxing_boy/huanxing-mall-ui
- uniapp 移动端：https://gitee.com/lijiaxing_boy/huanxing-mall-uniapp

#### 系统功能

1. 用户管理
2. 角色管理
3. 菜单管理
4. 部门管理
5. 登录日志
6. 操作日志
7. 服务监控
8. 文件存储配置（阿里云、腾讯云、七牛云）
9. 租户管理
10. 租户套餐
11. 短信配置

#### 商城功能

1. 商品管理
2. 商城分类
3. 商城用户
4. 商城订单
5. 商城退单
6. 素材分组
7. 运费模版
8. 素材库（支持素材管理）
10. 主题配置（5种全店风格、底部导航）
11. DIY 页面设计（标题文本、公告、商品、图片广告、图文导航、魔方）
12. 优惠券
13. 物流公司管理

#### 小程序功能
1. 微信小程序管理
2. 微信小程序码生成
3. 微信小程序用户

#### 支付功能
1. 微信支付（JSAPI、APP）
2. 微信退款
3. 支付宝支付（小程序、APP、H5）
4. 支付宝退款


