package com.huanxing.cloud.promotion.api.remote;

import com.huanxing.cloud.promotion.api.dto.CouponUserReqDTO;
import com.huanxing.cloud.promotion.api.vo.CouponUserRespVO;

/**
 * @author lijx
 */
public interface RemoteCouponUserService {

	/**
	 * 修改用户优惠券状态
	 * @param couponUserReqDTO
	 * @return
	 */
	boolean updateCouponUserStatus(CouponUserReqDTO couponUserReqDTO);

	/**
	 * 查询用户优惠券信息
	 * @param id
	 * @return
	 */
	CouponUserRespVO getById(String id);

}
