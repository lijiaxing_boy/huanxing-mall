package com.huanxing.cloud.promotion.dubbo;

import com.huanxing.cloud.promotion.api.dto.CouponUserReqDTO;
import com.huanxing.cloud.promotion.api.remote.RemoteCouponUserService;
import com.huanxing.cloud.promotion.api.vo.CouponUserRespVO;
import com.huanxing.cloud.promotion.service.ICouponUserService;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;

/**
 * @author lijia
 * @description
 * @date 2024/11/23
 */
@Service
@DubboService
@RequiredArgsConstructor
public class RemoteCouponUserServiceImpl implements RemoteCouponUserService {

	private final ICouponUserService couponUserService;

	@Override
	public boolean updateCouponUserStatus(CouponUserReqDTO request) {
		return couponUserService.updateCouponUserStatus(request);
	}

	@Override
	public CouponUserRespVO getById(String id) {
		return couponUserService.getCouponUserById(id);
	}

}
