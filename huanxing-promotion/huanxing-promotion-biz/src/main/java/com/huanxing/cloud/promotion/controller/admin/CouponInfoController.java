package com.huanxing.cloud.promotion.controller.admin;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.common.log.annotation.SysLog;
import com.huanxing.cloud.promotion.api.entity.CouponInfo;
import com.huanxing.cloud.promotion.api.entity.CouponUser;
import com.huanxing.cloud.promotion.service.ICouponInfoService;
import com.huanxing.cloud.promotion.service.ICouponUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/couponinfo")
@Tag(description = "couponinfo", name = "优惠券")
public class CouponInfoController {

	private final ICouponInfoService couponInfoService;

	private final ICouponUserService couponUserService;

	@Operation(summary = "优惠券列表")
	@SaCheckPermission("promotion:couponinfo:page")
	@GetMapping("/page")
	public Result page(Page page, CouponInfo couponInfo) {
		return Result.success(couponInfoService.getAdminPage(page, couponInfo));
	}

	@Operation(summary = "优惠券查询")
	@SaCheckPermission("promotion:couponinfo:get")
	@GetMapping("/{id}")
	public Result getById(@PathVariable("id") String id) {
		return Result.success(couponInfoService.getCouponById(id));
	}

	@SysLog("新增优惠券")
	@Operation(summary = "优惠券新增")
	@SaCheckPermission("promotion:couponinfo:add")
	@PostMapping
	public Result add(@Valid @RequestBody CouponInfo couponInfo) {
		couponInfo.setRemainNum(couponInfo.getTotalNum());
		return Result.success(couponInfoService.save(couponInfo));
	}

	@SysLog("修改优惠券")
	@Operation(summary = "优惠券修改")
	@SaCheckPermission("promotion:couponinfo:edit")
	@PutMapping
	public Result edit(@Valid @RequestBody CouponInfo couponInfo) {
		return Result.success(couponInfoService.updateById(couponInfo));
	}

	@SysLog("删除优惠券")
	@Operation(summary = "优惠券删除")
	@SaCheckPermission("promotion:couponinfo:del")
	@DeleteMapping("/{id}")
	public Result del(@PathVariable("id") String id) {
		if (couponUserService.count(Wrappers.<CouponUser>lambdaQuery().eq(CouponUser::getCouponId, id)) > 0) {
			return Result.fail("已有人领取不可删除");
		}
		return Result.success(couponInfoService.removeById(id));
	}

}
