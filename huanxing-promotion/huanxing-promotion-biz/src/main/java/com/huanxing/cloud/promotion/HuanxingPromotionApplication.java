package com.huanxing.cloud.promotion;

import com.huanxing.cloud.common.job.annotation.HxEnableXxlJob;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 商城营销启动类
 *
 * @author lijx
 * @since 2022/2/26 16:38
 */
@HxEnableXxlJob
@EnableDubbo
@SpringBootApplication
public class HuanxingPromotionApplication {

	public static void main(String[] args) {
		SpringApplication.run(HuanxingPromotionApplication.class, args);
	}

}
