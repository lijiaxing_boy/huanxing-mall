package com.huanxing.cloud.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huanxing.cloud.promotion.api.entity.CouponUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CouponUserMapper extends BaseMapper<CouponUser> {

	IPage<CouponUser> selectAdminPage(Page page, @Param("query") CouponUser couponUser);

	IPage<CouponUser> selectApiPage(Page page, @Param("query") CouponUser couponUser);

	List<CouponUser> selectExpireCouponList();

}
