package com.huanxing.cloud.promotion.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.promotion.api.dto.CouponUserReqDTO;
import com.huanxing.cloud.promotion.api.vo.CouponUserRespVO;
import com.huanxing.cloud.promotion.api.entity.CouponUser;

import java.util.List;

public interface ICouponUserService extends IService<CouponUser> {

	IPage<CouponUser> getPage(Page page, CouponUser couponUser);

	CouponUser receive(CouponUser couponUser);

	boolean rollBackCoupon(String couponUserId);

	IPage<CouponUser> getApiPage(Page page, CouponUser couponUser);

	Boolean updateCouponUserStatus(CouponUserReqDTO request);

	CouponUserRespVO getCouponUserById(String id);

	List<CouponUser> getExpireCouponList();

}
