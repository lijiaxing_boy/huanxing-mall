package com.huanxing.cloud.promotion.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.promotion.api.entity.CouponGoods;

import java.util.List;

public interface ICouponGoodsService extends IService<CouponGoods> {

	List<CouponGoods> getByCouponId(String couponId);

}
