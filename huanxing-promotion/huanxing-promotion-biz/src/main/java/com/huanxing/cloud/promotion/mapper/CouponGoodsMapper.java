package com.huanxing.cloud.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanxing.cloud.promotion.api.entity.CouponGoods;
import io.lettuce.core.dynamic.annotation.Param;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CouponGoodsMapper extends BaseMapper<CouponGoods> {

	List<CouponGoods> selectByCouponId(@Param("couponId") String couponId);

}
