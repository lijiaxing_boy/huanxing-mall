package com.huanxing.cloud.promotion.controller.app;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.common.security.util.SecurityUtils;
import com.huanxing.cloud.promotion.api.entity.CouponUser;
import com.huanxing.cloud.promotion.service.ICouponUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/app/couponuser")
@Tag(description = "app-couponuser", name = "用户领券记录-API")
public class AppCouponUserController {

	private final ICouponUserService couponUserService;

	@Operation(summary = "用户领券记录列表")
	@GetMapping("/page")
	public Result<IPage<CouponUser>> page(Page page, CouponUser couponUser) {
		couponUser.setUserId(SecurityUtils.getUser().getUserId());
		return Result.success(couponUserService.getApiPage(page, couponUser));
	}

	@Operation(summary = "用户领取优惠券")
	@PostMapping
	public Result<CouponUser> page(@RequestBody CouponUser couponUser) {
		couponUser.setUserId(SecurityUtils.getUser().getUserId());
		return Result.success(couponUserService.receive(couponUser));
	}

}
