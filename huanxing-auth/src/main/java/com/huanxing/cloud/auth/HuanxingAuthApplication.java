package com.huanxing.cloud.auth;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 认证授权模块
 *
 * @author lijx
 * @since 2022/2/18 14:45
 */
@EnableDubbo
@SpringBootApplication
public class HuanxingAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(HuanxingAuthApplication.class, args);
	}

}
