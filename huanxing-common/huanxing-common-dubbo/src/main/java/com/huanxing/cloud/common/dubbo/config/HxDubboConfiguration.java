package com.huanxing.cloud.common.dubbo.config;

import com.huanxing.cloud.common.core.factory.YamlPropertySourceFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author lijx
 * @description dubbo配置
 * @date 2024/11/25
 */
@PropertySource(value = "classpath:dubbo-config.yml", factory = YamlPropertySourceFactory.class)
@Configuration(proxyBeanMethods = false)
public class HxDubboConfiguration {

}
