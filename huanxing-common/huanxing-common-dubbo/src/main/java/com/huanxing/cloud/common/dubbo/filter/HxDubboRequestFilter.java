package com.huanxing.cloud.common.dubbo.filter;

import com.huanxing.cloud.common.myabtis.tenant.HxTenantContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.*;

/**
 * Sa-Token 整合 Dubbo过滤器
 */
@Slf4j
@Activate(group = { CommonConstants.PROVIDER, CommonConstants.CONSUMER }, order = Integer.MAX_VALUE)
public class HxDubboRequestFilter implements Filter {

	@Override
	public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
		// 判断是消费者 还是 服务提供者
		if (RpcContext.getServiceContext().isConsumerSide()) {
			// 传递租户ID
			invocation.setAttachment("tenantId", HxTenantContextHolder.getTenantId());
		}
		else {
			// 设置租户ID
			HxTenantContextHolder.setTenantId(invocation.getAttachment("tenantId"));
		}
		return invoker.invoke(invocation);
	}

}