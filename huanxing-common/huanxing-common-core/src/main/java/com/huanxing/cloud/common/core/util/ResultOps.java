package com.huanxing.cloud.common.core.util;

import com.huanxing.cloud.common.core.constant.CommonConstants;

import java.util.Objects;
import java.util.Optional;

public class ResultOps<T> {

	private final Result<T> original;

	public ResultOps(Result<T> original) {
		this.original = original;
	}

	/**
	 * 读取code
	 * @return 返回code的值
	 */
	public int getCode() {
		return original.getCode();
	}

	/**
	 * 读取data
	 * @return 返回 Optional 包装的data
	 */
	public Optional<T> getData() {
		return Optional.ofNullable(original.getData());
	}

	public static <T> ResultOps<T> of(Result<T> original) {
		return new ResultOps<>(Objects.requireNonNull(original));
	}

	/**
	 * 判断状态
	 * @param value
	 * @return 返回ture表示相等
	 */
	public boolean codeEquals(int value) {
		return original.getCode() == value;
	}

	/**
	 * 是否成功
	 * @return 返回ture表示成功
	 */
	public boolean isSuccess() {
		return codeEquals(CommonConstants.SUCCESS);
	}

}
