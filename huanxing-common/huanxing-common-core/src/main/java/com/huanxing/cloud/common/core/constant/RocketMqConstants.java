package com.huanxing.cloud.common.core.constant;

/**
 * RocketMq常量
 *
 * @author lijx
 * @date 2022/8/6
 */
public interface RocketMqConstants {

	/** 订单取消 topic */
	String ORDER_CANCEL_TOPIC = "order-cancel-topic";

	/** 支付通知topic */
	String PAY_NOTIFY_TOPIC = "pay-notify-topic";

	/** 退款通知topic */
	String PAY_REFUND_NOTIFY_TOPIC = "pay-refund-notify-topic";

	/** 支付通知group */
	String PAY_GROUP = "pay-group";

	/** 发送消息超时时间 */
	long TIME_OUT = 3000;

	/** 订单超时取消等级 30分钟 */
	int ORDER_CANCEL_LEVEL = 16;

}
