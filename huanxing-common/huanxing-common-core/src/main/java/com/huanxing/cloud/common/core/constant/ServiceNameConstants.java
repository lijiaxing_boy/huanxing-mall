package com.huanxing.cloud.common.core.constant;

/**
 * 服务名称
 *
 * @author lijx
 * @version 1.0
 * @date 2022/5/3 22:02
 */
public interface ServiceNameConstants {

	/** UMPS ADMIN模块 */
	String UMPS_SERVICE = "huanxing-upms-biz";

	/** 商城用户模块 */
	String MALL_USER_SERVICE = "huanxing-user-biz";

	/** 微信小程序应用模块 */
	String MINIAPP_WEIXIN_SERVICE = "huanxing-miniapp-biz";

	/** 支付 模块 */
	String PAY_API_SERVICE = "huanxing-pay-biz";

	/** 商品 模块 */
	String PRODUCT_SERVICE = "huanxing-product-biz";

	/** 营销 模块 */
	String PROMOTION_SERVICE = "huanxing-promotion-biz";

	/** 店铺 模块 */
	String SHOP_SERVICE = "huanxing-shop-biz";

}
