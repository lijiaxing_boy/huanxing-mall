package com.huanxing.cloud.common.core.constant;

/**
 * 商城公共常量
 *
 * @author lijx
 * @date 2022/6/8
 */
public interface MallCommonConstants {

	/**
	 * header中的appId
	 */
	String HEADER_APP_ID = "app-id";

	/**
	 * header中的客户端类型
	 */
	String HEADER_CLIENT_TYPE = "client-type";

	/**
	 * header中的租户id
	 */
	String HEADER_TENANT_ID = "tenant-id";

	/**
	 * header中的wx-user-id
	 */
	String WX_USER_ID = "wx-user-id";

}
