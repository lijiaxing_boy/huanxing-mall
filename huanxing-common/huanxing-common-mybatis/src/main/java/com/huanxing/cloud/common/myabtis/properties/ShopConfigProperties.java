package com.huanxing.cloud.common.myabtis.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@RefreshScope
@Configuration
@ConfigurationProperties(prefix = "hx.shop")
public class ShopConfigProperties {

	// mapper名称
	private List<String> mappers;

}
