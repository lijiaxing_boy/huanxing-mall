package com.huanxing.cloud.common.myabtis.tenant;

import com.huanxing.cloud.common.core.constant.CommonConstants;
import com.huanxing.cloud.common.security.util.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

@Slf4j
@Component
public class HxTenantContextFilter extends GenericFilterBean {

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		// 获取请求头 租户id
		String tenantId = request.getHeader(CommonConstants.TENANT_ID);
		String token = request.getHeader(CommonConstants.TOKEN_KEY);
		if (StringUtils.hasText(token) && Objects.nonNull(SecurityUtils.getUser())) {
			HxTenantContextHolder.setTenantId(SecurityUtils.getUser().getTenantId());
		}
		else {
			HxTenantContextHolder.setTenantId(tenantId);
		}
		filterChain.doFilter(request, response);
	}

}
