package com.huanxing.cloud.common.seata.config;

import com.huanxing.cloud.common.core.factory.YamlPropertySourceFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author lijia
 * @description
 * @date 2024/11/25
 */
@PropertySource(value = "classpath:seata-config.yml", factory = YamlPropertySourceFactory.class)
@Configuration(proxyBeanMethods = false)
public class HxSeataConfiguration {

}
