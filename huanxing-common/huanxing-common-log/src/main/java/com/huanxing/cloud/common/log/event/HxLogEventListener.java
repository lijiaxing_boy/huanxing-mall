package com.huanxing.cloud.common.log.event;

import com.huanxing.cloud.upms.api.entity.SysLog;
import com.huanxing.cloud.upms.api.entity.SysLoginLog;
import com.huanxing.cloud.upms.api.remote.RemoteSysLogService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;

/**
 * @author: lijx
 * @date: 2023/6/26 11:51
 **/
@Slf4j
@RequiredArgsConstructor
public class HxLogEventListener {

	@DubboReference
	private final RemoteSysLogService remoteSysLogService;

	@Async
	@EventListener(HxLogEvent.class)
	public void saveSysLog(HxLogEvent event) {
		SysLog sysLog = event.getSysLog();
		remoteSysLogService.saveLog(sysLog);
	}

	@Async
	@EventListener(HxLoginLogEvent.class)
	public void saveSysLoginLog(HxLoginLogEvent event) {
		SysLoginLog sysLoginLog = event.getSysLoginLog();
		remoteSysLogService.saveLoginLog(sysLoginLog);
	}

}
