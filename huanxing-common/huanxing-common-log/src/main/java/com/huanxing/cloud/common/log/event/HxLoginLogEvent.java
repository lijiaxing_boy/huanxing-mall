package com.huanxing.cloud.common.log.event;

import com.huanxing.cloud.upms.api.entity.SysLoginLog;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * @author: lijx
 * @date: 2023/6/26 11:51
 **/
public class HxLoginLogEvent extends ApplicationEvent {

	@Getter
	private final SysLoginLog sysLoginLog;

	public HxLoginLogEvent(Object source, SysLoginLog sysLoginLog) {
		super(source);
		this.sysLoginLog = sysLoginLog;
	}

}
