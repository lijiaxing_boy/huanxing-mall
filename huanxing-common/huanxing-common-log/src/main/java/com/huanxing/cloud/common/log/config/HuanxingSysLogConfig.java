package com.huanxing.cloud.common.log.config;

import com.huanxing.cloud.common.log.aop.LogAopAspect;
import com.huanxing.cloud.common.log.event.HxLogEventListener;
import com.huanxing.cloud.upms.api.remote.RemoteSysLogService;
import lombok.AllArgsConstructor;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 日志配置
 *
 * @author lijx
 * @date 2022/6/10
 */
@AllArgsConstructor
@Configuration
public class HuanxingSysLogConfig {

	@DubboReference
	private final RemoteSysLogService remoteSysLogService;

	@Bean
	public HxLogEventListener hxLogEventListener() {
		return new HxLogEventListener(remoteSysLogService);
	}

	@Bean
	public LogAopAspect logAopAspect(ApplicationEventPublisher publisher) {
		return new LogAopAspect(publisher);
	}

}
