package com.huanxing.cloud.common.log.event;

import com.huanxing.cloud.upms.api.entity.SysLog;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * @author: lijx
 * @date: 2023/6/26 11:51
 **/
public class HxLogEvent extends ApplicationEvent {

	@Getter
	private final SysLog sysLog;

	public HxLogEvent(Object source, SysLog sysLog) {
		super(source);
		this.sysLog = sysLog;
	}

}
