package com.huanxing.cloud.common.storage.handler;

import com.huanxing.cloud.upms.api.dto.SysStorageConfigDTO;

import java.io.File;

public interface HxUploadFileHandler {

	String uploadFile(SysStorageConfigDTO sysStorageConfig, File file, String contextType);

}
