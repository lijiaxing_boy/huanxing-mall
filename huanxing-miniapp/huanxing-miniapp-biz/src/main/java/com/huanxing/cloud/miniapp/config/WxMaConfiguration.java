package com.huanxing.cloud.miniapp.config;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.api.impl.WxMaServiceImpl;
import cn.binarywang.wx.miniapp.config.impl.WxMaDefaultConfigImpl;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.huanxing.cloud.miniapp.api.entity.WxApp;
import com.huanxing.cloud.miniapp.service.IWxAppService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * 微信小程序配置
 *
 * @author lijx
 * @date 2022/6/10
 */
@Slf4j
@Configuration
@RequiredArgsConstructor
public class WxMaConfiguration {

	private static IWxAppService wxAppService;

	private static RedisTemplate<String, String> redisTemplate;

	@Autowired
	public WxMaConfiguration(IWxAppService wxAppService, RedisTemplate<String, String> redisTemplate) {
		WxMaConfiguration.wxAppService = wxAppService;
		WxMaConfiguration.redisTemplate = redisTemplate;
	}

	public static WxMaService getMaService(String appId) {
		WxApp wxApp = wxAppService.getById(appId);
		if (ObjectUtil.isNull(wxApp)) {
			wxApp = wxAppService.getOne(Wrappers.lambdaQuery());
		}
		WxMaRedisConfigStorage configStorage = new WxMaRedisConfigStorage(redisTemplate);
		configStorage.setAppid(wxApp.getAppId());
		configStorage.setSecret(wxApp.getAppSecret());
		configStorage.setToken(wxApp.getToken());
		configStorage.setAesKey(wxApp.getAesKey());
		WxMaService wxMaService = new WxMaServiceImpl();
		wxMaService.setWxMaConfig(configStorage);

		return wxMaService;
	}

}
