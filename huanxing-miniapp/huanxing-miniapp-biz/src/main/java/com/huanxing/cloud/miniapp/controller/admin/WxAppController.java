package com.huanxing.cloud.miniapp.controller.admin;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huanxing.cloud.common.core.constant.CacheConstants;
import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.common.log.annotation.SysLog;
import com.huanxing.cloud.common.security.annotation.HxInner;
import com.huanxing.cloud.miniapp.api.entity.WxApp;
import com.huanxing.cloud.miniapp.service.IWxAppService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.*;

/**
 * 微信应用
 *
 * @author lijx
 * @since 2022/3/14 15:36
 */
@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/wxapp")
@Tag(description = "wxapp", name = "微信应用")
public class WxAppController {

	private final IWxAppService wxAppService;

	@Operation(summary = "微信应用列表")
	@SaCheckPermission("weixin:wxapp:page")
	@GetMapping("/page")
	public Result page(Page page, WxApp wxApp) {
		return Result.success(wxAppService.getPage(page, wxApp));
	}

	@Operation(summary = "微信应用查询")
	@SaCheckPermission("weixin:wxapp:get")
	@GetMapping("/{appId}")
	public Result getById(@PathVariable("appId") String appId) {
		return Result.success(wxAppService.getWxAppById(appId));
	}

	@HxInner
	@Operation(summary = "通过id查询微信应用")
	@GetMapping("/inner/{appId}")
	public Result getInnerById(@PathVariable("appId") String appId) {
		return Result.success(wxAppService.findByAppId(appId));
	}

	@SysLog("新增微信应用")
	@Operation(summary = "微信应用新增")
	@SaCheckPermission("weixin:wxapp:add")
	@PostMapping
	public Result add(@RequestBody WxApp wxApp) {
		return Result.success(wxAppService.save(wxApp));
	}

	@SysLog("修改微信应用")
	@Operation(summary = "微信应用修改")
	@SaCheckPermission("weixin:wxapp:edit")
	@PutMapping
	public Result edit(@RequestBody @Valid WxApp wxApp) {
		return Result.success(wxAppService.updateWxAppById(wxApp));
	}

	@SysLog("删除微信应用")
	@Operation(summary = "微信应用删除")
	@SaCheckPermission("weixin:wxapp:del")
	@DeleteMapping("/{id}")
	@CacheEvict(value = CacheConstants.WX_APP_CACHE, key = "#id", allEntries = true)
	public Result del(@PathVariable String id) {
		return Result.success(wxAppService.removeById(id));
	}

}
