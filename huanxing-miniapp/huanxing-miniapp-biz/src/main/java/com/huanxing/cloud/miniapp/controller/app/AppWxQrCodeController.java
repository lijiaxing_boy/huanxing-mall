package com.huanxing.cloud.miniapp.controller.app;

import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.miniapp.api.dto.WxQrCodeDTO;
import com.huanxing.cloud.miniapp.service.IWxAppService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 微信小程序码
 *
 * @author lijx
 * @date 2022/7/12
 */
@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/app/wxqrcode")
@Tag(description = "app-wxqrcode", name = "微信小程序码-API")
public class AppWxQrCodeController {

	private final IWxAppService wxAppService;

	@Operation(summary = "获取微信小程序码")
	@PostMapping("/unlimit")
	public Result<String> createWxaCodeUnlimit(@RequestBody WxQrCodeDTO wxQrCodeDTO) {
		return Result.success(wxAppService.createWxaCodeUnlimit(wxQrCodeDTO));
	}

}
