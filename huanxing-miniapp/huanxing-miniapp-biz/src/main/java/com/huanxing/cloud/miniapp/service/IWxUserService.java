package com.huanxing.cloud.miniapp.service;

import cn.binarywang.wx.miniapp.bean.WxMaPhoneNumberInfo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.miniapp.api.dto.WxUserLoginReqDTO;
import com.huanxing.cloud.miniapp.api.dto.WxUserReqDTO;
import com.huanxing.cloud.miniapp.api.entity.WxUser;
import com.huanxing.cloud.miniapp.api.vo.WxUserVo;

/**
 * 微信用户
 *
 * @author lijx
 * @since 2022/2/26 16:36
 */
public interface IWxUserService extends IService<WxUser> {

	/**
	 * 微信用户列表
	 *
	 * @author lijx
	 * @date 2022/6/1
	 * @param page
	 * @param wxUser
	 * @return: com.baomidou.mybatisplus.core.metadata.IPage<com.huanxing.cloud.weixin.common.vo.WxUserVo>
	 */
	IPage<WxUserVo> adminPage(Page page, WxUser wxUser);

	/**
	 * 小程序登录
	 * @param wxUserLoginReqDTO
	 * @return
	 */
	WxUser login(WxUserLoginReqDTO wxUserLoginReqDTO);

	/**
	 * 解密手机号
	 * @param wxUserLoginReqDTO
	 * @return
	 */
	WxMaPhoneNumberInfo decryptPhone(WxUserLoginReqDTO wxUserLoginReqDTO);

	/**
	 * 绑定用户id
	 * @param wxUserReqDTO
	 * @return
	 */
	Boolean bindUserId(WxUserReqDTO wxUserReqDTO);

	/**
	 * 解绑用户id
	 * @param wxUserReqDTO
	 * @return
	 */
	Boolean unbindUserId(WxUserReqDTO wxUserReqDTO);

}
