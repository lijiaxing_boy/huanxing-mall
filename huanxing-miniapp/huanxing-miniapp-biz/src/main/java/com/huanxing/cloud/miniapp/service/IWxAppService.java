package com.huanxing.cloud.miniapp.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.miniapp.api.dto.WxQrCodeDTO;
import com.huanxing.cloud.miniapp.api.entity.WxApp;
import com.huanxing.cloud.miniapp.api.vo.WxAppV0;

/**
 * 微信应用
 *
 * @author lijx
 * @since 2022/2/26 16:36
 */
public interface IWxAppService extends IService<WxApp> {

	WxAppV0 findByAppId(String appId);

	IPage<WxAppV0> getPage(Page page, WxApp wxApp);

	WxAppV0 getWxAppById(String appId);

	/**
	 * 创建-小程序码
	 * @param wxQrCodeDTO
	 * @return
	 */
	String createWxaCodeUnlimit(WxQrCodeDTO wxQrCodeDTO);

	/**
	 * 修改-应用信息
	 * @param wxApp
	 * @return
	 */
	Boolean updateWxAppById(WxApp wxApp);

}
