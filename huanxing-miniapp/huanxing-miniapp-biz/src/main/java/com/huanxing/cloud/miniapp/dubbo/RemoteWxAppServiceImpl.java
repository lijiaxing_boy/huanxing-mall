package com.huanxing.cloud.miniapp.dubbo;

import com.huanxing.cloud.miniapp.api.remote.RemoteWxAppService;
import com.huanxing.cloud.miniapp.api.vo.WxAppV0;
import com.huanxing.cloud.miniapp.service.IWxAppService;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;

/**
 * @author lijia
 * @description
 * @date 2024/11/23
 */
@Service
@DubboService
@RequiredArgsConstructor
public class RemoteWxAppServiceImpl implements RemoteWxAppService {

	private final IWxAppService wxAppService;

	@Override
	public WxAppV0 getById(String appId) {
		return wxAppService.findByAppId(appId);
	}

}
