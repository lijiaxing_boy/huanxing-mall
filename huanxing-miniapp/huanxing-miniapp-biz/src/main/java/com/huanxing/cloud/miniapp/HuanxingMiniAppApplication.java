package com.huanxing.cloud.miniapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 微信小程序应用模块启动类
 *
 * @author lijx
 * @since 2022/2/26 16:38
 */
@SpringBootApplication
public class HuanxingMiniAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(HuanxingMiniAppApplication.class, args);
	}

}
