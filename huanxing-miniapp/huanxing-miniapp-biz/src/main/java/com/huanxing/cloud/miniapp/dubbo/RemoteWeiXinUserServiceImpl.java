package com.huanxing.cloud.miniapp.dubbo;

import cn.binarywang.wx.miniapp.bean.WxMaPhoneNumberInfo;
import com.huanxing.cloud.miniapp.api.dto.WxUserLoginReqDTO;
import com.huanxing.cloud.miniapp.api.dto.WxUserReqDTO;
import com.huanxing.cloud.miniapp.api.entity.WxUser;
import com.huanxing.cloud.miniapp.api.remote.RemoteWeiXinUserService;
import com.huanxing.cloud.miniapp.service.IWxUserService;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;

/**
 * @author lijia
 * @description
 * @date 2024/11/23
 */
@Service
@DubboService
@RequiredArgsConstructor
public class RemoteWeiXinUserServiceImpl implements RemoteWeiXinUserService {

	private final IWxUserService wxUserService;

	@Override
	public WxUser maLogin(WxUserLoginReqDTO wxUserLoginReqDTO) {
		return wxUserService.login(wxUserLoginReqDTO);
	}

	@Override
	public WxMaPhoneNumberInfo getPhoneNumberInfo(WxUserLoginReqDTO wxUserLoginReqDTO) {
		return wxUserService.decryptPhone(wxUserLoginReqDTO);
	}

	@Override
	public boolean bindUserId(WxUserReqDTO wxUserReqDTO) {
		return wxUserService.bindUserId(wxUserReqDTO);
	}

	@Override
	public boolean unbindUserId(WxUserReqDTO wxUserReqDTO) {
		return wxUserService.unbindUserId(wxUserReqDTO);
	}

	@Override
	public WxUser getInnerById(String id) {
		return wxUserService.getById(id);
	}

}
