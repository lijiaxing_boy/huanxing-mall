package com.huanxing.cloud.miniapp.controller.admin;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.miniapp.api.entity.WxUser;
import com.huanxing.cloud.miniapp.service.IWxUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 微信用户
 *
 * @author lijx
 * @since 2022/3/14 15:36
 */
@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/wxuser")
@Tag(description = "wxuser", name = "微信用户")
public class WxUserController {

	private final IWxUserService wxUserService;

	@Operation(summary = "微信用户列表")
	@SaCheckPermission("weixin:wxuser:page")
	@GetMapping("/page")
	public Result page(Page page, WxUser wxUser) {
		return Result.success(wxUserService.adminPage(page, wxUser));
	}

}
