package com.huanxing.cloud.miniapp.api.vo;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 小程序用户VO
 *
 * @author lijx
 * @date 2022/7/11
 */
@Data
public class MaUserVO implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;

	private String appId;

	private String encryptedData;

	private String iv;

	private String shareUserNumber;

}
