package com.huanxing.cloud.miniapp.api.dto;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

@Data
public class WxUserReqDTO implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;

	private String openId;

	private String userId;

	private String appId;

	private String wxUserId;

}
