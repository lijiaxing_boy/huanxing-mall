package com.huanxing.cloud.miniapp.api.vo;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 微信小程序码
 *
 * @author lijx
 * @date 2022/7/11
 */
@Data
public class WxQrCodeVO implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;

	private String appId;

	private String scene;

	private String page;

	private String envVersion;

}
