package com.huanxing.cloud.miniapp.api.dto;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 小程序用户DTO
 *
 * @author lijx
 * @date 2022/6/11
 */
@Data
public class MaUserDTO implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;

	private String appId;

	private String encryptedData;

	private String iv;

	private Integer shareUserNumber;

}
