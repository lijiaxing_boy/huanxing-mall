package com.huanxing.cloud.miniapp.api.remote;

import com.huanxing.cloud.miniapp.api.vo.WxAppV0;

/**
 * @author lijia
 */
public interface RemoteWxAppService {

	WxAppV0 getById(String appId);

}
