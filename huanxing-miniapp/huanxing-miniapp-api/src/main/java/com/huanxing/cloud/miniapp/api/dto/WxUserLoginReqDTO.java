package com.huanxing.cloud.miniapp.api.dto;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 微信用户DTO
 *
 * @author lijx
 * @date 2022/6/11
 */
@Data
public class WxUserLoginReqDTO implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;

	@NotEmpty(message = "appId不能为空")
	private String appId;

	private String jsCode;

	private String mallUserId;

	private String clientType;

	private String wxUserId;

	private String code;

}
