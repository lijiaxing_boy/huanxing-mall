package com.huanxing.cloud.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.upms.api.entity.SysLog;

/**
 * 操作日志
 *
 * @author lijx
 * @since 2022/2/26 16:47
 */
public interface ISysLogService extends IService<SysLog> {

}
