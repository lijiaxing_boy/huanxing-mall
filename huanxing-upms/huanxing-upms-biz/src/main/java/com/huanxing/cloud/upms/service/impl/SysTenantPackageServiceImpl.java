package com.huanxing.cloud.upms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huanxing.cloud.upms.mapper.SysTenantPackageMapper;
import com.huanxing.cloud.upms.api.entity.*;
import com.huanxing.cloud.upms.service.ISysTenantPackageService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 租户套餐
 *
 * @author lijx
 * @since 2022/2/26 16:51
 */
@Service
@AllArgsConstructor
public class SysTenantPackageServiceImpl extends ServiceImpl<SysTenantPackageMapper, SysTenantPackage>
		implements ISysTenantPackageService {

}
