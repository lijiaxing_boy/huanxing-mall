package com.huanxing.cloud.upms.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.huanxing.cloud.common.core.constant.CommonConstants;
import com.huanxing.cloud.common.core.desensitization.KeyDesensitization;
import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.common.log.annotation.SysLog;
import com.huanxing.cloud.common.security.annotation.HxInner;
import com.huanxing.cloud.upms.service.ISysStorageConfigService;
import com.huanxing.cloud.upms.api.entity.SysStorageConfig;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * 文件存储配置
 *
 * @author lijx
 * @since 2022/2/26 16:48
 */
@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/storageconfig")
@Tag(description = "storageconfig", name = "文件存储配置")
public class SysStorageConfigController {

	private final ISysStorageConfigService sysStorageConfigService;

	private final KeyDesensitization keyDesensitization = new KeyDesensitization();

	@Operation(summary = "查询文件存储配置")
	@SaCheckPermission("upms:storageconfig:get")
	@GetMapping
	public Result get() {
		SysStorageConfig sysStorageConfig = sysStorageConfigService.getOne(Wrappers.lambdaQuery());
		if (ObjectUtil.isNull(sysStorageConfig)) {
			sysStorageConfig = new SysStorageConfig();
			sysStorageConfig.setType(CommonConstants.STORAGE_TYPE_1);
			sysStorageConfigService.save(sysStorageConfig);
		}
		if (StringUtils.hasText(sysStorageConfig.getAccessKey())) {
			sysStorageConfig.setAccessKey(keyDesensitization.serialize(sysStorageConfig.getAccessKey()));
		}
		if (StringUtils.hasText(sysStorageConfig.getAccessSecret())) {
			sysStorageConfig.setAccessSecret(keyDesensitization.serialize(sysStorageConfig.getAccessSecret()));
		}
		return Result.success(sysStorageConfig);
	}

	@HxInner
	@Operation(summary = "feign接口查询文件存储配置")
	@GetMapping("/getconfig")
	public Result getConfig() {
		return Result.success(sysStorageConfigService.getConfig());
	}

	@Operation(summary = "文件存储配置修改")
	@SysLog("修改文件存储配置")
	@SaCheckPermission("upms:storageconfig:edit")
	@PostMapping
	public Result edit(@RequestBody SysStorageConfig sysStorageConfig) {
		String id = sysStorageConfig.getId();
		if (StrUtil.isBlank(id)) {
			return Result.fail("文件存储配置主键为空");
		}
		SysStorageConfig target = sysStorageConfigService.getById(id);

		if (keyDesensitization.serialize(target.getAccessKey()).equals(sysStorageConfig.getAccessKey())) {
			sysStorageConfig.setAccessKey(null);
		}
		if (keyDesensitization.serialize(target.getAccessSecret()).equals(sysStorageConfig.getAccessSecret())) {
			sysStorageConfig.setAccessSecret(null);
		}

		return Result.success(sysStorageConfigService.updateById(sysStorageConfig));
	}

}
