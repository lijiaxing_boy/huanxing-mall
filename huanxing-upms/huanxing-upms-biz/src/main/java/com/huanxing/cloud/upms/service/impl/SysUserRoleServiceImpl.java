package com.huanxing.cloud.upms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huanxing.cloud.upms.mapper.SysUserRoleMapper;
import com.huanxing.cloud.upms.service.ISysUserRoleService;
import com.huanxing.cloud.upms.api.entity.SysUserRole;
import org.springframework.stereotype.Service;

/**
 * 用户关联角色
 *
 * @author lijx
 * @since 2022/2/26 16:47
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {

}
