package com.huanxing.cloud.upms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huanxing.cloud.upms.api.entity.SysMaterialGroup;
import com.huanxing.cloud.upms.mapper.SysMaterialGroupMapper;
import com.huanxing.cloud.upms.service.ISysMaterialGroupService;
import org.springframework.stereotype.Service;

/**
 * 素材分组
 *
 * @author lijx
 * @since 2022/2/26 16:37
 */
@Service
public class SysMaterialGroupServiceImpl extends ServiceImpl<SysMaterialGroupMapper, SysMaterialGroup>
		implements ISysMaterialGroupService {

}
