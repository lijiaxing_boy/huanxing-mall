package com.huanxing.cloud.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.upms.api.entity.SysMaterial;

/**
 * 素材
 *
 * @author lijx
 * @since 2022/2/26 16:37
 */
public interface ISysMaterialService extends IService<SysMaterial> {

}
