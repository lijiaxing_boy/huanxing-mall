package com.huanxing.cloud.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.upms.api.entity.SysMaterialGroup;

/**
 * 素材分组
 *
 * @author lijx
 * @since 2022/2/26 16:36
 */
public interface ISysMaterialGroupService extends IService<SysMaterialGroup> {

}
