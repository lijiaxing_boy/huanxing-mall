package com.huanxing.cloud.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.upms.api.entity.SysTenantPackage;

/**
 * 租户套餐
 *
 * @author lijx
 * @since 2022/2/26 16:47
 */
public interface ISysTenantPackageService extends IService<SysTenantPackage> {

}
