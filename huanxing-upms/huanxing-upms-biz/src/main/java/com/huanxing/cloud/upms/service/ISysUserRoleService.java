package com.huanxing.cloud.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.upms.api.entity.SysUserRole;

/**
 * 用户关联角色
 *
 * @author lijx
 * @since 2022/2/26 16:47
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

}
