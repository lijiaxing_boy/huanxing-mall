package com.huanxing.cloud.upms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huanxing.cloud.upms.mapper.SysDictMapper;
import com.huanxing.cloud.upms.service.ISysDictService;
import com.huanxing.cloud.upms.api.entity.SysDict;
import org.springframework.stereotype.Service;

/**
 * 字典
 *
 * @author lijx
 * @date 2022/10/21
 */
@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict> implements ISysDictService {

}
