package com.huanxing.cloud.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanxing.cloud.upms.api.entity.SysLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * 操作日志
 *
 * @author lijx
 * @since 2022/2/26 16:46
 */
@Mapper
public interface SysLogMapper extends BaseMapper<SysLog> {

}
