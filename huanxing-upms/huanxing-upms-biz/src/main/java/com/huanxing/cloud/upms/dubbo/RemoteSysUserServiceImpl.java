package com.huanxing.cloud.upms.dubbo;

import cn.hutool.core.util.ObjectUtil;
import com.huanxing.cloud.common.myabtis.tenant.HxTenantContextHolder;
import com.huanxing.cloud.upms.api.dto.SysUserReqDTO;
import com.huanxing.cloud.upms.api.entity.SysUser;
import com.huanxing.cloud.upms.api.remote.RemoteSysUserService;
import com.huanxing.cloud.upms.service.ISysUserService;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;

/**
 * @author lijia
 * @description
 * @date 2024/11/22
 */
@Service
@RequiredArgsConstructor
@DubboService
public class RemoteSysUserServiceImpl implements RemoteSysUserService {

	private final ISysUserService sysUserService;

	@Override
	public SysUser getUserInfo(String username) {
		SysUser sysUser = sysUserService.findUserByName(username);

		if (ObjectUtil.isNull(sysUser)) {
			return null;
		}
		HxTenantContextHolder.setTenantId(sysUser.getTenantId());
		return sysUserService.findUserInfo(sysUser);
	}

	@Override
	public SysUser getUserInfoByPhone(String phone) {
		SysUser sysUser = sysUserService.findUserByPhone(phone);
		if (ObjectUtil.isNull(sysUser)) {
			return null;
		}
		HxTenantContextHolder.setTenantId(sysUser.getTenantId());
		return sysUserService.findUserInfo(sysUser);
	}

	@Override
	public void createUser(SysUserReqDTO sysUserReqDTO) {
		sysUserService.createUser(sysUserReqDTO);
	}

}
