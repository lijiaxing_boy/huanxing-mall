package com.huanxing.cloud.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanxing.cloud.upms.api.entity.SysDict;
import org.apache.ibatis.annotations.Mapper;

/**
 * 字典
 *
 * @author lijx
 * @date 2022/10/21
 */
@Mapper
public interface SysDictMapper extends BaseMapper<SysDict> {

}
