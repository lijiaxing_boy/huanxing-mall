package com.huanxing.cloud.upms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huanxing.cloud.common.core.constant.CacheConstants;
import com.huanxing.cloud.upms.api.entity.SysDict;
import com.huanxing.cloud.upms.mapper.SysDictMapper;
import com.huanxing.cloud.upms.mapper.SysDictValueMapper;
import com.huanxing.cloud.upms.service.ISysDictValueService;
import com.huanxing.cloud.upms.api.entity.SysDictValue;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * 字典键值
 *
 * @author lijx
 * @date 2022/10/21
 */
@Service
@RequiredArgsConstructor
public class SysDictValueServiceImpl extends ServiceImpl<SysDictValueMapper, SysDictValue>
		implements ISysDictValueService {

	private final SysDictMapper sysDictMapper;

	@Override
	@CacheEvict(value = CacheConstants.DICT_CACHE, allEntries = true)
	public boolean saveDictValue(SysDictValue sysDictValue) {
		SysDict sysDict = sysDictMapper.selectById(sysDictValue.getDictId());
		if (Objects.isNull(sysDict)) {
			throw new IllegalArgumentException("字典不存在");
		}
		sysDictValue.setDictType(sysDict.getType());
		return this.save(sysDictValue);
	}

}
