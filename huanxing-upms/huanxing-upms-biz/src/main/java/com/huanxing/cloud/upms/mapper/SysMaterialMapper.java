package com.huanxing.cloud.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanxing.cloud.upms.api.entity.SysMaterial;
import org.apache.ibatis.annotations.Mapper;

/**
 * 素材
 *
 * @author lijx
 * @since 2022/2/22 15:02
 */
@Mapper
public interface SysMaterialMapper extends BaseMapper<SysMaterial> {

}
