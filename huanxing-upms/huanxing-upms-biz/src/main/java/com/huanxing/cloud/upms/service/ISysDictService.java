package com.huanxing.cloud.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.upms.api.entity.SysDict;

/**
 * 字典
 *
 * @author lijx
 * @date 2022/10/21
 */
public interface ISysDictService extends IService<SysDict> {

}
