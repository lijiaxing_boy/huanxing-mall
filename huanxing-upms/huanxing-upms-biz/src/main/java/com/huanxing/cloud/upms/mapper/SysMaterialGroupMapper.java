package com.huanxing.cloud.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanxing.cloud.upms.api.entity.SysMaterialGroup;
import org.apache.ibatis.annotations.Mapper;

/**
 * 素材分组
 *
 * @author lijx
 * @since 2022/2/22 15:27
 */
@Mapper
public interface SysMaterialGroupMapper extends BaseMapper<SysMaterialGroup> {

}
