package com.huanxing.cloud.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanxing.cloud.upms.api.entity.SysDept;
import org.apache.ibatis.annotations.Mapper;

/**
 * 部门管理
 *
 * @author lijx
 * @since 2022/2/26 16:49
 */
@Mapper
public interface SysDeptMapper extends BaseMapper<SysDept> {

}
