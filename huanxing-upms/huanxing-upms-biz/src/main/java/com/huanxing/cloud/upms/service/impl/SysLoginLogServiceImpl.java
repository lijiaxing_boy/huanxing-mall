package com.huanxing.cloud.upms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huanxing.cloud.upms.mapper.SysLoginLogMapper;
import com.huanxing.cloud.upms.service.ISysLoginLogService;
import com.huanxing.cloud.upms.api.entity.SysLoginLog;
import org.springframework.stereotype.Service;

/**
 * 登录日志
 *
 * @author lijx
 * @since 2022/2/26 16:47
 */
@Service
public class SysLoginLogServiceImpl extends ServiceImpl<SysLoginLogMapper, SysLoginLog> implements ISysLoginLogService {

}
