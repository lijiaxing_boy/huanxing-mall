package com.huanxing.cloud.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanxing.cloud.upms.api.entity.SysTenantPackage;
import org.apache.ibatis.annotations.Mapper;

/**
 * 租户套餐
 *
 * @author lijx
 * @since 2022/2/26 16:49
 */
@Mapper
public interface SysTenantPackageMapper extends BaseMapper<SysTenantPackage> {

}
