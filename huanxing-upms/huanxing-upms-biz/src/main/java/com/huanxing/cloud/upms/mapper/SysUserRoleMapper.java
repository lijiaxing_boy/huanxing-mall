package com.huanxing.cloud.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanxing.cloud.upms.api.entity.SysUserRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户关联角色
 *
 * @author lijx
 * @since 2022/2/26 16:50
 */
@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
