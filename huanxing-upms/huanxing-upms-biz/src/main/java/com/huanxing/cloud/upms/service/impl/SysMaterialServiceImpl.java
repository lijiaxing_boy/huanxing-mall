package com.huanxing.cloud.upms.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huanxing.cloud.upms.api.entity.SysMaterial;
import com.huanxing.cloud.upms.mapper.SysMaterialMapper;
import com.huanxing.cloud.upms.service.ISysMaterialService;
import org.springframework.stereotype.Service;

/**
 * 素材
 *
 * @author lijx
 * @since 2022/2/26 16:37
 */
@Service
public class SysMaterialServiceImpl extends ServiceImpl<SysMaterialMapper, SysMaterial> implements ISysMaterialService {

}
