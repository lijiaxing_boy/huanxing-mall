package com.huanxing.cloud.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huanxing.cloud.upms.api.entity.SysDictValue;
import org.apache.ibatis.annotations.Mapper;

/**
 * 字典键值
 *
 * @author lijx
 * @date 2022/10/21
 */
@Mapper
public interface SysDictValueMapper extends BaseMapper<SysDictValue> {

}
