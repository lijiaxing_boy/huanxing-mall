package com.huanxing.cloud.upms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.upms.api.entity.SysMenu;
import com.huanxing.cloud.upms.api.vo.MenuVO;

import java.util.List;

/**
 * 系统菜单
 *
 * @author lijx
 * @since 2022/2/26 16:47
 */
public interface ISysMenuService extends IService<SysMenu> {

	/**
	 * 通过角色主键查询菜单
	 * @param roleId
	 * @author lijx
	 * @date 2022/7/18
	 * @return: java.util.List<com.huanxing.cloud.upms.common.vo.MenuVO>
	 */
	List<MenuVO> findMenuByRoleId(String roleId);

	/**
	 * 保存
	 *
	 * @author: lijx
	 * @date: 2023/4/27 13:32
	 * @param: [sysMenu]
	 * @return: boolean
	 **/
	boolean saveMenu(SysMenu sysMenu);

	/**
	 * 删除
	 *
	 * @author: lijx
	 * @date: 2023/4/27 13:32
	 * @param: [id]
	 * @return: boolean
	 **/
	boolean removeMenuById(String id);

	/**
	 * 修改
	 *
	 * @author: lijx
	 * @date: 2023/4/27 13:32
	 * @param: [sysMenu]
	 * @return: boolean
	 **/
	boolean updateMenuById(SysMenu sysMenu);

}
