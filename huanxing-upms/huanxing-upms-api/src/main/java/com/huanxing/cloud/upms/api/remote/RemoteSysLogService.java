package com.huanxing.cloud.upms.api.remote;

import com.huanxing.cloud.upms.api.entity.SysLog;
import com.huanxing.cloud.upms.api.entity.SysLoginLog;

/**
 * 系统操作日志
 *
 * @author: lijx
 * @date: 2023/6/26 11:01
 **/
public interface RemoteSysLogService {

	/**
	 * 保存系统操作日志
	 * @param sysLog
	 * @author lijx
	 * @date 2022/6/28
	 * @return: com.huanxing.cloud.common.core.util.Result
	 */
	Boolean saveLog(SysLog sysLog);

	/**
	 * 保存登录日志
	 * @param sysLoginLog
	 * @author lijx
	 * @date 2022/6/28
	 * @return: com.huanxing.cloud.common.core.util.Result
	 */
	Boolean saveLoginLog(SysLoginLog sysLoginLog);

}
