package com.huanxing.cloud.upms.api.remote;

import com.huanxing.cloud.upms.api.dto.SysUserReqDTO;
import com.huanxing.cloud.upms.api.entity.SysUser;

/**
 * 系统用户 feign
 *
 * @author lijx
 * @date 2022/6/28
 */
public interface RemoteSysUserService {

	/**
	 * 通过用户名查询用户、角色信息
	 *
	 * @author lijx
	 * @date 2022/6/28
	 * @param username
	 * @return: com.huanxing.cloud.upms.common.entity.SysUser
	 */
	SysUser getUserInfo(String username);

	/**
	 * 通过手机号查询用户、角色信息
	 *
	 * @author lijx
	 * @date 2022/7/5
	 * @param phone
	 * @return: com.huanxing.cloud.common.core.util.Result<com.huanxing.cloud.upms.common.entity.SysUser>
	 */
	SysUser getUserInfoByPhone(String phone);

	/**
	 * 创建系统用户
	 * @param sysUserReqDTO
	 */
	void createUser(SysUserReqDTO sysUserReqDTO);

}
