package com.huanxing.cloud.upms.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 素材
 *
 * @author lijx
 * @since 2022/2/22 14:28
 */
@Data
@Schema(description = "素材")
@EqualsAndHashCode(callSuper = true)
@TableName(value = "sys_material")
public class SysMaterial extends Model<SysMaterial> {

	@Schema(description = "PK")
	@TableId(type = IdType.ASSIGN_ID)
	private String id;

	@Schema(description = "素材名")
	private String name;

	@Schema(description = "类型1、图片；2、视频")
	private String type;

	@Schema(description = "分组ID")
	private String groupId;

	@Schema(description = "素材链接")
	private String url;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建人")
	private String createBy;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改人")
	private String updateBy;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建时间")
	private LocalDateTime createTime;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改时间")
	private LocalDateTime updateTime;

	@TableLogic
	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "逻辑删除：0.显示；1.隐藏；")
	private String delFlag;

	@Schema(description = "租户id")
	private String tenantId;

	@Schema(description = "店铺ID")
	private String shopId;

	@Schema(description = "素材大小")
	private Long fileSize;

}
