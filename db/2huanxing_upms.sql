USE huanxing_upms;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
                             `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                             `parent_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '父级id',
                             `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '部门名称',
                             `leader` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '负责人',
                             `leader_phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '负责人联系电话',
                             `sort` int NULL DEFAULT NULL COMMENT '排序序号',
                             `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                             `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                             `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
                             `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户id',
                             `status` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '状态：0.正常；1.停用；',
                             `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                             `update_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('1', '0', '环兴科技有限公司', 'lijx', '17615123397', 1, '0', '2022-02-18 17:46:40', '2024-11-08 16:25:23', '1590229800633634816', '0', NULL, 'admin');
INSERT INTO `sys_dept` VALUES ('1494674952704667649', '1', '销售部门', '李家兴', NULL, 2, '0', '2022-02-18 22:07:34', '2024-11-08 15:29:17', '1590229800633634816', '0', NULL, 'admin');
INSERT INTO `sys_dept` VALUES ('2', '1', '开发部门', 'menglt', '17615123398', 1, '0', '2022-02-18 17:53:18', '2024-11-08 15:29:20', '1590229800633634816', '0', NULL, 'admin');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
                             `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                             `type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类型',
                             `description` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述',
                             `status` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '状态：0.正常；1.停用；',
                             `remarks` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                             `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                             `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                             `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
                             `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                             `update_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('1583357541572108290', 'sys_storage_type', '文件存储类型', '0', '文件存储配置类型', '0', '2022-10-21 15:20:31', '2022-10-21 15:55:51', NULL, NULL);
INSERT INTO `sys_dict` VALUES ('1585639342383202305', 'status', '状态', '0', '状态', '0', '2022-10-27 22:27:33', NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES ('1590901687026937857', 'application_key', '应用key', '0', '应用key用于租户授权', '0', '2022-11-11 10:58:14', NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES ('1597072635358756866', 'log_status', '日志状态', '0', NULL, '0', '2022-11-28 11:39:23', NULL, NULL, NULL);
INSERT INTO `sys_dict` VALUES ('1825783438989979649', 'shop_verify_status', '店铺审核状态', '0', '店铺审核状态', '0', '2024-08-20 14:34:28', NULL, 'admin', NULL);
INSERT INTO `sys_dict` VALUES ('1825786096614240258', 'pay_type', '支付类型', '0', '支付类型', '0', '2024-08-20 14:45:02', '2024-08-20 14:46:05', 'admin', 'admin');
INSERT INTO `sys_dict` VALUES ('1825786632403021826', 'pay_status', '支付状态', '0', '支付状态：0.未支付；1.已支付;', '0', '2024-08-20 14:47:09', '2024-08-20 14:47:09', 'admin', 'admin');
INSERT INTO `sys_dict` VALUES ('1825786791295840258', 'order_type', '订单类型', '0', '订单类型', '0', '2024-08-20 14:47:47', '2024-08-20 14:47:47', 'admin', 'admin');
INSERT INTO `sys_dict` VALUES ('1825787265549987842', 'delivery_way', '配送方式', '0', '配送方式', '0', '2024-08-20 14:49:40', '2024-08-20 14:49:40', 'admin', 'admin');
INSERT INTO `sys_dict` VALUES ('1826111485836263426', 'user_mission_type', '用户任务类型', '0', '用户任务类型', '0', '2024-08-21 12:18:00', NULL, 'admin', NULL);
INSERT INTO `sys_dict` VALUES ('1845468560374734850', 'user_source', '用户来源', '0', '用户来源', '0', '2024-10-13 22:16:07', NULL, 'admin', NULL);
INSERT INTO `sys_dict` VALUES ('1855529374229479425', 'menu_type', '菜单类型', '0', '菜单类型', '0', '2024-11-10 16:34:12', NULL, 'admin', NULL);
INSERT INTO `sys_dict` VALUES ('1869292192170770434', 'user_sex', '用户性别', '0', '用户性别', '0', '2024-12-18 16:02:43', NULL, 'lijx', NULL);
INSERT INTO `sys_dict` VALUES ('1871032024484069377', 'goods_verify_status', '商品审核状态', '0', '商品审核状态', '0', '2024-12-23 11:16:11', NULL, 'admin', NULL);
INSERT INTO `sys_dict` VALUES ('1871032754213273601', 'goods_status', '商品状态', '0', '商品状态', '0', '2024-12-23 11:19:05', NULL, 'admin', NULL);
INSERT INTO `sys_dict` VALUES ('1871032929883308033', 'goods_specs', '商品规格类型', '0', '商品规格类型', '0', '2024-12-23 11:19:47', NULL, 'admin', NULL);
INSERT INTO `sys_dict` VALUES ('1871381190397775873', 'refund_status', '退款状态', '0', '退款状态', '0', '2024-12-24 10:23:39', '2024-12-24 10:23:44', 'lijx', 'lijx');

-- ----------------------------
-- Table structure for sys_dict_value
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_value`;
CREATE TABLE `sys_dict_value`  (
                                   `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                                   `dict_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典主键',
                                   `dict_label` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典标签',
                                   `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典键值',
                                   `dict_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典类型',
                                   `status` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '状态：0.正常；1.停用；',
                                   `remarks` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
                                   `sort` int NULL DEFAULT NULL COMMENT '排序序号',
                                   `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                   `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                   `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
                                   `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                   `update_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                                   `show_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '回显样式',
                                   PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典键值表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_value
-- ----------------------------
INSERT INTO `sys_dict_value` VALUES ('1583358198555303938', '1583357541572108290', '阿里OSS', '1', 'sys_storage_type', '0', '阿里OSS', 1, '0', '2022-10-21 15:23:07', NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1583358231816134658', '1583357541572108290', '七牛云', '2', 'sys_storage_type', '0', '七牛云', 2, '0', '2022-10-21 15:23:15', '2022-10-21 15:25:30', NULL, NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1583364488060952577', '1583357541572108290', '腾讯云', '3', 'sys_storage_type', '0', '腾讯云', 3, '0', '2022-10-21 15:48:06', NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1585639417511575553', '1585639342383202305', '正常', '0', 'status', '0', '正常', 1, '0', '2022-10-27 22:27:51', NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1585639464043184129', '1585639342383202305', '停用', '1', 'status', '0', '停用', 2, '0', '2022-10-27 22:28:02', '2024-11-08 15:10:50', NULL, 'admin', NULL);
INSERT INTO `sys_dict_value` VALUES ('1590901958058668034', '1590901687026937857', '基础应用', 'app_base', 'application_key', '0', '商城基础功能', 1, '0', '2022-11-11 10:59:19', '2022-11-11 16:05:37', NULL, NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1590902082256203777', '1590901687026937857', '营销应用', 'app_market', 'application_key', '0', '营销应用包括（优惠券/多人拼团）', 2, '0', '2022-11-11 10:59:48', '2022-11-11 16:05:38', NULL, NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1590902440177135618', '1590901687026937857', '支付宝小程序', 'app_alipay', 'application_key', '0', '支付宝小程序', 3, '0', '2022-11-11 11:01:14', '2022-11-11 16:05:39', NULL, NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1590902629520601090', '1590901687026937857', '微信小程序', 'app_wechat', 'application_key', '0', '微信小程序', 4, '0', '2022-11-11 11:01:59', '2022-11-11 16:05:40', NULL, NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1592543489640525826', '1590901687026937857', '平台应用', 'sys_key', 'application_key', '0', '平台应用', 5, '0', '2022-11-15 23:42:11', '2022-11-15 23:44:14', NULL, NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1597072888086544385', '1597072635358756866', '成功', '1', 'log_status', '0', '成功', 1, '0', '2022-11-28 11:40:23', NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1597072943552020482', '1597072635358756866', '失败', '0', 'log_status', '0', '失败', 2, '0', '2022-11-28 11:40:36', NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1825783511652102146', '1825783438989979649', '审核中', '0', 'shop_verify_status', '0', '审核中', 0, '0', '2024-08-20 14:34:45', NULL, 'admin', NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1825783547135913986', '1825783438989979649', '已驳回', '1', 'shop_verify_status', '0', '已驳回', 1, '0', '2024-08-20 14:34:54', NULL, 'admin', NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1825783585480241154', '1825783438989979649', '已通过', '2', 'shop_verify_status', '0', '已通过', 2, '0', '2024-08-20 14:35:03', NULL, 'admin', NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1825786144924233730', '1825786096614240258', '微信支付', '1', '支付类型', '0', '微信支付', 1, '0', '2024-08-20 14:45:13', NULL, 'admin', NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1825786177023242241', '1825786096614240258', '支付宝支付', '2', '支付类型', '0', '支付宝支付', 2, '0', '2024-08-20 14:45:21', NULL, 'admin', NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1825786695783149569', '1825786632403021826', '未支付', '0', 'pay_status', '0', '未支付', 0, '0', '2024-08-20 14:47:24', NULL, 'admin', NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1825786736451121153', '1825786632403021826', '已支付', '1', 'pay_status', '0', '已支付', 1, '0', '2024-08-20 14:47:34', NULL, 'admin', NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1825786892013662209', '1825786791295840258', '普通订单', '1', 'order_type', '0', '普通订单', 1, '0', '2024-08-20 14:48:11', NULL, 'admin', NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1825786920325214210', '1825786791295840258', '拼团订单', '2', 'order_type', '0', '拼团订单', 2, '0', '2024-08-20 14:48:18', NULL, 'admin', NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1825787338904170497', '1825787265549987842', '普通快递', '1', 'delivery_way', '0', '普通快递', 1, '0', '2024-08-20 14:49:58', NULL, 'admin', NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1825787372362133505', '1825787265549987842', '上门自提', '2', 'delivery_way', '0', '上门自提', 2, '0', '2024-08-20 14:50:06', NULL, 'admin', NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1826111740216606722', '1826111485836263426', '商城下单', '1', 'user_mission_type', '0', '1', 1, '0', '2024-08-21 12:19:01', NULL, 'admin', NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1826111780091854849', '1826111485836263426', '商品分享', '2', 'user_mission_type', '0', '商品分享', 2, '0', '2024-08-21 12:19:11', NULL, 'admin', NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1826111808856391681', '1826111485836263426', '积分兑换', '3', 'user_mission_type', '0', '积分兑换', 3, '0', '2024-08-21 12:19:17', NULL, 'admin', NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1845468623192825858', '1845468560374734850', '微信小程序', 'WX_MA', 'user_source', '0', '微信小程序', 1, '0', '2024-10-13 22:16:22', NULL, 'admin', NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1845468655694487554', '1845468560374734850', 'APP', 'APP', 'user_source', '0', 'APP', 2, '0', '2024-10-13 22:16:29', NULL, 'admin', NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1845468737055596546', '1845468560374734850', '普通H5', 'H5', 'user_source', '0', '普通H5', 3, '0', '2024-10-13 22:16:49', NULL, 'admin', NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1855529435244019713', '1855529374229479425', '菜单', '0', 'menu_type', '0', '菜单', 0, '0', '2024-11-10 16:34:26', NULL, 'admin', NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1855529480534114306', '1855529374229479425', '按钮', '1', 'menu_type', '0', '按钮', 1, '0', '2024-11-10 16:34:37', NULL, 'admin', NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1869292244662484994', '1869292192170770434', '男', '1', 'user_sex', '0', '男', 1, '0', '2024-12-18 16:02:56', NULL, 'lijx', NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1869292302673903618', '1869292192170770434', '女', '2', 'user_sex', '0', '女', 2, '0', '2024-12-18 16:03:09', NULL, 'lijx', NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1871032165806948354', '1871032024484069377', '审核中', '0', 'goods_verify_status', '0', '审核中', 0, '0', '2024-12-23 11:16:45', NULL, 'admin', NULL, NULL);
INSERT INTO `sys_dict_value` VALUES ('1871032209813585921', '1871032024484069377', '已通过', '1', 'goods_verify_status', '0', '已通过', 1, '0', '2024-12-23 11:16:56', '2024-12-23 16:04:24', 'admin', 'lijx', 'success');
INSERT INTO `sys_dict_value` VALUES ('1871032254361288705', '1871032024484069377', '已驳回', '2', 'goods_verify_status', '0', '已驳回', 2, '0', '2024-12-23 11:17:06', '2024-12-23 16:04:14', 'admin', 'lijx', 'danger');
INSERT INTO `sys_dict_value` VALUES ('1871032820101595138', '1871032754213273601', '已下架', '0', 'goods_status', '0', '已下架', 0, '0', '2024-12-23 11:19:21', '2024-12-23 11:31:45', 'admin', 'admin', 'danger');
INSERT INTO `sys_dict_value` VALUES ('1871032850510299138', '1871032754213273601', '已上架', '1', 'goods_status', '0', '已上架', 1, '0', '2024-12-23 11:19:28', '2024-12-23 11:31:49', 'admin', 'admin', 'success');
INSERT INTO `sys_dict_value` VALUES ('1871033002629316609', '1871032929883308033', '单规格', '0', 'goods_specs', '0', '单规格', 0, '0', '2024-12-23 11:20:05', '2024-12-23 11:31:19', 'admin', 'admin', 'primary');
INSERT INTO `sys_dict_value` VALUES ('1871033035990810626', '1871032929883308033', '多规格', '1', 'goods_specs', '0', '多规格', 1, '0', '2024-12-23 11:20:13', '2024-12-23 11:31:25', 'admin', 'admin', 'success');
INSERT INTO `sys_dict_value` VALUES ('1871376256277168130', '1825786096614240258', '余额支付', '3', 'pay_type', '0', '余额支付', 3, '0', '2024-12-24 10:04:03', NULL, 'lijx', NULL, 'info');
INSERT INTO `sys_dict_value` VALUES ('1871381497785733121', '1871381190397775873', '退款中', '1', 'refund_status', '0', '退款中', 1, '0', '2024-12-24 10:24:52', NULL, 'lijx', NULL, 'primary');
INSERT INTO `sys_dict_value` VALUES ('1871381545097482242', '1871381190397775873', '退货退款中', '2', 'refund_status', '0', '退货退款中', 2, '0', '2024-12-24 10:25:04', NULL, 'lijx', NULL, 'primary');
INSERT INTO `sys_dict_value` VALUES ('1871381596116996098', '1871381190397775873', '同意退款', '11', 'refund_status', '0', '同意退款', 11, '0', '2024-12-24 10:25:16', NULL, 'lijx', NULL, 'success');
INSERT INTO `sys_dict_value` VALUES ('1871381634645872642', '1871381190397775873', '同意退货退款', '12', 'refund_status', '0', '同意退货退款', 12, '0', '2024-12-24 10:25:25', NULL, 'lijx', NULL, 'success');
INSERT INTO `sys_dict_value` VALUES ('1871381692225277953', '1871381190397775873', '拒绝退款', '21', 'refund_status', '0', '拒绝退款', 21, '0', '2024-12-24 10:25:39', NULL, 'lijx', NULL, 'warning');
INSERT INTO `sys_dict_value` VALUES ('1871381734331895809', '1871381190397775873', '拒绝退货退款', '22', 'refund_status', '0', '拒绝退货退款', 22, '0', '2024-12-24 10:25:49', NULL, 'lijx', NULL, 'warning');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
                            `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                            `ip_addr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'ip地址',
                            `status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '状态：0.失败；1.成功；',
                            `create_time` datetime NULL DEFAULT NULL COMMENT '新增时间',
                            `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录地点',
                            `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录用户',
                            `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '日志标题',
                            `request_method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求方式',
                            `request_uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求URI',
                            `request_params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '请求数据',
                            `request_time` bigint NULL DEFAULT NULL COMMENT '请求时长',
                            `ex_msg` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '异常信息',
                            `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作方法',
                            `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                            `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户id',
                            `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '操作日志' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_login_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_log`;
CREATE TABLE `sys_login_log`  (
                                  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                                  `ip_addr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'ip地址',
                                  `status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '状态：0.失败；1.成功；',
                                  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录用户',
                                  `location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录地点',
                                  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '信息',
                                  `browser` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '浏览器',
                                  `os` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作系统',
                                  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                  `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户id',
                                  `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '登录日志' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_login_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_material
-- ----------------------------
DROP TABLE IF EXISTS `sys_material`;
CREATE TABLE `sys_material`  (
                                 `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'PK',
                                 `type` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '类型1、图片；2、视频',
                                 `group_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '-1' COMMENT '分组ID  -1.未分组',
                                 `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '素材名',
                                 `url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '素材链接',
                                 `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                 `update_time` datetime NULL DEFAULT NULL COMMENT '最后更新时间',
                                 `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户id',
                                 `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                 `update_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                                 `shop_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '店铺ID',
                                 `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                 `file_size` int NULL DEFAULT NULL COMMENT '素材大小'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '素材' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_material
-- ----------------------------

-- ----------------------------
-- Table structure for sys_material_group
-- ----------------------------
DROP TABLE IF EXISTS `sys_material_group`;
CREATE TABLE `sys_material_group`  (
                                       `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                                       `name` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '分组名称',
                                       `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                       `update_time` datetime NULL DEFAULT NULL COMMENT '最后更新时间',
                                       `sort` int NOT NULL DEFAULT 0 COMMENT '排序',
                                       `type` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '1' COMMENT '分组类型：1.图片；2.视频',
                                       `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户id',
                                       `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                       `update_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                                       `shop_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '店铺ID',
                                       `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                       PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '素材分组' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_material_group
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
                             `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                             `name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
                             `permission` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单权限',
                             `path` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'URL',
                             `redirect` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '重定向url',
                             `parent_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '父菜单ID',
                             `icon` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图标',
                             `component` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '页面地址',
                             `sort` int NOT NULL DEFAULT 0 COMMENT '排序',
                             `type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类型: 0.菜单; 1.按钮;',
                             `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                             `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                             `outer_status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '外链状态：0.否；1.是；',
                             `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                             `application_key` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '应用id',
                             `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                             `update_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('100001', '菜单管理新增', 'upms:sysmenu:add', NULL, NULL, '10002', NULL, '', 0, '1', '2021-12-01 09:44:37', '2022-11-11 17:34:43', '0', '0', 'sys_key', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('10001', 'message.router.system', NULL, '/system', '/system/user', '0', 'iconfont icon-xitongshezhi', 'Layout', 80, '0', '2021-11-26 11:38:57', '2024-11-08 11:52:05', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('10002', 'message.router.systemMenu', NULL, '/system/menu', NULL, '10001', 'iconfont icon-caidan', 'upms/menu/index', 3, '0', '2021-11-26 11:37:40', '2022-11-27 16:19:14', '0', '0', 'sys_key', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1491684226094198786', 'message.router.systemRole', NULL, '/system/role', NULL, '10001', 'iconfont icon-icon-', 'upms/role/index', 2, '0', '2022-02-10 16:03:27', '2022-11-27 16:34:58', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1491690996678021121', '角色管理列表', 'upms:sysrole:page', NULL, NULL, '1491684226094198786', '', NULL, 1, '1', '2022-02-10 16:30:21', '2022-11-11 17:34:48', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1491752531735490561', 'message.router.systemUser', NULL, '/system/user', NULL, '10001', 'iconfont icon-shuaxin', 'upms/user/index', 1, '0', '2022-02-10 20:34:54', '2022-11-27 16:43:15', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1491756888363307009', '用户列表', 'upms:sysuser:page', NULL, NULL, '1491752531735490561', '', NULL, 1, '1', '2022-02-10 20:52:13', '2022-11-10 17:04:12', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1491757020773289986', '用户查询', 'upms:sysuser:get', NULL, NULL, '1491752531735490561', '', NULL, 1, '1', '2022-02-10 20:52:44', '2022-11-10 17:04:12', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1491757382771085313', '用户新增', 'upms:sysuser:add', NULL, NULL, '1491752531735490561', '', NULL, 1, '1', '2022-02-10 20:54:11', '2022-11-10 17:04:13', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1491948958826921986', '文件存储配置', NULL, '/system/storageconfig', NULL, '10001', 'iconfont icon-xitongshezhi', 'upms/storageconfig/index', 15, '0', '2022-02-11 09:35:25', '2022-11-11 17:34:50', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1491949315883827201', '文件存储配置查询', 'upms:storageconfig:get', NULL, NULL, '1491948958826921986', '', NULL, 1, '1', '2022-02-11 09:36:50', '2022-11-11 17:34:51', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1491969633293729794', '文件存储配置修改', 'upms:storageconfig:edit', NULL, NULL, '1491948958826921986', '', NULL, 1, '1', '2022-02-11 10:57:34', '2022-11-11 17:34:53', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1491973212968632322', '素材中心', NULL, '/system/material', NULL, '10001', 'iconfont icon-duosucai', 'upms/material/index', 50, '0', '2022-02-11 11:11:47', '2024-11-08 13:10:36', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1493578977630121986', '素材中心列表', 'upms:material:page', NULL, NULL, '1491973212968632322', NULL, NULL, 1, '1', '2022-02-15 21:32:38', '2024-08-21 21:23:37', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1493578977978249218', '素材中心查询', 'upms:material:get', NULL, NULL, '1491973212968632322', NULL, NULL, 1, '1', '2022-02-15 21:32:38', '2024-08-21 21:24:11', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1493578978313793537', '素材中心添加', 'upms:material:add', NULL, NULL, '1491973212968632322', NULL, NULL, 1, '1', '2022-02-15 21:32:38', '2024-08-21 21:33:54', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1493578978649337858', '素材中心修改', 'upms:material:edit', NULL, NULL, '1491973212968632322', NULL, NULL, 1, '1', '2022-02-15 21:32:38', '2024-08-21 21:34:23', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1493587429257539586', '商城商品', NULL, '/product/goods', '/product/goods/spu', '1779386487675092994', 'iconfont icon-shangpin', 'Layout', 1, '0', '2022-02-15 22:06:07', '2024-11-10 16:29:47', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1493587910381957121', '商品管理查询', 'product:goodsspu:get', NULL, NULL, '1532620395988029442', NULL, NULL, 1, '1', '2022-02-15 22:08:02', '2024-10-15 10:38:17', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1493587910721695745', '商品管理添加', 'product:goodsspu:add', NULL, NULL, '1532620395988029442', NULL, NULL, 1, '1', '2022-02-15 22:08:02', '2024-10-15 10:38:24', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1493587911057240066', '商品管理修改', 'product:goodsspu:edit', NULL, NULL, '1532620395988029442', NULL, NULL, 1, '1', '2022-02-15 22:08:02', '2024-10-15 10:38:30', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1493596733666652162', '素材分组查询', 'upms:materialgroup:get', NULL, NULL, '1491973212968632322', '', NULL, 2, '1', '2022-02-15 22:43:05', '2024-08-21 21:36:48', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1493766204259942401', '素材分组新增', 'upms:materialgroup:add', NULL, NULL, '1491973212968632322', '', NULL, 2, '1', '2022-02-16 09:56:27', '2024-08-21 21:36:55', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1493836091183411202', '角色管理查询', 'upms:sysrole:get', NULL, NULL, '1491684226094198786', '', NULL, 1, '1', '2022-02-16 14:34:10', '2022-11-11 17:34:56', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1493836209106268161', '角色管理新增', 'upms:sysrole:add', NULL, NULL, '1491684226094198786', '', NULL, 1, '1', '2022-02-16 14:34:38', '2022-11-11 17:34:57', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1493836280589791233', '角色管理修改', 'upms:sysrole:edit', NULL, NULL, '1491684226094198786', '', NULL, 1, '1', '2022-02-16 14:34:55', '2022-11-11 17:34:58', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1493841029473042434', '角色管理删除', 'upms:sysrole:del', NULL, NULL, '1491684226094198786', '', NULL, 1, '1', '2022-02-16 14:53:47', '2022-11-11 17:35:00', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1493884088730529793', 'message.router.systemLog', NULL, '/system/log', '/system/log/loginlog', '10001', 'iconfont icon-rizhiguanli', 'Layout', 10, '0', '2022-02-16 17:44:53', '2024-11-10 16:28:49', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1493884379760701442', '登录日志', NULL, '/system/log/loginlog', NULL, '1493884088730529793', 'iconfont icon-diannao1', 'upms/loginlog/index', 1, '0', '2022-02-16 17:46:02', '2022-11-11 17:35:07', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1494151991157673985', '操作日志', NULL, '/system/log/log', NULL, '1493884088730529793', 'iconfont icon-diannao1', 'upms/log/index', 20, '0', '2022-02-17 11:29:28', '2022-11-11 17:35:07', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1494153372996255746', '操作日志列表', 'upms:syslog:page', NULL, NULL, '1494151991157673985', NULL, NULL, 1, '1', '2022-02-17 11:34:57', '2022-11-11 17:35:07', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1494153373352771586', '操作日志查询', 'upms:syslog:get', NULL, NULL, '1494151991157673985', NULL, NULL, 1, '1', '2022-02-17 11:34:58', '2022-11-11 17:35:07', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1494153373696704514', '操作日志新增', 'upms:syslog:add', NULL, NULL, '1494151991157673985', NULL, NULL, 1, '1', '2022-02-17 11:34:58', '2022-11-11 17:35:07', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1494153374053220354', '操作日志修改', 'upms:syslog:edit', NULL, NULL, '1494151991157673985', NULL, NULL, 1, '1', '2022-02-17 11:34:58', '2022-11-11 17:35:07', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1494153374401347585', '操作日志删除', 'upms:syslog:del', NULL, NULL, '1494151991157673985', NULL, NULL, 1, '1', '2022-02-17 11:34:58', '2022-11-11 17:35:07', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1494192758630694913', '登录日志列表', 'upms:sysloginlog:page', NULL, NULL, '1493884379760701442', NULL, NULL, 1, '1', '2022-02-17 14:11:28', '2022-11-11 17:35:07', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1494192758974627842', '登录日志查询', 'upms:sysloginlog:get', NULL, NULL, '1493884379760701442', NULL, NULL, 1, '1', '2022-02-17 14:11:28', '2022-11-11 17:35:07', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1494192759318560770', '登录日志新增', 'upms:sysloginlog:add', NULL, NULL, '1493884379760701442', NULL, NULL, 1, '1', '2022-02-17 14:11:28', '2022-11-11 17:35:07', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1494192759662493698', '登录日志修改', 'upms:sysloginlog:edit', NULL, NULL, '1493884379760701442', NULL, NULL, 1, '1', '2022-02-17 14:11:28', '2022-11-11 17:35:07', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1494192760010620930', '登录日志删除', 'upms:sysloginlog:del', NULL, NULL, '1493884379760701442', NULL, NULL, 1, '1', '2022-02-17 14:11:28', '2022-11-11 17:35:08', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1494216988311183361', 'message.router.systemDept', NULL, '/system/dept', NULL, '10001', 'iconfont icon-bumenguanli', 'upms/dept/index', 4, '0', '2022-02-17 15:47:45', '2024-10-15 11:49:16', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1494217080162246658', '部门管理列表', 'upms:sysdept:page', NULL, NULL, '1494216988311183361', NULL, NULL, 1, '1', '2022-02-17 15:48:06', '2022-11-11 17:35:08', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1494217080510373890', '部门管理查询', 'upms:sysdept:get', NULL, NULL, '1494216988311183361', NULL, NULL, 1, '1', '2022-02-17 15:48:06', '2022-11-11 17:35:08', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1494217080858501121', '部门管理新增', 'upms:sysdept:add', NULL, NULL, '1494216988311183361', NULL, NULL, 1, '1', '2022-02-17 15:48:06', '2022-11-11 17:35:08', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1494217081206628354', '部门管理修改', 'upms:sysdept:edit', NULL, NULL, '1494216988311183361', NULL, NULL, 1, '1', '2022-02-17 15:48:06', '2022-11-11 17:35:08', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1494217081558949890', '部门管理删除', 'upms:sysdept:del', NULL, NULL, '1494216988311183361', NULL, NULL, 1, '1', '2022-02-17 15:48:07', '2022-11-11 17:35:08', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1494514119857180674', '用户修改', 'upms:sysuser:edit', NULL, NULL, '1491752531735490561', '', NULL, 1, '1', '2022-02-18 11:28:25', '2022-11-10 17:04:16', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1495687621054353410', '菜单管理修改', 'upms:sysmenu:edit', NULL, NULL, '10002', '', NULL, 1, '1', '2022-02-21 17:11:31', '2022-11-11 17:35:10', '0', '0', 'sys_key', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1495687858816864257', '菜单管理删除', 'upms:sysmenu:del', NULL, NULL, '10002', '', NULL, 1, '1', '2022-02-21 17:12:28', '2022-11-11 17:35:11', '0', '0', 'sys_key', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1496012487833960450', '商品管理删除', 'product:goodsspu:del', NULL, NULL, '1532620395988029442', '', NULL, 1, '1', '2022-02-22 14:42:24', '2024-10-15 10:38:38', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1496327483721789441', '商品类目', NULL, '/product/goodscategory', NULL, '1779386487675092994', 'iconfont icon-leimu', 'product/goodscategory/index', 10, '0', '2022-02-23 11:34:04', '2022-11-11 16:33:57', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1496327552973942785', '商品类目列表', 'product:goodscategory:page', NULL, NULL, '1496327483721789441', NULL, NULL, 1, '1', '2022-02-23 11:34:20', '2024-10-15 10:41:07', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1496327553334652930', '商品类目查询', 'product:goodscategory:get', NULL, NULL, '1496327483721789441', NULL, NULL, 1, '1', '2022-02-23 11:34:21', '2024-10-15 10:41:21', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1496327553699557377', '商品类目新增', 'product:goodscategory:add', NULL, NULL, '1496327483721789441', NULL, NULL, 1, '1', '2022-02-23 11:34:21', '2024-10-15 10:41:28', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1496327554068656130', '商品类目修改', 'product:goodscategory:edit', NULL, NULL, '1496327483721789441', NULL, NULL, 1, '1', '2022-02-23 11:34:21', '2024-10-15 10:42:06', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1496327554433560577', '商品类目删除', 'product:goodscategory:del', NULL, NULL, '1496327483721789441', NULL, NULL, 1, '1', '2022-02-23 11:34:21', '2024-10-15 10:42:12', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1496327802522447873', '运费模板', NULL, '/product/freighttemplate', NULL, '1779386487675092994', 'iconfont icon-yunfeib', 'product/freighttemplate/index', 60, '0', '2022-02-23 11:35:20', '2022-11-11 16:33:57', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1496327860647112706', '运费模板列表', 'product:freighttemplate:page', NULL, NULL, '1496327802522447873', NULL, NULL, 1, '1', '2022-02-23 11:35:34', '2024-10-15 10:44:05', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1496327861003628545', '运费模板查询', 'product:freighttemplate:get', NULL, NULL, '1496327802522447873', NULL, NULL, 1, '1', '2022-02-23 11:35:34', '2024-10-15 10:44:11', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1496327861351755778', '运费模板新增', 'product:freighttemplate:add', NULL, NULL, '1496327802522447873', NULL, NULL, 1, '1', '2022-02-23 11:35:34', '2024-10-15 10:44:21', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1496327861712465921', '运费模板修改', 'product:freighttemplate:edit', NULL, NULL, '1496327802522447873', NULL, NULL, 1, '1', '2022-02-23 11:35:34', '2024-10-15 10:44:27', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1496327862068981762', '运费模板删除', 'product:freighttemplate:del', NULL, NULL, '1496327802522447873', NULL, NULL, 1, '1', '2022-02-23 11:35:34', '2024-10-15 10:44:52', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1497468294740176898', '商品管理列表', 'product:goodsspu:page', NULL, NULL, '1532620395988029442', '', NULL, 1, '1', '2022-02-26 15:07:17', '2024-10-15 10:38:44', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1521469373525716994', '菜单管理列表', 'upms:sysmenu:page', NULL, NULL, '10002', '', NULL, 1, '1', '2022-05-03 20:39:03', '2022-11-11 17:35:14', '0', '0', 'sys_key', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1521496866882236418', '订单管理', NULL, '/order', '/order/orderinfo', '0', 'iconfont icon-shangchengdingdan', 'Layout', 40, '0', '2022-05-03 22:28:18', '2024-11-08 11:51:13', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1526179827628048385', '素材中心删除', 'upms:material:del', NULL, NULL, '1491973212968632322', 'icon-m-fuwenben', NULL, 1, '1', '2022-05-16 20:36:42', '2024-08-21 21:36:41', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1527471479688798209', '商城用户', NULL, '/user/userinfo', NULL, '1779386898750439425', 'iconfont icon-yonghuguanli', 'user/userinfo/index', 1, '0', '2022-05-20 10:09:14', '2022-11-11 16:33:57', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1527471918001954818', '商城用户列表', 'user:userinfo:page', NULL, NULL, '1527471479688798209', NULL, NULL, 1, '1', '2022-05-20 10:10:58', '2024-10-13 22:23:17', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1527471918337499138', '商城用户查询', 'user:userinfo:get', NULL, NULL, '1527471479688798209', NULL, NULL, 1, '1', '2022-05-20 10:10:58', '2024-10-13 22:23:23', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1527471918694014977', '商城用户新增', 'user:userinfo:add', NULL, NULL, '1527471479688798209', NULL, NULL, 1, '1', '2022-05-20 10:10:58', '2024-10-13 22:23:28', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1527471919042142209', '商城用户修改', 'user:userinfo:edit', NULL, NULL, '1527471479688798209', NULL, NULL, 1, '1', '2022-05-20 10:10:58', '2024-10-13 22:23:35', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1527471919386075137', '商城用户删除', 'user:userinfo:del', NULL, NULL, '1527471479688798209', NULL, NULL, 1, '1', '2022-05-20 10:10:58', '2024-10-13 22:23:40', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1527835787455164418', '商城装修', NULL, '/shop/theme', '/shop/theme/pagedesign', '1779386782480138242', 'iconfont icon-zhuti_tiaosepan_o', 'Layout', 10, '0', '2022-05-21 10:16:50', '2024-11-10 16:27:46', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1527835963171336193', '主题设置', NULL, '/shop/theme/mobile', NULL, '1527835787455164418', 'iconfont icon-zhuti', 'shop/mobiletheme/index', 1, '0', '2022-05-21 10:17:32', '2022-11-11 16:33:57', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1527836009900077057', '商城装修列表', 'mall:mobiletheme:page', NULL, NULL, '1527835963171336193', NULL, NULL, 1, '1', '2022-05-21 10:17:43', '2022-11-10 17:04:19', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1527836010248204290', '商城装修查询', 'mall:mobiletheme:get', NULL, NULL, '1527835963171336193', NULL, NULL, 1, '1', '2022-05-21 10:17:43', '2022-11-10 17:04:19', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1527836010596331522', '商城装修新增', 'mall:mobiletheme:add', NULL, NULL, '1527835963171336193', NULL, NULL, 1, '1', '2022-05-21 10:17:43', '2022-11-10 17:04:19', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1527836010944458754', '商城装修修改', 'mall:mobiletheme:edit', NULL, NULL, '1527835963171336193', NULL, NULL, 1, '1', '2022-05-21 10:17:43', '2022-11-10 17:04:19', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1527836011300974593', '商城装修删除', 'mall:mobiletheme:del', NULL, NULL, '1527835963171336193', NULL, NULL, 1, '1', '2022-05-21 10:17:43', '2022-11-10 17:04:20', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1527928859455168514', '服务监控', NULL, '/system/server', NULL, '10001', 'iconfont icon-diannao1', 'upms/sysserver/index', 25, '0', '2022-05-21 16:26:40', '2022-11-11 17:35:18', '0', '0', 'sys_key', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1527947365856878593', '服务监控查询', 'upms:sysserver:get', NULL, NULL, '1527928859455168514', '', NULL, 1, '1', '2022-05-21 17:40:14', '2022-11-11 17:35:18', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1531445896986435585', '支付配置', NULL, '/pay/payconfig', NULL, '1670722611195203586', 'iconfont icon-xitongshezhi', 'pay/payconfig/index', 5, '0', '2022-05-31 09:22:09', '2022-11-11 17:35:19', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1531446009649635329', '支付配置列表', 'pay:payconfig:page', NULL, NULL, '1531445896986435585', NULL, NULL, 1, '1', '2022-05-31 09:22:36', '2024-10-15 10:59:58', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1531446009779658754', '支付配置查询', 'pay:payconfig:get', NULL, NULL, '1531445896986435585', NULL, NULL, 1, '1', '2022-05-31 09:22:36', '2024-10-15 11:00:04', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1531446009913876482', '支付配置新增', 'pay:payconfig:add', NULL, NULL, '1531445896986435585', NULL, NULL, 1, '1', '2022-05-31 09:22:36', '2024-10-15 11:00:11', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1531446010039705602', '支付配置修改', 'pay:payconfig:edit', NULL, NULL, '1531445896986435585', NULL, NULL, 1, '1', '2022-05-31 09:22:36', '2024-10-15 11:00:17', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1531446010169729026', '支付配置删除', 'pay:payconfig:del', NULL, NULL, '1531445896986435585', NULL, NULL, 1, '1', '2022-05-31 09:22:36', '2024-10-15 11:00:23', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1531528760525074434', '商城订单', NULL, '/order/orderinfo', NULL, '1521496866882236418', 'iconfont icon-shangchengdingdan', 'order/orderinfo/index', 10, '0', '2022-05-31 14:51:23', '2022-11-11 16:33:57', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1531529196871102466', '商城退单', NULL, '/order/orderrefund', NULL, '1521496866882236418', 'iconfont icon-tuidan', 'order/orderrefund/index', 20, '0', '2022-05-31 14:53:07', '2022-11-11 16:33:57', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1531536449854517250', '商城订单列表', 'order:orderinfo:page', NULL, NULL, '1531528760525074434', '', NULL, 1, '1', '2022-05-31 15:21:57', '2024-10-15 10:49:27', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1531536545572728833', '商城订单查询', 'order:orderinfo:get', NULL, NULL, '1531528760525074434', '', NULL, 1, '1', '2022-05-31 15:22:19', '2024-10-15 10:49:33', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1531536746446336001', '商城订单删除', 'order:orderinfo:del', NULL, NULL, '1531528760525074434', '', NULL, 1, '1', '2022-05-31 15:23:07', '2024-10-15 10:49:39', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1531536866638311426', '商城订单发货', 'order:orderinfo:deliver', NULL, NULL, '1531528760525074434', '', NULL, 1, '1', '2022-05-31 15:23:36', '2024-10-15 10:49:56', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1531537172243689474', '商城退单列表', 'order:orderrefund:page', NULL, NULL, '1531529196871102466', '', NULL, 1, '1', '2022-05-31 15:24:49', '2024-10-15 10:59:31', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1531537289042472961', '商城退单查询', 'order:orderrefund:get', NULL, NULL, '1531529196871102466', '', NULL, 1, '1', '2022-05-31 15:25:17', '2024-10-15 10:59:36', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1531846396944785409', '微信管理', NULL, '/miniapp/weixin', '/miniapp/weixin/wxapp', '1539129183310196738', 'iconfont icon-weixin', 'Layout', 30, '0', '2022-06-01 11:53:36', '2024-11-10 16:28:35', '0', '0', 'app_wechat', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1531846725585281025', '微信用户', NULL, '/miniapp/weixin/wxuser', NULL, '1531846396944785409', 'iconfont icon-weixin', 'miniapp/weixin/wxuser/index', 10, '0', '2022-06-01 11:54:54', '2022-11-11 16:35:19', '0', '0', 'app_wechat', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1531846893999169537', '微信应用', NULL, '/miniapp/weixin/wxapp', NULL, '1531846396944785409', 'iconfont icon-weixin', 'miniapp/weixin/wxapp/index', 5, '0', '2022-06-01 11:55:34', '2022-11-10 17:04:22', '0', '0', 'app_wechat', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1531882726605758465', '微信用户列表', 'weixin:wxuser:page', NULL, NULL, '1531846725585281025', '', NULL, 1, '1', '2022-06-01 14:17:54', '2022-11-10 17:04:22', '0', '0', 'app_wechat', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1531882841361915906', '微信应用列表', 'weixin:wxapp:page', NULL, NULL, '1531846893999169537', '', NULL, 1, '1', '2022-06-01 14:18:22', '2022-11-10 17:04:22', '0', '0', 'app_wechat', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1532620101065543681', '商品规格', NULL, '/product/goods/specs', NULL, '1493587429257539586', 'iconfont icon-bolangneng', 'product/goodsspecs/index', 20, '0', '2022-06-03 15:08:01', '2022-11-11 16:33:58', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1532620395988029442', '全部商品', NULL, '/product/goods/spu', '', '1493587429257539586', 'iconfont icon-shangpin', 'product/goodsspu/index', 10, '0', '2022-06-03 15:09:11', '2024-11-10 16:29:40', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1532620840659750914', '商品规格列表', 'product:goodsspecs:page', NULL, NULL, '1532620101065543681', '', NULL, 1, '1', '2022-06-03 15:10:57', '2024-10-15 10:38:53', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1532620931885862913', '商品规格查询', 'product:goodsspecs:get', '', NULL, '1532620101065543681', '', NULL, 1, '1', '2022-06-03 15:11:19', '2024-10-15 10:39:00', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1532621007333003266', '商品规格新增', 'product:goodsspecs:add', NULL, NULL, '1532620101065543681', '', NULL, 1, '1', '2022-06-03 15:11:37', '2024-10-15 10:39:07', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1532621107589451778', '商品规格修改', 'product:goodsspecs:edit', NULL, NULL, '1532620101065543681', '', NULL, 1, '1', '2022-06-03 15:12:01', '2024-10-15 10:39:14', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1532621196013768706', '商品规格删除', 'product:goodsspecs:del', NULL, NULL, '1532620101065543681', '', NULL, 1, '1', '2022-06-03 15:12:22', '2024-10-15 10:39:23', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1532634358100430850', '商品规格值列表', 'product:goodsspecsvalue:page', NULL, NULL, '1532620101065543681', '', NULL, 2, '1', '2022-06-03 16:04:40', '2024-10-15 10:39:30', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1532634435510505473', '商品规格值查询', 'product:goodsspecsvalue:get', NULL, NULL, '1532620101065543681', '', NULL, 2, '1', '2022-06-03 16:04:59', '2024-10-15 10:40:20', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1532634517374930946', '商品规格值新增', 'product:goodsspecsvalue:add', NULL, NULL, '1532620101065543681', '', NULL, 2, '1', '2022-06-03 16:05:18', '2024-10-15 10:40:26', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1532634586568364034', '商品规格值修改', 'product:goodsspecsvalue:edit', NULL, NULL, '1532620101065543681', '', NULL, 2, '1', '2022-06-03 16:05:35', '2024-10-15 10:40:32', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1532634687902748674', '商品规格值删除', 'product:goodsspecsvaluedel', NULL, NULL, '1532620101065543681', 'icon-ziti', NULL, 2, '1', '2022-06-03 16:05:59', '2024-10-15 10:40:38', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1535633963410956289', '物流信息查询', 'order:orderLogistics:get', NULL, NULL, '1531528760525074434', '', NULL, 1, '1', '2022-06-11 22:44:03', '2024-10-15 10:50:02', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1536174316656533505', '物流公司', NULL, '/shop/logisticscompany', NULL, '1779386782480138242', 'iconfont icon-wuliu', 'shop/logisticscompany/index', 100, '0', '2022-06-13 10:31:10', '2022-11-11 16:33:58', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1536174413406543874', '物流公司查询', 'mall:logisticscompany:get', NULL, NULL, '1536174316656533505', '', NULL, 1, '1', '2022-06-13 10:31:33', '2022-11-11 17:35:26', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1537048076783616001', '素材分组列表', 'upms:materialgroup:page', NULL, NULL, '1491973212968632322', '', NULL, 2, '1', '2022-06-15 20:23:13', '2024-08-21 21:37:01', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1537066492991012865', '素材分组修改', 'upms:materialgroup:edit', NULL, NULL, '1491973212968632322', '', NULL, 2, '1', '2022-06-15 21:36:23', '2024-08-21 21:37:07', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1537066557067395074', '素材分组删除', 'upms:materialgroup:del', NULL, NULL, '1491973212968632322', '', NULL, 2, '1', '2022-06-15 21:36:39', '2024-08-21 22:07:04', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1539129183310196738', 'message.router.miniapp', NULL, '/miniapp', '/miniapp/weixin/wxapp', '0', 'iconfont icon-xiaochengxu', 'Layout', 70, '0', '2022-06-21 14:12:46', '2024-11-08 11:57:22', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1543116535774879745', '商城退单退款', 'order:orderrefund:refund', NULL, NULL, '1531529196871102466', '', NULL, 1, '1', '2022-07-02 14:17:06', '2024-10-15 10:59:43', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1547110946242326529', '用户修改密码', 'upms:sysuser:password', NULL, NULL, '1491752531735490561', '', NULL, 1, '1', '2022-07-13 14:49:27', '2022-11-10 17:04:26', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1547111164111253505', '用户删除', 'upms:sysuser:del', NULL, NULL, '1491752531735490561', '', NULL, 1, '1', '2022-07-13 14:50:19', '2022-11-10 17:04:26', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1579402420152627202', '用户账单分页列表', 'user:userbill:page', NULL, NULL, '1527471479688798209', '', NULL, 2, '1', '2022-10-10 17:24:16', '2024-10-13 22:23:46', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1583296344638287874', 'message.router.systemDict', NULL, '/system/dict', NULL, '10001', 'iconfont icon-zidianguanli', 'upms/dict/index', 5, '0', '2022-10-21 11:17:20', '2024-10-15 11:53:21', '0', '0', 'sys_key', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1583296419947016193', '字典列表', 'upms:sysdict:page', NULL, NULL, '1583296344638287874', '', NULL, 1, '1', '2022-10-21 11:17:38', '2022-11-10 17:04:30', '0', '0', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1583296499370356738', '字典键值列表', 'upms:sysdictvalue:page', NULL, NULL, '1583296344638287874', '', NULL, 2, '1', '2022-10-21 11:17:57', '2022-11-10 17:04:30', '0', '0', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1583346865315131394', '字典新增', 'upms:sysdict:add', NULL, NULL, '1583296344638287874', '', NULL, 1, '1', '2022-10-21 14:38:05', '2022-11-10 17:04:30', '0', '0', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1583346942263832578', '字典修改', 'upms:sysdict:edit', NULL, NULL, '1583296344638287874', '', NULL, 1, '1', '2022-10-21 14:38:23', '2022-11-10 17:04:30', '0', '0', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1583347000673710082', '字典删除', 'upms:sysdict:del', NULL, NULL, '1583296344638287874', '', NULL, 1, '1', '2022-10-21 14:38:37', '2022-11-10 17:04:31', '0', '0', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1583351948518031361', '字典查询', 'upms:sysdict:get', NULL, NULL, '1583296344638287874', '', NULL, 1, '1', '2022-10-21 14:58:17', '2022-11-10 17:04:31', '0', '0', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1583355071571648514', '字典键值查询', 'upms:sysdictvalue:get', NULL, NULL, '1583296344638287874', '', NULL, 2, '1', '2022-10-21 15:10:41', '2022-11-10 17:04:31', '0', '0', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1583355125187436546', '字典键值新增', 'upms:sysdictvalue:add', NULL, NULL, '1583296344638287874', '', NULL, 2, '1', '2022-10-21 15:10:54', '2022-11-10 17:04:31', '0', '0', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1583355179121991681', '字典键值修改', 'upms:sysdictvalue:edit', NULL, NULL, '1583296344638287874', '', NULL, 2, '1', '2022-10-21 15:11:07', '2022-11-10 17:04:31', '0', '0', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1583355228157599745', '字典键值删除', 'upms:sysdictvalue:del', NULL, NULL, '1583296344638287874', '', NULL, 2, '1', '2022-10-21 15:11:19', '2022-11-10 17:04:31', '0', '0', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1585191363078467586', '优惠券', NULL, '/promotion/coupon', '/promotion/coupon/info', '1779386604402573314', 'iconfont icon-youhuiquan', 'Layout', 1, '0', '2022-10-26 16:47:27', '2024-11-10 16:28:24', '0', '0', 'app_market', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1585191589231144962', '优惠券管理', NULL, '/promotion/coupon/info', NULL, '1585191363078467586', 'iconfont icon-quanjushezhi_o', 'promotion/couponinfo/index', 1, '0', '2022-10-26 16:48:21', '2022-11-11 16:33:59', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1585192004932808706', '优惠券列表', 'promotion:couponinfo:page', NULL, NULL, '1585191589231144962', '', NULL, 1, '1', '2022-10-26 16:50:00', '2024-10-15 11:04:26', '0', '0', 'app_market', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1585192140589182977', '优惠券查询', 'promotion:couponinfo:get', NULL, NULL, '1585191589231144962', '', NULL, 1, '1', '2022-10-26 16:50:32', '2024-10-15 11:04:34', '0', '0', 'app_market', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1585192296575348738', '优惠券新增', 'promotion:couponinfo:add', NULL, NULL, '1585191589231144962', '', NULL, 1, '1', '2022-10-26 16:51:09', '2024-10-15 11:04:40', '0', '0', 'app_market', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1585192489970511873', '优惠券修改', 'promotion:couponinfo:edit', NULL, NULL, '1585191589231144962', '', NULL, 1, '1', '2022-10-26 16:51:55', '2024-10-15 11:04:47', '0', '0', 'app_market', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1585192568882147330', '优惠券删除', 'promotion:couponinfo:del', NULL, NULL, '1585191589231144962', '', NULL, 1, '1', '2022-10-26 16:52:14', '2024-10-15 11:04:59', '0', '0', 'app_market', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1585916026725281793', '用户领券记录', NULL, '/promotion/coupon/user', NULL, '1585191363078467586', 'iconfont icon-zhongduancanshu', 'promotion/couponuser/index', 10, '0', '2022-10-28 16:47:00', '2022-11-11 16:33:59', '0', '0', 'app_market', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1585916172586397697', '用户领券记录列表', 'promotion:couponuser:page', NULL, NULL, '1585916026725281793', '', NULL, 1, '1', '2022-10-28 16:47:35', '2024-10-15 11:05:07', '0', '0', 'app_market', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1590265776187023362', '租户管理', NULL, '/system/tenant', NULL, '10001', 'iconfont icon-zuhuguanli', 'upms/tenant/index', 6, '0', '2022-11-09 16:51:21', '2024-10-15 11:53:12', '0', '0', 'sys_key', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1590265951848669185', '租户列表', 'upms:systenant:page', NULL, NULL, '1590265776187023362', '', NULL, 1, '1', '2022-11-09 16:52:03', '2022-11-10 17:04:32', '0', '0', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1590266107100831746', '租户新增', 'upms:systenant:add', NULL, NULL, '1590265776187023362', '', NULL, 1, '1', '2022-11-09 16:52:40', '2022-11-10 17:04:32', '0', '0', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1590700851537625090', '更新角色菜单', 'upms:sysrole:update', NULL, NULL, '1491684226094198786', '', NULL, 1, '1', '2022-11-10 21:40:11', '2022-11-11 17:36:07', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1590704386824192002', '租户查询', 'upms:systenant:get', NULL, NULL, '1590265776187023362', '', NULL, 1, '1', '2022-11-10 21:54:14', '2022-11-10 21:54:14', '0', '0', '', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1590704512468762626', '租户修改', 'upms:systenant:edit', NULL, NULL, '1590265776187023362', '', NULL, 1, '1', '2022-11-10 21:54:44', '2022-11-10 21:54:44', '0', '0', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1590972046640115713', '租户套餐', NULL, '/system/tenantpackage', NULL, '10001', 'iconfont icon-zuhutaocan', 'upms/tenantpackage/index', 8, '0', '2022-11-11 15:37:49', '2024-10-15 11:53:02', '0', '0', 'sys_key', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1590972187874914305', '租户套餐列表', 'upms:tenantpackage:page', NULL, NULL, '1590972046640115713', '', NULL, 1, '1', '2022-11-11 15:38:23', '2022-11-11 15:38:23', '0', '0', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1590972258943201281', '租户套餐查询', 'upms:tenantpackage:get', NULL, NULL, '1590972046640115713', '', NULL, 1, '1', '2022-11-11 15:38:40', '2022-11-11 15:38:40', '0', '0', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1590972321283141633', '租户套餐新增', 'upms:tenantpackage:add', NULL, NULL, '1590972046640115713', '', NULL, 1, '1', '2022-11-11 15:38:54', '2022-11-11 15:38:54', '0', '0', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1590972375171559426', '租户套餐修改', 'upms:tenantpackage:edit', NULL, NULL, '1590972046640115713', '', NULL, 1, '1', '2022-11-11 15:39:07', '2022-11-11 15:39:07', '0', '0', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1590972439457656833', '租户删除套餐', 'upms:tenantpackage:del', NULL, NULL, '1590972046640115713', '', NULL, 1, '1', '2022-11-11 15:39:23', '2022-11-11 15:39:23', '0', '0', NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1600477837933047810', '微页面', '', '/shop/theme/pagedesign', '', '1527835787455164418', 'iconfont icon-quanjushezhi_o', 'shop/pagedesign/index', 0, '0', '2022-12-07 21:10:26', '2024-11-10 16:27:34', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1600478029277196290', '页面设计列表', 'shop:pagedesign:page', '', NULL, '1600477837933047810', '', '', 1, '1', '2022-12-07 21:10:26', '2024-10-15 11:55:01', '', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1600478336006647809', '页面设计新增', 'shop:pagedesign:add', '', NULL, '1600477837933047810', '', '', 1, '1', '2022-12-07 21:12:25', '2024-10-15 11:55:08', '', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1600478397226708994', '页面设计修改', 'shop:pagedesign:edit', '', NULL, '1600477837933047810', '', '', 1, '1', '2022-12-07 21:12:39', '2024-10-15 11:55:15', '', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1600478467569381377', '页面设计删除', 'shop:pagedesign:del', '', NULL, '1600477837933047810', '', '', 1, '1', '2022-12-07 21:12:56', '2024-10-15 11:55:22', '', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1600785452746854401', '页面设计查询', 'shop:pagedesign:get', '', NULL, '1600477837933047810', '', '', 1, '1', '2022-12-08 17:32:48', '2024-10-15 11:55:28', '', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1605129651156598786', '在线用户', '', '/system/onlineuser', NULL, '10001', 'iconfont icon-shuaxin', 'upms/onlineuser/index', 9, '0', '2022-12-20 17:15:05', '2022-12-20 17:15:05', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1605129783281369089', '在线用户查询', 'upms:onlineuser:get', '', NULL, '1605129651156598786', '', '', 1, '1', '2022-12-20 17:15:36', '2022-12-20 17:15:36', '', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1605130091042619393', '在线用户强退', 'upms:onlineuser:forced', '', NULL, '1605129651156598786', '', '', 1, '1', '2022-12-20 17:15:05', '2022-12-20 17:15:05', '', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1612481861469302786', '物流公司修改', 'mall:logisticscompany:edit', '', NULL, '1536174316656533505', '', '', 1, '1', '2023-01-10 00:10:09', '2023-01-10 00:10:09', '', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1612683975455657985', '物流公司列表', 'mall:logisticscompany:page', NULL, NULL, '1536174316656533505', '', NULL, 1, '1', '2022-06-13 10:31:33', '2022-11-11 17:35:26', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1612684125251031042', '物流公司新增', 'mall:logisticscompany:add', '', NULL, '1536174316656533505', '', '', 1, '1', '2023-01-10 00:10:09', '2023-01-10 00:10:09', '', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1612684389953556481', '物流公司删除', 'mall:logisticscompany:del', '', NULL, '1536174316656533505', '', '', 1, '1', '2023-01-10 13:34:55', '2023-01-10 13:34:55', '', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1615718593878704130', '微信应用查询', 'weixin:wxapp:get', '', NULL, '1531846893999169537', '', '', 1, '1', '2023-01-18 22:31:48', '2023-01-18 22:31:48', '', '0', 'app_wechat', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1615718665584525314', '微信应用新增', 'weixin:wxapp:add', '', NULL, '1531846893999169537', '', '', 1, '1', '2023-01-18 22:32:05', '2023-01-18 22:32:05', '', '0', 'app_wechat', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1615718738766741505', '微信应用修改', 'weixin:wxapp:edit', '', NULL, '1531846893999169537', '', '', 1, '1', '2023-01-18 22:32:22', '2023-01-18 22:32:22', '', '0', 'app_wechat', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1615718811491778562', '微信应用删除', 'weixin:wxapp:del', '', NULL, '1531846893999169537', '', '', 1, '1', '2023-01-18 22:32:40', '2023-01-18 22:32:40', '', '0', 'app_wechat', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1670722611195203586', '支付管理', '', '/pay', '/pay/payconfig', '0', 'iconfont icon-zhifu', 'Layout', 50, '0', '2023-06-19 17:18:05', '2024-11-08 11:51:21', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1670722888228982785', '支付订单', '', '/pay/payorder', NULL, '1670722611195203586', 'iconfont icon-zhifudingdan', 'pay/paytradeorder/index', 10, '0', '2023-06-19 17:19:11', '2024-10-15 11:45:47', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1670723480636674049', '支付退单', '', '/pay/payrefund', NULL, '1670722611195203586', 'iconfont icon-bolangneng', 'pay/payrefundorder/index', 15, '0', '2023-06-19 17:19:11', '2023-06-19 17:19:11', '0', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1670724676428550146', '支付订单列表', 'pay:paytradeorder:page', '', NULL, '1670722888228982785', '', '', 1, '1', '2023-06-19 17:19:11', '2024-10-15 11:01:02', '', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1670725150502342658', '支付订单详情', 'pay:paytradeorder:get', '', NULL, '1670722888228982785', '', '', 1, '1', '2023-06-19 17:19:11', '2024-10-15 11:01:10', '', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1670725580288479233', '支付退单列表', 'pay:payrefundorder:page', '', NULL, '1670723480636674049', '', '', 1, '1', '2023-06-19 17:19:11', '2024-10-15 11:02:06', '', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1670725673255227394', '支付退单详情', 'pay:payrefundorder:get', '', NULL, '1670723480636674049', '', '', 1, '1', '2023-06-19 17:19:11', '2024-10-15 11:02:13', '', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1670732624412332034', '回调记录', '', '/pay/notifyrecord', NULL, '1670722611195203586', 'iconfont icon-jilu', 'pay/paynotifyrecord/index', 20, '0', '2023-06-19 17:57:53', '2024-10-15 11:45:59', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1670732985965531137', '回调记录列表', 'pay:paynotifyrecord:page', '', NULL, '1670732624412332034', '', '', 1, '1', '2023-06-19 17:57:53', '2024-10-15 11:03:08', '', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1671121808444674049', '回调记录查询', 'pay:paynotifyrecord:get', '', NULL, '1670732624412332034', '', '', 1, '1', '2023-06-19 17:57:53', '2024-10-15 11:03:14', '', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1681556362827534338', '商城订单取消', 'order:orderinfo:cancel', '', NULL, '1531528760525074434', '', '', 1, '1', '2023-07-19 14:47:33', '2024-10-15 10:50:11', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1779386487675092994', '商品管理', '', '/product', '/product/goods/spu', '0', 'iconfont icon-shangpin', 'Layout', 30, '0', '2024-04-14 13:49:32', '2024-11-10 16:29:28', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1779386604402573314', '营销管理', '', '/promotion', '/promotion/coupon/info', '0', 'iconfont icon-yingxiaoguanli1', 'Layout', 60, '0', '2024-04-14 13:50:00', '2024-11-08 11:57:14', '0', '0', 'app_market', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1779386782480138242', '店铺管理', '', '/shop', '/shop/all', '0', 'iconfont icon-dianpu', 'Layout', 20, '0', '2024-04-14 13:50:43', '2024-11-08 11:50:55', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1779386898750439425', '会员管理', '', '/user', '/user/userinfo', '0', 'iconfont icon-yonghuguanli', 'Layout', 10, '0', '2024-04-14 13:51:10', '2024-11-08 11:50:43', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1788196420663123969', '全部店铺', '', '/shop/all', NULL, '1779386782480138242', 'iconfont icon-dianpu', 'shop/shopinfo/index', 1, '0', '2024-05-08 21:17:04', '2024-10-15 11:39:20', '', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1788196662338920449', '店铺列表查询', 'shop:shopinfo:page', '', NULL, '1788196420663123969', '', '', 1, '1', '2024-05-08 21:17:04', '2024-05-08 21:17:04', '', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1788196733688225793', '店铺查询', 'shop:shopinfo:get', '', NULL, '1788196420663123969', '', '', 1, '1', '2024-05-08 21:17:04', '2024-05-08 21:17:04', '', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1788196790093225986', '店铺新增', 'shop:shopinfo:add', '', NULL, '1788196420663123969', '', '', 1, '1', '2024-05-08 21:17:04', '2024-05-08 21:17:04', '', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1788196840919801858', '店铺修改', 'shop:shopinfo:edit', '', NULL, '1788196420663123969', '', '', 1, '1', '2024-05-08 21:17:04', '2024-05-08 21:17:04', '', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1788196890592944129', '店铺删除', 'shop:shopinfo:del', '', NULL, '1788196420663123969', '', '', 1, '1', '2024-05-08 21:17:04', '2024-05-08 21:17:04', '', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1788375299184926722', '店铺商品分类', '', '/product/goodsshopcategory', NULL, '1779386487675092994', 'iconfont icon-leimu', 'product/goodsshopcategory/index', 20, '0', '2024-05-09 09:07:52', '2024-10-15 11:41:57', '0', '0', 'app_base', NULL, 'admin');
INSERT INTO `sys_menu` VALUES ('1788375557960900610', '店铺商品类目列表', 'product:goodsshopcategory:page', '', NULL, '1788375299184926722', '', '', 1, '1', '2024-05-09 09:07:52', '2024-05-09 09:07:52', '', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1788375651716177922', '店铺商品类目查询', 'product:goodsshopcategory:get', '', NULL, '1788375299184926722', '', '', 1, '1', '2024-05-09 09:07:52', '2024-05-09 09:07:52', '', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1788375752018763778', '店铺商品类目新增', 'product:goodsshopcategory:add', '', NULL, '1788375299184926722', '', '', 1, '1', '2024-05-09 09:07:52', '2024-05-09 09:07:52', '', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1788375826039840770', '店铺商品类目修改', 'product:goodsshopcategory:edit', '', NULL, '1788375299184926722', '', '', 1, '1', '2024-05-09 09:07:52', '2024-05-09 09:07:52', '', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1788375917437919234', '店铺商品类目删除', 'product:goodsshopcategory:del', '', NULL, '1788375299184926722', '', '', 1, '1', '2024-05-09 09:07:52', '2024-05-09 09:07:52', '', '0', 'app_base', NULL, NULL);
INSERT INTO `sys_menu` VALUES ('1792432863812636673', '店铺入驻', '', '/shop/apply', NULL, '1779386782480138242', 'iconfont icon-dianpuruzhu', 'shop/apply/index', 5, '0', '2024-05-20 13:51:11', '2024-10-15 11:41:05', '0', '0', 'app_base', 'lijx', 'admin');
INSERT INTO `sys_menu` VALUES ('1792433080825925634', '店铺入驻列表', 'shop:shopapply:page', '', NULL, '1792432863812636673', '', '', 1, '1', '2024-05-20 13:52:03', NULL, '', '0', 'app_base', 'lijx', NULL);
INSERT INTO `sys_menu` VALUES ('1792433157640409089', '店铺入驻查询', 'shop:shopapply:get', '', NULL, '1792432863812636673', '', '', 1, '1', '2024-05-20 13:52:21', NULL, '', '0', 'app_base', 'lijx', NULL);
INSERT INTO `sys_menu` VALUES ('1792433241006395393', '店铺入驻单新增', 'shop:shopapply:add', '', NULL, '1792432863812636673', '', '', 1, '1', '2024-05-20 13:52:41', NULL, '', '0', 'app_base', 'lijx', NULL);
INSERT INTO `sys_menu` VALUES ('1792433334379991041', '店铺入驻单修改', 'shop:shopapply:edit', '', NULL, '1792432863812636673', '', '', 1, '1', '2024-05-20 13:53:03', NULL, '', '0', 'app_base', 'lijx', NULL);
INSERT INTO `sys_menu` VALUES ('1792433410624049154', '店铺入驻单删除', 'shop:shopapply:del', '', NULL, '1792432863812636673', '', '', 1, '1', '2024-05-20 13:53:22', NULL, '', '0', 'app_base', 'lijx', NULL);
INSERT INTO `sys_menu` VALUES ('1792433529037639682', '店铺入驻单审核', 'shop:shopapply:verify', '', NULL, '1792432863812636673', '', '', 1, '1', '2024-05-20 13:53:50', NULL, '', '0', 'app_base', 'lijx', NULL);
INSERT INTO `sys_menu` VALUES ('1826217975943266305', '商品管理上架', 'mall:goodsspu:up', '', NULL, '1532620395988029442', '', '', 1, '1', '2024-08-21 19:21:10', '2024-08-21 19:21:41', '', '1', 'app_base', 'admin', 'admin');
INSERT INTO `sys_menu` VALUES ('1871099260888489985', '商品审核', 'product:goodsspu:verify', NULL, NULL, '1532620395988029442', NULL, NULL, 1, '1', '2024-12-23 15:43:22', NULL, '0', '0', 'app_base', 'lijx', NULL);
INSERT INTO `sys_menu` VALUES ('999999999999', '菜单管理查询', 'upms:sysmenu:get', NULL, NULL, '10002', NULL, NULL, 1, '1', '2022-02-21 16:11:30', '2022-11-10 17:04:32', '0', '0', 'app_base', NULL, NULL);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
                             `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                             `role_name` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
                             `role_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色编码',
                             `role_desc` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色描述',
                             `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                             `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
                             `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                             `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户id',
                             `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                             `update_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '超级管理员', 'ROLE_ADMIN', '系统管理员拥有全部权限', '2021-11-26 11:34:48', '2022-11-09 14:29:09', '0', '1590229800633634816', 'admin', NULL);
INSERT INTO `sys_role` VALUES ('1789672842141433858', '店铺管理员角色', 'ROLE_SHOP_ADMIN', '店铺管理员角色', '2024-05-12 23:03:51', NULL, '0', '1590229800633634816', 'admin', NULL);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
                                  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                                  `role_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色ID',
                                  `menu_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单ID',
                                  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                  `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户id',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色关联菜单表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('1590354323100123138', '1', '10003', '2022-11-09 22:43:16', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323100123139', '1', '1571769952871616513', '2022-11-09 22:43:16', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323100123140', '1', '1571769423743389697', '2022-11-09 22:43:16', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323100123141', '1', '1536174316656533505', '2022-11-09 22:43:16', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323100123143', '1', '1539129183310196738', '2022-11-09 22:43:16', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323100123145', '1', '1531976380791881730', '2022-11-09 22:43:17', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323100123147', '1', '1527835787455164418', '2022-11-09 22:43:17', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323100123149', '1', '1521496866882236418', '2022-11-09 22:43:17', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323100123151', '1', '1491973212968632322', '2022-11-09 22:43:17', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323100123155', '1', '1574664686803849217', '2022-11-09 22:43:17', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323100123157', '1', '1573537862937870338', '2022-11-09 22:43:17', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323100123158', '1', '1531846396944785409', '2022-11-09 22:43:17', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323100123159', '1', '1527928859455168514', '2022-11-09 22:43:17', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323100123160', '1', '1532620101065543681', '2022-11-09 22:43:17', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232002', '1', '1531529196871102466', '2022-11-09 22:43:17', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232003', '1', '1585191363078467586', '2022-11-09 22:43:17', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232004', '1', '1494151991157673985', '2022-11-09 22:43:17', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232006', '1', '1496327802522447873', '2022-11-09 22:43:17', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232007', '1', '1491948958826921986', '2022-11-09 22:43:17', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232008', '1', '1531445896986435585', '2022-11-09 22:43:17', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232009', '1', '1532620395988029442', '2022-11-09 22:43:17', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232010', '1', '1496327483721789441', '2022-11-09 22:43:18', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232011', '1', '1531528760525074434', '2022-11-09 22:43:18', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232012', '1', '1531976761387220994', '2022-11-09 22:43:18', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232013', '1', '1581291466647580673', '2022-11-09 22:43:18', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232014', '1', '1581292068928663554', '2022-11-09 22:43:18', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232015', '1', '1585916026725281793', '2022-11-09 22:43:18', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232016', '1', '10001', '2022-11-09 22:43:18', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232017', '1', '1493884088730529793', '2022-11-09 22:43:18', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232018', '1', '1531846725585281025', '2022-11-09 22:43:18', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232020', '1', '1590265776187023362', '2022-11-09 22:43:18', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232022', '1', '1531977016149245953', '2022-11-09 22:43:18', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232023', '1', '1581291111155150850', '2022-11-09 22:43:18', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232024', '1', '1583296344638287874', '2022-11-09 22:43:18', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232025', '1', '1531846893999169537', '2022-11-09 22:43:18', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232027', '1', '1494216988311183361', '2022-11-09 22:43:18', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232028', '1', '10002', '2022-11-09 22:43:18', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232029', '1', '1532634358100430850', '2022-11-09 22:43:18', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232030', '1', '1532634435510505473', '2022-11-09 22:43:18', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232031', '1', '1532634517374930946', '2022-11-09 22:43:18', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232032', '1', '1532634586568364034', '2022-11-09 22:43:18', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232033', '1', '1532634687902748674', '2022-11-09 22:43:18', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232034', '1', '1579402420152627202', '2022-11-09 22:43:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232035', '1', '1493596733666652162', '2022-11-09 22:43:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232036', '1', '1493766204259942401', '2022-11-09 22:43:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232037', '1', '1537048076783616001', '2022-11-09 22:43:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232038', '1', '1537066492991012865', '2022-11-09 22:43:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232039', '1', '1537066557067395074', '2022-11-09 22:43:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232040', '1', '1491684226094198786', '2022-11-09 22:43:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232041', '1', '1583296499370356738', '2022-11-09 22:43:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232042', '1', '1583355071571648514', '2022-11-09 22:43:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232043', '1', '1583355125187436546', '2022-11-09 22:43:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232044', '1', '1583355179121991681', '2022-11-09 22:43:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232045', '1', '1583355228157599745', '2022-11-09 22:43:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232046', '1', '1493587429257539586', '2022-11-09 22:43:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232047', '1', '1493587910381957121', '2022-11-09 22:43:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232048', '1', '1493587910721695745', '2022-11-09 22:43:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232049', '1', '1493587911057240066', '2022-11-09 22:43:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232050', '1', '1496012487833960450', '2022-11-09 22:43:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232051', '1', '1497468294740176898', '2022-11-09 22:43:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232052', '1', '1532620840659750914', '2022-11-09 22:43:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232053', '1', '1532620931885862913', '2022-11-09 22:43:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232054', '1', '1532621007333003266', '2022-11-09 22:43:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232055', '1', '1532621107589451778', '2022-11-09 22:43:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232056', '1', '1532621196013768706', '2022-11-09 22:43:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232057', '1', '1527471479688798209', '2022-11-09 22:43:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232058', '1', '1527471918001954818', '2022-11-09 22:43:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232059', '1', '1527471918337499138', '2022-11-09 22:43:20', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232060', '1', '1527471918694014977', '2022-11-09 22:43:20', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232061', '1', '1527471919042142209', '2022-11-09 22:43:20', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232062', '1', '1527471919386075137', '2022-11-09 22:43:20', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232065', '1', '1496327552973942785', '2022-11-09 22:43:20', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232066', '1', '1496327553334652930', '2022-11-09 22:43:20', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232067', '1', '1496327553699557377', '2022-11-09 22:43:20', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232068', '1', '1496327554068656130', '2022-11-09 22:43:20', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232069', '1', '1496327554433560577', '2022-11-09 22:43:20', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232070', '1', '1496327860647112706', '2022-11-09 22:43:20', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232071', '1', '1496327861003628545', '2022-11-09 22:43:20', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232072', '1', '1496327861351755778', '2022-11-09 22:43:20', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232073', '1', '1496327861712465921', '2022-11-09 22:43:20', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232074', '1', '1496327862068981762', '2022-11-09 22:43:20', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232080', '1', '1493578977630121986', '2022-11-09 22:43:20', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232081', '1', '1493578977978249218', '2022-11-09 22:43:20', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232082', '1', '1493578978313793537', '2022-11-09 22:43:20', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232083', '1', '1493578978649337858', '2022-11-09 22:43:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232084', '1', '1526179827628048385', '2022-11-09 22:43:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232085', '1', '1531536449854517250', '2022-11-09 22:43:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232086', '1', '1531536545572728833', '2022-11-09 22:43:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232087', '1', '1531536746446336001', '2022-11-09 22:43:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232088', '1', '1531536866638311426', '2022-11-09 22:43:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232089', '1', '1535633963410956289', '2022-11-09 22:43:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232090', '1', '1531537172243689474', '2022-11-09 22:43:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232091', '1', '1531537289042472961', '2022-11-09 22:43:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232092', '1', '1543116535774879745', '2022-11-09 22:43:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232093', '1', '1573537990679592962', '2022-11-09 22:43:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232094', '1', '1574949070966583298', '2022-11-09 22:43:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232095', '1', '1574949207214354434', '2022-11-09 22:43:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232096', '1', '1527835963171336193', '2022-11-09 22:43:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232097', '1', '1527836009900077057', '2022-11-09 22:43:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232098', '1', '1527836010248204290', '2022-11-09 22:43:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323167232099', '1', '1527836010596331522', '2022-11-09 22:43:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340865', '1', '1527836010944458754', '2022-11-09 22:43:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340866', '1', '1527836011300974593', '2022-11-09 22:43:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340867', '1', '1571769500494958594', '2022-11-09 22:43:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340868', '1', '1571769562595823618', '2022-11-09 22:43:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340869', '1', '1571769649816375298', '2022-11-09 22:43:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340870', '1', '1571769706296872962', '2022-11-09 22:43:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340871', '1', '1571769763259715585', '2022-11-09 22:43:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340872', '1', '1571770011910639618', '2022-11-09 22:43:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340873', '1', '1571770073550131201', '2022-11-09 22:43:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340874', '1', '1571770141757902850', '2022-11-09 22:43:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340875', '1', '1571770196032196610', '2022-11-09 22:43:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340876', '1', '1571770249522155521', '2022-11-09 22:43:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340877', '1', '1531976538732593154', '2022-11-09 22:43:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340878', '1', '1531977159263092738', '2022-11-09 22:43:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340879', '1', '1531977248807288834', '2022-11-09 22:43:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340880', '1', '1531977369196396545', '2022-11-09 22:43:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340881', '1', '1531977444341547010', '2022-11-09 22:43:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340882', '1', '1531977518870134785', '2022-11-09 22:43:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340884', '1', '1536174413406543874', '2022-11-09 22:43:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340885', '1', '1581291823989698562', '2022-11-09 22:43:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340886', '1', '1581292150105223170', '2022-11-09 22:43:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340887', '1', '1581292208498323458', '2022-11-09 22:43:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340888', '1', '1581292261484965889', '2022-11-09 22:43:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340889', '1', '1581292315700539394', '2022-11-09 22:43:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340890', '1', '1581292371807744002', '2022-11-09 22:43:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340891', '1', '1582557185250394113', '2022-11-09 22:43:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340892', '1', '1585191589231144962', '2022-11-09 22:43:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340893', '1', '1585192004932808706', '2022-11-09 22:43:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340894', '1', '1585192140589182977', '2022-11-09 22:43:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340895', '1', '1585192296575348738', '2022-11-09 22:43:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340896', '1', '1585192489970511873', '2022-11-09 22:43:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340897', '1', '1585192568882147330', '2022-11-09 22:43:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340898', '1', '1585916172586397697', '2022-11-09 22:43:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340899', '1', '1491752531735490561', '2022-11-09 22:43:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340900', '1', '1491756888363307009', '2022-11-09 22:43:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340901', '1', '1491757020773289986', '2022-11-09 22:43:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340902', '1', '1491757382771085313', '2022-11-09 22:43:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340903', '1', '1494514119857180674', '2022-11-09 22:43:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340904', '1', '1547110946242326529', '2022-11-09 22:43:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340905', '1', '1547111164111253505', '2022-11-09 22:43:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340906', '1', '1491690996678021121', '2022-11-09 22:43:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340907', '1', '1493836091183411202', '2022-11-09 22:43:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340908', '1', '1493836209106268161', '2022-11-09 22:43:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340909', '1', '1493836280589791233', '2022-11-09 22:43:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340910', '1', '1493841029473042434', '2022-11-09 22:43:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340911', '1', '1495687621054353410', '2022-11-09 22:43:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340912', '1', '1495687858816864257', '2022-11-09 22:43:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340913', '1', '1521469373525716994', '2022-11-09 22:43:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340914', '1', '999999999999', '2022-11-09 22:43:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340915', '1', '1494217080162246658', '2022-11-09 22:43:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340916', '1', '1494217080510373890', '2022-11-09 22:43:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340917', '1', '1494217080858501121', '2022-11-09 22:43:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340918', '1', '1494217081206628354', '2022-11-09 22:43:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340919', '1', '1494217081558949890', '2022-11-09 22:43:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340920', '1', '1583296419947016193', '2022-11-09 22:43:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340921', '1', '1583346865315131394', '2022-11-09 22:43:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340922', '1', '1583346942263832578', '2022-11-09 22:43:24', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340923', '1', '1583347000673710082', '2022-11-09 22:43:24', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340924', '1', '1583351948518031361', '2022-11-09 22:43:24', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340925', '1', '1590265951848669185', '2022-11-09 22:43:24', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340926', '1', '1590266107100831746', '2022-11-09 22:43:24', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340927', '1', '1493884379760701442', '2022-11-09 22:43:24', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340928', '1', '1494192758630694913', '2022-11-09 22:43:24', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340929', '1', '1494192758974627842', '2022-11-09 22:43:24', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340930', '1', '1494192759318560770', '2022-11-09 22:43:24', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340931', '1', '1494192759662493698', '2022-11-09 22:43:24', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340932', '1', '1494192760010620930', '2022-11-09 22:43:24', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340933', '1', '1494153372996255746', '2022-11-09 22:43:24', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340934', '1', '1494153373352771586', '2022-11-09 22:43:24', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340935', '1', '1494153373696704514', '2022-11-09 22:43:24', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340936', '1', '1494153374053220354', '2022-11-09 22:43:24', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340937', '1', '1494153374401347585', '2022-11-09 22:43:24', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340938', '1', '1531446009649635329', '2022-11-09 22:43:24', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340939', '1', '1531446009779658754', '2022-11-09 22:43:24', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340940', '1', '1531446009913876482', '2022-11-09 22:43:24', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340941', '1', '1531446010039705602', '2022-11-09 22:43:24', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340942', '1', '1531446010169729026', '2022-11-09 22:43:24', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340943', '1', '1491949315883827201', '2022-11-09 22:43:24', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340944', '1', '1491969633293729794', '2022-11-09 22:43:24', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340945', '1', '1527947365856878593', '2022-11-09 22:43:24', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340946', '1', '1531882841361915906', '2022-11-09 22:43:25', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340947', '1', '1531882726605758465', '2022-11-09 22:43:25', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590354323234340951', '1', '100001', '2022-11-09 22:43:25', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590700851931889666', '1', '1590700851537625090', '2022-11-10 21:40:11', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590704387214262274', '1', '1590704386824192002', '2022-11-10 21:54:14', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590704512858832898', '1', '1590704512468762626', '2022-11-10 21:54:44', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590972047151820801', '1', '1590972046640115713', '2022-11-11 15:37:49', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590972188281761794', '1', '1590972187874914305', '2022-11-11 15:38:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590972259320688641', '1', '1590972258943201281', '2022-11-11 15:38:40', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590972321660628994', '1', '1590972321283141633', '2022-11-11 15:38:55', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590972375549046786', '1', '1590972375171559426', '2022-11-11 15:39:07', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1590972439843532801', '1', '1590972439457656833', '2022-11-11 15:39:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1600477838260203522', '1', '1600477837933047810', '2022-12-07 21:10:26', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1600478029667266562', '1', '1600478029277196290', '2022-12-07 21:11:12', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1600478336329609217', '1', '1600478336006647809', '2022-12-07 21:12:25', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1600478397620973569', '1', '1600478397226708994', '2022-12-07 21:12:39', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1600478467963645953', '1', '1600478467569381377', '2022-12-07 21:12:56', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1600785453334056962', '1', '1600785452746854401', '2022-12-08 17:32:48', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1605129651525697537', '1', '1605129651156598786', '2022-12-20 17:15:05', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1605129783608524802', '1', '1605129783281369089', '2022-12-20 17:15:37', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1605130091428495361', '1', '1605130091042619393', '2022-12-20 17:16:50', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1612481861796458497', '1', '1612481861469302786', '2023-01-10 00:10:09', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1612683976084803586', '1', '1612683975455657985', '2023-01-10 13:33:17', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1612684125838233601', '1', '1612684125251031042', '2023-01-10 13:33:52', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1612684390532370434', '1', '1612684389953556481', '2023-01-10 13:34:56', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1615718594201665538', '1', '1615718593878704130', '2023-01-18 22:31:48', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1615718665911681026', '1', '1615718665584525314', '2023-01-18 22:32:05', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1615718739093897218', '1', '1615718738766741505', '2023-01-18 22:32:23', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1615718811818934273', '1', '1615718811491778562', '2023-01-18 22:32:40', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1670722611623022594', '1', '1670722611195203586', '2023-06-19 17:18:05', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1670722888610664449', '1', '1670722888228982785', '2023-06-19 17:19:11', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1670723481018355713', '1', '1670723480636674049', '2023-06-19 17:21:33', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1670724676822814721', '1', '1670724676428550146', '2023-06-19 17:26:18', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1670725150896607233', '1', '1670725150502342658', '2023-06-19 17:28:11', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1670725580682743809', '1', '1670725580288479233', '2023-06-19 17:29:53', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1670725673653686273', '1', '1670725673255227394', '2023-06-19 17:30:15', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1670732624806596610', '1', '1670732624412332034', '2023-06-19 17:57:53', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1670732986355601409', '1', '1670732985965531137', '2023-06-19 17:59:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1671121808826355714', '1', '1671121808444674049', '2023-06-20 19:44:21', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1681556363284713474', '1', '1681556362827534338', '2023-07-19 14:47:33', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1779386488136466433', '1', '1779386487675092994', '2024-04-14 13:49:32', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1779386604813615105', '1', '1779386604402573314', '2024-04-14 13:50:00', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1779386782891180034', '1', '1779386782480138242', '2024-04-14 13:50:43', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1779386899161481217', '1', '1779386898750439425', '2024-04-14 13:51:10', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1788196421250326529', '1', '1788196420663123969', '2024-05-08 21:17:04', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1788196662838042626', '1', '1788196662338920449', '2024-05-08 21:18:02', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1788196734183153666', '1', '1788196733688225793', '2024-05-08 21:18:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1788196790592348162', '1', '1788196790093225986', '2024-05-08 21:18:32', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1788196841410535425', '1', '1788196840919801858', '2024-05-08 21:18:44', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1788196891092066306', '1', '1788196890592944129', '2024-05-08 21:18:56', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1788375299562414081', '1', '1788375299184926722', '2024-05-09 09:07:52', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1788375558346776578', '1', '1788375557960900610', '2024-05-09 09:08:54', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1788375652097859585', '1', '1788375651716177922', '2024-05-09 09:09:16', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1788375752433999874', '1', '1788375752018763778', '2024-05-09 09:09:40', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1788375826450882561', '1', '1788375826039840770', '2024-05-09 09:09:58', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1788375917848961026', '1', '1788375917437919234', '2024-05-09 09:10:19', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1792432865620381697', '1', '1792432863812636673', '2024-05-20 13:51:12', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1792433082394595330', '1', '1792433080825925634', '2024-05-20 13:52:03', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1792433159209078786', '1', '1792433157640409089', '2024-05-20 13:52:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1792433242524733441', '1', '1792433241006395393', '2024-05-20 13:52:41', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1792433336061906946', '1', '1792433334379991041', '2024-05-20 13:53:04', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1792433412289187841', '1', '1792433410624049154', '2024-05-20 13:53:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1792433530660835329', '1', '1792433529037639682', '2024-05-20 13:53:50', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1826532609892093953', '1', '1826532607924965377', '2024-08-22 16:11:24', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871099261819625473', '1', '1871099260888489985', '2024-12-23 15:43:22', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750832781873154', '1789672842141433858', '1788196733688225793', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750832844787714', '1789672842141433858', '1788196840919801858', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750832844787715', '1789672842141433858', '1788196662338920449', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750832844787716', '1789672842141433858', '1612683975455657985', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750832907702273', '1789672842141433858', '1536174413406543874', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750832907702274', '1789672842141433858', '1493587429257539586', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750832907702275', '1789672842141433858', '1532620395988029442', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750832907702276', '1789672842141433858', '1493587911057240066', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750832974811137', '1789672842141433858', '1493587910381957121', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750832974811138', '1789672842141433858', '1497468294740176898', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750832974811139', '1789672842141433858', '1496012487833960450', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833037725698', '1789672842141433858', '1493587910721695745', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833037725699', '1789672842141433858', '1532620101065543681', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833100640257', '1789672842141433858', '1532621107589451778', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833100640258', '1789672842141433858', '1532621007333003266', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833100640259', '1789672842141433858', '1532620840659750914', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833163554818', '1789672842141433858', '1532621196013768706', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833163554819', '1789672842141433858', '1532620931885862913', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833230663681', '1789672842141433858', '1532634586568364034', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833230663682', '1789672842141433858', '1532634435510505473', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833230663683', '1789672842141433858', '1532634517374930946', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833293578242', '1789672842141433858', '1532634687902748674', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833293578243', '1789672842141433858', '1532634358100430850', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833293578244', '1789672842141433858', '1496327552973942785', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833360687106', '1789672842141433858', '1496327553334652930', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833360687107', '1789672842141433858', '1788375299184926722', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833360687108', '1789672842141433858', '1788375651716177922', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833360687109', '1789672842141433858', '1788375917437919234', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833360687110', '1789672842141433858', '1788375826039840770', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833360687111', '1789672842141433858', '1788375752018763778', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833423601665', '1789672842141433858', '1788375557960900610', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833423601666', '1789672842141433858', '1496327802522447873', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833423601667', '1789672842141433858', '1496327860647112706', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833423601668', '1789672842141433858', '1496327861003628545', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833494904834', '1789672842141433858', '1496327861712465921', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833494904835', '1789672842141433858', '1496327861351755778', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833494904836', '1789672842141433858', '1496327862068981762', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833494904837', '1789672842141433858', '1521496866882236418', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833566208001', '1789672842141433858', '1531528760525074434', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833566208002', '1789672842141433858', '1681556362827534338', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833620733954', '1789672842141433858', '1535633963410956289', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833620733955', '1789672842141433858', '1531536746446336001', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833620733956', '1789672842141433858', '1531536545572728833', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833679454209', '1789672842141433858', '1531536449854517250', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833679454210', '1789672842141433858', '1531536866638311426', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833679454211', '1789672842141433858', '1531529196871102466', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833679454212', '1789672842141433858', '1531537289042472961', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833679454213', '1789672842141433858', '1531537172243689474', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833746563073', '1789672842141433858', '1543116535774879745', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833813671937', '1789672842141433858', '1779386604402573314', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833813671938', '1789672842141433858', '1585191363078467586', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833813671939', '1789672842141433858', '1585191589231144962', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833813671940', '1789672842141433858', '1585192489970511873', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833880780801', '1789672842141433858', '1585192568882147330', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833880780802', '1789672842141433858', '1585192296575348738', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833880780803', '1789672842141433858', '1585192004932808706', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833880780804', '1789672842141433858', '1585192140589182977', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833947889666', '1789672842141433858', '1585916026725281793', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833947889667', '1789672842141433858', '1585916172586397697', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750833947889668', '1789672842141433858', '1491752531735490561', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750834006609921', '1789672842141433858', '1491757020773289986', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750834006609922', '1789672842141433858', '1491756888363307009', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750834006609923', '1789672842141433858', '1491973212968632322', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750834006609924', '1789672842141433858', '1493578978649337858', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750834073718785', '1789672842141433858', '1526179827628048385', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750834073718786', '1789672842141433858', '1493578978313793537', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750834073718787', '1789672842141433858', '1493578977978249218', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750834136633346', '1789672842141433858', '1493578977630121986', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750834136633347', '1789672842141433858', '1537066557067395074', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750834136633348', '1789672842141433858', '1493766204259942401', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750834136633349', '1789672842141433858', '1537048076783616001', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750834136633350', '1789672842141433858', '1493596733666652162', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750834199547905', '1789672842141433858', '1537066492991012865', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750834199547906', '1789672842141433858', '1779386782480138242', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750834199547907', '1789672842141433858', '1788196420663123969', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750834266656770', '1789672842141433858', '1536174316656533505', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750834266656771', '1789672842141433858', '1779386487675092994', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750834266656772', '1789672842141433858', '1496327483721789441', '2024-12-25 10:52:29', '1590229800633634816');
INSERT INTO `sys_role_menu` VALUES ('1871750834266656773', '1789672842141433858', '10001', '2024-12-25 10:52:29', '1590229800633634816');

-- ----------------------------
-- Table structure for sys_storage_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_storage_config`;
CREATE TABLE `sys_storage_config`  (
                                       `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                                       `access_key` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'access_key',
                                       `access_secret` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'access_secret',
                                       `endpoint` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地域节点',
                                       `bucket` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '域名',
                                       `type` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '存储类型1、阿里OSS；2、七牛云；3、腾讯云',
                                       `create_time` datetime NULL DEFAULT NULL COMMENT '新增时间',
                                       `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
                                       `dir` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'material' COMMENT '指定文件夹',
                                       `is_https` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                                       `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                       `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户id',
                                       `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                       `update_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                                       PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文件存储配置' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_storage_config
-- ----------------------------
INSERT INTO `sys_storage_config` VALUES ('1491967331820404738', 's4K9h9g1KN1ilw7pV3lk', 'L2dxP24LWijtRhJ1eIYndwrf9aSRfnpbd811ptdO', 'http://localhost:9000', 'huanxing', '4', '2022-02-11 10:48:25', '2024-12-04 11:21:08', 'file', NULL, '0', '1590229800633634816', NULL, 'admin');

-- ----------------------------
-- Table structure for sys_tenant
-- ----------------------------
DROP TABLE IF EXISTS `sys_tenant`;
CREATE TABLE `sys_tenant`  (
                               `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                               `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户名称',
                               `logo_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '租户logo',
                               `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '租户地址',
                               `site_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '官网地址',
                               `status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '状态：0.正常；1.停用；',
                               `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
                               `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
                               `auth_begin_time` datetime NOT NULL COMMENT '授权开始时间',
                               `auth_end_time` datetime NOT NULL COMMENT '授权结束时间',
                               `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                               `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                               `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                               `package_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户套餐id',
                               `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                               `update_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '租户管理' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_tenant
-- ----------------------------
INSERT INTO `sys_tenant` VALUES ('1590229800633634816', '环兴商城', NULL, NULL, 'www.ihuanxing.cn', '0', NULL, '176****3397', '2022-11-09 17:43:26', '2023-12-31 17:43:28', '0', '2022-11-09 17:43:41', '2022-11-09 17:46:13', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_tenant_package
-- ----------------------------
DROP TABLE IF EXISTS `sys_tenant_package`;
CREATE TABLE `sys_tenant_package`  (
                                       `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                                       `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '套餐名称',
                                       `sub_title` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '子标题',
                                       `sales_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '销售价格（元）',
                                       `original_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '原价（元）',
                                       `status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '1' COMMENT '状态：0.正常；1.停用；',
                                       `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '描述',
                                       `app_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '应用key',
                                       `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                       `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                       `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                       `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                       `update_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                                       PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '租户套餐' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_tenant_package
-- ----------------------------
INSERT INTO `sys_tenant_package` VALUES ('1590981945990287361', '环兴商城专业版', '商城基础版/营销功能/微信小程序', 1299.00, 2000.00, '0', '<p><br></p><p><img src=\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/ea86c408-21eb-47af-81df-6367cb398075.jpg\" alt=\"\" data-href=\"\" style=\"\"/></p>', 'app_base,app_market,app_wechat', '2022-11-11 16:17:10', '2024-11-07 19:39:40', '0', NULL, 'admin');
INSERT INTO `sys_tenant_package` VALUES ('1591735271929356289', '环兴商城基础版', '商城基础功能', 888.00, 999.00, '0', '<p>阿达打撒 阿斯顿撒打算</p>', 'app_base,app_wechat', '2022-11-13 18:10:36', '2022-11-13 18:10:36', '0', NULL, NULL);
INSERT INTO `sys_tenant_package` VALUES ('1639458123460681730', '环兴商城旗舰版', '环兴商城旗舰版', 0.01, 0.01, '0', '<p><br></p><p><img src=\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/72ca917c-9cc3-4452-9aa7-93d99196782e.jpg\" alt=\"\" data-href=\"\" style=\"\"/></p>', 'app_base,app_market,app_alipay,app_wechat', '2022-11-11 16:17:10', '2022-11-13 18:11:39', '0', NULL, NULL);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
                             `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                             `username` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
                             `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
                             `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
                             `nike_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '昵称',
                             `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
                             `dept_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门ID',
                             `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
                             `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                             `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                             `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                             `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户id',
                             `status` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '状态：0.正常；1.停用；',
                             `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                             `update_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                             `type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '账号类型：0.系统主账户（管理全部店铺）；1.店铺账号（关联店铺）',
                             `shop_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '店铺ID',
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '806@163.com', '李', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/278a943b-52ac-4f6b-a6b6-506cc67f8d0a.jpg', '2', '17615123399', '0', '2022-05-20 17:33:24', '2024-11-08 16:19:39', '1590229800633634816', '0', NULL, 'admin', '0', NULL);
INSERT INTO `sys_user` VALUES ('1789675114619535362', 'huawei', 'e10adc3949ba59abbe56e057f20f883e', NULL, 'huawei', '', '1494674952704667649', '13333333333', '0', '2024-05-12 23:12:53', '2024-11-08 16:23:54', '1590229800633634816', '0', 'admin', 'admin', '1', '1');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
                                  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                                  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户ID',
                                  `role_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色ID',
                                  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                  `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户id',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户关联角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1854800940654137345', '1', '1', '2024-11-08 16:19:39', '1590229800633634816');
INSERT INTO `sys_user_role` VALUES ('1854802008041590785', '1789675114619535362', '1789672842141433858', '2024-11-08 16:23:54', '1590229800633634816');

SET FOREIGN_KEY_CHECKS = 1;