USE huanxing_product;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for freight_template
-- ----------------------------
DROP TABLE IF EXISTS `freight_template`;
CREATE TABLE `freight_template`  (
                                   `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                                   `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '运费模板名称',
                                   `send_time` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发货时间',
                                   `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发货地址',
                                   `is_incl_postage` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否包邮：0.否、1.是',
                                   `is_incl_postage_by_if` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否指定条件包邮：0.否、1.是',
                                   `pricing_type` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '计价方式：1.按件数、2.按重量、3.按体积',
                                   `first_num` decimal(10, 2) NULL DEFAULT NULL COMMENT '首件（个）首重（kg）',
                                   `continue_num` decimal(10, 2) NULL DEFAULT NULL COMMENT '续件（个）续重（kg）',
                                   `first_freight` decimal(10, 2) NULL DEFAULT NULL COMMENT '首费（元）',
                                   `continue_freight` decimal(10, 2) NULL DEFAULT NULL COMMENT '续费（元）',
                                   `full_amount` decimal(10, 2) NULL DEFAULT NULL COMMENT '满包邮（元）',
                                   `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                   `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
                                   `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                   `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户id',
                                   `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                   `update_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                                   `shop_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '店铺ID',
                                   `shop_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '店铺名称',
                                   PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '运费模板' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of freight_template
-- ----------------------------
INSERT INTO `freight_template` VALUES ('1790247983355805697', '默认包邮模板', NULL, '辽宁沈阳', '1', NULL, '', 0.00, 0.00, 0.00, 0.00, 0.00, '2024-05-14 13:09:15', '2024-05-14 13:09:16', '0', '1590229800633634816', 'huawei', NULL, '1', '环兴商城自营旗舰店');
INSERT INTO `freight_template` VALUES ('1790248340651786242', '默认包邮模板', NULL, '辽宁沈阳', '1', NULL, '', 0.00, 0.00, 0.00, 0.00, 0.00, '2024-05-14 13:10:40', '2024-05-14 13:10:41', '0', '1590229800633634816', 'huawei', NULL, '1', '环兴商城自营旗舰店');
INSERT INTO `freight_template` VALUES ('1790249346462683138', '默认包邮模板', NULL, '辽宁沈阳', '1', NULL, '', 0.00, 0.00, 0.00, 0.00, 0.00, '2024-05-14 13:14:40', '2024-05-14 13:14:41', '0', '1590229800633634816', 'huawei', NULL, '1', '环兴商城自营旗舰店');
INSERT INTO `freight_template` VALUES ('1790250180579725313', '默认包邮模板', NULL, '辽宁沈阳', '1', NULL, '', 0.00, 0.00, 0.00, 0.00, 0.00, '2024-05-14 13:17:59', '2024-05-14 13:34:11', '1', '1590229800633634816', 'huawei', 'huawei', '1', '环兴商城自营旗舰店');

-- ----------------------------
-- Table structure for goods_appraise
-- ----------------------------
DROP TABLE IF EXISTS `goods_appraise`;
CREATE TABLE `goods_appraise`  (
                                 `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                                 `spu_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'spu主键',
                                 `order_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '订单主键',
                                 `order_item_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '子订单主键',
                                 `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户ID',
                                 `avatar_url` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
                                 `nick_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '昵称',
                                 `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                 `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                 `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                 `pic_urls` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片',
                                 `goods_score` int NULL DEFAULT NULL COMMENT '商品评分',
                                 `logistics_score` int NULL DEFAULT NULL COMMENT '物流评分',
                                 `service_score` int NULL DEFAULT NULL COMMENT '服务评分',
                                 `business_reply` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商家回复',
                                 `reply_time` datetime NULL DEFAULT NULL COMMENT '回复时间',
                                 `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '评论内容',
                                 `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户id',
                                 `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                 `update_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                                 `shop_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '店铺ID',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品评价' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of goods_appraise
-- ----------------------------

-- ----------------------------
-- Table structure for goods_category
-- ----------------------------
DROP TABLE IF EXISTS `goods_category`;
CREATE TABLE `goods_category`  (
                                 `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                                 `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类目名称',
                                 `parent_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '上级类目（0.顶级类目）',
                                 `category_pic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类目图片',
                                 `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类目描述',
                                 `status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '1' COMMENT '状态：0.正常；1.停用；',
                                 `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                 `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                 `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                 `sort` int NULL DEFAULT NULL COMMENT '排序序号',
                                 `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户id',
                                 `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                 `update_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品类目' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of goods_category
-- ----------------------------
INSERT INTO `goods_category` VALUES ('1496672394983804930', '小米', '4', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/bb60e527-092c-4fc5-974e-2697404c5d8f.jpg', NULL, '0', '2022-02-24 10:24:36', '2022-11-09 22:11:03', '0', 1, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1496677018553872385', '数码家电', '0', '', '家用电器', '0', '2022-02-24 10:42:58', '2022-11-09 22:11:03', '0', 70, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1496686626970333185', '数据线', '1496677018553872385', 'https://cdn.uviewui.com/uview/common/classify/7/1.jpg', NULL, '0', '2022-02-24 11:21:13', '2022-11-09 22:11:03', '0', 1, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1498510244406673410', '家装家纺', '0', '', '家装家纺', '0', '2022-03-01 12:07:37', '2022-11-09 22:11:03', '0', 110, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1498510397314220034', '耳机/耳麦', '1498510244406673410', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/bb60e527-092c-4fc5-974e-2697404c5d8f.jpg', '耳机/耳麦', '0', '2022-03-01 12:08:13', '2022-11-09 22:11:03', '0', 1, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1498864835983876097', 'OPPO', '4', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/f1201f1f-5c36-4468-a1f8-a634c894685d.jpg', NULL, '0', '2022-03-02 11:36:37', '2022-11-09 22:11:03', '0', 11, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1499342326879887362', '手表', '1498510244406673410', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/bb60e527-092c-4fc5-974e-2697404c5d8f.jpg', NULL, '0', '2022-03-03 19:14:04', '2022-11-09 22:11:03', '0', 5, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1504307346357383170', '男装', '0', '', '男装', '0', '2022-03-17 12:03:14', '2022-11-09 22:11:03', '0', 50, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1504307475378368514', '爸爸装', '1504307346357383170', 'https://cdn.uviewui.com/uview/common/classify/5/1.jpg', NULL, '0', '2022-03-17 12:03:44', '2022-11-09 22:11:04', '0', 1, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1504307581011914753', '牛仔裤', '1504307346357383170', 'https://cdn.uviewui.com/uview/common/classify/5/2.jpg', NULL, '0', '2022-03-17 12:04:09', '2022-11-09 22:11:04', '0', 2, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1504307713140879362', '女装', '0', '', NULL, '0', '2022-03-17 12:04:41', '2024-06-12 14:43:04', '0', 10, '1590229800633634816', NULL, 'lijx');
INSERT INTO `goods_category` VALUES ('1504307782850211841', 'A字裙', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/1.jpg', 'A字裙', '0', '2022-03-17 12:04:58', '2022-11-09 22:11:04', '0', 1, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1504307835576807425', 'T恤', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/2.jpg', NULL, '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 2, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1528663744649547778', '内衣', '0', '', '内衣', '0', '2022-05-23 17:06:52', '2022-11-09 22:11:04', '0', 100, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1528947151174553602', '联想', '4', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/5137138c-1436-412c-9fb7-74e80b357061.jpg', '联想', '0', '2022-05-24 11:53:01', '2022-11-09 22:11:04', '0', 20, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1528947222288977921', '海尔', '4', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/5137138c-1436-412c-9fb7-74e80b357061.jpg', '海尔', '0', '2022-05-24 11:53:18', '2022-11-09 22:11:04', '0', 22, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1528947336764116993', '耳机', '1496677018553872385', 'https://cdn.uviewui.com/uview/common/classify/7/2.jpg', '耳机', '0', '2022-05-24 11:53:46', '2022-11-09 22:11:04', '0', 2, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1528947409455599617', '生活家电', '1496677018553872385', 'https://cdn.uviewui.com/uview/common/classify/7/3.jpg', '生活家电', '0', '2022-05-24 11:54:03', '2022-11-09 22:11:04', '0', 3, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1528947472009449473', '电风扇', '1496677018553872385', 'https://cdn.uviewui.com/uview/common/classify/7/4.jpg', '电脑', '0', '2022-05-24 11:54:18', '2022-11-09 22:11:04', '0', 4, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1528947743129260034', '居家日用', '0', '', '居家日用', '0', '2022-05-24 11:55:23', '2022-11-09 22:11:05', '0', 40, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1528947872280268802', '鞋品', '0', '', NULL, '0', '2022-05-24 11:55:53', '2022-11-09 22:11:05', '0', 60, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1528948092074381314', '箱包', '0', '', '箱包', '0', '2022-05-24 11:56:46', '2022-11-09 22:11:05', '0', 90, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1528948268147068930', '母婴', '0', '', '母婴', '0', '2022-05-24 11:57:28', '2022-11-09 22:11:05', '0', 80, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1634571151397249025', '三星', '4', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/41eb73b4-4668-429a-97d8-a53c22baa278.jpg', '', '0', '2023-03-11 23:05:06', '2023-03-11 23:05:06', '0', 11, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779744640409305090', '半身裙', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/3.jpg', '半身裙', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 3, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779744693672771585', '衬衫', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/4.jpg', '衬衫', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 4, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779744749645758466', '短裙', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/5.jpg', '短裙', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 5, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779744797574070273', '阔腿裤', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/6.jpg', '阔腿裤', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 6, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779744849587634177', '连衣裙', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/7.jpg', '连衣裙', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 7, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779744916155432961', '妈妈装', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/8.jpg', '妈妈装', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 8, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779744976683433985', '牛仔裤', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/9.jpg', '牛仔裤', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 9, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779745085554982914', '美食', '0', '', '美食', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 20, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779745165934624769', '火锅', '1779745085554982914', 'https://cdn.uviewui.com/uview/common/classify/2/1.jpg', '火锅', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 1, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779745230275248130', '糕点饼干', '1779745085554982914', 'https://cdn.uviewui.com/uview/common/classify/2/2.jpg', '糕点饼干', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 2, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779745284285300738', '坚果果干', '1779745085554982914', 'https://cdn.uviewui.com/uview/common/classify/2/3.jpg', '坚果果干', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 3, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779745330770771969', '酒类', '1779745085554982914', 'https://cdn.uviewui.com/uview/common/classify/2/4.jpg', '', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 4, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779745425444601857', '辣条', '1779745085554982914', 'https://cdn.uviewui.com/uview/common/classify/2/5.jpg', '辣条', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 5, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779745476535418882', '大礼包', '1779745085554982914', 'https://cdn.uviewui.com/uview/common/classify/2/6.jpg', '大礼包', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 6, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779745526569271297', '精品茗茶', '1779745085554982914', 'https://cdn.uviewui.com/uview/common/classify/2/7.jpg', '', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 7, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779745583142043649', '休闲食品', '1779745085554982914', 'https://cdn.uviewui.com/uview/common/classify/2/8.jpg', '', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 8, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779745630567038978', '方便速食', '1779745085554982914', 'https://cdn.uviewui.com/uview/common/classify/2/10.jpg', '', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 10, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779745687970283521', '饮品', '1779745085554982914', 'https://cdn.uviewui.com/uview/common/classify/2/14.jpg', '饮品', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 14, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779745743423176705', '美妆', '0', '', '', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 30, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779745806308376577', '化妆刷', '1779745743423176705', 'https://cdn.uviewui.com/uview/common/classify/3/1.jpg', '', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 1, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779745855247515649', '粉底', '1779745743423176705', 'https://cdn.uviewui.com/uview/common/classify/3/2.jpg', '粉底', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 2, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779745905382031361', '洗发护发', '1779745743423176705', 'https://cdn.uviewui.com/uview/common/classify/3/3.jpg', '', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 3, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779745947287322625', '美容工具', '1779745743423176705', 'https://cdn.uviewui.com/uview/common/classify/3/4.jpg', '', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 4, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779746006007578626', '眼部护理', '1779745743423176705', 'https://cdn.uviewui.com/uview/common/classify/3/5.jpg', '', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 5, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779746052816011265', '眉妆', '1779745743423176705', 'https://cdn.uviewui.com/uview/common/classify/3/6.jpg', '', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 6, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779746107962720257', '卸妆品', '1779745743423176705', 'https://cdn.uviewui.com/uview/common/classify/3/7.jpg', '', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 7, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779746398036590594', '基础护肤', '1779745743423176705', 'https://cdn.uviewui.com/uview/common/classify/3/8.jpg', '', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 8, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779746466324054018', '眼妆', '1779745743423176705', 'https://cdn.uviewui.com/uview/common/classify/3/9.jpg', '眼妆', '0', '2022-03-17 12:05:10', '2022-11-09 22:11:04', '0', 9, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779746576323870721', '垃圾袋', '1528947743129260034', 'https://cdn.uviewui.com/uview/common/classify/4/1.jpg', '', '0', '2022-05-24 11:55:23', '2022-11-09 22:11:05', '0', 1, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779746626668101634', '纸巾', '1528947743129260034', 'https://cdn.uviewui.com/uview/common/classify/4/2.jpg', '', '0', '2022-05-24 11:55:23', '2022-11-09 22:11:05', '0', 2, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779746678002188289', '驱蚊用品', '1528947743129260034', 'https://cdn.uviewui.com/uview/common/classify/4/3.jpg', '', '0', '2022-05-24 11:55:23', '2022-11-09 22:11:05', '0', 3, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779746742900654082', '收纳神器', '1528947743129260034', 'https://cdn.uviewui.com/uview/common/classify/4/4.jpg', '', '0', '2022-05-24 11:55:23', '2022-11-09 22:11:05', '0', 4, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779746796533219329', '厨房用品', '1528947743129260034', 'https://cdn.uviewui.com/uview/common/classify/4/5.jpg', '', '0', '2022-05-24 11:55:23', '2022-11-09 22:11:05', '0', 5, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779746852162273282', '厨房烹饪', '1528947743129260034', 'https://cdn.uviewui.com/uview/common/classify/4/6.jpg', '', '0', '2022-05-24 11:55:23', '2022-11-09 22:11:05', '0', 6, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779746900463878145', '衣物晾晒', '1528947743129260034', 'https://cdn.uviewui.com/uview/common/classify/4/7.jpg', '衣物晾晒', '0', '2022-05-24 11:55:23', '2022-11-09 22:11:05', '0', 7, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779746953488269313', '衣物护理', '1528947743129260034', 'https://cdn.uviewui.com/uview/common/classify/4/8.jpg', '衣物护理', '0', '2022-05-24 11:55:23', '2022-11-09 22:11:05', '0', 8, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779747011034120194', '宠物用品', '1528947743129260034', 'https://cdn.uviewui.com/uview/common/classify/4/9.jpg', '', '0', '2022-05-24 11:55:23', '2022-11-09 22:11:05', '0', 9, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779747226793312257', '衬衫', '1504307346357383170', 'https://cdn.uviewui.com/uview/common/classify/5/3.jpg', '', '0', '2022-03-17 12:04:09', '2022-11-09 22:11:04', '0', 3, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779747276890079233', '休闲裤', '1504307346357383170', 'https://cdn.uviewui.com/uview/common/classify/5/4.jpg', '', '0', '2022-03-17 12:04:09', '2022-11-09 22:11:04', '0', 4, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779747323681734657', '外套', '1504307346357383170', 'https://cdn.uviewui.com/uview/common/classify/5/5.jpg', '', '0', '2022-03-17 12:04:09', '2022-11-09 22:11:04', '0', 5, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779747380296450049', '套装', '1504307346357383170', 'https://cdn.uviewui.com/uview/common/classify/5/7.jpg', '', '0', '2022-03-17 12:04:09', '2022-11-09 22:11:04', '0', 7, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779747452757245954', '马甲/背心', '1504307346357383170', 'https://cdn.uviewui.com/uview/common/classify/5/9.jpg', '', '0', '2022-03-17 12:04:09', '2022-11-09 22:11:04', '0', 9, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779747498689069058', 'POLO衫', '1504307346357383170', 'https://cdn.uviewui.com/uview/common/classify/5/10.jpg', '', '0', '2022-03-17 12:04:09', '2022-11-09 22:11:04', '0', 10, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779747634639044610', '单鞋', '1528947872280268802', 'https://cdn.uviewui.com/uview/common/classify/6/1.jpg', '', '0', '2022-05-24 11:55:53', '2022-11-09 22:11:05', '0', 1, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779747687227228162', '皮鞋', '1528947872280268802', 'https://cdn.uviewui.com/uview/common/classify/6/2.jpg', '', '0', '2022-05-24 11:55:53', '2022-11-09 22:11:05', '0', 2, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779747729694556161', '帆布鞋', '1528947872280268802', 'https://cdn.uviewui.com/uview/common/classify/6/3.jpg', '', '0', '2022-05-24 11:55:53', '2022-11-09 22:11:05', '0', 3, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779747783675248641', '老北京布鞋', '1528947872280268802', 'https://cdn.uviewui.com/uview/common/classify/6/4.jpg', '', '0', '2022-05-24 11:55:53', '2022-11-09 22:11:05', '0', 4, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779747835965636609', '运动鞋', '1528947872280268802', 'https://cdn.uviewui.com/uview/common/classify/6/5.jpg', '', '0', '2022-05-24 11:55:53', '2022-11-09 22:11:05', '0', 5, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779747881461252097', '拖鞋', '1528947872280268802', 'https://cdn.uviewui.com/uview/common/classify/6/6.jpg', '', '0', '2022-05-24 11:55:53', '2022-11-09 22:11:05', '0', 6, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779747942740033537', '凉鞋', '1528947872280268802', 'https://cdn.uviewui.com/uview/common/classify/6/7.jpg', '', '0', '2022-05-24 11:55:53', '2022-11-09 22:11:05', '0', 7, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779748037267062785', '高跟鞋', '1528947872280268802', 'https://cdn.uviewui.com/uview/common/classify/6/9.jpg', '', '0', '2022-05-24 11:55:53', '2022-11-09 22:11:05', '0', 9, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779748394336550914', '电吹风', '1496677018553872385', 'https://cdn.uviewui.com/uview/common/classify/7/5.jpg', '', '0', '2022-05-24 11:54:18', '2022-11-09 22:11:04', '0', 5, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779748449403568129', '手机壳', '1496677018553872385', 'https://cdn.uviewui.com/uview/common/classify/7/6.jpg', '手机壳', '0', '2022-05-24 11:54:18', '2022-11-09 22:11:04', '0', 6, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779748495998091265', '榨汁机', '1496677018553872385', 'https://cdn.uviewui.com/uview/common/classify/7/7.jpg', '', '0', '2022-05-24 11:54:18', '2022-11-09 22:11:04', '0', 7, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779748546547843074', '小家电', '1496677018553872385', 'https://cdn.uviewui.com/uview/common/classify/7/8.jpg', '', '0', '2022-05-24 11:54:18', '2022-11-09 22:11:04', '0', 8, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779748622376665090', '手机配件', '1496677018553872385', 'https://cdn.uviewui.com/uview/common/classify/7/14.jpg', '', '0', '2022-05-24 11:54:18', '2022-11-09 22:11:04', '0', 14, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779750112394772481', '情侣装', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/10.jpg', '', '0', '2022-03-17 12:04:41', '2022-11-09 22:11:04', '0', 10, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779750154912432129', '休闲裤', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/11.jpg', '', '0', '2022-03-17 12:04:41', '2022-11-09 22:11:04', '0', 11, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779750201825722370', '雪纺衫', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/12.jpg', '', '0', '2022-03-17 12:04:41', '2022-11-09 22:11:04', '0', 12, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779750257714823170', '防晒衣', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/13.jpg', '', '0', '2022-03-17 12:04:41', '2022-11-09 22:11:04', '0', 13, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('1779750312597291009', '礼服/婚纱', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/14.jpg', '', '0', '2022-03-17 12:04:41', '2022-11-09 22:11:04', '0', 14, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('4', '配饰', '0', '', '配饰', '0', '2022-02-23 15:42:45', '2022-11-09 22:11:05', '0', 120, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('5', 'Apple', '4', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0a69731e-cb13-49d9-950f-0649bf32d044.jpg', 'Apple手机', '0', '2022-02-23 15:43:02', '2022-11-09 22:11:05', '0', 1, '1590229800633634816', NULL, NULL);
INSERT INTO `goods_category` VALUES ('7', '华为', '4', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d03d26cc-4c43-4575-a7ff-7f5e500a2ee6.jpg', '华为', '0', '2022-02-23 16:00:58', '2022-11-09 22:11:05', '0', 1, '1590229800633634816', NULL, NULL);

-- ----------------------------
-- Table structure for goods_collect
-- ----------------------------
DROP TABLE IF EXISTS `goods_collect`;
CREATE TABLE `goods_collect`  (
                                `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                                `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户id',
                                `spu_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品id',
                                `sales_price` decimal(10, 2) NOT NULL COMMENT '加入时销售价格（元）',
                                `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0、显示；1、隐藏',
                                `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户id',
                                `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                `update_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                                PRIMARY KEY (`id`) USING BTREE,
                                UNIQUE INDEX `uc_user_spu_id`(`user_id` ASC, `spu_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品收藏' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of goods_collect
-- ----------------------------

-- ----------------------------
-- Table structure for goods_down_price_notify
-- ----------------------------
DROP TABLE IF EXISTS `goods_down_price_notify`;
CREATE TABLE `goods_down_price_notify`  (
                                          `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                                          `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户id',
                                          `spu_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品id',
                                          `sales_price` decimal(10, 2) NOT NULL COMMENT '加入时价格（元）',
                                          `expected_price` decimal(10, 2) NOT NULL COMMENT '期望价格（元）',
                                          `status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '通知状态：0.未通知；1.已通知；2.通知失败',
                                          `fail_msg` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '失败描述',
                                          `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                          `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                          `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0、显示；1、隐藏',
                                          `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户id',
                                          `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                          `update_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                                          PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品降价通知' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of goods_down_price_notify
-- ----------------------------

-- ----------------------------
-- Table structure for goods_footprint
-- ----------------------------
DROP TABLE IF EXISTS `goods_footprint`;
CREATE TABLE `goods_footprint`  (
                                  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                                  `user_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户主键',
                                  `spu_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品主键',
                                  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                  `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                  `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户id',
                                  `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                  `update_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品足迹' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of goods_footprint
-- ----------------------------
INSERT INTO `goods_footprint` VALUES ('1858776989024915457', '1', '1496390644407762945', '2024-11-19 15:39:03', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858777004736778241', '1', '1496390644407762945', '2024-11-19 15:39:07', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858777020574470145', '1', '1496394935734403073', '2024-11-19 15:39:11', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858777050333057026', '1', '1496394935734403073', '2024-11-19 15:39:18', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858777093203038210', '1', '1496394935734403073', '2024-11-19 15:39:28', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858777132142956546', '1', '1496687219931672578', '2024-11-19 15:39:37', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858779318289698818', '1', '1498511063457775618', '2024-11-19 15:48:19', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858779334702010369', '1', '1533298170302447618', '2024-11-19 15:48:23', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858779883719626754', '1', '1496390644407762945', '2024-11-19 15:50:33', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858779898458411010', '1', '1496394935734403073', '2024-11-19 15:50:37', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858779912232505346', '1', '1496687219931672578', '2024-11-19 15:50:40', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858779925654278145', '1', '1498511063457775618', '2024-11-19 15:50:43', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858779932981727234', '1', '1533298170302447618', '2024-11-19 15:50:45', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858779983313375234', '1', '1533380695339196418', '2024-11-19 15:50:57', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858780002166771713', '1', '1533792035048427522', '2024-11-19 15:51:02', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858781073702391810', '1', '1496394935734403073', '2024-11-19 15:55:17', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858781085039595522', '1', '1496390644407762945', '2024-11-19 15:55:20', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858781104010432514', '1', '1533298170302447618', '2024-11-19 15:55:24', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858781136369487874', '1', '1533380695339196418', '2024-11-19 15:55:32', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858781157240344578', '1', '1580763325952360450', '2024-11-19 15:55:37', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858786997787693057', '1', '1496394935734403073', '2024-11-19 16:18:50', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858788830308470785', '1', '1496394935734403073', '2024-11-19 16:26:06', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858788892782628865', '1', '1496394935734403073', '2024-11-19 16:26:21', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858789021384183810', '1', '1496394935734403073', '2024-11-19 16:26:52', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858789086974709761', '1', '1496390644407762945', '2024-11-19 16:27:08', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858789218113818626', '1', '1496390644407762945', '2024-11-19 16:27:39', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858790403906473985', '1', '1496390644407762945', '2024-11-19 16:32:22', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858790526963159041', '1', '1496390644407762945', '2024-11-19 16:32:51', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858790749487763458', '1', '1496390644407762945', '2024-11-19 16:33:44', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858790763987472386', '1', '1496390644407762945', '2024-11-19 16:33:47', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858790811018203137', '1', '1496390644407762945', '2024-11-19 16:33:59', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858791088844705793', '1', '1496390644407762945', '2024-11-19 16:35:05', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858791618228785154', '1', '1496390644407762945', '2024-11-19 16:37:11', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858791690660220929', '1', '1496390644407762945', '2024-11-19 16:37:28', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858791810701201409', '1', '1496390644407762945', '2024-11-19 16:37:57', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858791821618974721', '1', '1496390644407762945', '2024-11-19 16:38:00', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858791900455112705', '1', '1496390644407762945', '2024-11-19 16:38:18', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858793330339483649', '1', '1496390644407762945', '2024-11-19 16:43:59', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858793370965512194', '1', '1496390644407762945', '2024-11-19 16:44:09', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858796674890076162', '1', '1496390644407762945', '2024-11-19 16:57:17', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858797169755033602', '1', '1496390644407762945', '2024-11-19 16:59:15', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858797309001732098', '1', '1496390644407762945', '2024-11-19 16:59:48', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858797352022708226', '1', '1496390644407762945', '2024-11-19 16:59:58', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858797440459608066', '1', '1496390644407762945', '2024-11-19 17:00:19', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858797505873973249', '1', '1496390644407762945', '2024-11-19 17:00:35', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858797521615196162', '1', '1496390644407762945', '2024-11-19 17:00:39', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858797592238886913', '1', '1496390644407762945', '2024-11-19 17:00:55', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858797642448900097', '1', '1496390644407762945', '2024-11-19 17:01:07', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858797674455633922', '1', '1496390644407762945', '2024-11-19 17:01:15', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858797789559918593', '1', '1496390644407762945', '2024-11-19 17:01:42', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858797866449899522', '1', '1496390644407762945', '2024-11-19 17:02:01', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858797881704583169', '1', '1496390644407762945', '2024-11-19 17:02:04', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858797895847776258', '1', '1496390644407762945', '2024-11-19 17:02:08', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858797906144792578', '1', '1496390644407762945', '2024-11-19 17:02:10', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858798285184045058', '1', '1496390644407762945', '2024-11-19 17:03:41', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858798330344116225', '1', '1496390644407762945', '2024-11-19 17:03:51', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858799828302041090', '1', '1496390644407762945', '2024-11-19 17:09:49', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858799862909243393', '1', '1496390644407762945', '2024-11-19 17:09:57', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858799956370919426', '1', '1496390644407762945', '2024-11-19 17:10:19', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858800216677814274', '1', '1496390644407762945', '2024-11-19 17:11:21', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858800255366074369', '1', '1496390644407762945', '2024-11-19 17:11:30', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858800354070630401', '1', '1496390644407762945', '2024-11-19 17:11:54', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858800385217531906', '1', '1496390644407762945', '2024-11-19 17:12:01', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858800417941491713', '1', '1496390644407762945', '2024-11-19 17:12:09', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858801379619901441', '1', '1496390644407762945', '2024-11-19 17:15:58', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858801423374880769', '1', '1496390644407762945', '2024-11-19 17:16:09', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858801551699611649', '1', '1496390644407762945', '2024-11-19 17:16:39', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858801573698736129', '1', '1496390644407762945', '2024-11-19 17:16:45', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858801658696306689', '1', '1496390644407762945', '2024-11-19 17:17:05', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858801696696700929', '1', '1496390644407762945', '2024-11-19 17:17:14', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858801716783222786', '1', '1496390644407762945', '2024-11-19 17:17:19', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858801766401839106', '1', '1496390644407762945', '2024-11-19 17:17:31', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858801850514411521', '1', '1496390644407762945', '2024-11-19 17:17:51', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858801992999112706', '1', '1496390644407762945', '2024-11-19 17:18:25', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858802021440688130', '1', '1496390644407762945', '2024-11-19 17:18:31', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858802058296037377', '1', '1496390644407762945', '2024-11-19 17:18:40', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858802124834476033', '1', '1496390644407762945', '2024-11-19 17:18:56', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858802133009174530', '1', '1496390644407762945', '2024-11-19 17:18:58', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858803473202548738', '1', '1496390644407762945', '2024-11-19 17:24:18', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858803495755321345', '1', '1496390644407762945', '2024-11-19 17:24:23', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858803792397471746', '1', '1496390644407762945', '2024-11-19 17:25:34', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858805963075620866', '1', '1496390644407762945', '2024-11-19 17:34:11', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858806021900734465', '1', '1496390644407762945', '2024-11-19 17:34:25', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858806357893844993', '1', '1496390644407762945', '2024-11-19 17:35:45', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858806371038793729', '1', '1496390644407762945', '2024-11-19 17:35:48', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858806391121121281', '1', '1496390644407762945', '2024-11-19 17:35:53', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858806532771155970', '1', '1496390644407762945', '2024-11-19 17:36:27', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858806584503701505', '1', '1496390644407762945', '2024-11-19 17:36:39', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1858806797482070018', '1', '1496390644407762945', '2024-11-19 17:37:30', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859112800459276290', '1', '1496390644407762945', '2024-11-20 13:53:27', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859113026515484673', '1', '1496390644407762945', '2024-11-20 13:54:21', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859113200038035458', '1', '1496390644407762945', '2024-11-20 13:55:02', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859113234984976385', '1', '1496390644407762945', '2024-11-20 13:55:11', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859113352685535234', '1', '1496390644407762945', '2024-11-20 13:55:39', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859113481014460417', '1', '1496390644407762945', '2024-11-20 13:56:09', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859113502308941826', '1', '1496390644407762945', '2024-11-20 13:56:14', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859113681216978946', '1', '1496390644407762945', '2024-11-20 13:56:57', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859113891959783426', '1', '1496390644407762945', '2024-11-20 13:57:47', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859114112626311170', '1', '1496390644407762945', '2024-11-20 13:58:40', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859114247972306945', '1', '1496390644407762945', '2024-11-20 13:59:12', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859114288900325377', '1', '1496390644407762945', '2024-11-20 13:59:22', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859114474682826753', '1', '1496390644407762945', '2024-11-20 14:00:06', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859114507490672641', '1', '1496390644407762945', '2024-11-20 14:00:14', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859114635211423746', '1', '1496390644407762945', '2024-11-20 14:00:44', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859114709970698241', '1', '1496390644407762945', '2024-11-20 14:01:02', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859114800899014657', '1', '1496390644407762945', '2024-11-20 14:01:24', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859114860470714369', '1', '1496390644407762945', '2024-11-20 14:01:38', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859114894645903362', '1', '1496390644407762945', '2024-11-20 14:01:46', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859115177279078401', '1', '1496390644407762945', '2024-11-20 14:02:54', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859115399392641026', '1', '1496390644407762945', '2024-11-20 14:03:47', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859115790297579522', '1', '1496390644407762945', '2024-11-20 14:05:20', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859115948649332737', '1', '1496390644407762945', '2024-11-20 14:05:58', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859116188819374082', '1', '1496390644407762945', '2024-11-20 14:06:55', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859116539316387841', '1', '1496390644407762945', '2024-11-20 14:08:18', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859117174669557762', '1', '1496390644407762945', '2024-11-20 14:10:50', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859117464286248961', '1', '1496390644407762945', '2024-11-20 14:11:59', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859117993578053634', '1', '1496390644407762945', '2024-11-20 14:14:05', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859118075316649985', '1', '1496390644407762945', '2024-11-20 14:14:25', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859118115250618370', '1', '1496390644407762945', '2024-11-20 14:14:34', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859118131616792578', '1', '1496390644407762945', '2024-11-20 14:14:38', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859120036002775041', '1', '1496390644407762945', '2024-11-20 14:22:12', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859120663223189506', '1', '1496390644407762945', '2024-11-20 14:24:42', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859120776112881665', '1', '1496390644407762945', '2024-11-20 14:25:08', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859120819326795777', '1', '1496390644407762945', '2024-11-20 14:25:19', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859120864579141634', '1', '1496390644407762945', '2024-11-20 14:25:30', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859120885538078722', '1', '1496390644407762945', '2024-11-20 14:25:35', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859120969692594177', '1', '1496390644407762945', '2024-11-20 14:25:55', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859121002072621057', '1', '1496390644407762945', '2024-11-20 14:26:02', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859121021701963777', '1', '1496390644407762945', '2024-11-20 14:26:07', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859121086927585282', '1', '1496390644407762945', '2024-11-20 14:26:23', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859121208155553794', '1', '1496390644407762945', '2024-11-20 14:26:51', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859121224261681153', '1', '1496390644407762945', '2024-11-20 14:26:55', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859121299754958850', '1', '1496390644407762945', '2024-11-20 14:27:13', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859121493091401729', '1', '1496390644407762945', '2024-11-20 14:27:59', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859121540143104002', '1', '1496390644407762945', '2024-11-20 14:28:11', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859121804312952834', '1', '1496390644407762945', '2024-11-20 14:29:14', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859122093384384514', '1', '1496390644407762945', '2024-11-20 14:30:23', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859122149630001154', '1', '1496390644407762945', '2024-11-20 14:30:36', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859122338080079874', '1', '1496390644407762945', '2024-11-20 14:31:21', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859123000603951105', '1', '1496390644407762945', '2024-11-20 14:33:59', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859123129503301634', '1', '1496390644407762945', '2024-11-20 14:34:30', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859123150609039361', '1', '1496390644407762945', '2024-11-20 14:34:35', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859123224722391042', '1', '1496390644407762945', '2024-11-20 14:34:52', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859123323145928706', '1', '1496390644407762945', '2024-11-20 14:35:16', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859123328552386562', '1', '1496390644407762945', '2024-11-20 14:35:17', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859123345447043074', '1', '1496390644407762945', '2024-11-20 14:35:21', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859123671671619585', '1', '1496390644407762945', '2024-11-20 14:36:39', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859123710150164482', '1', '1496390644407762945', '2024-11-20 14:36:48', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859123902266064897', '1', '1496390644407762945', '2024-11-20 14:37:34', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859123998617616385', '1', '1496390644407762945', '2024-11-20 14:37:57', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859124037419122690', '1', '1496390644407762945', '2024-11-20 14:38:06', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859124051528761346', '1', '1496390644407762945', '2024-11-20 14:38:09', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859124096760135681', '1', '1496390644407762945', '2024-11-20 14:38:20', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859124171682988034', '1', '1496390644407762945', '2024-11-20 14:38:38', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859124352923058178', '1', '1496390644407762945', '2024-11-20 14:39:21', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859124374087516162', '1', '1496390644407762945', '2024-11-20 14:39:26', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859124442433699842', '1', '1496390644407762945', '2024-11-20 14:39:43', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859124475392540674', '1', '1496390644407762945', '2024-11-20 14:39:50', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859124623300476930', '1', '1496390644407762945', '2024-11-20 14:40:26', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859124859154579458', '1', '1496390644407762945', '2024-11-20 14:41:22', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859124889441648642', '1', '1496390644407762945', '2024-11-20 14:41:29', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859124913433067522', '1', '1496390644407762945', '2024-11-20 14:41:35', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859125207957094402', '1', '1496390644407762945', '2024-11-20 14:42:45', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859125723558690817', '1', '1496390644407762945', '2024-11-20 14:44:48', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859126239554551810', '1', '1496390644407762945', '2024-11-20 14:46:51', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859126248140292097', '1', '1496390644407762945', '2024-11-20 14:46:53', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859126257757831169', '1', '1496390644407762945', '2024-11-20 14:46:55', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859126405137285121', '1', '1496390644407762945', '2024-11-20 14:47:31', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859126498519269377', '1', '1496390644407762945', '2024-11-20 14:47:53', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859126528273661954', '1', '1496390644407762945', '2024-11-20 14:48:00', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859126532786733058', '1', '1496390644407762945', '2024-11-20 14:48:01', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859127616032849921', '1', '1496390644407762945', '2024-11-20 14:52:19', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859128754182344706', '1', '1496390644407762945', '2024-11-20 14:56:51', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859128791608119298', '1', '1496390644407762945', '2024-11-20 14:57:00', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859130289708650497', '1', '1496390644407762945', '2024-11-20 15:02:57', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859130681590861826', '1', '1533792035048427522', '2024-11-20 15:04:30', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859131047204147201', '1', '1496390644407762945', '2024-11-20 15:05:57', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859131181119885313', '1', '1496390644407762945', '2024-11-20 15:06:29', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859131181442846721', '1', '1496390644407762945', '2024-11-20 15:06:29', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859131200862474242', '1', '1496390644407762945', '2024-11-20 15:06:34', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859131242721628161', '1', '1496390644407762945', '2024-11-20 15:06:44', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859131423621959681', '1', '1496390644407762945', '2024-11-20 15:07:27', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859131437513494530', '1', '1496390644407762945', '2024-11-20 15:07:30', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859131477560709121', '1', '1496390644407762945', '2024-11-20 15:07:40', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859131498767110145', '1', '1498511063457775618', '2024-11-20 15:07:45', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859131533575639041', '1', '1496390644407762945', '2024-11-20 15:07:53', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859131545575542786', '1', '1498511063457775618', '2024-11-20 15:07:56', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859131572037406721', '1', '1498511063457775618', '2024-11-20 15:08:02', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859132436751896578', '1', '1496390644407762945', '2024-11-20 15:11:29', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859132478007070721', '1', '1496390644407762945', '2024-11-20 15:11:38', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859133067000598530', '1', '1496390644407762945', '2024-11-20 15:13:59', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859133234093281282', '1', '1496390644407762945', '2024-11-20 15:14:39', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859133245690531842', '1', '1496390644407762945', '2024-11-20 15:14:41', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859133366704590849', '1', '1496390644407762945', '2024-11-20 15:15:10', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859133372635336705', '1', '1496390644407762945', '2024-11-20 15:15:12', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859133411860467714', '1', '1496390644407762945', '2024-11-20 15:15:21', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859133552260599809', '1', '1496390644407762945', '2024-11-20 15:15:55', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859133564260503553', '1', '1496390644407762945', '2024-11-20 15:15:57', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859133618870341634', '1', '1496390644407762945', '2024-11-20 15:16:10', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859133630790553602', '1', '1496390644407762945', '2024-11-20 15:16:13', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859133789163278337', '1', '1496390644407762945', '2024-11-20 15:16:51', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859133802597634049', '1', '1496390644407762945', '2024-11-20 15:16:54', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859133852845395969', '1', '1496390644407762945', '2024-11-20 15:17:06', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859133863993856001', '1', '1496390644407762945', '2024-11-20 15:17:09', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859134002623991810', '1', '1496390644407762945', '2024-11-20 15:17:42', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859134014305128450', '1', '1496390644407762945', '2024-11-20 15:17:45', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859134062837420034', '1', '1496390644407762945', '2024-11-20 15:17:56', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859134074837323778', '1', '1496390644407762945', '2024-11-20 15:17:59', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859134103778021377', '1', '1496390644407762945', '2024-11-20 15:18:06', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859134121117274114', '1', '1496390644407762945', '2024-11-20 15:18:10', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859134138053873665', '1', '1496390644407762945', '2024-11-20 15:18:14', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859134150158635009', '1', '1496390644407762945', '2024-11-20 15:18:17', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859134156747886593', '1', '1496390644407762945', '2024-11-20 15:18:19', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859134238553591809', '1', '1496390644407762945', '2024-11-20 15:18:38', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859134384607645697', '1', '1496390644407762945', '2024-11-20 15:19:13', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859134395168903169', '1', '1496390644407762945', '2024-11-20 15:19:16', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859134588417265665', '1', '1496390644407762945', '2024-11-20 15:20:02', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859134596667461633', '1', '1496390644407762945', '2024-11-20 15:20:04', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859134914855751681', '1', '1496390644407762945', '2024-11-20 15:21:19', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859135359661690881', '1', '1496390644407762945', '2024-11-20 15:23:05', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859135851821322241', '1', '1496390644407762945', '2024-11-20 15:25:03', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859135934730129409', '1', '1496390644407762945', '2024-11-20 15:25:23', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859135952191016962', '1', '1496390644407762945', '2024-11-20 15:25:27', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859135971472232449', '1', '1496390644407762945', '2024-11-20 15:25:31', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859135985779003393', '1', '1496390644407762945', '2024-11-20 15:25:35', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859136094457614337', '1', '1496390644407762945', '2024-11-20 15:26:01', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859136123075350530', '1', '1496390644407762945', '2024-11-20 15:26:07', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859136422896783362', '1', '1496390644407762945', '2024-11-20 15:27:19', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859136471273885698', '1', '1496390644407762945', '2024-11-20 15:27:30', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859136834823573505', '1', '1496390644407762945', '2024-11-20 15:28:57', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859138058415296513', '1', '1496390644407762945', '2024-11-20 15:33:49', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859138076153008130', '1', '1496390644407762945', '2024-11-20 15:33:53', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859138099607556097', '1', '1496390644407762945', '2024-11-20 15:33:59', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859138534217142274', '1', '1496390644407762945', '2024-11-20 15:35:42', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859138545210413057', '1', '1496390644407762945', '2024-11-20 15:35:45', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859138558300835841', '1', '1496390644407762945', '2024-11-20 15:35:48', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859138566198710274', '1', '1496390644407762945', '2024-11-20 15:35:50', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859138592346001410', '1', '1496390644407762945', '2024-11-20 15:35:56', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859138727117377537', '1', '1496390644407762945', '2024-11-20 15:36:28', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859138918381834241', '1', '1496390644407762945', '2024-11-20 15:37:14', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859138971213287426', '1', '1496390644407762945', '2024-11-20 15:37:27', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859139011268890626', '1', '1496390644407762945', '2024-11-20 15:37:36', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859139141019684866', '1', '1496390644407762945', '2024-11-20 15:38:07', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859139171520663553', '1', '1496390644407762945', '2024-11-20 15:38:14', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859139251984191489', '1', '1496390644407762945', '2024-11-20 15:38:33', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859139263258480641', '1', '1496390644407762945', '2024-11-20 15:38:36', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859139287182790658', '1', '1496390644407762945', '2024-11-20 15:38:42', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859139294547988481', '1', '1496390644407762945', '2024-11-20 15:38:44', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859139311513948161', '1', '1496390644407762945', '2024-11-20 15:38:48', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859139318992392193', '1', '1496390644407762945', '2024-11-20 15:38:49', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859139363389100033', '1', '1496390644407762945', '2024-11-20 15:39:00', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859139938193297410', '1', '1496390644407762945', '2024-11-20 15:41:17', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859140443250413569', '1', '1496390644407762945', '2024-11-20 15:43:17', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859140648267993090', '1', '1496390644407762945', '2024-11-20 15:44:06', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859140908419698690', '1', '1496390644407762945', '2024-11-20 15:45:08', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859141095535988738', '1', '1496390644407762945', '2024-11-20 15:45:53', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859143097968021505', '1', '1496390644407762945', '2024-11-20 15:53:50', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859143848047988737', '1', '1496390644407762945', '2024-11-20 15:56:49', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859143866679087106', '1', '1496390644407762945', '2024-11-20 15:56:54', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859143885922553857', '1', '1496390644407762945', '2024-11-20 15:56:58', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859154512451784706', '1', '1496394935734403073', '2024-11-20 16:39:12', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859154574523289602', '1', '1496394935734403073', '2024-11-20 16:39:27', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859167029232193538', '1', '1496390644407762945', '2024-11-20 17:28:56', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859167395877277697', '1', '1496390644407762945', '2024-11-20 17:30:23', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859167469336317954', '1', '1496390644407762945', '2024-11-20 17:30:41', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859167469520867329', '1', '1496390644407762945', '2024-11-20 17:30:41', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859167488554618881', '1', '1496390644407762945', '2024-11-20 17:30:46', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859167512726392833', '1', '1496390644407762945', '2024-11-20 17:30:51', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859168410072567809', '1', '1496390644407762945', '2024-11-20 17:34:25', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859168554562146306', '1', '1496390644407762945', '2024-11-20 17:35:00', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859168644202811394', '1', '1496390644407762945', '2024-11-20 17:35:21', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859168718265831426', '1', '1496390644407762945', '2024-11-20 17:35:39', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859169123318157314', '1', '1496390644407762945', '2024-11-20 17:37:15', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859169180398440449', '1', '1496390644407762945', '2024-11-20 17:37:29', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859169207711748098', '1', '1496390644407762945', '2024-11-20 17:37:35', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859169289173520385', '1', '1496390644407762945', '2024-11-20 17:37:55', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859169304197517313', '1', '1496390644407762945', '2024-11-20 17:37:58', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859169596372733954', '1', '1496390644407762945', '2024-11-20 17:39:08', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859172693962706946', '1', '1496390644407762945', '2024-11-20 17:51:27', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859172730759335937', '1', '1496390644407762945', '2024-11-20 17:51:35', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859234378639495170', '1', '1496390644407762945', '2024-11-20 21:56:33', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859234544457109505', '1', '1496390644407762945', '2024-11-20 21:57:13', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859234709528137730', '1', '1496390644407762945', '2024-11-20 21:57:52', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859235065335140354', '1', '1496390644407762945', '2024-11-20 21:59:17', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859235085773983745', '1', '1496390644407762945', '2024-11-20 21:59:22', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859235257170022402', '1', '1496390644407762945', '2024-11-20 22:00:03', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859235273385201666', '1', '1496390644407762945', '2024-11-20 22:00:07', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859235325805613058', '1', '1496390644407762945', '2024-11-20 22:00:19', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859235369631895554', '1', '1496390644407762945', '2024-11-20 22:00:30', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859235399512117249', '1', '1496390644407762945', '2024-11-20 22:00:37', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859236063910842370', '1', '1496390644407762945', '2024-11-20 22:03:15', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859237033902362626', '1', '1496390644407762945', '2024-11-20 22:07:07', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859237139007426562', '1', '1496390644407762945', '2024-11-20 22:07:32', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859237156740943874', '1', '1496390644407762945', '2024-11-20 22:07:36', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859237215343759361', '1', '1496390644407762945', '2024-11-20 22:07:50', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859237357580996609', '1', '1496390644407762945', '2024-11-20 22:08:24', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859237528716988418', '1', '1496390644407762945', '2024-11-20 22:09:04', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859237556181291009', '1', '1496390644407762945', '2024-11-20 22:09:11', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859237614297567234', '1', '1496390644407762945', '2024-11-20 22:09:25', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859237643921936385', '1', '1496390644407762945', '2024-11-20 22:09:32', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859237674053816322', '1', '1496390644407762945', '2024-11-20 22:09:39', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859237733554212865', '1', '1496390644407762945', '2024-11-20 22:09:53', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859238261847773186', '1', '1496390644407762945', '2024-11-20 22:11:59', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859238336674156546', '1', '1496390644407762945', '2024-11-20 22:12:17', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859238396724006914', '1', '1496390644407762945', '2024-11-20 22:12:31', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859238436204990465', '1', '1496390644407762945', '2024-11-20 22:12:41', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859238700181901313', '1', '1496390644407762945', '2024-11-20 22:13:44', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859239113975156737', '1', '1496390644407762945', '2024-11-20 22:15:22', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859239173890789378', '1', '1496390644407762945', '2024-11-20 22:15:37', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859239228509016066', '1', '1496390644407762945', '2024-11-20 22:15:50', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859240760432414721', '1', '1496390644407762945', '2024-11-20 22:21:55', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859240884218908674', '1', '1496390644407762945', '2024-11-20 22:22:24', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859241053626847234', '1', '1496390644407762945', '2024-11-20 22:23:05', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859241123894022146', '1', '1496390644407762945', '2024-11-20 22:23:22', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859241140969033730', '1', '1496390644407762945', '2024-11-20 22:23:26', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859241187181875201', '1', '1496390644407762945', '2024-11-20 22:23:37', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859241420464869378', '1', '1496390644407762945', '2024-11-20 22:24:32', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859241438697508866', '1', '1496390644407762945', '2024-11-20 22:24:37', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859241480313393154', '1', '1496390644407762945', '2024-11-20 22:24:47', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859241535497850881', '1', '1496390644407762945', '2024-11-20 22:25:00', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859241546499510273', '1', '1496390644407762945', '2024-11-20 22:25:02', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859241584814477313', '1', '1496390644407762945', '2024-11-20 22:25:12', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859241667974942721', '1', '1496390644407762945', '2024-11-20 22:25:31', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859241685096091650', '1', '1496390644407762945', '2024-11-20 22:25:35', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859241748258115585', '1', '1496390644407762945', '2024-11-20 22:25:50', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859241756512505857', '1', '1496390644407762945', '2024-11-20 22:25:52', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859241787760070658', '1', '1496390644407762945', '2024-11-20 22:26:00', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859241824074354689', '1', '1496390644407762945', '2024-11-20 22:26:09', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859241870866010114', '1', '1496390644407762945', '2024-11-20 22:26:20', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859241922556612610', '1', '1496390644407762945', '2024-11-20 22:26:32', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859242020166455297', '1', '1496390644407762945', '2024-11-20 22:26:55', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859251456184438786', '1', '1859246202889981954', '2024-11-20 23:04:25', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859416034633646082', '1', '1859268410819559425', '2024-11-21 09:58:24', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859416174123614209', '1', '1859268410819559425', '2024-11-21 09:58:57', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859416233854697474', '1', '1859268410819559425', '2024-11-21 09:59:11', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859416312661475330', '1', '1859268410819559425', '2024-11-21 09:59:30', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859416319468830721', '1', '1859268410819559425', '2024-11-21 09:59:32', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859418380767920129', '1', '1859268410819559425', '2024-11-21 10:07:43', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859418711090331649', '1', '1859268410819559425', '2024-11-21 10:09:02', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859418914463744002', '1', '1859268410819559425', '2024-11-21 10:09:50', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859419022425128961', '1', '1859268410819559425', '2024-11-21 10:10:16', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859419074937815042', '1', '1859268410819559425', '2024-11-21 10:10:28', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859419396947116034', '1', '1859268410819559425', '2024-11-21 10:11:45', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859419405948092417', '1', '1859268410819559425', '2024-11-21 10:11:47', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859419439129231361', '1', '1859268410819559425', '2024-11-21 10:11:55', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859419616468598786', '1', '1859268410819559425', '2024-11-21 10:12:38', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859419623116570625', '1', '1859268410819559425', '2024-11-21 10:12:39', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859419724828442626', '1', '1859268410819559425', '2024-11-21 10:13:03', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859419807598837761', '1', '1859268410819559425', '2024-11-21 10:13:23', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859420124138766338', '1', '1859268410819559425', '2024-11-21 10:14:39', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859420158225874945', '1', '1859268410819559425', '2024-11-21 10:14:47', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859421618221133825', '1', '1859268410819559425', '2024-11-21 10:20:35', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859421648675975170', '1', '1859268410819559425', '2024-11-21 10:20:42', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859421669584580609', '1', '1859268410819559425', '2024-11-21 10:20:47', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859421896529981441', '1', '1859268410819559425', '2024-11-21 10:21:41', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859421925726531586', '1', '1859268410819559425', '2024-11-21 10:21:48', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859421932450000898', '1', '1859268410819559425', '2024-11-21 10:21:50', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859421939886501890', '1', '1859268410819559425', '2024-11-21 10:21:52', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859421962103730178', '1', '1859268410819559425', '2024-11-21 10:21:57', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859421969666060289', '1', '1859268410819559425', '2024-11-21 10:21:59', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859421976897040386', '1', '1859268410819559425', '2024-11-21 10:22:00', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859422006399774722', '1', '1859268410819559425', '2024-11-21 10:22:07', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859422781473599490', '1', '1496390644407762945', '2024-11-21 10:25:12', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859422798447947777', '1', '1496394935734403073', '2024-11-21 10:25:16', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859422814445023233', '1', '1498511063457775618', '2024-11-21 10:25:20', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859422850193076225', '1', '1533298170302447618', '2024-11-21 10:25:29', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859422869105192962', '1', '1533380695339196418', '2024-11-21 10:25:33', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859422896070373378', '1', '1533792035048427522', '2024-11-21 10:25:39', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859423017189289985', '1', '1859268410819559425', '2024-11-21 10:26:08', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859423061548249089', '1', '1859268410819559425', '2024-11-21 10:26:19', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859423073959194625', '1', '1859268410819559425', '2024-11-21 10:26:22', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859425461713547265', '1', '1859268410819559425', '2024-11-21 10:35:51', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859790224733818881', '1', '1496390644407762945', '2024-11-22 10:45:17', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859790937996185601', '1', '1496390644407762945', '2024-11-22 10:48:08', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859790962759356417', '1', '1496394935734403073', '2024-11-22 10:48:13', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859790974545350658', '1', '1496687219931672578', '2024-11-22 10:48:16', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859791052131586050', '1', '1498511063457775618', '2024-11-22 10:48:35', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859791486648897537', '1', '1859268410819559425', '2024-11-22 10:50:18', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859791537680994305', '1', '1496390644407762945', '2024-11-22 10:50:31', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1859791719415992321', '1', '1826218575774961665', '2024-11-22 10:51:14', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1860326582330191873', '1', '1496390644407762945', '2024-11-23 22:16:35', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1860326593847750657', '1', '1496394935734403073', '2024-11-23 22:16:38', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1860326605054930945', '1', '1496687219931672578', '2024-11-23 22:16:41', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1860326620703879169', '1', '1498511063457775618', '2024-11-23 22:16:44', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1860326634935152641', '1', '1533380695339196418', '2024-11-23 22:16:48', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1860326646897307650', '1', '1533298170302447618', '2024-11-23 22:16:50', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1860326676517482498', '1', '1580763325952360450', '2024-11-23 22:16:58', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1860326692699107330', '1', '1619358224880439298', '2024-11-23 22:17:01', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1860326713360248834', '1', '1496394935734403073', '2024-11-23 22:17:06', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1860326727369224193', '1', '1496687219931672578', '2024-11-23 22:17:10', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1860338126887690242', '1', '1496390644407762945', '2024-11-23 23:02:28', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1860691936323608578', '1', '1496390644407762945', '2024-11-24 22:28:22', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1861031570815705089', '1', '1533298170302447618', '2024-11-25 20:57:57', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1861031580001230850', '1', '1533298170302447618', '2024-11-25 20:58:00', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1861968758252265473', '1', '1496394935734403073', '2024-11-28 11:02:00', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1861968769493000194', '1', '1496394935734403073', '2024-11-28 11:02:03', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1861968779735490561', '1', '1496394935734403073', '2024-11-28 11:02:05', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1861968800946085890', '1', '1496390644407762945', '2024-11-28 11:02:11', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1861968858076700674', '1', '1496394935734403073', '2024-11-28 11:02:24', NULL, '0', '1590229800633634816', '环兴', NULL);
INSERT INTO `goods_footprint` VALUES ('1863223086791380993', '1', '1496394935734403073', '2024-12-01 22:06:16', NULL, '0', '1590229800633634816', 'oGMF95cU5PFhHxdUQBj3TtH_2DOo', NULL);
INSERT INTO `goods_footprint` VALUES ('1863224124881620993', '1', '1496390644407762945', '2024-12-01 22:10:23', NULL, '0', '1590229800633634816', 'oGMF95cU5PFhHxdUQBj3TtH_2DOo', NULL);
INSERT INTO `goods_footprint` VALUES ('1863224134562074625', '1', '1496390644407762945', '2024-12-01 22:10:25', NULL, '0', '1590229800633634816', 'oGMF95cU5PFhHxdUQBj3TtH_2DOo', NULL);
INSERT INTO `goods_footprint` VALUES ('1863224151645474818', '1', '1496390644407762945', '2024-12-01 22:10:29', NULL, '0', '1590229800633634816', 'oGMF95cU5PFhHxdUQBj3TtH_2DOo', NULL);
INSERT INTO `goods_footprint` VALUES ('1863225699641446402', '1', '1788392175009656833', '2024-12-01 22:16:39', NULL, '0', '1590229800633634816', 'oGMF95cU5PFhHxdUQBj3TtH_2DOo', NULL);
INSERT INTO `goods_footprint` VALUES ('1863228845138759681', '1', '1496687219931672578', '2024-12-01 22:29:08', NULL, '0', '1590229800633634816', '环兴', NULL);

-- ----------------------------
-- Table structure for goods_shop_category
-- ----------------------------
DROP TABLE IF EXISTS `goods_shop_category`;
CREATE TABLE `goods_shop_category`  (
                                      `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                                      `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类目名称',
                                      `parent_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '上级类目（0.顶级类目）',
                                      `category_pic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类目图片',
                                      `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类目描述',
                                      `status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '1' COMMENT '状态：0.正常；1.停用；',
                                      `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                      `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                      `sort` int NULL DEFAULT NULL COMMENT '排序序号',
                                      `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户id',
                                      `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                      `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                      `update_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                                      `shop_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '店铺ID',
                                      PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '店铺商品分类表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of goods_shop_category
-- ----------------------------
INSERT INTO `goods_shop_category` VALUES ('1496672394983804930', '小米', '4', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/bb60e527-092c-4fc5-974e-2697404c5d8f.jpg', NULL, '0', '2022-02-24 10:24:36', '2024-05-09 08:48:48', 1, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1496677018553872385', '数码家电', '0', '', '家用电器', '0', '2022-02-24 10:42:58', '2024-05-09 08:48:48', 70, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1496686626970333185', '数据线', '1496677018553872385', 'https://cdn.uviewui.com/uview/common/classify/7/1.jpg', NULL, '0', '2022-02-24 11:21:13', '2024-05-09 08:48:48', 1, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1498510244406673410', '家装家纺', '0', '', '家装家纺', '0', '2022-03-01 12:07:37', '2024-05-09 08:48:48', 110, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1498510397314220034', '耳机/耳麦', '1498510244406673410', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/bb60e527-092c-4fc5-974e-2697404c5d8f.jpg', '耳机/耳麦', '0', '2022-03-01 12:08:13', '2024-05-09 08:48:48', 1, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1498864835983876097', 'OPPO', '4', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/f1201f1f-5c36-4468-a1f8-a634c894685d.jpg', NULL, '0', '2022-03-02 11:36:37', '2024-05-09 08:48:48', 11, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1499342326879887362', '手表', '1498510244406673410', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/bb60e527-092c-4fc5-974e-2697404c5d8f.jpg', NULL, '0', '2022-03-03 19:14:04', '2024-05-09 08:48:48', 5, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1504307346357383170', '男装', '0', '', '男装', '0', '2022-03-17 12:03:14', '2024-05-09 08:48:48', 50, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1504307475378368514', '爸爸装', '1504307346357383170', 'https://cdn.uviewui.com/uview/common/classify/5/1.jpg', NULL, '0', '2022-03-17 12:03:44', '2024-05-09 08:48:48', 1, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1504307581011914753', '牛仔裤', '1504307346357383170', 'https://cdn.uviewui.com/uview/common/classify/5/2.jpg', NULL, '0', '2022-03-17 12:04:09', '2024-05-09 08:48:48', 2, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1504307713140879362', '女装', '0', '', NULL, '0', '2022-03-17 12:04:41', '2024-05-09 08:48:48', 10, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1504307782850211841', 'A字裙', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/1.jpg', 'A字裙', '0', '2022-03-17 12:04:58', '2024-05-09 08:48:49', 1, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1504307835576807425', 'T恤', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/2.jpg', NULL, '0', '2022-03-17 12:05:10', '2024-05-09 08:48:49', 2, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1528663744649547778', '内衣', '0', '', '内衣', '0', '2022-05-23 17:06:52', '2024-05-09 08:48:49', 100, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1528947151174553602', '联想', '4', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/5137138c-1436-412c-9fb7-74e80b357061.jpg', '联想', '0', '2022-05-24 11:53:01', '2024-05-09 08:48:49', 20, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1528947222288977921', '海尔', '4', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/5137138c-1436-412c-9fb7-74e80b357061.jpg', '海尔', '0', '2022-05-24 11:53:18', '2024-05-09 08:48:49', 22, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1528947336764116993', '耳机', '1496677018553872385', 'https://cdn.uviewui.com/uview/common/classify/7/2.jpg', '耳机', '0', '2022-05-24 11:53:46', '2024-05-09 08:48:49', 2, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1528947409455599617', '生活家电', '1496677018553872385', 'https://cdn.uviewui.com/uview/common/classify/7/3.jpg', '生活家电', '0', '2022-05-24 11:54:03', '2024-05-09 08:48:49', 3, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1528947472009449473', '电风扇', '1496677018553872385', 'https://cdn.uviewui.com/uview/common/classify/7/4.jpg', '电脑', '0', '2022-05-24 11:54:18', '2024-05-09 08:48:49', 4, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1528947743129260034', '居家日用', '0', '', '居家日用', '0', '2022-05-24 11:55:23', '2024-05-09 08:48:49', 40, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1528947872280268802', '鞋品', '0', '', NULL, '0', '2022-05-24 11:55:53', '2024-05-09 08:48:49', 60, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1528948092074381314', '箱包', '0', '', '箱包', '0', '2022-05-24 11:56:46', '2024-05-09 08:48:49', 90, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1528948268147068930', '母婴', '0', '', '母婴', '0', '2022-05-24 11:57:28', '2024-05-09 08:48:49', 80, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1634571151397249025', '三星', '4', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/41eb73b4-4668-429a-97d8-a53c22baa278.jpg', '', '0', '2023-03-11 23:05:06', '2024-05-09 08:48:49', 11, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779744640409305090', '半身裙', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/3.jpg', '半身裙', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:49', 3, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779744693672771585', '衬衫', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/4.jpg', '衬衫', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:49', 4, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779744749645758466', '短裙', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/5.jpg', '短裙', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:49', 5, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779744797574070273', '阔腿裤', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/6.jpg', '阔腿裤', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:50', 6, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779744849587634177', '连衣裙', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/7.jpg', '连衣裙', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:50', 7, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779744916155432961', '妈妈装', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/8.jpg', '妈妈装', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:50', 8, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779744976683433985', '牛仔裤', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/9.jpg', '牛仔裤', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:50', 9, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779745085554982914', '美食', '0', '', '美食', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:50', 20, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779745165934624769', '火锅', '1779745085554982914', 'https://cdn.uviewui.com/uview/common/classify/2/1.jpg', '火锅', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:50', 1, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779745230275248130', '糕点饼干', '1779745085554982914', 'https://cdn.uviewui.com/uview/common/classify/2/2.jpg', '糕点饼干', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:50', 2, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779745284285300738', '坚果果干', '1779745085554982914', 'https://cdn.uviewui.com/uview/common/classify/2/3.jpg', '坚果果干', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:50', 3, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779745330770771969', '酒类', '1779745085554982914', 'https://cdn.uviewui.com/uview/common/classify/2/4.jpg', '', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:50', 4, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779745425444601857', '辣条', '1779745085554982914', 'https://cdn.uviewui.com/uview/common/classify/2/5.jpg', '辣条', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:50', 5, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779745476535418882', '大礼包', '1779745085554982914', 'https://cdn.uviewui.com/uview/common/classify/2/6.jpg', '大礼包', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:50', 6, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779745526569271297', '精品茗茶', '1779745085554982914', 'https://cdn.uviewui.com/uview/common/classify/2/7.jpg', '', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:50', 7, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779745583142043649', '休闲食品', '1779745085554982914', 'https://cdn.uviewui.com/uview/common/classify/2/8.jpg', '', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:50', 8, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779745630567038978', '方便速食', '1779745085554982914', 'https://cdn.uviewui.com/uview/common/classify/2/10.jpg', '', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:50', 10, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779745687970283521', '饮品', '1779745085554982914', 'https://cdn.uviewui.com/uview/common/classify/2/14.jpg', '饮品', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:50', 14, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779745743423176705', '美妆', '0', '', '', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:50', 30, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779745806308376577', '化妆刷', '1779745743423176705', 'https://cdn.uviewui.com/uview/common/classify/3/1.jpg', '', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:51', 1, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779745855247515649', '粉底', '1779745743423176705', 'https://cdn.uviewui.com/uview/common/classify/3/2.jpg', '粉底', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:51', 2, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779745905382031361', '洗发护发', '1779745743423176705', 'https://cdn.uviewui.com/uview/common/classify/3/3.jpg', '', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:51', 3, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779745947287322625', '美容工具', '1779745743423176705', 'https://cdn.uviewui.com/uview/common/classify/3/4.jpg', '', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:51', 4, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779746006007578626', '眼部护理', '1779745743423176705', 'https://cdn.uviewui.com/uview/common/classify/3/5.jpg', '', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:51', 5, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779746052816011265', '眉妆', '1779745743423176705', 'https://cdn.uviewui.com/uview/common/classify/3/6.jpg', '', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:51', 6, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779746107962720257', '卸妆品', '1779745743423176705', 'https://cdn.uviewui.com/uview/common/classify/3/7.jpg', '', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:51', 7, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779746398036590594', '基础护肤', '1779745743423176705', 'https://cdn.uviewui.com/uview/common/classify/3/8.jpg', '', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:51', 8, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779746466324054018', '眼妆', '1779745743423176705', 'https://cdn.uviewui.com/uview/common/classify/3/9.jpg', '眼妆', '0', '2022-03-17 12:05:10', '2024-05-09 08:48:51', 9, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779746576323870721', '垃圾袋', '1528947743129260034', 'https://cdn.uviewui.com/uview/common/classify/4/1.jpg', '', '0', '2022-05-24 11:55:23', '2024-05-09 08:48:51', 1, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779746626668101634', '纸巾', '1528947743129260034', 'https://cdn.uviewui.com/uview/common/classify/4/2.jpg', '', '0', '2022-05-24 11:55:23', '2024-05-09 08:48:51', 2, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779746678002188289', '驱蚊用品', '1528947743129260034', 'https://cdn.uviewui.com/uview/common/classify/4/3.jpg', '', '0', '2022-05-24 11:55:23', '2024-05-09 08:48:51', 3, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779746742900654082', '收纳神器', '1528947743129260034', 'https://cdn.uviewui.com/uview/common/classify/4/4.jpg', '', '0', '2022-05-24 11:55:23', '2024-05-09 08:48:51', 4, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779746796533219329', '厨房用品', '1528947743129260034', 'https://cdn.uviewui.com/uview/common/classify/4/5.jpg', '', '0', '2022-05-24 11:55:23', '2024-05-09 08:48:51', 5, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779746852162273282', '厨房烹饪', '1528947743129260034', 'https://cdn.uviewui.com/uview/common/classify/4/6.jpg', '', '0', '2022-05-24 11:55:23', '2024-05-09 08:48:51', 6, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779746900463878145', '衣物晾晒', '1528947743129260034', 'https://cdn.uviewui.com/uview/common/classify/4/7.jpg', '衣物晾晒', '0', '2022-05-24 11:55:23', '2024-05-09 08:48:51', 7, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779746953488269313', '衣物护理', '1528947743129260034', 'https://cdn.uviewui.com/uview/common/classify/4/8.jpg', '衣物护理', '0', '2022-05-24 11:55:23', '2024-05-09 08:48:51', 8, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779747011034120194', '宠物用品', '1528947743129260034', 'https://cdn.uviewui.com/uview/common/classify/4/9.jpg', '', '0', '2022-05-24 11:55:23', '2024-05-09 08:48:52', 9, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779747226793312257', '衬衫', '1504307346357383170', 'https://cdn.uviewui.com/uview/common/classify/5/3.jpg', '', '0', '2022-03-17 12:04:09', '2024-05-09 08:48:52', 3, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779747276890079233', '休闲裤', '1504307346357383170', 'https://cdn.uviewui.com/uview/common/classify/5/4.jpg', '', '0', '2022-03-17 12:04:09', '2024-05-09 08:48:52', 4, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779747323681734657', '外套', '1504307346357383170', 'https://cdn.uviewui.com/uview/common/classify/5/5.jpg', '', '0', '2022-03-17 12:04:09', '2024-05-09 08:48:52', 5, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779747380296450049', '套装', '1504307346357383170', 'https://cdn.uviewui.com/uview/common/classify/5/7.jpg', '', '0', '2022-03-17 12:04:09', '2024-05-09 08:48:52', 7, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779747452757245954', '马甲/背心', '1504307346357383170', 'https://cdn.uviewui.com/uview/common/classify/5/9.jpg', '', '0', '2022-03-17 12:04:09', '2024-05-09 08:48:52', 9, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779747498689069058', 'POLO衫', '1504307346357383170', 'https://cdn.uviewui.com/uview/common/classify/5/10.jpg', '', '0', '2022-03-17 12:04:09', '2024-05-09 08:48:52', 10, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779747634639044610', '单鞋', '1528947872280268802', 'https://cdn.uviewui.com/uview/common/classify/6/1.jpg', '', '0', '2022-05-24 11:55:53', '2024-05-09 08:48:52', 1, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779747687227228162', '皮鞋', '1528947872280268802', 'https://cdn.uviewui.com/uview/common/classify/6/2.jpg', '', '0', '2022-05-24 11:55:53', '2024-05-09 08:48:52', 2, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779747729694556161', '帆布鞋', '1528947872280268802', 'https://cdn.uviewui.com/uview/common/classify/6/3.jpg', '', '0', '2022-05-24 11:55:53', '2024-05-09 08:48:52', 3, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779747783675248641', '老北京布鞋', '1528947872280268802', 'https://cdn.uviewui.com/uview/common/classify/6/4.jpg', '', '0', '2022-05-24 11:55:53', '2024-05-09 08:48:52', 4, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779747835965636609', '运动鞋', '1528947872280268802', 'https://cdn.uviewui.com/uview/common/classify/6/5.jpg', '', '0', '2022-05-24 11:55:53', '2024-05-09 08:48:52', 5, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779747881461252097', '拖鞋', '1528947872280268802', 'https://cdn.uviewui.com/uview/common/classify/6/6.jpg', '', '0', '2022-05-24 11:55:53', '2024-05-09 08:48:52', 6, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779747942740033537', '凉鞋', '1528947872280268802', 'https://cdn.uviewui.com/uview/common/classify/6/7.jpg', '', '0', '2022-05-24 11:55:53', '2024-05-09 08:48:52', 7, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779748037267062785', '高跟鞋', '1528947872280268802', 'https://cdn.uviewui.com/uview/common/classify/6/9.jpg', '', '0', '2022-05-24 11:55:53', '2024-05-09 08:48:52', 9, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779748394336550914', '电吹风', '1496677018553872385', 'https://cdn.uviewui.com/uview/common/classify/7/5.jpg', '', '0', '2022-05-24 11:54:18', '2024-05-09 08:48:52', 5, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779748449403568129', '手机壳', '1496677018553872385', 'https://cdn.uviewui.com/uview/common/classify/7/6.jpg', '手机壳', '0', '2022-05-24 11:54:18', '2024-05-09 08:48:53', 6, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779748495998091265', '榨汁机', '1496677018553872385', 'https://cdn.uviewui.com/uview/common/classify/7/7.jpg', '', '0', '2022-05-24 11:54:18', '2024-05-09 08:48:53', 7, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779748546547843074', '小家电', '1496677018553872385', 'https://cdn.uviewui.com/uview/common/classify/7/8.jpg', '', '0', '2022-05-24 11:54:18', '2024-05-09 08:48:53', 8, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779748622376665090', '手机配件', '1496677018553872385', 'https://cdn.uviewui.com/uview/common/classify/7/14.jpg', '', '0', '2022-05-24 11:54:18', '2024-05-09 08:48:53', 14, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779750112394772481', '情侣装', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/10.jpg', '', '0', '2022-03-17 12:04:41', '2024-05-09 08:48:53', 10, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779750154912432129', '休闲裤', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/11.jpg', '', '0', '2022-03-17 12:04:41', '2024-05-09 08:48:53', 11, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779750201825722370', '雪纺衫', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/12.jpg', '', '0', '2022-03-17 12:04:41', '2024-05-09 08:48:53', 12, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779750257714823170', '防晒衣', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/13.jpg', '', '0', '2022-03-17 12:04:41', '2024-05-09 08:48:53', 13, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1779750312597291009', '礼服/婚纱', '1504307713140879362', 'https://cdn.uviewui.com/uview/common/classify/1/14.jpg', '', '0', '2022-03-17 12:04:41', '2024-05-09 08:48:53', 14, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('1788397357608996866', '手机', '0', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/e9654b65-2d3a-4a2c-9945-ef161ec50288.jpg', '手机', '0', '2024-05-09 10:35:32', '2024-05-09 10:35:31', 1, '1590229800633634816', '0', 'lijx', NULL, '1788201114357780481');
INSERT INTO `goods_shop_category` VALUES ('1788397494343307265', '配件', '1788397357608996866', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/e9654b65-2d3a-4a2c-9945-ef161ec50288.jpg', '配件', '0', '2024-05-09 10:36:04', '2024-05-09 10:36:04', 1, '1590229800633634816', '0', 'lijx', NULL, '1788201114357780481');
INSERT INTO `goods_shop_category` VALUES ('4', '配饰', '0', '', '配饰', '0', '2022-02-23 15:42:45', '2024-05-09 08:48:53', 120, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('5', 'Apple', '4', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0a69731e-cb13-49d9-950f-0649bf32d044.jpg', 'Apple手机', '0', '2022-02-23 15:43:02', '2024-05-09 08:48:53', 1, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('7', '华为', '4', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d03d26cc-4c43-4575-a7ff-7f5e500a2ee6.jpg', '华为', '0', '2022-02-23 16:00:58', '2024-05-09 08:48:53', 1, '1590229800633634816', '0', NULL, NULL, '1');
INSERT INTO `goods_shop_category` VALUES ('9999', '默认', '0', NULL, '默认', '0', '2024-05-09 10:13:54', '2024-05-09 10:14:24', 1, '1590229800633634816', '0', NULL, NULL, '1788201114357780481');
INSERT INTO `goods_shop_category` VALUES ('99999', '默认2', '9999', NULL, '默认2', '0', '2024-05-09 10:18:29', '2024-05-09 10:18:36', 1, '1590229800633634816', '0', NULL, NULL, '1788201114357780481');

-- ----------------------------
-- Table structure for goods_sku
-- ----------------------------
DROP TABLE IF EXISTS `goods_sku`;
CREATE TABLE `goods_sku`  (
                            `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                            `spu_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'spuId',
                            `pic_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片',
                            `sales_price` decimal(10, 2) NOT NULL COMMENT '销售价格（元）',
                            `original_price` decimal(10, 2) NOT NULL COMMENT '原价（元）',
                            `cost_price` decimal(10, 2) NOT NULL COMMENT '成本价（元）',
                            `stock` int NOT NULL DEFAULT 0 COMMENT '库存',
                            `weight` decimal(10, 2) NULL DEFAULT NULL COMMENT '重量',
                            `volume` decimal(10, 2) NULL DEFAULT NULL COMMENT '体积',
                            `enable` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '是否启用：0.否；1.是；',
                            `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                            `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                            `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                            `first_rate` int NOT NULL DEFAULT 0 COMMENT '一级分销比例（%）',
                            `second_rate` int NOT NULL DEFAULT 0 COMMENT '二级分销比例（%）',
                            `version` int NULL DEFAULT 0 COMMENT '版本号',
                            `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户id',
                            `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                            `update_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                            `shop_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '店铺ID',
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品SKU' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of goods_sku
-- ----------------------------
INSERT INTO `goods_sku` VALUES ('1533298173649502209', '1533298170302447618', NULL, 100.00, 100.00, 100.00, 67, NULL, NULL, '1', '2022-12-16 15:26:49', '2024-11-19 15:53:04', '0', 10, 4, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1533298174073126913', '1533298170302447618', NULL, 100.00, 100.00, 100.00, 995, NULL, NULL, '1', '2022-12-16 15:26:49', '2024-08-21 13:49:44', '0', 10, 4, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1533298174479974402', '1533298170302447618', NULL, 100.00, 100.00, 100.00, 101, NULL, NULL, '1', '2022-12-16 15:26:49', '2024-06-13 09:12:59', '0', 10, 4, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1533379915051880450', '1496390644407762945', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0a69731e-cb13-49d9-950f-0649bf32d044.jpg', 99999.00, 99999.00, 99999.00, 99948, NULL, NULL, '1', '2022-06-05 17:24:53', '2024-11-22 10:53:07', '0', 0, 0, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1533380695737655297', '1533380695339196418', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/251d8c72-fcdb-4576-897a-2f46025c2bf4.jpg', 12245.00, 12245.00, 12245.00, 2, NULL, NULL, '1', '2022-06-05 17:30:23', '2024-08-21 13:49:22', '0', 0, 0, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1533792035451080705', '1533792035048427522', NULL, 18699.00, 30000.00, 12000.00, 0, NULL, NULL, '1', '2023-01-12 23:41:39', '2024-11-20 15:04:59', '0', 0, 0, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1533792035811790849', '1533792035048427522', NULL, 28699.00, 30000.00, 12000.00, 1, NULL, NULL, '1', '2023-01-12 23:41:39', '2024-08-21 13:48:40', '0', 0, 0, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1533792036172500994', '1533792035048427522', NULL, 17699.00, 30000.00, 12000.00, 0, NULL, NULL, '1', '2023-01-12 23:41:39', '2023-01-12 23:41:39', '0', 0, 0, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1533792036533211137', '1533792035048427522', NULL, 18699.00, 30000.00, 12000.00, 0, NULL, NULL, '1', '2023-01-12 23:41:39', '2023-01-12 23:41:39', '0', 0, 0, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1533792036889726977', '1533792035048427522', NULL, 28699.00, 30000.00, 12000.00, 0, NULL, NULL, '1', '2023-01-12 23:41:39', '2023-01-12 23:41:39', '0', 0, 0, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1533792037250437122', '1533792035048427522', NULL, 17699.00, 30000.00, 12000.00, 105, NULL, NULL, '1', '2023-01-12 23:41:39', '2024-08-21 13:49:44', '0', 0, 0, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1535187344563052546', '1535187344403668993', NULL, 4399.00, 5699.00, 1299.00, 0, NULL, NULL, '1', '2022-10-28 11:44:02', '2022-11-09 22:10:56', '0', 0, 0, NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1535187344667910145', '1535187344403668993', NULL, 4399.00, 5699.00, 1299.00, 0, NULL, NULL, '1', '2022-10-28 11:44:02', '2022-11-09 22:10:56', '0', 0, 0, NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1535191486941569026', '1535191486845100033', NULL, 0.10, 4188.10, 4185.10, 991, NULL, NULL, '1', '2022-12-16 15:26:20', '2024-08-21 13:49:38', '0', 10, 5, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1535191487033843713', '1535191486845100033', NULL, 4888.10, 4888.10, 4888.10, 992, NULL, NULL, '1', '2022-12-16 15:26:20', '2024-08-21 13:49:38', '0', 10, 5, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1535191487126118402', '1535191486845100033', NULL, 4188.10, 4188.10, 4188.10, 999, NULL, NULL, '1', '2022-12-16 15:26:20', '2024-08-21 13:49:10', '0', 10, 5, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1535191487222587394', '1535191486845100033', NULL, 4888.10, 4888.10, 4888.10, 999, NULL, NULL, '1', '2022-12-16 15:26:20', '2024-08-21 13:49:27', '0', 10, 5, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1535999718430232577', '1535999718077911042', NULL, 1.00, 1.00, 1.00, 1, NULL, NULL, '1', '2022-06-12 22:57:26', '2022-11-09 22:10:57', '0', 0, 0, NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1536003714071023617', '1498511063457775618', NULL, 12999.00, 99999.00, 99999.00, 0, NULL, NULL, '1', '2022-06-12 23:13:18', '2022-11-09 22:10:57', '0', 0, 0, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1536003782480121857', '1496687219931672578', NULL, 99999.00, 99999.00, 99999.00, 128, NULL, NULL, '1', '2022-06-12 23:13:34', '2024-11-22 10:53:07', '0', 0, 0, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1536003831662530562', '1496394935734403073', NULL, 99999.00, 99999.00, 99999.00, 203, NULL, NULL, '1', '2022-06-12 23:13:46', '2024-11-22 10:53:07', '0', 0, 0, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1544221247647547394', '1533298170302447618', NULL, 100.00, 100.00, 100.00, 100, NULL, NULL, '1', '2022-12-16 15:26:49', '2024-08-21 13:49:03', '0', 10, 5, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1580763326086578178', '1580763325952360450', NULL, 15999.00, 15999.00, 15999.00, 83, NULL, NULL, '1', '2022-10-14 11:32:00', '2024-08-21 13:48:42', '0', 0, 0, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1619358225165651970', '1619358224880439298', NULL, 8499.00, 8499.00, 7499.00, 100, NULL, NULL, '1', '2023-03-11 22:05:07', '2023-03-11 22:05:07', '0', 1, 1, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1619358225421504514', '1619358224880439298', NULL, 9499.00, 9499.00, 8499.00, 100, NULL, NULL, '1', '2023-03-11 22:05:07', '2023-03-11 22:05:07', '0', 1, 1, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1619358225664774145', '1619358224880439298', NULL, 8499.00, 8499.00, 7499.00, 100, NULL, NULL, '1', '2023-03-11 22:05:07', '2023-03-11 22:05:07', '0', 1, 1, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1619358225912238081', '1619358224880439298', NULL, 9499.00, 9499.00, 8499.00, 100, NULL, NULL, '1', '2023-03-11 22:05:07', '2024-08-21 13:49:25', '0', 1, 1, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1619358226155507713', '1619358224880439298', NULL, 8499.00, 8499.00, 7499.00, 100, NULL, NULL, '1', '2023-03-11 22:05:07', '2023-03-11 22:05:07', '0', 1, 1, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1619358226407165954', '1619358224880439298', NULL, 9499.00, 9499.00, 8499.00, 100, NULL, NULL, '1', '2023-03-11 22:05:07', '2023-03-11 22:05:07', '0', 1, 1, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1619358226650435586', '1619358224880439298', NULL, 8499.00, 8499.00, 7499.00, 100, NULL, NULL, '1', '2023-03-11 22:05:07', '2023-03-11 22:05:07', '0', 1, 1, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1619358226897899522', '1619358224880439298', NULL, 9499.00, 9499.00, 8499.00, 100, NULL, NULL, '1', '2023-03-11 22:05:07', '2023-03-11 22:05:07', '0', 1, 1, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1634555921736818689', '1634555921480966145', NULL, 0.01, 13499.00, 11499.00, 92, NULL, NULL, '1', '2023-04-07 14:05:58', '2024-08-21 13:49:36', '0', 0, 0, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1634555921929756674', '1634555921480966145', NULL, 13499.00, 14499.00, 12499.00, 100, NULL, NULL, '1', '2023-04-07 14:05:58', '2023-04-07 14:05:58', '0', 0, 0, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1634555922126888962', '1634555921480966145', NULL, 12499.00, 13499.00, 11499.00, 100, NULL, NULL, '1', '2023-04-07 14:05:58', '2024-08-21 13:49:07', '0', 0, 0, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1634555922319826946', '1634555921480966145', NULL, 13499.00, 14499.00, 12499.00, 100, NULL, NULL, '1', '2023-04-07 14:05:58', '2024-08-21 13:49:01', '0', 0, 0, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1634555922575679490', '1634555921480966145', NULL, 12499.00, 13499.00, 11499.00, 100, NULL, NULL, '1', '2023-04-07 14:05:58', '2024-08-21 13:49:22', '0', 0, 0, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1634555922772811778', '1634555921480966145', NULL, 13499.00, 14499.00, 12499.00, 98, NULL, NULL, '1', '2023-04-07 14:05:58', '2023-04-07 14:05:58', '0', 0, 0, 0, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1788392176179867650', '1788392175009656833', NULL, 0.01, 0.01, 0.01, 0, NULL, NULL, '1', '2024-05-09 10:14:56', '2024-05-09 10:14:56', '0', 0, 0, 0, '1590229800633634816', 'lijx', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1826218577926639618', '1826218575774961665', NULL, 1.00, 1.00, 1.00, 1000, NULL, NULL, '1', '2024-08-21 19:23:33', '2024-11-22 10:53:07', '0', 0, 0, 0, '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1826218579356897282', '1826218575774961665', NULL, 1.00, 1.00, 1.00, 1000, NULL, NULL, '1', '2024-08-21 19:23:33', '2024-10-15 10:22:09', '0', 0, 0, 0, '1590229800633634816', 'huawei', 'admin', NULL);
INSERT INTO `goods_sku` VALUES ('1826218580791349250', '1826218575774961665', NULL, 1.00, 1.00, 1.00, 1000, NULL, NULL, '1', '2024-08-21 19:23:34', '2024-10-15 10:22:09', '0', 0, 0, 0, '1590229800633634816', 'huawei', 'admin', NULL);
INSERT INTO `goods_sku` VALUES ('1826218582175469569', '1826218575774961665', NULL, 1.00, 1.00, 1.00, 1001, NULL, NULL, '1', '2024-08-21 19:23:34', '2024-11-22 10:53:07', '0', 0, 0, 0, '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1826218583614115842', '1826218575774961665', NULL, 1.00, 1.00, 1.00, 1000, NULL, NULL, '1', '2024-08-21 19:23:34', '2024-10-15 10:22:10', '0', 0, 0, 0, '1590229800633634816', 'huawei', 'admin', NULL);
INSERT INTO `goods_sku` VALUES ('1826218585174396929', '1826218575774961665', NULL, 1.00, 1.00, 1.00, 1000, NULL, NULL, '1', '2024-08-21 19:23:35', '2024-10-15 10:22:10', '0', 0, 0, 0, '1590229800633634816', 'huawei', 'admin', NULL);
INSERT INTO `goods_sku` VALUES ('1826218586541740033', '1826218575774961665', NULL, 1.00, 1.00, 1.00, 1001, NULL, NULL, '1', '2024-08-21 19:23:35', '2024-11-22 10:53:07', '0', 0, 0, 0, '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1826218588496285698', '1826218575774961665', NULL, 1.00, 1.00, 1.00, 1001, NULL, NULL, '1', '2024-08-21 19:23:36', '2024-11-22 10:53:07', '0', 0, 0, 0, '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1826218590585049089', '1826218575774961665', NULL, 1.00, 1.00, 1.00, 1001, NULL, NULL, '1', '2024-08-21 19:23:36', '2024-11-22 10:53:07', '0', 0, 0, 0, '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859246203015811073', '1859246202889981954', NULL, 7999.00, 8999.00, 6999.00, 101, NULL, NULL, '1', '2024-11-20 22:43:33', '2024-12-25 10:35:48', '0', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859246203082919938', '1859246202889981954', NULL, 6999.00, 7999.00, 5999.00, 100, NULL, NULL, '1', '2024-11-20 22:43:33', '2024-11-20 23:46:31', '0', 0, 0, 0, '1590229800633634816', 'admin', 'admin', NULL);
INSERT INTO `goods_sku` VALUES ('1859262199101411330', '1859246202889981954', NULL, 6999.00, 7999.00, 5999.00, 100, NULL, NULL, '1', '2024-11-20 23:47:06', NULL, '0', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859262199227240450', '1859246202889981954', NULL, 6999.00, 7999.00, 5999.00, 100, NULL, NULL, '1', '2024-11-20 23:47:06', NULL, '0', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859262199298543617', '1859246202889981954', NULL, 6999.00, 7999.00, 5999.00, 100, NULL, NULL, '1', '2024-11-20 23:47:06', NULL, '0', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859262199420178434', '1859246202889981954', NULL, 6999.00, 7999.00, 5999.00, 100, NULL, NULL, '1', '2024-11-20 23:47:06', NULL, '0', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859266702076825602', '1859266702013911042', NULL, 1.00, 1.00, 1.00, 1, NULL, NULL, '1', '2024-11-21 00:05:00', NULL, '0', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859266702143934466', '1859266702013911042', NULL, 1.00, 1.00, 1.00, 1, NULL, NULL, '1', '2024-11-21 00:05:00', NULL, '0', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859266984718389249', '1859266702013911042', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:06:07', NULL, '0', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859266984781303809', '1859266702013911042', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:06:07', NULL, '0', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859268096854978561', '1859268096586543105', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:10:32', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859268096985001985', '1859268096586543105', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:10:33', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859268162915266561', '1859268096586543105', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:10:48', NULL, '0', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859268162990764034', '1859268096586543105', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:10:48', NULL, '0', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859268163045289986', '1859268096586543105', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:10:48', NULL, '0', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859268163116593154', '1859268096586543105', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:10:48', NULL, '0', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859268410920222722', '1859268410819559425', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:11:47', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859268410987331586', '1859268410819559425', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:11:47', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859268453018451970', '1859268410819559425', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:11:57', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859268453081366529', '1859268410819559425', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:11:57', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859269446506463233', '1859268410819559425', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:15:54', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859269446573572097', '1859268410819559425', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:15:54', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859269601246920705', '1859268410819559425', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:16:31', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859269601314029570', '1859268410819559425', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:16:31', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859271741776412673', '1859268410819559425', NULL, 1.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:25:02', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859271741843521537', '1859268410819559425', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:25:02', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859271792053534722', '1859268410819559425', NULL, 1.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:25:13', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859271792116449281', '1859268410819559425', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:25:14', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859271850878648322', '1859268410819559425', NULL, 1.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:25:28', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859271850945757185', '1859268410819559425', NULL, 1.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:25:28', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859272163027140609', '1859268410819559425', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:26:42', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859272163094249474', '1859268410819559425', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:26:42', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859272190621466626', '1859268410819559425', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:26:49', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859272190680186882', '1859268410819559425', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:26:49', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859272221906780162', '1859268410819559425', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:26:56', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859272221973889026', '1859268410819559425', NULL, 0.00, 0.00, 0.00, 0, NULL, NULL, '1', '2024-11-21 00:26:56', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859272255540903937', '1859268410819559425', NULL, 0.00, 0.00, 0.00, 1, NULL, NULL, '1', '2024-11-21 00:27:04', '2024-11-21 09:59:24', '1', 0, 0, 0, '1590229800633634816', 'admin', 'admin', NULL);
INSERT INTO `goods_sku` VALUES ('1859272255608012802', '1859268410819559425', NULL, 0.00, 0.00, 0.00, 1, NULL, NULL, '1', '2024-11-21 00:27:04', '2024-11-21 09:59:24', '1', 0, 0, 0, '1590229800633634816', 'admin', 'admin', NULL);
INSERT INTO `goods_sku` VALUES ('1859416360707227650', '1859268410819559425', NULL, 0.00, 0.00, 0.00, 1, NULL, NULL, '1', '2024-11-21 09:59:41', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859416360707227651', '1859268410819559425', NULL, 0.00, 0.00, 0.00, 1, NULL, NULL, '1', '2024-11-21 09:59:41', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859421138921193474', '1859268410819559425', NULL, 0.00, 0.00, 0.00, 1, NULL, NULL, '1', '2024-11-21 10:18:41', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859421149847355394', '1859268410819559425', NULL, 0.00, 0.00, 0.00, 1, NULL, NULL, '1', '2024-11-21 10:18:43', NULL, '1', 0, 0, 0, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku` VALUES ('1859421564123000834', '1859268410819559425', NULL, 0.00, 0.00, 0.00, 1333, NULL, NULL, '1', '2024-11-21 10:20:22', '2024-11-23 23:44:28', '0', 0, 0, 0, '1590229800633634816', 'admin', 'admin', NULL);
INSERT INTO `goods_sku` VALUES ('1859421576039018498', '1859268410819559425', NULL, 0.00, 0.00, 0.00, 13, NULL, NULL, '1', '2024-11-21 10:20:25', '2024-11-23 23:44:28', '0', 0, 0, 0, '1590229800633634816', 'admin', 'admin', NULL);

-- ----------------------------
-- Table structure for goods_sku_specs_value
-- ----------------------------
DROP TABLE IF EXISTS `goods_sku_specs_value`;
CREATE TABLE `goods_sku_specs_value`  (
                                        `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                                        `sku_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'skuId',
                                        `specs_value_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '规格值主键',
                                        `sort` int NOT NULL COMMENT '排序字段',
                                        `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                        `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                        `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                        `spu_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'spuId',
                                        `pic_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片',
                                        `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户id',
                                        `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                        `update_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                                        `shop_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '店铺ID',
                                        PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品sku关联规格值' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of goods_sku_specs_value
-- ----------------------------
INSERT INTO `goods_sku_specs_value` VALUES ('1533298220822839298', '1533298174903599106', '1532635928418504706', 0, '2022-06-05 12:02:39', '2022-11-09 22:10:46', '0', NULL, NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1533298221233881090', '1533298174903599106', '1532637684972048386', 1, '2022-06-05 12:02:40', '2022-11-09 22:10:47', '0', NULL, NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1585839785666129922', '1535187344563052546', '1535186400517496833', 0, '2022-10-28 11:44:03', '2022-11-09 22:10:47', '0', '1535187344403668993', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d256cde2-208a-4ec9-84bb-851f5d2a82e0.png', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1585839785947148290', '1535187344667910145', '1535186511922405378', 0, '2022-10-28 11:44:03', '2022-11-09 22:10:47', '0', '1535187344403668993', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/7a63cb22-9657-4579-86e6-bb9a947220cf.png', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1603652730668101634', '1535191486941569026', '1535190856290213889', 0, '2022-12-16 15:26:20', '2022-12-16 15:26:20', '0', '1535191486845100033', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/03c835f5-c9b2-4bfe-bcff-7be4f05461d3.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1603652730672295938', '1535191486941569026', '1535190914179997697', 1, '2022-12-16 15:26:20', '2022-12-16 15:26:20', '0', '1535191486845100033', NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1603652730680684545', '1535191487033843713', '1535190856290213889', 0, '2022-12-16 15:26:20', '2022-12-16 15:26:20', '0', '1535191486845100033', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/03c835f5-c9b2-4bfe-bcff-7be4f05461d3.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1603652730689073154', '1535191487033843713', '1535190950188097538', 1, '2022-12-16 15:26:20', '2022-12-16 15:26:20', '0', '1535191486845100033', NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1603652730693267457', '1535191487126118402', '1535190779425398785', 0, '2022-12-16 15:26:20', '2022-12-16 15:26:20', '0', '1535191486845100033', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/690fba69-9470-4949-90b5-0ff7c20df1ac.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1603652730697461762', '1535191487126118402', '1535190914179997697', 1, '2022-12-16 15:26:20', '2022-12-16 15:26:20', '0', '1535191486845100033', NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1603652730701656065', '1535191487222587394', '1535190779425398785', 0, '2022-12-16 15:26:20', '2022-12-16 15:26:20', '0', '1535191486845100033', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/690fba69-9470-4949-90b5-0ff7c20df1ac.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1603652730710044674', '1535191487222587394', '1535190950188097538', 1, '2022-12-16 15:26:20', '2022-12-16 15:26:20', '0', '1535191486845100033', NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1603652855687720961', '1533298173649502209', '1532635376146108418', 0, '2022-12-16 15:26:49', '2022-12-16 15:26:49', '0', '1533298170302447618', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0a69731e-cb13-49d9-950f-0649bf32d044.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1603652855687720962', '1533298173649502209', '1532637661773352962', 1, '2022-12-16 15:26:49', '2022-12-16 15:26:49', '0', '1533298170302447618', '', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1603652855691915265', '1533298174073126913', '1532635376146108418', 0, '2022-12-16 15:26:49', '2022-12-16 15:26:49', '0', '1533298170302447618', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0a69731e-cb13-49d9-950f-0649bf32d044.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1603652855696109569', '1533298174073126913', '1532637684972048386', 1, '2022-12-16 15:26:49', '2022-12-16 15:26:49', '0', '1533298170302447618', '', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1603652855700303874', '1533298174479974402', '1532635928418504706', 0, '2022-12-16 15:26:49', '2022-12-16 15:26:49', '0', '1533298170302447618', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3da3d902-ec1c-4107-b8e7-0614bbc1b89b.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1603652855704498177', '1533298174479974402', '1532637661773352962', 1, '2022-12-16 15:26:49', '2022-12-16 15:26:49', '0', '1533298170302447618', '', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1603652855733858305', '1544221247647547394', '1532635928418504706', 0, '2022-12-16 15:26:49', '2022-12-16 15:26:49', '0', '1533298170302447618', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3da3d902-ec1c-4107-b8e7-0614bbc1b89b.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1603652855733858306', '1544221247647547394', '1532637684972048386', 1, '2022-12-16 15:26:49', '2022-12-16 15:26:49', '0', '1533298170302447618', '', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1613561854621106177', '1533792035451080705', '1533790877215002625', 0, '2023-01-12 23:41:39', '2023-01-12 23:41:39', '0', '1533792035048427522', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3dd8f6ba-cd27-42f6-9de8-47008fd98fcd.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1613561854621106178', '1533792035451080705', '1533790357578485761', 1, '2023-01-12 23:41:39', '2023-01-12 23:41:39', '0', '1533792035048427522', NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1613561854625300482', '1533792035811790849', '1533790877215002625', 0, '2023-01-12 23:41:39', '2023-01-12 23:41:39', '0', '1533792035048427522', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3dd8f6ba-cd27-42f6-9de8-47008fd98fcd.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1613561854629494786', '1533792035811790849', '1533790579952095234', 1, '2023-01-12 23:41:39', '2023-01-12 23:41:39', '0', '1533792035048427522', NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1613561854633689089', '1533792036172500994', '1533790877215002625', 0, '2023-01-12 23:41:39', '2023-01-12 23:41:39', '0', '1533792035048427522', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3dd8f6ba-cd27-42f6-9de8-47008fd98fcd.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1613561854637883393', '1533792036172500994', '1533790772885884930', 1, '2023-01-12 23:41:39', '2023-01-12 23:41:39', '0', '1533792035048427522', NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1613561854646272002', '1533792036533211137', '1533790905182621697', 0, '2023-01-12 23:41:39', '2023-01-12 23:41:39', '0', '1533792035048427522', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d6ac601c-90fe-49bb-9531-8b7c441a0782.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1613561854650466305', '1533792036533211137', '1533790357578485761', 1, '2023-01-12 23:41:39', '2023-01-12 23:41:39', '0', '1533792035048427522', NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1613561854654660610', '1533792036889726977', '1533790905182621697', 0, '2023-01-12 23:41:39', '2023-01-12 23:41:39', '0', '1533792035048427522', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d6ac601c-90fe-49bb-9531-8b7c441a0782.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1613561854658854914', '1533792036889726977', '1533790579952095234', 1, '2023-01-12 23:41:39', '2023-01-12 23:41:39', '0', '1533792035048427522', NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1613561854663049217', '1533792037250437122', '1533790905182621697', 0, '2023-01-12 23:41:39', '2023-01-12 23:41:39', '0', '1533792035048427522', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d6ac601c-90fe-49bb-9531-8b7c441a0782.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1613561854667243521', '1533792037250437122', '1533790772885884930', 1, '2023-01-12 23:41:39', '2023-01-12 23:41:39', '0', '1533792035048427522', NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1634556054771752962', '1619358225165651970', '1619350888342294530', 0, '2023-03-11 22:05:07', '2023-03-11 22:05:07', '0', '1619358224880439298', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/a8644cc1-460e-40f8-9e0a-89556a1a7630.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1634556054964690946', '1619358225165651970', '1619351214076137474', 1, '2023-03-11 22:05:07', '2023-03-11 22:05:07', '0', '1619358224880439298', NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1634556055166017538', '1619358225421504514', '1619350888342294530', 0, '2023-03-11 22:05:07', '2023-03-11 22:05:07', '0', '1619358224880439298', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/a8644cc1-460e-40f8-9e0a-89556a1a7630.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1634556055363149825', '1619358225421504514', '1619351276529324033', 1, '2023-03-11 22:05:07', '2023-03-11 22:05:07', '0', '1619358224880439298', NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1634556055627390977', '1619358225664774145', '1619350950564794370', 0, '2023-03-11 22:05:07', '2023-03-11 22:05:07', '0', '1619358224880439298', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d31ca68a-e944-404e-b117-edbfc5a58539.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1634556055820328961', '1619358225664774145', '1619351214076137474', 1, '2023-03-11 22:05:07', '2023-03-11 22:05:07', '0', '1619358224880439298', NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1634556056017461249', '1619358225912238081', '1619350950564794370', 0, '2023-03-11 22:05:07', '2023-03-11 22:05:07', '0', '1619358224880439298', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d31ca68a-e944-404e-b117-edbfc5a58539.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1634556056210399234', '1619358225912238081', '1619351276529324033', 1, '2023-03-11 22:05:07', '2023-03-11 22:05:07', '0', '1619358224880439298', NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1634556056407531521', '1619358226155507713', '1619350996324651010', 0, '2023-03-11 22:05:08', '2023-03-11 22:05:08', '0', '1619358224880439298', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d80e2723-597d-443d-8cda-4b0c388bf7b9.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1634556056663384065', '1619358226155507713', '1619351214076137474', 1, '2023-03-11 22:05:08', '2023-03-11 22:05:08', '0', '1619358224880439298', NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1634556056856322050', '1619358226407165954', '1619350996324651010', 0, '2023-03-11 22:05:08', '2023-03-11 22:05:08', '0', '1619358224880439298', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d80e2723-597d-443d-8cda-4b0c388bf7b9.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1634556057049260034', '1619358226407165954', '1619351276529324033', 1, '2023-03-11 22:05:08', '2023-03-11 22:05:08', '0', '1619358224880439298', NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1634556057246392322', '1619358226650435586', '1619351044588507137', 0, '2023-03-11 22:05:08', '2023-03-11 22:05:08', '0', '1619358224880439298', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/a8644cc1-460e-40f8-9e0a-89556a1a7630.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1634556057510633473', '1619358226650435586', '1619351214076137474', 1, '2023-03-11 22:05:08', '2023-03-11 22:05:08', '0', '1619358224880439298', NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1634556057707765761', '1619358226897899522', '1619351044588507137', 0, '2023-03-11 22:05:08', '2023-03-11 22:05:08', '0', '1619358224880439298', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/a8644cc1-460e-40f8-9e0a-89556a1a7630.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1634556057904898049', '1619358226897899522', '1619351276529324033', 1, '2023-03-11 22:05:08', '2023-03-11 22:05:08', '0', '1619358224880439298', NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1644219951390773250', '1634555921736818689', '1634554063530123265', 0, '2023-04-07 14:05:59', '2023-04-07 14:05:59', '0', '1634555921480966145', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/5c745e01-b57a-4dac-872c-472009cb1bfd.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1644219951583711233', '1634555921736818689', '1634555261842776065', 1, '2023-04-07 14:05:59', '2023-04-07 14:05:59', '0', '1634555921480966145', NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1644219951843758081', '1634555921929756674', '1634554063530123265', 0, '2023-04-07 14:05:59', '2023-04-07 14:05:59', '0', '1634555921480966145', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/5c745e01-b57a-4dac-872c-472009cb1bfd.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1644219952103804930', '1634555921929756674', '1634555317476024322', 1, '2023-04-07 14:05:59', '2023-04-07 14:05:59', '0', '1634555921480966145', NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1644219952359657474', '1634555922126888962', '1634554127627476993', 0, '2023-04-07 14:05:59', '2023-04-07 14:05:59', '0', '1634555921480966145', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/bf969350-f956-4895-9b5c-bad42d170243.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1644219952552595457', '1634555922126888962', '1634555261842776065', 1, '2023-04-07 14:05:59', '2023-04-07 14:05:59', '0', '1634555921480966145', NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1644219952808448002', '1634555922319826946', '1634554127627476993', 0, '2023-04-07 14:05:59', '2023-04-07 14:05:59', '0', '1634555921480966145', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/bf969350-f956-4895-9b5c-bad42d170243.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1644219953001385986', '1634555922319826946', '1634555317476024322', 1, '2023-04-07 14:05:59', '2023-04-07 14:05:59', '0', '1634555921480966145', NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1644219953257238529', '1634555922575679490', '1634554188243558402', 0, '2023-04-07 14:05:59', '2023-04-07 14:05:59', '0', '1634555921480966145', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/41eb73b4-4668-429a-97d8-a53c22baa278.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1644219953454370817', '1634555922575679490', '1634555261842776065', 1, '2023-04-07 14:05:59', '2023-04-07 14:05:59', '0', '1634555921480966145', NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1644219953710223361', '1634555922772811778', '1634554188243558402', 0, '2023-04-07 14:05:59', '2023-04-07 14:05:59', '0', '1634555921480966145', 'https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/41eb73b4-4668-429a-97d8-a53c22baa278.jpg', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1644219953966075906', '1634555922772811778', '1634555317476024322', 1, '2023-04-07 14:05:59', '2023-04-07 14:05:59', '0', '1634555921480966145', NULL, '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1826218597253992450', '1826218577926639618', '1826218277299900418', 0, '2024-08-21 19:23:38', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1826218600320028674', '1826218577926639618', '1826218397814837250', 1, '2024-08-21 19:23:38', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1826218601691566082', '1826218579356897282', '1826218277299900418', 0, '2024-08-21 19:23:39', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1826218603121823746', '1826218579356897282', '1826218416961835009', 1, '2024-08-21 19:23:39', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1826218604480778242', '1826218580791349250', '1826218277299900418', 0, '2024-08-21 19:23:39', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1826218605839732737', '1826218580791349250', '1826218436788310018', 1, '2024-08-21 19:23:40', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1826218607278379010', '1826218582175469569', '1826218302700605442', 0, '2024-08-21 19:23:40', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1826218608771551233', '1826218582175469569', '1826218397814837250', 1, '2024-08-21 19:23:41', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1826218610210197506', '1826218583614115842', '1826218302700605442', 0, '2024-08-21 19:23:41', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1826218611581734914', '1826218583614115842', '1826218416961835009', 1, '2024-08-21 19:23:41', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1826218613016186882', '1826218585174396929', '1826218302700605442', 0, '2024-08-21 19:23:42', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1826218614702297089', '1826218585174396929', '1826218436788310018', 1, '2024-08-21 19:23:42', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1826218616069640193', '1826218586541740033', '1826218338914226178', 0, '2024-08-21 19:23:42', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1826218617449566210', '1826218586541740033', '1826218397814837250', 1, '2024-08-21 19:23:43', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1826218618888212481', '1826218588496285698', '1826218338914226178', 0, '2024-08-21 19:23:43', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1826218620230389761', '1826218588496285698', '1826218416961835009', 1, '2024-08-21 19:23:43', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1826218621610315777', '1826218590585049089', '1826218338914226178', 0, '2024-08-21 19:23:44', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1826218622939910146', '1826218590585049089', '1826218436788310018', 1, '2024-08-21 19:23:44', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013556903477250', '1826218577926639618', '1826218277299900418', 0, '2024-10-15 10:21:44', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013558480535554', '1826218577926639618', '1826218397814837250', 1, '2024-10-15 10:21:44', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013559977902082', '1826218579356897282', '1826218277299900418', 0, '2024-10-15 10:21:45', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013561483657218', '1826218579356897282', '1826218416961835009', 1, '2024-10-15 10:21:45', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013562993606658', '1826218580791349250', '1826218277299900418', 0, '2024-10-15 10:21:45', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013564558082050', '1826218580791349250', '1826218436788310018', 1, '2024-10-15 10:21:46', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013566109974530', '1826218582175469569', '1826218302700605442', 0, '2024-10-15 10:21:46', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013567678644226', '1826218582175469569', '1826218397814837250', 1, '2024-10-15 10:21:46', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013569167622146', '1826218583614115842', '1826218302700605442', 0, '2024-10-15 10:21:47', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013570685960194', '1826218583614115842', '1826218416961835009', 1, '2024-10-15 10:21:47', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013572263018498', '1826218585174396929', '1826218302700605442', 0, '2024-10-15 10:21:48', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013573764579330', '1826218585174396929', '1826218436788310018', 1, '2024-10-15 10:21:48', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013575073202178', '1826218586541740033', '1826218338914226178', 0, '2024-10-15 10:21:48', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013576952250370', '1826218586541740033', '1826218397814837250', 1, '2024-10-15 10:21:49', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013578445422593', '1826218588496285698', '1826218338914226178', 0, '2024-10-15 10:21:49', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013580026675202', '1826218588496285698', '1826218416961835009', 1, '2024-10-15 10:21:49', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013581578567681', '1826218590585049089', '1826218338914226178', 0, '2024-10-15 10:21:50', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013583071739906', '1826218590585049089', '1826218436788310018', 1, '2024-10-15 10:21:50', NULL, '1', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013674046193666', '1826218577926639618', '1826218277299900418', 0, '2024-10-15 10:22:12', NULL, '0', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013675547754498', '1826218577926639618', '1826218397814837250', 1, '2024-10-15 10:22:12', NULL, '0', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013677053509633', '1826218579356897282', '1826218277299900418', 0, '2024-10-15 10:22:13', NULL, '0', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013678567653378', '1826218579356897282', '1826218416961835009', 1, '2024-10-15 10:22:13', NULL, '0', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013680069214209', '1826218580791349250', '1826218277299900418', 0, '2024-10-15 10:22:13', NULL, '0', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013681621106690', '1826218580791349250', '1826218436788310018', 1, '2024-10-15 10:22:14', NULL, '0', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013683055558658', '1826218582175469569', '1826218302700605442', 0, '2024-10-15 10:22:14', NULL, '0', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013684561313793', '1826218582175469569', '1826218397814837250', 1, '2024-10-15 10:22:14', NULL, '0', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013686054486017', '1826218583614115842', '1826218302700605442', 0, '2024-10-15 10:22:15', NULL, '0', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013687556046849', '1826218583614115842', '1826218416961835009', 1, '2024-10-15 10:22:15', NULL, '0', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013689065996289', '1826218585174396929', '1826218302700605442', 0, '2024-10-15 10:22:15', NULL, '0', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013690580140033', '1826218585174396929', '1826218436788310018', 1, '2024-10-15 10:22:16', NULL, '0', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013692081700866', '1826218586541740033', '1826218338914226178', 0, '2024-10-15 10:22:16', NULL, '0', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013693583261697', '1826218586541740033', '1826218397814837250', 1, '2024-10-15 10:22:16', NULL, '0', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013695101599746', '1826218588496285698', '1826218338914226178', 0, '2024-10-15 10:22:17', NULL, '0', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013696666075138', '1826218588496285698', '1826218416961835009', 1, '2024-10-15 10:22:17', NULL, '0', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013698155053057', '1826218590585049089', '1826218338914226178', 0, '2024-10-15 10:22:18', NULL, '0', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1846013699618865153', '1826218590585049089', '1826218436788310018', 1, '2024-10-15 10:22:18', NULL, '0', '1826218575774961665', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859246203217137666', '1859246203015811073', '1859245304222601218', 0, '2024-11-20 22:43:33', NULL, '1', '1859246202889981954', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/6331a13b-336c-4392-b617-191469d74b7c.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859246203217137667', '1859246203082919938', '1859245549320949761', 0, '2024-11-20 22:43:33', NULL, '1', '1859246202889981954', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/6331a13b-336c-4392-b617-191469d74b7c.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859262050174259202', '1859246203015811073', '1859245304222601218', 0, '2024-11-20 23:46:31', NULL, '1', '1859246202889981954', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/6331a13b-336c-4392-b617-191469d74b7c.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859262050237173762', '1859246203082919938', '1859245549320949761', 0, '2024-11-20 23:46:31', NULL, '1', '1859246202889981954', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/6331a13b-336c-4392-b617-191469d74b7c.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859262199684419586', '1859262199101411330', '1859245304222601218', 0, '2024-11-20 23:47:06', NULL, '0', '1859246202889981954', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/6331a13b-336c-4392-b617-191469d74b7c.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859262199684419587', '1859262199101411330', '1532637661773352962', 1, '2024-11-20 23:47:06', NULL, '0', '1859246202889981954', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859262199747334145', '1859262199227240450', '1859245304222601218', 0, '2024-11-20 23:47:07', NULL, '0', '1859246202889981954', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/6331a13b-336c-4392-b617-191469d74b7c.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859262199810248705', '1859262199227240450', '1532637684972048386', 1, '2024-11-20 23:47:07', NULL, '0', '1859246202889981954', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859262199873163266', '1859262199298543617', '1859245549320949761', 0, '2024-11-20 23:47:07', NULL, '0', '1859246202889981954', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/6331a13b-336c-4392-b617-191469d74b7c.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859262199873163267', '1859262199298543617', '1532637661773352962', 1, '2024-11-20 23:47:07', NULL, '0', '1859246202889981954', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859262199940272129', '1859262199420178434', '1859245549320949761', 0, '2024-11-20 23:47:07', NULL, '0', '1859246202889981954', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/6331a13b-336c-4392-b617-191469d74b7c.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859262199940272130', '1859262199420178434', '1532637684972048386', 1, '2024-11-20 23:47:07', NULL, '0', '1859246202889981954', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859266702211043329', '1859266702076825602', '1859245304222601218', 0, '2024-11-21 00:05:00', NULL, '1', '1859266702013911042', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859266702211043330', '1859266702143934466', '1859245549320949761', 0, '2024-11-21 00:05:00', NULL, '1', '1859266702013911042', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859266985104265218', '1859266984718389249', '1859245304222601218', 0, '2024-11-21 00:06:07', NULL, '0', '1859266702013911042', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859266985104265219', '1859266984718389249', '1532635376146108418', 1, '2024-11-21 00:06:07', NULL, '0', '1859266702013911042', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859266985171374082', '1859266984781303809', '1859245549320949761', 0, '2024-11-21 00:06:07', NULL, '0', '1859266702013911042', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859266985234288641', '1859266984781303809', '1532635376146108418', 1, '2024-11-21 00:06:07', NULL, '0', '1859266702013911042', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859268097110831105', '1859268096854978561', '1859245304222601218', 0, '2024-11-21 00:10:33', NULL, '1', '1859268096586543105', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859268097177939969', '1859268096985001985', '1859245549320949761', 0, '2024-11-21 00:10:33', NULL, '1', '1859268096586543105', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859268163305336834', '1859268162915266561', '1859245304222601218', 0, '2024-11-21 00:10:48', NULL, '0', '1859268096586543105', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859268163372445698', '1859268162915266561', '1532635376146108418', 1, '2024-11-21 00:10:48', NULL, '0', '1859268096586543105', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859268163439554561', '1859268162990764034', '1859245304222601218', 0, '2024-11-21 00:10:48', NULL, '0', '1859268096586543105', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859268163502469122', '1859268162990764034', '1532635928418504706', 1, '2024-11-21 00:10:48', NULL, '0', '1859268096586543105', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859268163569577986', '1859268163045289986', '1859245549320949761', 0, '2024-11-21 00:10:48', NULL, '0', '1859268096586543105', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859268163636686850', '1859268163045289986', '1532635376146108418', 1, '2024-11-21 00:10:48', NULL, '0', '1859268096586543105', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859268163699601410', '1859268163116593154', '1859245549320949761', 0, '2024-11-21 00:10:48', NULL, '0', '1859268096586543105', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859268163762515969', '1859268163116593154', '1532635928418504706', 1, '2024-11-21 00:10:48', NULL, '0', '1859268096586543105', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859268411121549314', '1859268410920222722', '1859245304222601218', 0, '2024-11-21 00:11:47', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859268411121549315', '1859268410987331586', '1859245549320949761', 0, '2024-11-21 00:11:47', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859268453408522241', '1859268453018451970', '1859245304222601218', 0, '2024-11-21 00:11:57', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859268453408522242', '1859268453018451970', '1533056307653984257', 1, '2024-11-21 00:11:58', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859268453534351361', '1859268453081366529', '1859245549320949761', 0, '2024-11-21 00:11:58', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859268453534351362', '1859268453081366529', '1533056307653984257', 1, '2024-11-21 00:11:58', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859269446762315778', '1859269446506463233', '1859245304222601218', 0, '2024-11-21 00:15:54', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/e26d9fde-7baa-45ff-ac3d-d059b48c32dd.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859269446829424642', '1859269446506463233', '1533056307653984257', 1, '2024-11-21 00:15:54', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859269446829424643', '1859269446573572097', '1859245549320949761', 0, '2024-11-21 00:15:54', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859269446892339201', '1859269446573572097', '1533056307653984257', 1, '2024-11-21 00:15:54', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859269601439858690', '1859269601246920705', '1859245304222601218', 0, '2024-11-21 00:16:31', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/e26d9fde-7baa-45ff-ac3d-d059b48c32dd.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859269601502773249', '1859269601246920705', '1533056307653984257', 1, '2024-11-21 00:16:31', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859269601502773250', '1859269601314029570', '1859245549320949761', 0, '2024-11-21 00:16:31', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/ef3b75e9-9d18-47cb-a4ca-1e5daa93ec82.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859269601565687809', '1859269601314029570', '1533056307653984257', 1, '2024-11-21 00:16:31', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859271742036459522', '1859271741776412673', '1859245304222601218', 0, '2024-11-21 00:25:02', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/e26d9fde-7baa-45ff-ac3d-d059b48c32dd.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859271742099374082', '1859271741776412673', '1533056307653984257', 1, '2024-11-21 00:25:02', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859271742166482945', '1859271741843521537', '1859245549320949761', 0, '2024-11-21 00:25:02', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/ef3b75e9-9d18-47cb-a4ca-1e5daa93ec82.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859271742166482946', '1859271741843521537', '1533056307653984257', 1, '2024-11-21 00:25:02', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859271792309387265', '1859271792053534722', '1859245304222601218', 0, '2024-11-21 00:25:14', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/e26d9fde-7baa-45ff-ac3d-d059b48c32dd.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859271792376496129', '1859271792053534722', '1533056307653984257', 1, '2024-11-21 00:25:14', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859271792439410690', '1859271792116449281', '1859245549320949761', 0, '2024-11-21 00:25:14', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/ef3b75e9-9d18-47cb-a4ca-1e5daa93ec82.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859271792506519554', '1859271792116449281', '1533056307653984257', 1, '2024-11-21 00:25:14', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859271851134500866', '1859271850878648322', '1859245304222601218', 0, '2024-11-21 00:25:28', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/e26d9fde-7baa-45ff-ac3d-d059b48c32dd.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859271851197415425', '1859271850878648322', '1533056307653984257', 1, '2024-11-21 00:25:28', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859271851197415426', '1859271850945757185', '1859245549320949761', 0, '2024-11-21 00:25:28', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/ef3b75e9-9d18-47cb-a4ca-1e5daa93ec82.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859271851268718593', '1859271850945757185', '1533056307653984257', 1, '2024-11-21 00:25:28', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859272163282993154', '1859272163027140609', '1859245304222601218', 0, '2024-11-21 00:26:42', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/e26d9fde-7baa-45ff-ac3d-d059b48c32dd.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859272163350102018', '1859272163027140609', '1533056307653984257', 1, '2024-11-21 00:26:42', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859272163350102019', '1859272163094249474', '1859245549320949761', 0, '2024-11-21 00:26:42', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/ef3b75e9-9d18-47cb-a4ca-1e5daa93ec82.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859272163417210882', '1859272163094249474', '1533056307653984257', 1, '2024-11-21 00:26:42', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859272190944428033', '1859272190621466626', '1859245304222601218', 0, '2024-11-21 00:26:49', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/e26d9fde-7baa-45ff-ac3d-d059b48c32dd.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859272191074451458', '1859272190621466626', '1533056307653984257', 1, '2024-11-21 00:26:49', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859272191137366018', '1859272190680186882', '1859245549320949761', 0, '2024-11-21 00:26:49', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/ef3b75e9-9d18-47cb-a4ca-1e5daa93ec82.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859272191208669185', '1859272190680186882', '1533056307653984257', 1, '2024-11-21 00:26:49', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859272222238130177', '1859272221906780162', '1859245304222601218', 0, '2024-11-21 00:26:56', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/e26d9fde-7baa-45ff-ac3d-d059b48c32dd.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859272222301044737', '1859272221906780162', '1533056307653984257', 1, '2024-11-21 00:26:56', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859272222363959297', '1859272221973889026', '1859245549320949761', 0, '2024-11-21 00:26:56', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/ef3b75e9-9d18-47cb-a4ca-1e5daa93ec82.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859272222431068161', '1859272221973889026', '1533056307653984257', 1, '2024-11-21 00:26:56', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859272255796756482', '1859272255540903937', '1859245304222601218', 0, '2024-11-21 00:27:04', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/e26d9fde-7baa-45ff-ac3d-d059b48c32dd.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859272255863865346', '1859272255540903937', '1533056307653984257', 1, '2024-11-21 00:27:04', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859272255930974210', '1859272255608012802', '1859245549320949761', 0, '2024-11-21 00:27:04', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/ef3b75e9-9d18-47cb-a4ca-1e5daa93ec82.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859272255993888769', '1859272255608012802', '1533056307653984257', 1, '2024-11-21 00:27:04', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859416152837521410', '1859272255540903937', '1859245304222601218', 0, '2024-11-21 09:58:52', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/e26d9fde-7baa-45ff-ac3d-d059b48c32dd.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859416152904630274', '1859272255540903937', '1533056307653984257', 1, '2024-11-21 09:58:52', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859416152904630275', '1859272255608012802', '1859245549320949761', 0, '2024-11-21 09:58:52', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/ef3b75e9-9d18-47cb-a4ca-1e5daa93ec82.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859416152904630276', '1859272255608012802', '1533056307653984257', 1, '2024-11-21 09:58:52', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859416289110458371', '1859272255540903937', '1859245304222601218', 0, '2024-11-21 09:59:24', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/e26d9fde-7baa-45ff-ac3d-d059b48c32dd.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859416289110458372', '1859272255540903937', '1533056307653984257', 1, '2024-11-21 09:59:24', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859416289177567234', '', '1859245304222601218', 0, '2024-11-21 09:59:24', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/e26d9fde-7baa-45ff-ac3d-d059b48c32dd.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859416289177567235', '', '1533056325844680706', 1, '2024-11-21 09:59:24', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859416289177567236', '1859272255608012802', '1859245549320949761', 0, '2024-11-21 09:59:24', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/ef3b75e9-9d18-47cb-a4ca-1e5daa93ec82.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859416289177567237', '1859272255608012802', '1533056307653984257', 1, '2024-11-21 09:59:24', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859416289177567238', '', '1859245549320949761', 0, '2024-11-21 09:59:24', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/ef3b75e9-9d18-47cb-a4ca-1e5daa93ec82.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859416289244676098', '', '1533056325844680706', 1, '2024-11-21 09:59:24', NULL, '1', '1859268410819559425', NULL, '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859416360774336515', '1859416360707227650', '1859245304222601218', 0, '2024-11-21 09:59:41', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/e26d9fde-7baa-45ff-ac3d-d059b48c32dd.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859416360841445377', '1859416360707227651', '1859245549320949761', 0, '2024-11-21 09:59:41', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/ef3b75e9-9d18-47cb-a4ca-1e5daa93ec82.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859418983791394819', '', '1859245304222601218', 0, '2024-11-21 10:10:07', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/e26d9fde-7baa-45ff-ac3d-d059b48c32dd.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859418983791394820', '', '1859245549320949761', 0, '2024-11-21 10:10:07', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/ef3b75e9-9d18-47cb-a4ca-1e5daa93ec82.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859420760695701506', '', '1859245304222601218', 0, '2024-11-21 10:17:10', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/e26d9fde-7baa-45ff-ac3d-d059b48c32dd.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859420760758616066', '', '1859245549320949761', 0, '2024-11-21 10:17:10', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/ef3b75e9-9d18-47cb-a4ca-1e5daa93ec82.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859421033694560259', '', '1859245304222601218', 0, '2024-11-21 10:18:15', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/e26d9fde-7baa-45ff-ac3d-d059b48c32dd.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859421033757474817', '', '1859245549320949761', 0, '2024-11-21 10:18:15', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/ef3b75e9-9d18-47cb-a4ca-1e5daa93ec82.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859421250087026689', '1859421138921193474', '1859245304222601218', 0, '2024-11-21 10:19:07', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/e26d9fde-7baa-45ff-ac3d-d059b48c32dd.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859421250149941249', '1859421149847355394', '1859245549320949761', 0, '2024-11-21 10:19:07', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/ef3b75e9-9d18-47cb-a4ca-1e5daa93ec82.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859421599581646849', '1859421564123000834', '1859245304222601218', 0, '2024-11-21 10:20:30', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/e26d9fde-7baa-45ff-ac3d-d059b48c32dd.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859421599678115841', '1859421576039018498', '1859245549320949761', 0, '2024-11-21 10:20:30', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/ef3b75e9-9d18-47cb-a4ca-1e5daa93ec82.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859422994489716737', '1859421564123000834', '1859245304222601218', 0, '2024-11-21 10:26:03', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/e26d9fde-7baa-45ff-ac3d-d059b48c32dd.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859422994569408513', '1859421576039018498', '1859245549320949761', 0, '2024-11-21 10:26:03', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/ef3b75e9-9d18-47cb-a4ca-1e5daa93ec82.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859426211906830337', '1859421564123000834', '1859245304222601218', 0, '2024-11-21 10:38:50', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/e26d9fde-7baa-45ff-ac3d-d059b48c32dd.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1859426211994910722', '1859421576039018498', '1859245549320949761', 0, '2024-11-21 10:38:50', NULL, '1', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/ef3b75e9-9d18-47cb-a4ca-1e5daa93ec82.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1860348697972416514', '1859421564123000834', '1859245304222601218', 0, '2024-11-23 23:44:28', NULL, '0', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/e26d9fde-7baa-45ff-ac3d-d059b48c32dd.jpg', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_sku_specs_value` VALUES ('1860348697989193729', '1859421576039018498', '1859245549320949761', 0, '2024-11-23 23:44:28', NULL, '0', '1859268410819559425', 'https://huanxing.oss-cn-beijing.aliyuncs.com/admin/ef3b75e9-9d18-47cb-a4ca-1e5daa93ec82.jpg', '1590229800633634816', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for goods_specs
-- ----------------------------
DROP TABLE IF EXISTS `goods_specs`;
CREATE TABLE `goods_specs`  (
                              `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                              `name` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '规格名',
                              `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                              `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                              `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建人',
                              `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                              `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                              `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户id',
                              `shop_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '店铺ID',
                              PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品规格' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of goods_specs
-- ----------------------------
INSERT INTO `goods_specs` VALUES ('1532626600345128962', '尺寸', '2022-06-03 15:33:51', '2022-11-09 22:10:43', 'lijx', 'lijx', '0', '1590229800633634816', NULL);
INSERT INTO `goods_specs` VALUES ('1532637571717451778', '属性', '2022-06-03 16:17:26', '2022-11-09 22:10:43', 'lijx', 'lijx', '0', '1590229800633634816', NULL);
INSERT INTO `goods_specs` VALUES ('1532637637920346114', '颜色', '2022-06-03 16:17:42', '2022-11-09 22:10:43', 'lijx', 'lijx', '0', '1590229800633634816', NULL);
INSERT INTO `goods_specs` VALUES ('1533790070751006721', '选择颜色', '2022-06-06 20:37:04', '2022-11-09 22:10:43', 'lijx', 'lijx', '0', '1590229800633634816', NULL);
INSERT INTO `goods_specs` VALUES ('1533790184030769154', '选择属性', '2022-06-06 20:37:31', '2022-11-09 22:10:43', 'lijx', 'lijx', '0', '1590229800633634816', NULL);
INSERT INTO `goods_specs` VALUES ('1580799619042574338', '杯型', '2022-10-14 13:56:13', '2022-11-09 22:10:44', 'lijx', 'lijx', '0', '1590229800633634816', NULL);
INSERT INTO `goods_specs` VALUES ('1580800257365311489', '大小', '2022-10-14 13:58:45', '2022-11-09 22:10:44', 'lijx', 'lijx', '0', '1590229800633634816', NULL);
INSERT INTO `goods_specs` VALUES ('1598839491279101953', '123', '2022-12-03 08:40:14', '2022-12-03 08:40:14', 'lijx', 'lijx', '0', '1590632726717206529', NULL);
INSERT INTO `goods_specs` VALUES ('1598839514708484098', 'shul', '2022-12-03 08:40:19', '2022-12-03 08:40:19', 'lijx', 'lijx', '0', '1590632726717206529', NULL);
INSERT INTO `goods_specs` VALUES ('1634554371580780546', '选择版本', '2023-03-11 21:58:26', '2023-03-11 21:58:26', 'lijx', 'lijx', '0', '1590229800633634816', NULL);
INSERT INTO `goods_specs` VALUES ('1826218265664901122', 's', '2024-08-21 19:22:19', NULL, 'huawei', NULL, '0', '1590229800633634816', NULL);
INSERT INTO `goods_specs` VALUES ('1826218386737680385', 'a', '2024-08-21 19:22:48', NULL, 'huawei', NULL, '0', '1590229800633634816', NULL);
INSERT INTO `goods_specs` VALUES ('1859245110806466562', '选择匹数', '2024-11-20 22:39:12', NULL, 'admin', NULL, '0', '1590229800633634816', NULL);

-- ----------------------------
-- Table structure for goods_specs_value
-- ----------------------------
DROP TABLE IF EXISTS `goods_specs_value`;
CREATE TABLE `goods_specs_value`  (
                                    `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                                    `specs_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '规格ID',
                                    `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '规格值',
                                    `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                    `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                    `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                    `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户id',
                                    `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                    `update_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                                    `shop_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '店铺ID',
                                    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品规格值' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of goods_specs_value
-- ----------------------------
INSERT INTO `goods_specs_value` VALUES ('1532635376146108418', '1532626600345128962', 'xl', '2022-06-03 16:08:43', '2022-11-09 22:10:40', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1532635600386183170', '', 'xxl', '2022-06-03 16:09:36', '2022-11-09 22:10:40', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1532635928418504706', '1532626600345128962', 'xxl', '2022-06-03 16:10:55', '2022-11-09 22:10:40', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1532635958902706178', '1532626600345128962', 'xxxl', '2022-06-03 16:11:02', '2022-11-09 22:10:41', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1532637661773352962', '1532637637920346114', '白色', '2022-06-03 16:17:48', '2022-11-09 22:10:41', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1532637684972048386', '1532637637920346114', '黑色', '2022-06-03 16:17:53', '2022-11-09 22:10:41', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1533056307653984257', '1532637571717451778', '16kg', '2022-06-04 20:01:22', '2022-11-09 22:10:41', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1533056325844680706', '1532637571717451778', '17kg', '2022-06-04 20:01:26', '2022-11-09 22:10:41', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1533790357578485761', '1533790184030769154', ' i5-12500H 16G 512G', '2022-06-06 20:38:13', '2022-11-09 22:10:41', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1533790579952095234', '1533790184030769154', ' i7-12700H 16G 512G', '2022-06-06 20:39:06', '2022-11-09 22:10:41', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1533790772885884930', '1533790184030769154', ' i5-12500H 16G 512G RTX2050', '2022-06-06 20:39:52', '2022-11-09 22:10:41', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1533790877215002625', '1533790070751006721', '晨雾灰', '2022-06-06 20:40:17', '2022-11-09 22:10:41', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1533790905182621697', '1533790070751006721', '暗夜黑', '2022-06-06 20:40:23', '2022-11-09 22:10:41', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1534908518105264129', '1533790070751006721', '土豪金', '2022-06-09 22:41:22', '2022-11-09 22:10:41', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1534908798779699202', '1533790184030769154', 'i10', '2022-06-09 22:42:29', '2022-11-09 22:10:41', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1535186400517496833', '1532637637920346114', '月亮银 8GB+128GB', '2022-06-10 17:05:34', '2022-11-09 22:10:42', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1535186511922405378', '1532637637920346114', '星际黑 8GB+128GB', '2022-06-10 17:06:01', '2022-11-09 22:10:42', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1535190779425398785', '1532637637920346114', '雪域白', '2022-06-10 17:22:58', '2022-11-09 22:10:42', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1535190856290213889', '1532637637920346114', '曜金黑', '2022-06-10 17:23:17', '2022-11-09 22:10:42', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1535190914179997697', '1532637571717451778', '8+128', '2022-06-10 17:23:31', '2022-11-09 22:10:42', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1535190950188097538', '1532637571717451778', '8+256', '2022-06-10 17:23:39', '2022-11-09 22:10:42', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1580809302654472194', '1551769333781536770', '64g', '2022-10-14 14:34:42', '2022-11-09 22:10:42', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1580810303063085058', '1532637637920346114', '暗黑色', '2022-10-14 14:38:40', '2022-11-09 22:10:42', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1598839535243796482', '1598839514708484098', '1', '2022-12-03 08:40:24', '2022-12-03 08:40:24', '0', '1590632726717206529', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1619350888342294530', '1533790070751006721', '幽紫秘境', '2023-01-28 23:05:12', '2023-01-28 23:05:12', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1619350950564794370', '1533790070751006721', '哥特太空', '2023-01-28 23:05:27', '2023-01-28 23:05:27', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1619350996324651010', '1533790070751006721', '繁樱花园', '2023-01-28 23:05:38', '2023-01-28 23:05:38', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1619351044588507137', '1533790070751006721', '蓝海假日', '2023-01-28 23:05:49', '2023-01-28 23:05:49', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1619351214076137474', '1533790184030769154', '8G+256GB', '2023-01-28 23:06:30', '2023-01-28 23:06:30', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1619351276529324033', '1533790184030769154', '8G+512GB', '2023-01-28 23:06:45', '2023-01-28 23:06:45', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1634554063530123265', '1533790070751006721', '空山绿', '2023-03-11 21:57:12', '2023-03-11 21:57:12', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1634554127627476993', '1533790070751006721', '铂萃黑', '2023-03-11 21:57:28', '2023-03-11 21:57:28', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1634554188243558402', '1533790070751006721', '云粉金', '2023-03-11 21:57:42', '2023-03-11 21:57:42', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1634555261842776065', '1533790184030769154', '12GB+256GB', '2023-03-11 22:01:58', '2023-03-11 22:01:58', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1634555317476024322', '1533790184030769154', '12GB+512GB', '2023-03-11 22:02:11', '2023-03-11 22:02:11', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1634558104054161410', '1533790184030769154', '123', '2023-03-11 22:13:16', '2023-03-11 22:13:16', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1634558415644811265', '1634558344480055298', '123123', '2023-03-11 22:14:30', '2023-03-11 22:14:30', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1634568124246417409', '1634568098183012354', 'asdasd', '2023-03-11 22:53:05', '2023-03-11 22:53:05', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1634568172199895042', '1634568154227302401', 'sda ', '2023-03-11 22:53:16', '2023-03-11 22:53:16', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1634568201996230657', '1634566900541448194', 'asd', '2023-03-11 22:53:23', '2023-03-11 22:53:23', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1667091651136344065', '1532626600345128962', 's', '2023-06-09 16:49:57', '2023-06-09 16:49:57', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1667091724939317249', '1532637637920346114', '灰色', '2023-06-09 16:50:14', '2023-06-09 16:50:14', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1826218277299900418', '1826218265664901122', 's1', '2024-08-21 19:22:21', NULL, '0', '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1826218302700605442', '1826218265664901122', 's2', '2024-08-21 19:22:28', NULL, '0', '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1826218338914226178', '1826218265664901122', 's3', '2024-08-21 19:22:36', NULL, '0', '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1826218397814837250', '1826218386737680385', 'a1', '2024-08-21 19:22:50', NULL, '0', '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1826218416961835009', '1826218386737680385', 'a2', '2024-08-21 19:22:55', NULL, '0', '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1826218436788310018', '1826218386737680385', 'a3', '2024-08-21 19:22:59', NULL, '0', '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1859245304222601218', '1859245110806466562', '3匹 一级能效 静悦节能省电空调', '2024-11-20 22:39:58', NULL, '0', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1859245549320949761', '1859245110806466562', '3匹 一级能效 劲爽四季除醛版', '2024-11-20 22:40:57', NULL, '0', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1869397773812543489', '1826218386737680385', '1123', '2024-12-18 23:02:16', NULL, '0', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_specs_value` VALUES ('1869401676331397121', '1826218265664901122', '2', '2024-12-18 23:17:46', NULL, '0', '1590229800633634816', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for goods_spu
-- ----------------------------
DROP TABLE IF EXISTS `goods_spu`;
CREATE TABLE `goods_spu`  (
                            `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                            `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品名称',
                            `sub_title` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '子标题',
                            `spu_urls` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品图地址',
                            `status` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '1' COMMENT '状态：0.下架；1.上架',
                            `sales_volume` int NOT NULL DEFAULT 0 COMMENT '销量',
                            `freight_template_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '运费模板id',
                            `place_shipment_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发货地点id',
                            `category_first_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '一级类目id',
                            `category_second_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '二级类目id',
                            `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                            `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                            `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                            `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '描述',
                            `enable_specs` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '多规格：0.否；1.是',
                            `low_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '最低价（元）',
                            `high_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '最高价（元）',
                            `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户id',
                            `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                            `update_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                            `shop_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '店铺ID',
                            `shop_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '店铺名称',
                            `shop_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '店铺类型',
                            `shop_category_first_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '店铺一级类目id',
                            `shop_category_second_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '店铺二级类目id',
                            `verify_status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '审核状态',
                            `verify_desc` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '审核描述',
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品spu' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of goods_spu
-- ----------------------------
INSERT INTO `goods_spu` VALUES ('1496390644407762945', 'Apple iPhone 13 Pro Max (A2644) 256GB 远峰蓝', '支持移动联通电信5G 双卡双待手机', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0a69731e-cb13-49d9-950f-0649bf32d044.jpg\"]', '1', 101, '1790247983355805697', NULL, '4', '5', '2022-02-23 15:45:03', '2024-12-25 10:33:29', '0', '<p><img src=\"https://img10.360buyimg.com/cms/jfs/t1/34607/36/15934/514434/61410fa8E2cd2eb63/05b406882a48787a.jpg\" alt=\"\" data-href=\"\" style=\"\"/></p>', '0', 99999.00, 99999.00, '1590229800633634816', NULL, 'admin', '1', '环兴商城自营旗舰店', '1', NULL, NULL, '1', '');
INSERT INTO `goods_spu` VALUES ('1496394935734403073', '华为 HUAWEI Mate 40 RS 保时捷设计麒麟9000芯片 超感知徕卡电影五摄 8GB+256GB陶瓷黑5G全网通手机', '超感知徕卡电影五摄 8GB+256GB陶瓷黑5G全网通手机', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d03d26cc-4c43-4575-a7ff-7f5e500a2ee6.jpg\"]', '1', 115, '1790247983355805697', NULL, '4', '7', '2022-02-23 16:02:05', '2024-12-25 10:33:29', '0', '<p><img src=\"//img30.360buyimg.com/sku/jfs/t1/142137/25/11595/481220/5f918368Ec6a933d2/20917dcdfe53f26c.jpg\" alt=\"\" data-href=\"\" style=\"\"/></p>', '0', 99999.00, 99999.00, '1590229800633634816', NULL, 'admin', '1', '环兴商城自营旗舰店', '1', NULL, NULL, '1', '');
INSERT INTO `goods_spu` VALUES ('1496687219931672578', '索尼（SONY）XR-65X95J 65英寸 全面屏电视 4K超高清HDR XR认知芯片 4K 120fps输入 ', '全面屏电视 4K超高清HDR XR认知芯片 4K 120fps输入 ', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/b640cc30-c999-46e8-b9c2-677e3ce61b9c.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d2f9b018-d304-4654-aa94-bafa746bf822.jpg\"]', '1', 11, '1790247983355805697', NULL, '1496677018553872385', '1496686626970333185', '2022-02-24 11:23:35', '2024-12-25 10:33:29', '0', '<p><img src=\"//img30.360buyimg.com/sku/jfs/t1/197296/23/3498/326122/611a2fa1E4cf9fd6d/19141d62f89961b9.jpg\" alt=\"\" data-href=\"\" style=\"\"/></p>', '0', 99999.00, 99999.00, '1590229800633634816', NULL, 'admin', '1', '环兴商城自营旗舰店', '1', NULL, NULL, '1', '');
INSERT INTO `goods_spu` VALUES ('1498511063457775618', 'Apple AirPods (第三代) 配MagSafe无线充电盒 无线蓝牙耳机 Apple耳机 适用iPhone/iPad/Apple Watch', '配MagSafe无线充电盒', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/f1201f1f-5c36-4468-a1f8-a634c894685d.jpg\"]', '1', 12, '1790247983355805697', NULL, '1498510244406673410', '1498510397314220034', '2022-03-01 12:10:53', '2024-12-25 10:33:29', '0', '<p><img src=\"https://img30.360buyimg.com/sku/jfs/t1/117805/35/24622/280548/616dd475E3940eed4/a43190fbff6ae1c5.jpg\" alt=\"\" data-href=\"\" style=\"\"/></p>', '0', 12999.00, 12999.00, '1590229800633634816', NULL, 'admin', '1', '环兴商城自营旗舰店', '1', NULL, NULL, '1', '');
INSERT INTO `goods_spu` VALUES ('1533298170302447618', 'Apple MacBook Pro 14英寸 M1 Pro芯片(8核中央处理器 14核图形处理器) ', '16G 512G 深空灰 笔记本 MKGP3CH/A', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/ea86c408-21eb-47af-81df-6367cb398075.jpg\"]', '1', 125, '1790247983355805697', NULL, '1504307346357383170', '1504307475378368514', '2022-06-05 12:02:27', '2024-12-25 10:33:23', '0', '<p><img src=\"https://img10.360buyimg.com/cms/jfs/t1/34607/36/15934/514434/61410fa8E2cd2eb63/05b406882a48787a.jpg\" alt=\"\" data-href=\"\" style=\"\"/></p>', '1', 100.00, 100.00, '1590229800633634816', NULL, 'admin', '1', '环兴商城自营旗舰店', '1', NULL, NULL, '1', '');
INSERT INTO `goods_spu` VALUES ('1533380695339196418', 'Apple Watch SE【30天无忧试用套装】智能手表 GPS款 40毫米银色铝金属表壳 深渊蓝', '', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/251d8c72-fcdb-4576-897a-2f46025c2bf4.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/291baa92-9273-44b7-bcd9-f4bf2501195c.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0196ccfd-911a-468e-b7fe-37a089c9627f.jpg\"]', '1', 100, '1790247983355805697', NULL, '4', '5', '2022-06-05 17:30:23', '2024-12-25 10:33:23', '0', '<p>123123123123123123</p>', '0', 12245.00, 12245.00, '1590229800633634816', NULL, 'admin', '1', '环兴商城自营旗舰店', '1', NULL, NULL, '1', '');
INSERT INTO `goods_spu` VALUES ('1533792035048427522', '联想ThinkPad neo 14英寸高性能标压商务办公轻薄笔记本电脑', '', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3dd8f6ba-cd27-42f6-9de8-47008fd98fcd.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0d0b2c3a-1572-48c0-b663-625c2f20689c.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/52d03a9c-0b2e-4ce4-ab80-bbbef377d5d7.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/59fae85b-5f4e-4725-b3c8-6af9fe7d4f98.jpg\"]', '1', 1005, '1790247983355805697', NULL, '4', '1528947151174553602', '2022-06-06 20:44:53', '2024-12-25 10:33:23', '0', '<p><img src=\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/6550f400-e789-4e9c-916c-fdd92457f2da.jpg\" alt=\"\" data-href=\"\" style=\"\"/></p>', '1', 17699.00, 28699.00, '1590229800633634816', NULL, 'admin', '1', '环兴商城自营旗舰店', '1', NULL, NULL, '1', '');
INSERT INTO `goods_spu` VALUES ('1535191486845100033', 'HUAWEI P50 原色双影像单元 搭载HarmonyOS 2 万象双环设计 支持66W超级快充', '', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/03c835f5-c9b2-4bfe-bcff-7be4f05461d3.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/690fba69-9470-4949-90b5-0ff7c20df1ac.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/6be07b2f-0cc6-4f8c-9dde-35acab18a116.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/6623a234-5562-4b62-9438-aa255f9954bb.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/30357715-2984-40df-8148-709ce9117ce9.jpg\"]', '1', 1238, '1790247983355805697', NULL, '4', '7', '2022-06-10 17:25:47', '2024-12-25 10:33:23', '0', '<p><br></p><p><img src=\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/30357715-2984-40df-8148-709ce9117ce9.jpg\" alt=\"\" data-href=\"\" style=\"\"/></p><p><br></p><p><img src=\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/72e4b9a1-9a6a-4ed8-b90e-8595793c9361.jpg\" alt=\"\" data-href=\"\" style=\"\"/></p>', '1', 0.10, 4888.10, '1590229800633634816', NULL, 'admin', '1', '环兴商城自营旗舰店', '1', NULL, NULL, '1', '');
INSERT INTO `goods_spu` VALUES ('1580763325952360450', 'Apple 苹果 iphone 14 pro max 全网通5G手机（A2896） 暗紫色 256G 官方标配【搭配 20W 品牌闪充】', '【大量现货当日发】全新国行未拆封未激活！下单赠20W快充头+90天碎屏险+壳膜套装+50元晒单红包+', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/c461f186-7309-420c-b68d-b4c718202111.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/e9654b65-2d3a-4a2c-9945-ef161ec50288.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/b426bec6-1d61-4c49-8d0a-86185ac18245.jpg\"]', '1', 3, '1790247983355805697', NULL, '4', '5', '2022-10-14 11:32:00', '2024-12-25 10:33:23', '0', '<p><br></p><p><img src=\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d03d26cc-4c43-4575-a7ff-7f5e500a2ee6.jpg\" alt=\"\" data-href=\"\" style=\"\"/></p><p><img src=\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/0a69731e-cb13-49d9-950f-0649bf32d044.jpg\" alt=\"\" data-href=\"\" style=\"\"/></p>', '0', 15999.00, 15999.00, '1590229800633634816', NULL, 'admin', '1', '环兴商城自营旗舰店', '1', NULL, NULL, '1', '');
INSERT INTO `goods_spu` VALUES ('1619358224880439298', '三星 SAMSUNG Galaxy Z Flip4 掌心折叠设计 立式自由拍摄系统', '', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d382cd20-8da3-4d68-82d7-073e162b0a72.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/3c899bb2-8184-4425-aef9-5de14d164e89.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/a02922b5-cd64-4ada-aafb-4f7d21d85fa3.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/57e8652f-7021-4d86-b1aa-5096e3f1a387.jpg\"]', '1', 0, '1790247983355805697', NULL, '4', '1496672394983804930', '2023-01-28 23:34:21', '2024-12-25 10:33:23', '0', '<p><br></p><p><img src=\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/57e8652f-7021-4d86-b1aa-5096e3f1a387.jpg\" alt=\"\" data-href=\"\" style=\"\"/></p>', '1', 8499.00, 9499.00, '1590229800633634816', NULL, 'admin', '1', '环兴商城自营旗舰店', '1', NULL, NULL, '1', '');
INSERT INTO `goods_spu` VALUES ('1634555921480966145', '三星 SAMSUNG Galaxy Z Fold4 沉浸大屏体验 PC般强大生产力5G折叠手机', '', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/5c745e01-b57a-4dac-872c-472009cb1bfd.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/fe7c8fc4-0820-4bf8-8e82-a38eefdf4681.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/2554908c-d7e8-423d-862c-5c0da044c72f.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/bf969350-f956-4895-9b5c-bad42d170243.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/d2383b85-6806-4b43-b82f-cd87884cc4e8.jpg\"]', '1', 107, '1790247983355805697', NULL, '4', '1496672394983804930', '2023-03-11 22:04:35', '2024-12-25 10:33:23', '0', '<p><br></p><p><img src=\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/72ca917c-9cc3-4452-9aa7-93d99196782e.jpg\" alt=\"\" data-href=\"\" style=\"\"/></p>', '1', 0.01, 13499.00, '1590229800633634816', NULL, 'admin', '1', '环兴商城自营旗舰店', '1', NULL, NULL, '1', '');
INSERT INTO `goods_spu` VALUES ('1788392175009656833', '苹果16promax', '苹果16promax', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/e9654b65-2d3a-4a2c-9945-ef161ec50288.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/c461f186-7309-420c-b68d-b4c718202111.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/b426bec6-1d61-4c49-8d0a-86185ac18245.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/55576d6d-d344-4718-8040-41f239e13127.jpg\"]', '1', 1, '1790247983355805697', NULL, '1496677018553872385', '1779748622376665090', '2024-05-09 10:14:56', '2024-12-25 10:33:23', '0', '<p>去</p>', '0', 0.01, 0.01, '1590229800633634816', 'lijx', 'admin', '1', '环兴商城自营旗舰店', '1', '9999', '99999', '1', '');
INSERT INTO `goods_spu` VALUES ('1826218575774961665', 'cesssss', 'asasd', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/7597aa6a-95fa-48f1-8f20-9e2ab3211344.jpg\"]', '1', 0, '1790247983355805697', NULL, '1504307713140879362', '1504307782850211841', '2024-08-21 19:23:33', '2024-12-25 10:33:23', '0', '<p><br></p><p><img src=\"https://huanxing.oss-cn-beijing.aliyuncs.com/lijx/7a45c477-f0a7-435c-b53a-8a75fb1ad56a.jpg\" alt=\"\" data-href=\"\" style=\"\"/></p>', '1', 1.00, 1.00, '1590229800633634816', 'huawei', 'admin', '1', '环兴商城自营旗舰店', '1', '1504307713140879362', '1504307782850211841', '1', '');
INSERT INTO `goods_spu` VALUES ('1859246202889981954', '海尔（Haier）3匹 静悦 新一级变频省电 客厅立式柜机空调', '123', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/admin/392f59a2-af4f-474b-bb97-ae678b3eacb7.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/admin/90baaf0c-3c92-4648-8363-1b8cc6379261.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/admin/f50c7566-d729-496e-b0d3-547e8773dbe7.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/admin/6b6ac356-46cc-4797-b7ba-1f2500125a71.jpg\",\"https://huanxing.oss-cn-beijing.aliyuncs.com/admin/8e64029a-ade8-46c7-b46d-532c4e4f49e7.jpg\"]', '1', 1000, '1790247983355805697', NULL, '1496677018553872385', '1528947409455599617', '2024-11-20 22:43:33', '2024-11-21 00:04:20', '1', '<p><br></p><p><img src=\"https://huanxing.oss-cn-beijing.aliyuncs.com/admin/e26d9fde-7baa-45ff-ac3d-d059b48c32dd.jpg\" alt=\"\" data-href=\"\" style=\"width: 100%;\"/></p><p><img src=\"https://huanxing.oss-cn-beijing.aliyuncs.com/admin/ef3b75e9-9d18-47cb-a4ca-1e5daa93ec82.jpg\" alt=\"\" data-href=\"\" style=\"width: 100%;\"/></p>', '1', 6999.00, 6999.00, '1590229800633634816', 'admin', 'admin', '1', '环兴商城自营旗舰店', '1', '1496677018553872385', '1528947409455599617', '0', NULL);
INSERT INTO `goods_spu` VALUES ('1859266702013911042', 'asdas', 'asdads', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/admin/e26d9fde-7baa-45ff-ac3d-d059b48c32dd.jpg\"]', '1', 11, '1790247983355805697', NULL, '1504307713140879362', '1504307782850211841', '2024-11-21 00:05:00', '2024-11-21 00:10:11', '1', '<p>123132</p>', '1', 0.00, 0.00, '1590229800633634816', 'admin', 'admin', '1', '环兴商城自营旗舰店', '1', '1504307713140879362', '1504307782850211841', '0', NULL);
INSERT INTO `goods_spu` VALUES ('1859268096586543105', '12313', '123132', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/admin/e26d9fde-7baa-45ff-ac3d-d059b48c32dd.jpg\"]', '1', 1230, '1790247983355805697', NULL, '1504307713140879362', '1504307782850211841', '2024-11-21 00:10:32', '2024-11-21 00:11:28', '1', '<p>123123</p>', '1', 0.00, 0.00, '1590229800633634816', 'admin', 'admin', '1', '环兴商城自营旗舰店', '1', '1504307713140879362', '1504307782850211841', '0', NULL);
INSERT INTO `goods_spu` VALUES ('1859268410819559425', '123', '123', '[\"https://huanxing.oss-cn-beijing.aliyuncs.com/admin/e26d9fde-7baa-45ff-ac3d-d059b48c32dd.jpg\"]', '1', 1232, '1790247983355805697', NULL, '1504307713140879362', '1504307782850211841', '2024-11-21 00:11:47', '2024-12-25 10:33:23', '0', '<p>1212</p>', '1', 0.00, 0.00, '1590229800633634816', 'admin', 'admin', '1', '环兴商城自营旗舰店', '1', '1504307713140879362', '1504307782850211841', '1', '');

-- ----------------------------
-- Table structure for goods_spu_specs
-- ----------------------------
DROP TABLE IF EXISTS `goods_spu_specs`;
CREATE TABLE `goods_spu_specs`  (
                                  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'PK',
                                  `spu_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品ID',
                                  `specs_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '规格ID',
                                  `sort` int NOT NULL COMMENT '排序字段',
                                  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                  `del_flag` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '逻辑删除：0.显示；1.隐藏；',
                                  `tenant_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户id',
                                  `create_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
                                  `update_by` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '修改人',
                                  `shop_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '店铺ID',
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '商品关联规格' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of goods_spu_specs
-- ----------------------------
INSERT INTO `goods_spu_specs` VALUES ('1585839785372528641', '1535187344403668993', '1532637637920346114', 0, '2022-06-10 17:09:20', '2022-11-09 22:10:34', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1603652730659713026', '1535191486845100033', '1532637637920346114', 0, '2022-06-10 17:25:47', '2022-11-09 22:10:34', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1603652730663907329', '1535191486845100033', '1532637571717451778', 1, '2022-06-10 17:25:47', '2022-11-09 22:10:34', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1603652855679332354', '1533298170302447618', '1532626600345128962', 0, '2022-06-05 12:02:34', '2022-11-09 22:10:34', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1603652855683526658', '1533298170302447618', '1532637637920346114', 1, '2022-06-05 12:02:36', '2022-11-09 22:10:34', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1613561854604328961', '1533792035048427522', '1533790070751006721', 0, '2022-06-06 20:45:00', '2022-11-09 22:10:34', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1613561854608523265', '1533792035048427522', '1533790184030769154', 1, '2022-06-06 20:45:00', '2022-11-09 22:10:34', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1634556054314573826', '1619358224880439298', '1533790070751006721', 0, '2023-01-28 23:34:22', '2023-01-28 23:34:22', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1634556054574620674', '1619358224880439298', '1533790184030769154', 1, '2023-01-28 23:34:22', '2023-01-28 23:34:22', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1644219950841319426', '1634555921480966145', '1533790070751006721', 0, '2023-03-11 22:04:36', '2023-03-11 22:04:36', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1644219951122337793', '1634555921480966145', '1533790184030769154', 1, '2023-03-11 22:04:36', '2023-03-11 22:04:36', '0', '1590229800633634816', NULL, NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1826218592883527682', '1826218575774961665', '1826218265664901122', 0, '2024-08-21 19:23:37', NULL, '1', '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1826218594905182210', '1826218575774961665', '1826218386737680385', 1, '2024-08-21 19:23:37', NULL, '1', '1590229800633634816', 'huawei', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1846013553896161282', '1826218575774961665', '1826218265664901122', 0, '2024-10-15 10:21:43', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1846013555397722113', '1826218575774961665', '1826218386737680385', 1, '2024-10-15 10:21:44', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1846013671038877697', '1826218575774961665', '1826218265664901122', 0, '2024-10-15 10:22:11', NULL, '0', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1846013672540438530', '1826218575774961665', '1826218386737680385', 1, '2024-10-15 10:22:11', NULL, '0', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859246203145834498', '1859246202889981954', '1859245110806466562', 0, '2024-11-20 22:43:33', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859262050107150337', '1859246202889981954', '1859245110806466562', 0, '2024-11-20 23:46:31', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859262199550201858', '1859246202889981954', '1859245110806466562', 0, '2024-11-20 23:47:06', NULL, '0', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859262199617310722', '1859246202889981954', '1532637637920346114', 1, '2024-11-20 23:47:06', NULL, '0', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859266702143934467', '1859266702013911042', '1859245110806466562', 0, '2024-11-21 00:05:00', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859266984978436098', '1859266702013911042', '1859245110806466562', 0, '2024-11-21 00:06:07', NULL, '0', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859266985045544961', '1859266702013911042', '1532626600345128962', 1, '2024-11-21 00:06:07', NULL, '0', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859268097047916546', '1859268096586543105', '1859245110806466562', 0, '2024-11-21 00:10:33', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859268163246616577', '1859268096586543105', '1859245110806466562', 0, '2024-11-21 00:10:48', NULL, '0', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859268163305336833', '1859268096586543105', '1532626600345128962', 1, '2024-11-21 00:10:48', NULL, '0', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859268411054440449', '1859268410819559425', '1859245110806466562', 0, '2024-11-21 00:11:47', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859268453211389953', '1859268410819559425', '1859245110806466562', 0, '2024-11-21 00:11:57', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859268453345607681', '1859268410819559425', '1532637571717451778', 1, '2024-11-21 00:11:57', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859269446699401217', '1859268410819559425', '1859245110806466562', 0, '2024-11-21 00:15:54', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859269446699401218', '1859268410819559425', '1532637571717451778', 1, '2024-11-21 00:15:54', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859269601376944129', '1859268410819559425', '1859245110806466562', 0, '2024-11-21 00:16:31', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859269601439858689', '1859268410819559425', '1532637571717451778', 1, '2024-11-21 00:16:31', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859271741910630401', '1859268410819559425', '1859245110806466562', 0, '2024-11-21 00:25:02', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859271741973544962', '1859268410819559425', '1532637571717451778', 1, '2024-11-21 00:25:02', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859271792179363842', '1859268410819559425', '1859245110806466562', 0, '2024-11-21 00:25:14', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859271792246472706', '1859268410819559425', '1532637571717451778', 1, '2024-11-21 00:25:14', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859271851071586306', '1859268410819559425', '1859245110806466562', 0, '2024-11-21 00:25:28', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859271851071586307', '1859268410819559425', '1532637571717451778', 1, '2024-11-21 00:25:28', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859272163220078594', '1859268410819559425', '1859245110806466562', 0, '2024-11-21 00:26:42', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859272163282993153', '1859268410819559425', '1532637571717451778', 1, '2024-11-21 00:26:42', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859272190873124865', '1859268410819559425', '1859245110806466562', 0, '2024-11-21 00:26:49', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859272190873124866', '1859268410819559425', '1532637571717451778', 1, '2024-11-21 00:26:49', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859272222099718145', '1859268410819559425', '1859245110806466562', 0, '2024-11-21 00:26:56', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859272222166827010', '1859268410819559425', '1532637571717451778', 1, '2024-11-21 00:26:56', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859272255733841921', '1859268410819559425', '1859245110806466562', 0, '2024-11-21 00:27:04', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859272255796756481', '1859268410819559425', '1532637571717451778', 1, '2024-11-21 00:27:04', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859416152774606850', '1859268410819559425', '1859245110806466562', 0, '2024-11-21 09:58:52', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859416152837521409', '1859268410819559425', '1532637571717451778', 1, '2024-11-21 09:58:52', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859416289047543809', '1859268410819559425', '1859245110806466562', 0, '2024-11-21 09:59:24', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859416289110458370', '1859268410819559425', '1532637571717451778', 1, '2024-11-21 09:59:24', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859416360774336514', '1859268410819559425', '1859245110806466562', 0, '2024-11-21 09:59:41', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859418983791394818', '1859268410819559425', '1859245110806466562', 0, '2024-11-21 10:10:07', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859420760632786945', '1859268410819559425', '1859245110806466562', 0, '2024-11-21 10:17:10', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859421033694560258', '1859268410819559425', '1859245110806466562', 0, '2024-11-21 10:18:15', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859421249973780481', '1859268410819559425', '1859245110806466562', 0, '2024-11-21 10:19:07', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859421599497760770', '1859268410819559425', '1859245110806466562', 0, '2024-11-21 10:20:30', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859422994418413569', '1859268410819559425', '1859245110806466562', 0, '2024-11-21 10:26:03', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1859426211801972738', '1859268410819559425', '1859245110806466562', 0, '2024-11-21 10:38:50', NULL, '1', '1590229800633634816', 'admin', NULL, NULL);
INSERT INTO `goods_spu_specs` VALUES ('1860348697930473474', '1859268410819559425', '1859245110806466562', 0, '2024-11-23 23:44:28', NULL, '0', '1590229800633634816', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for undo_log
-- ----------------------------
DROP TABLE IF EXISTS `undo_log`;
CREATE TABLE `undo_log`  (
                           `id` bigint NOT NULL AUTO_INCREMENT,
                           `branch_id` bigint NOT NULL,
                           `xid` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
                           `context` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
                           `rollback_info` longblob NOT NULL,
                           `log_status` int NOT NULL,
                           `log_created` datetime NOT NULL,
                           `log_modified` datetime NOT NULL,
                           `ext` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
                           PRIMARY KEY (`id`) USING BTREE,
                           UNIQUE INDEX `ux_undo_log`(`xid` ASC, `branch_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of undo_log
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;