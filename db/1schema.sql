-- 核心库
create database `huanxing_upms` default character set utf8mb4 collate utf8mb4_general_ci;
-- 小程序相关库
create database `huanxing_miniapp` default character set utf8mb4 collate utf8mb4_general_ci;

-- nacos配置相关库
create database `huanxing_nacos` default character set utf8mb4 collate utf8mb4_general_ci;

-- 支付相关库
create database `huanxing_pay` default character set utf8mb4 collate utf8mb4_general_ci;

-- 定时任务相关库
create database `huanxing_job` default character set utf8mb4 collate utf8mb4_general_ci;

-- 商城用户相关库
create database `huanxing_user` default character set utf8mb4 collate utf8mb4_general_ci;

-- 商城订单相关库
create database `huanxing_order` default character set utf8mb4 collate utf8mb4_general_ci;

-- 商城商品相关库
create database `huanxing_product` default character set utf8mb4 collate utf8mb4_general_ci;

-- 商城营销相关库
create database `huanxing_promotion` default character set utf8mb4 collate utf8mb4_general_ci;

-- 商城店铺相关库
create database `huanxing_shop` default character set utf8mb4 collate utf8mb4_general_ci;