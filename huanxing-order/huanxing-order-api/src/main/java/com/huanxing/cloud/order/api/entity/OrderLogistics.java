package com.huanxing.cloud.order.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.huanxing.cloud.order.api.enums.OrderLogisticsStateEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 物流信息
 *
 * @author lijx
 * @since 2022/2/22 14:28
 */
@Data
@Schema(description = "物流信息")
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName(value = "order_logistics")
public class OrderLogistics extends Model<OrderLogistics> {

	@Schema(description = "PK")
	@TableId(type = IdType.ASSIGN_ID)
	private String id;

	@Schema(description = "联系电话")
	private String telephone;

	@Schema(description = "收件人姓名")
	private String addresseeName;

	@Schema(description = "详细地址")
	private String detailAddress;

	@Schema(description = "邮政编码")
	private String postalCode;

	@Schema(description = "物流公司编码")
	private String logisticsCode;

	@Schema(description = "物流公司名称")
	private String logisticsName;

	@Schema(description = "物流单号")
	private String logisticsNo;

	@Schema(description = "物流状态：0在途，1揽收，2疑难，3签收，4退签，5派件，8清关，14拒签")
	private String state;

	@Schema(description = "店铺ID")
	private String shopId;

	@Schema(description = "物流状态描述：0在途，1揽收，2疑难，3签收，4退签，5派件，8清关，14拒签")
	@TableField(exist = false)
	private String stateDesc;

	public String getStateDesc() {
		if (this.state == null) {
			return null;
		}
		return OrderLogisticsStateEnum.getValue(this.state);
	}

	@Schema(description = "是否签收标记：0.未签收；1.已签收；")
	private String isCheck;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建人")
	private String createBy;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改人")
	private String updateBy;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建时间")
	private LocalDateTime createTime;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改时间")
	private LocalDateTime updateTime;

	@TableLogic
	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "逻辑删除：0.显示；1.隐藏；")
	private String delFlag;

	@Schema(description = "物流信息描述")
	private String logisticsDesc;

	@Schema(description = "租户id")
	private String tenantId;

	@Schema(description = "物流信息明细")
	@TableField(exist = false)
	private List<OrderLogisticsDetail> orderLogisticsDetailList;

}
