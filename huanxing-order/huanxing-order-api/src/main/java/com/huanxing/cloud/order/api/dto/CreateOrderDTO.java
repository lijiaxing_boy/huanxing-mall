package com.huanxing.cloud.order.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * 下单DTO
 *
 * @author lijx
 * @date 2022/6/10
 */
@Data
@Schema(description = "下单DTO")
public class CreateOrderDTO {

	@Schema(description = "用户ID")
	private String userId;

	@Schema(description = "配送方式：1.普通快递；2.上门自提")
	private String deliveryWay;

	@Schema(description = "支付类型：1.微信支付；2.支付宝支付")
	private String paymentType;

	@Schema(description = "交易类型")
	private String tradeType;

	@Schema(description = "订单类型：1.普通订单；")
	private String orderType;

	@Schema(description = "用户收货地址")
	private String userAddressId;

	@Schema(description = "订单创建方式：1.购物车下单；2.普通购买下单")
	private String createWay;

	@Schema(description = "关联Id（跟订单有关联的id）")
	private String relationId;

	@Schema(description = "活动Id （多人拼图主键）")
	private String eventId;

	@Schema(description = "店铺订单")
	private List<ShopOrderReqDTO> shopOrderReqList;

}
