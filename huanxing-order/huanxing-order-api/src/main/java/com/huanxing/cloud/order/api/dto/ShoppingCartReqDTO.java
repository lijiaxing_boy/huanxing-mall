package com.huanxing.cloud.order.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

@Data
@Schema(description = "购物车")
public class ShoppingCartReqDTO {

	@Schema(description = "用户主键")
	private String userId;

	@Schema(description = "spuId")
	private List<String> spuIds;

}
