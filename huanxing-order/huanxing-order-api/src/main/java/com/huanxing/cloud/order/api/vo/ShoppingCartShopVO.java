package com.huanxing.cloud.order.api.vo;

import com.huanxing.cloud.order.api.entity.ShoppingCart;
import lombok.Data;

import java.util.List;

@Data
public class ShoppingCartShopVO {

	private String shopId;

	private String shopName;

	private String logoUrl;

	private String shopType;

	private List<ShoppingCart> shoppingCartList;

}
