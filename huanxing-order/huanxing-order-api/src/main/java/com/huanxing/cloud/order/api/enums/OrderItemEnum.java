package com.huanxing.cloud.order.api.enums;

/**
 * 商城订单项状态枚举
 *
 * @author lijx
 * @date 2022/5/31
 */
public enum OrderItemEnum {

	/** 正常单 */
	NORMAL("0", "正常单"),
	/** 退款中 */
	REFUNDING("1", "退款中"),
	/** 退货退款中 */
	GOODS_REFUNDING("2", "退货退款中"),
	/** 已完成退款 */
	SUCCESS_REFUND("3", "已完成退款"),
	/** 已完成退货退款 */
	SUCCESS_GOODS_REFUND("4", "已完成退货退款");

	private String status;

	private String desc;

	OrderItemEnum(String status, String desc) {
		this.status = status;
		this.desc = desc;
	}

	/**
	 * 自己定义一个静态方法,通过code返回枚举描述
	 *
	 * @author lijx
	 * @date 2022/5/31
	 * @param status
	 * @return: java.lang.String
	 */
	public static String getValue(String status) {

		for (OrderItemEnum orderItemEnum : values()) {
			if (orderItemEnum.getStatus().equals(status)) {
				return orderItemEnum.getDesc();
			}
		}
		return null;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
