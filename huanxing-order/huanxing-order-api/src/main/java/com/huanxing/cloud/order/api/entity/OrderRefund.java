package com.huanxing.cloud.order.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.huanxing.cloud.order.api.enums.OrderRefundEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 商城退款单
 *
 * @author lijx
 * @date 2022/5/31
 */
@Data
@Schema(description = "商城退款单")
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName(value = "order_refund")
public class OrderRefund extends Model<OrderRefund> {

	@Schema(description = "PK")
	@TableId(type = IdType.ASSIGN_ID)
	private String id;

	@Schema(description = "订单主键")
	private String orderId;

	@Schema(description = "子订单主键")
	private String orderItemId;

	@Schema(description = "退款状态：1.退款中；2.退货退款中；11.同意退款；12.同意退货退款；21.拒绝退款；22.拒绝退货退款")
	private String status;

	@Schema(description = "退款是否到账：0.未到账；1.已到账")
	private String arrivalStatus;

	@Schema(description = "退款金额")
	private BigDecimal refundAmount;

	@Schema(description = "退款流水号")
	private String refundTradeNo;

	@Schema(description = "退款原因")
	private String refundReason;

	@Schema(description = "拒绝退款原因")
	private String refuseReason;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建人")
	private String createBy;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改人")
	private String updateBy;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建时间")
	private LocalDateTime createTime;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改时间")
	private LocalDateTime updateTime;

	@TableLogic
	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "逻辑删除：0.显示；1.隐藏；")
	private String delFlag;

	@Schema(description = "退款入账账户")
	private String userReceivedAccount;

	@Schema(description = "退款单号")
	private String refundId;

	@Schema(description = "用户Id")
	private String userId;

	@Schema(description = "租户id")
	private String tenantId;

	@Schema(description = "店铺ID")
	private String shopId;

	@Schema(description = "退款状态描述：1.退款中；2.退货退款中；11.同意退款；12.同意退货退款；21.拒绝退款；22.拒绝退货退款")
	@TableField(exist = false)
	private String statusDesc;

	public String getStatusDesc() {
		if (this.status == null) {
			return null;
		}
		return OrderRefundEnum.getValue(this.status);
	}

	@Schema(description = "子订单信息")
	@TableField(exist = false)
	private OrderItem orderItem;

}
