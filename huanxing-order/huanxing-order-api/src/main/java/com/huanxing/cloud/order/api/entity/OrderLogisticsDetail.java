package com.huanxing.cloud.order.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * 物流信息明细
 *
 * @author lijx
 * @since 2022/2/22 14:28
 */
@Data
@Schema(description = "物流信息明细")
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName(value = "order_logistics_detail")
public class OrderLogisticsDetail extends Model<OrderLogisticsDetail> {

	@Schema(description = "PK")
	@TableId(type = IdType.ASSIGN_ID)
	private String id;

	@Schema(description = "物流信息主键")
	private String logisticsId;

	@Schema(description = "物流时间")
	private LocalDateTime logisticsTime;

	@Schema(description = "物流信息")
	private String logisticsContext;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建人")
	private String createBy;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改人")
	private String updateBy;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建时间")
	private LocalDateTime createTime;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改时间")
	private LocalDateTime updateTime;

	@TableLogic
	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "逻辑删除：0.显示；1.隐藏；")
	private String delFlag;

	@Schema(description = "租户id")
	private String tenantId;

	@Schema(description = "店铺ID")
	private String shopId;

}
