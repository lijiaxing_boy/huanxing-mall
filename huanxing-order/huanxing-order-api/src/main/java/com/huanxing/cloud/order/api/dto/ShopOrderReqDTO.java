package com.huanxing.cloud.order.api.dto;

import com.huanxing.cloud.shop.api.vo.ShopInfoVO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

@Data
public class ShopOrderReqDTO {

	@Schema(description = "自提点id")
	private String shopSelfFetchId;

	@Schema(description = "备注")
	private String remark;

	@Schema(description = "店铺ID")
	private String shopId;

	@Schema(description = "店铺信息")
	private ShopInfoVO shopInfo;

	@Schema(description = "用户优惠券id")
	private String couponUserId;

	@Schema(description = "订单商品sku集合")
	private List<CreateOrderSkuReqDTO> skuReqList;

}
