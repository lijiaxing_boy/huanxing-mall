package com.huanxing.cloud.order.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.huanxing.cloud.order.api.constant.MallOrderConstants;
import com.huanxing.cloud.order.api.enums.OrderStatusEnum;
import com.huanxing.cloud.order.api.enums.OrderTypeEnum;
import com.huanxing.cloud.shop.api.vo.ShopInfoVO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 订单信息
 *
 * @author lijx
 * @since 2022/2/22 14:28
 */
@Data
@Schema(description = "订单信息")
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName(value = "order_info")
public class OrderInfo extends Model<OrderInfo> {

	@Schema(description = "PK")
	@TableId(type = IdType.ASSIGN_ID)
	private String id;

	@Schema(description = "用户主键")
	private String userId;

	@Schema(description = "配送方式：1.普通快递；2.上门自提；")
	private String deliveryWay;

	@Schema(description = "订单单号")
	private String orderNo;

	@Schema(description = "支付类型：1.微信支付；2.支付宝支付")
	private String paymentType;

	@Schema(description = "交易类型")
	private String tradeType;

	@Schema(description = "订单类型：1.普通订单；2.多人拼团；")
	private String orderType;

	@Schema(description = "备注")
	private String remark;

	@Schema(description = "支付状态：0.未支付；1.已支付;")
	private String payStatus;

	@Schema(description = "订单状态：1.待付款；2.待发货；3.待收货；4.已完成；5.退款中；6.拼团中；11.交易已关闭")
	private String status;

	@Schema(description = "评价状态：0.待评价；1.已平价;")
	private String appraiseStatus;

	@Schema(description = "订单总金额（元）")
	private BigDecimal totalPrice;

	@Schema(description = "运费（元）")
	private BigDecimal freightPrice;

	@Schema(description = "优惠券优惠金额（元）")
	private BigDecimal couponPrice;

	@Schema(description = "支付金额（总金额-优惠券优惠金额+运费 = 支付金额）")
	private BigDecimal paymentPrice;

	@Schema(description = "付款时间")
	private LocalDateTime paymentTime;

	@Schema(description = "发货时间")
	private LocalDateTime deliverTime;

	@Schema(description = "取消时间")
	private LocalDateTime cancelTime;

	@Schema(description = "收货时间")
	private LocalDateTime receiverTime;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建人")
	private String createBy;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改人")
	private String updateBy;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建时间")
	private LocalDateTime createTime;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改时间")
	private LocalDateTime updateTime;

	@TableLogic
	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "逻辑删除：0.显示；1.隐藏；")
	private String delFlag;

	@Schema(description = "微信支付单号")
	private String transactionId;

	@Schema(description = "订单物流ID")
	private String orderLogisticsId;

	@Schema(description = "关联Id（跟订单有关联的id）")
	private String relationId;

	@Schema(description = "活动Id （多人拼图主键）")
	private String eventId;

	@Schema(description = "用户优惠券id")
	private String couponUserId;

	@Schema(description = "租户id")
	private String tenantId;

	@Schema(description = "店铺ID")
	private String shopId;

	@Schema(description = "子订单集合")
	@TableField(exist = false)
	private List<OrderItem> orderItemList;

	@Schema(description = "物流信息")
	@TableField(exist = false)
	private OrderLogistics orderLogistics;

	@Schema(description = "订单状态描述：1.待付款；2.待发货；3.待收货；4.已完成；5.退款中；11.交易已关闭")
	@TableField(exist = false)
	private String statusDesc;

	@Schema(description = "订单类型描述：1.普通订单；2.多人拼团；")
	@TableField(exist = false)
	private String orderTypeDesc;

	@Schema(description = "物流公司编码")
	@TableField(exist = false)
	private String logisticsCode;

	@Schema(description = "物流公司名称")
	@TableField(exist = false)
	private String logisticsName;

	@Schema(description = "物流单号")
	@TableField(exist = false)
	private String logisticsNo;

	@Schema(description = "店铺信息")
	@TableField(exist = false)
	private ShopInfoVO shopInfo;

	@Schema(description = "搜索关键字")
	@TableField(exist = false)
	private String keyword;

	@Schema(description = "支付时间范围[开始时间,结束时间]")
	@TableField(exist = false)
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime[] paymentQueryTimes;

	public String getStatusDesc() {
		if (this.status == null) {
			return null;
		}
		if (this.status.equals(OrderStatusEnum.STATUS_2.getCode())
				&& this.deliveryWay.equals(MallOrderConstants.DELIVERY_WAY_2)) {
			return "待自提";
		}
		return OrderStatusEnum.getValue(this.status);
	}

	public String getOrderTypeDesc() {
		if (this.status == null) {
			return null;
		}
		return OrderTypeEnum.getValue(this.orderType);
	}

}
