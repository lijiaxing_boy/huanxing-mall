package com.huanxing.cloud.order.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.huanxing.cloud.order.api.enums.OrderItemEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 子订单信息
 *
 * @author lijx
 * @since 2022/2/22 14:28
 */
@Data
@Schema(description = "子订单信息")
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName(value = "order_item")
public class OrderItem extends Model<OrderItem> {

	@Schema(description = "PK")
	@TableId(type = IdType.ASSIGN_ID)
	private String id;

	@Schema(description = "订单主键")
	private String orderId;

	@Schema(description = "spuId")
	private String spuId;

	@Schema(description = "skuId")
	private String skuId;

	@Schema(description = "spu名称")
	private String spuName;

	@Schema(description = "商品图")
	private String picUrl;

	@Schema(description = "销售价格（元）")
	private BigDecimal salesPrice;

	@Schema(description = "购买数量")
	private Integer buyQuantity;

	@Schema(description = "总金额（元）")
	private BigDecimal totalPrice;

	@Schema(description = "运费（元）")
	private BigDecimal freightPrice;

	@Schema(description = "优惠券优惠金额（元）")
	private BigDecimal couponPrice;

	@Schema(description = "支付金额（总金额-优惠券优惠金额+运费 = 支付金额）")
	private BigDecimal paymentPrice;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建人")
	private String createBy;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改人")
	private String updateBy;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建时间")
	private LocalDateTime createTime;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改时间")
	private LocalDateTime updateTime;

	@TableLogic
	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "逻辑删除：0.显示；1.隐藏；")
	private String delFlag;

	@Schema(description = "状态：0.正常订单；1.退款中；2.退货退款中；3.已完成退款；4.已完成退货退款；")
	private String status;

	@Schema(description = "是否退款：0.否；1.是；")
	private String isRefund;

	@Schema(description = "租户id")
	private String tenantId;

	@Schema(description = "店铺ID")
	private String shopId;

	@Schema(description = "状态描述：0.正常订单；1.退款中；2.退货退款中；3.已完成退款；4.已完成退货退款；")
	@TableField(exist = false)
	private String statusDesc;

	public String getStatusDesc() {
		if (this.status == null) {
			return null;
		}
		return OrderItemEnum.getValue(this.status);
	}

	@Schema(description = "规格信息")
	private String specsInfo;

	@Schema(description = "商城退款单")
	@TableField(exist = false)
	private OrderRefund orderRefund;

	@Schema(description = "运费模板主键")
	@TableField(exist = false)
	private String freightTemplateId;

}
