package com.huanxing.cloud.order.api.constant;

/**
 * 商城订单常量
 *
 * @author lijx
 * @date 2022/6/8
 */
public class MallOrderConstants {

	/** 配送方式：1.普通快递；2.上门自提 */
	public static final String DELIVERY_WAY_1 = "1";

	public static final String DELIVERY_WAY_2 = "2";

	/** 支付类型：1.微信支付；2.支付宝支付 */
	public static final String PAYMENT_TYPE_0 = "0";

	public static final String PAYMENT_TYPE_1 = "1";

	public static final String PAYMENT_TYPE_2 = "2";

	/** 订单类型：1.普通订单； */
	public static final String ORDER_TYPE_1 = "1";

	/** 订单创建方式：1.购物车下单；2.普通购买下单 */
	public static final String ORDER_CREATE_WAY_1 = "1";

	/** 计价方式：1.按件数；2.按重量； */
	public static final String PRICING_TYPE_1 = "1";

	public static final String PRICING_TYPE_2 = "2";

	/** 物流回调接口 */
	public static final String NOTIFY_LOGISTICS_URL = "/mall-order/notify/logistics?logisticsId=%s&tenantId=%s";

}
