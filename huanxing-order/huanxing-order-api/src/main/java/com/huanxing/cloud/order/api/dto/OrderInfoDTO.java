package com.huanxing.cloud.order.api.dto;

import com.huanxing.cloud.order.api.entity.OrderItem;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 商城订单DTO
 *
 * @author lijx
 * @date 2022/6/10
 */
@Data
@Schema(description = "商城订单DTO")
public class OrderInfoDTO {

	@Schema(description = "用户ID")
	private String userId;

	@Schema(description = "配送方式：1.普通快递；2.上门自提")
	private String deliveryWay;

	@Schema(description = "订单单号")
	private String orderNo;

	@Schema(description = "支付类型：1.微信支付；2.支付宝支付")
	private String paymentType;

	@Schema(description = "交易类型")
	private String tradeType;

	@Schema(description = "订单类型：1.普通订单；")
	private String orderType;

	@Schema(description = "备注")
	private String remark;

	@Schema(description = "支付状态：0.未支付；1.已支付;")
	private String payStatus;

	@Schema(description = "订单状态：0.待付款；1.待发货；2.已完成")
	private String status;

	@Schema(description = "评价状态：0.待评价；1.已平价;")
	private String appraiseStatus;

	@Schema(description = "订单金额（元）")
	private BigDecimal salesPrice;

	@Schema(description = "订单总金额（元）")
	private BigDecimal totalPrice;

	@Schema(description = "运费（元）")
	private BigDecimal freightPrice;

	@Schema(description = "优惠券优惠金额（元）")
	private BigDecimal couponPrice;

	@Schema(description = "用户优惠券id")
	private String couponUserId;

	@Schema(description = "支付金额（总金额-优惠券优惠金额+运费 = 支付金额）")
	private BigDecimal paymentPrice;

	@Schema(description = "运费模板ID")
	private String freightTemplateId;

	@Schema(description = "用户收货地址")
	private String userAddressId;

	@Schema(description = "子订单集合")
	private List<OrderItem> orderItemList;

	@Schema(description = "订单创建方式：1.购物车下单；2.普通购买下单")
	private String createWay;

	@Schema(description = "关联Id（跟订单有关联的id）")
	private String relationId;

	@Schema(description = "活动Id （多人拼图主键）")
	private String eventId;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Schema(description = "查询开始时间")
	private LocalDateTime beginTime;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Schema(description = "查询结束时间")
	private LocalDateTime endTime;

}
