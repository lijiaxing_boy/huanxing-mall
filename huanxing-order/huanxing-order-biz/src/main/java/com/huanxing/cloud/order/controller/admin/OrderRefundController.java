package com.huanxing.cloud.order.controller.admin;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.common.log.annotation.SysLog;
import com.huanxing.cloud.order.api.entity.OrderRefund;
import com.huanxing.cloud.order.service.IOrderRefundService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * 商城退款单
 *
 * @author lijx
 * @date 2022/5/31
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/orderrefund")
@Tag(description = "orderrefund", name = "商城退款单")
public class OrderRefundController {

	private final IOrderRefundService orderRefundService;

	@Operation(summary = "退款单列表")
	@SaCheckPermission("order:orderrefund:page")
	@GetMapping("/page")
	public Result<IPage<OrderRefund>> page(Page page, OrderRefund orderRefund) {
		return Result.success(orderRefundService.adminPage(page, orderRefund));
	}

	@Operation(summary = "退款单查询")
	@SaCheckPermission("order:orderrefund:get")
	@GetMapping("/{id}")
	public Result<OrderRefund> page(@PathVariable("id") String id) {
		return Result.success(orderRefundService.getRefundById(id));
	}

	@SysLog("退款")
	@Operation(summary = "退款")
	@SaCheckPermission("order:orderrefund:refund")
	@PostMapping("/refund")
	public Result<Boolean> refund(@RequestBody OrderRefund orderRefund) {
		return Result.success(orderRefundService.refund(orderRefund));
	}

}
