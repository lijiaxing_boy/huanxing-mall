package com.huanxing.cloud.order.event;

import com.huanxing.cloud.order.api.entity.OrderItem;
import com.huanxing.cloud.order.api.entity.OrderRefund;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * @author: lijx
 * @date: 2023/4/24 11:57
 */
public class HxOrderRefundEvent extends ApplicationEvent {

	@Getter
	private final OrderRefund orderRefund;

	@Getter
	private final OrderItem orderItem;

	public HxOrderRefundEvent(Object source, OrderRefund orderRefund, OrderItem orderItem) {
		super(source);
		this.orderRefund = orderRefund;
		this.orderItem = orderItem;
	}

}
