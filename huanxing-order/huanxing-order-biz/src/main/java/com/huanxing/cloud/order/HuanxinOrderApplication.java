package com.huanxing.cloud.order;

import com.huanxing.cloud.common.job.annotation.HxEnableXxlJob;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 订单服务启动类
 *
 * @author lijx
 * @since 2022/2/26 16:38
 */
@HxEnableXxlJob
@EnableDubbo
@SpringBootApplication
public class HuanxinOrderApplication {

	public static void main(String[] args) {
		SpringApplication.run(HuanxinOrderApplication.class, args);
	}

}
