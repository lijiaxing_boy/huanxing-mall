package com.huanxing.cloud.order.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huanxing.cloud.order.api.entity.OrderLogisticsDetail;
import com.huanxing.cloud.order.mapper.OrderLogisticsDetailMapper;
import com.huanxing.cloud.order.service.IOrderLogisticsDetailService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 物流信息明细
 *
 * @author lijx
 * @since 2022/3/7 14:18
 */
@Service
@AllArgsConstructor
public class OrderLogisticsDetailServiceImpl extends ServiceImpl<OrderLogisticsDetailMapper, OrderLogisticsDetail>
		implements IOrderLogisticsDetailService {

}
