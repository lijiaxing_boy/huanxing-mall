package com.huanxing.cloud.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.order.api.entity.OrderItem;

/**
 * 子订单
 *
 * @author lijx
 * @since 2022/3/7 14:18
 */
public interface IOrderItemService extends IService<OrderItem> {

	/**
	 * 查询详情
	 *
	 * @author lijx
	 * @date 2022/7/1
	 * @param id
	 * @return: com.huanxing.cloud.mall.common.entity.OrderItem
	 */
	OrderItem getOrderItemById(String id);

}
