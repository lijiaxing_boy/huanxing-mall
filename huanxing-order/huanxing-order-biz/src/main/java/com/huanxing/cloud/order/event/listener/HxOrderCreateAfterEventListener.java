package com.huanxing.cloud.order.event.listener;

import com.huanxing.cloud.common.core.constant.RocketMqConstants;
import com.huanxing.cloud.order.api.constant.MallOrderConstants;
import com.huanxing.cloud.order.api.dto.OrderConsumerDTO;
import com.huanxing.cloud.order.api.entity.OrderInfo;
import com.huanxing.cloud.order.api.entity.OrderItem;
import com.huanxing.cloud.order.event.HxOrderCreateAfterEvent;
import com.huanxing.cloud.order.service.IShoppingCartService;
import com.huanxing.cloud.promotion.api.dto.CouponUserReqDTO;
import com.huanxing.cloud.promotion.api.enums.CouponUserStatusEnum;
import com.huanxing.cloud.promotion.api.remote.RemoteCouponUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @author: lijx
 * @date: 2023/4/24 11:57
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class HxOrderCreateAfterEventListener {

	private final RocketMQTemplate rocketMQTemplate;

	@DubboReference
	private final RemoteCouponUserService remoteCouponUserService;

	private final IShoppingCartService shoppingCartService;

	/**
	 * 清除购物车
	 * @param event
	 */
	@Async
	@EventListener(HxOrderCreateAfterEvent.class)
	public void clearShoppingCartEventListener(HxOrderCreateAfterEvent event) {
		if (!MallOrderConstants.ORDER_CREATE_WAY_1.equals(event.getCreateWay())) {
			return;
		}
		final OrderInfo orderInfo = event.getOrderInfo();
		final List<OrderItem> orderItemList = event.getOrderItemList();
		shoppingCartService.clear(orderInfo.getUserId(), orderItemList.stream()
			.map(OrderItem::getSpuId)
			.distinct() // 去重，如果有可能有重复的商品ID
			.toList());
	}

	/**
	 * rocketmq 延迟消息 30分钟取消订单
	 * @param event
	 */
	@Async
	@EventListener(HxOrderCreateAfterEvent.class)
	public void sendMqEventListener(HxOrderCreateAfterEvent event) {
		final OrderInfo orderInfo = event.getOrderInfo();
		OrderConsumerDTO orderConsumerDTO = new OrderConsumerDTO();
		orderConsumerDTO.setOrderId(orderInfo.getId());
		orderConsumerDTO.setTenantId(orderInfo.getTenantId());
		rocketMQTemplate.syncSend(RocketMqConstants.ORDER_CANCEL_TOPIC, new GenericMessage<>(orderConsumerDTO),
				RocketMqConstants.TIME_OUT, RocketMqConstants.ORDER_CANCEL_LEVEL);
	}

	/**
	 * TODO 发送微信小程序模板消息
	 * @param event
	 */
	@Async
	@EventListener(HxOrderCreateAfterEvent.class)
	public void sendWxMsgEventListener(HxOrderCreateAfterEvent event) {
		log.info("sendWxMsgEventListener");
	}

	/**
	 * 修改使用的优惠券状态
	 * @param event
	 */
	@Async
	@EventListener(HxOrderCreateAfterEvent.class)
	public void updateUserCouponStatusEventListener(HxOrderCreateAfterEvent event) {
		final OrderInfo orderInfo = event.getOrderInfo();
		if (!StringUtils.hasText(orderInfo.getCouponUserId())) {
			return;
		}
		CouponUserReqDTO couponUserReqDTO = new CouponUserReqDTO();
		couponUserReqDTO.setId(orderInfo.getCouponUserId());
		couponUserReqDTO.setCouponUserStatusEnum(CouponUserStatusEnum.STATUS_3);
		remoteCouponUserService.updateCouponUserStatus(couponUserReqDTO);

	}

}
