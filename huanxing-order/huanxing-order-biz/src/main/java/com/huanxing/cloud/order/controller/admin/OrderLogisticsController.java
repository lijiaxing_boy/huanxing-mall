package com.huanxing.cloud.order.controller.admin;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.order.api.entity.OrderLogistics;
import com.huanxing.cloud.order.service.IOrderLogisticsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 物流信息
 *
 * @author lijx
 * @since 2022/3/7 14:01
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/orderLogistics")
@Tag(description = "orderLogistics", name = "物流信息")
public class OrderLogisticsController {

	private final IOrderLogisticsService orderLogisticsService;

	@Operation(summary = "物流信息详情")
	@SaCheckPermission("order:orderLogistics:get")
	@GetMapping("/{id}")
	public Result<OrderLogistics> getById(@PathVariable("id") String id) {
		return Result.success(orderLogisticsService.getLogisticsById(id));
	}

}
