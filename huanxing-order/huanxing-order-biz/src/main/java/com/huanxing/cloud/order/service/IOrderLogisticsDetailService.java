package com.huanxing.cloud.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.order.api.entity.OrderLogisticsDetail;

/**
 * 物流信息明细
 *
 * @author lijx
 * @since 2022/3/7 14:18
 */
public interface IOrderLogisticsDetailService extends IService<OrderLogisticsDetail> {

}
