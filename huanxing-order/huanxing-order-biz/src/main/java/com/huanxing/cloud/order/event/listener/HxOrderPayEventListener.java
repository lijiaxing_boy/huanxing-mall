package com.huanxing.cloud.order.event.listener;

import com.huanxing.cloud.common.core.constant.CommonConstants;
import com.huanxing.cloud.order.api.entity.OrderInfo;
import com.huanxing.cloud.order.api.entity.OrderItem;
import com.huanxing.cloud.order.api.enums.OrderStatusEnum;
import com.huanxing.cloud.order.event.HxOrderPayEvent;
import com.huanxing.cloud.order.service.IOrderInfoService;
import com.huanxing.cloud.product.api.dto.GoodsSpuSalesVolumeReqDTO;
import com.huanxing.cloud.product.api.remote.RemoteGoodsSpuService;
import com.huanxing.cloud.promotion.api.dto.CouponUserReqDTO;
import com.huanxing.cloud.promotion.api.enums.CouponUserStatusEnum;
import com.huanxing.cloud.promotion.api.remote.RemoteCouponUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author: lijx
 * @date: 2023/4/24 11:57
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class HxOrderPayEventListener {

	private final IOrderInfoService orderInfoService;

	@DubboReference
	private final RemoteCouponUserService remoteCouponUserService;

	@DubboReference
	private final RemoteGoodsSpuService remoteGoodsSpuService;

	@EventListener(HxOrderPayEvent.class)
	public void hxPayEventListener(HxOrderPayEvent event) {
		// 获取订单信息
		final OrderInfo orderInfo = event.getOrder();
		if (null == orderInfo) {
			log.warn("order not found!");
			return;
		}
		orderInfo.setStatus(OrderStatusEnum.STATUS_2.getCode());
		orderInfo.setPayStatus(CommonConstants.YES);
		orderInfoService.updateById(orderInfo);
	}

	@EventListener(HxOrderPayEvent.class)
	public void hxPayCouponEventListener(HxOrderPayEvent event) {
		// 获取订单信息
		final OrderInfo orderInfo = event.getOrder();
		if (null == orderInfo) {
			log.warn("order not found!");
			return;
		}
		if (!StringUtils.hasText(orderInfo.getCouponUserId())) {
			return;
		}
		// 更新用户优惠券状态
		CouponUserReqDTO couponUserReqDTO = new CouponUserReqDTO();
		couponUserReqDTO.setId(orderInfo.getCouponUserId());
		couponUserReqDTO.setCouponUserStatusEnum(CouponUserStatusEnum.STATUS_1);
		remoteCouponUserService.updateCouponUserStatus(couponUserReqDTO);
	}

	@EventListener(HxOrderPayEvent.class)
	public void hxPayGoodsEventListener(HxOrderPayEvent event) {
		// 获取订单信息
		final OrderInfo orderInfo = event.getOrder();
		if (null == orderInfo) {
			log.warn("order not found!");
			return;
		}
		final List<OrderItem> orderItemList = event.getOrderItemList();
		// 增加销量
		List<GoodsSpuSalesVolumeReqDTO> goodsSpuSalesVolumeReqDTOList = orderItemList.stream().map(any -> {
			GoodsSpuSalesVolumeReqDTO goodsSpuSalesVolumeReqDTO = new GoodsSpuSalesVolumeReqDTO();
			goodsSpuSalesVolumeReqDTO.setSpuId(any.getSpuId());
			goodsSpuSalesVolumeReqDTO.setSalesVolume(any.getBuyQuantity());
			return goodsSpuSalesVolumeReqDTO;
		}).collect(Collectors.toList());
		remoteGoodsSpuService.updateSalesVolume(goodsSpuSalesVolumeReqDTOList);
	}

}
