package com.huanxing.cloud.order.event;

import com.huanxing.cloud.order.api.entity.OrderInfo;
import com.huanxing.cloud.order.api.entity.OrderItem;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.util.List;

/**
 * @author: lijx
 * @date: 2023/4/24 11:57
 */
public class HxOrderPayEvent extends ApplicationEvent {

	@Getter
	private final OrderInfo order;

	@Getter
	private final List<OrderItem> orderItemList;

	public HxOrderPayEvent(Object source, OrderInfo order, List<OrderItem> orderItemList) {
		super(source);
		this.order = order;
		this.orderItemList = orderItemList;
	}

}
