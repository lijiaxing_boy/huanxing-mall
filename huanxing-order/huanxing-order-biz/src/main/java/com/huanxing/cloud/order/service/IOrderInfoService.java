package com.huanxing.cloud.order.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.order.api.dto.*;
import com.huanxing.cloud.order.api.entity.OrderInfo;
import com.huanxing.cloud.order.api.vo.SettlementOrderVO;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 订单
 *
 * @author lijx
 * @since 2022/3/7 14:18
 */
public interface IOrderInfoService extends IService<OrderInfo> {

	/**
	 * 分页查询订单列表
	 *
	 * @author lijx
	 * @date 2022/6/11
	 * @param page
	 * @param orderInfo
	 * @return: com.baomidou.mybatisplus.core.metadata.IPage<com.huanxing.cloud.mall.common.entity.OrderInfo>
	 */
	IPage<OrderInfo> adminPage(Page page, OrderInfo orderInfo);

	/**
	 * 订单详情
	 *
	 * @author lijx
	 * @date 2022/6/11
	 * @param id
	 * @return: com.huanxing.cloud.mall.common.entity.OrderInfo
	 */
	OrderInfo getOrderById(String id);

	/**
	 * 订单发货
	 *
	 * @author lijx
	 * @date 2022/6/11
	 * @param orderInfo
	 * @return: java.lang.Object
	 */
	boolean deliverOrder(OrderInfo orderInfo);

	/**
	 * 订单取消
	 *
	 * @author lijx
	 * @date 2022/6/13
	 * @param orderInfo
	 * @return: boolean
	 */
	String cancelOrder(OrderInfo orderInfo);

	/**
	 * 支付金额统计
	 * @param orderInfoDTO
	 * @return
	 */
	BigDecimal getPaySumStatistics(OrderInfoDTO orderInfoDTO);

	/**
	 * 订单列表
	 *
	 * @author lijx
	 * @date 2022/6/11
	 * @param page
	 * @param orderInfo
	 * @return: com.baomidou.mybatisplus.core.metadata.IPage<com.huanxing.cloud.mall.common.entity.OrderInfo>
	 */
	IPage<OrderInfo> apiPage(Page page, OrderInfo orderInfo);

	/**
	 * 创建订单
	 *
	 * @author lijx
	 * @date 2022/6/11
	 * @param createOrderDTO
	 * @return: com.huanxing.cloud.mall.common.entity.OrderInfo
	 */
	List<OrderInfo> createOrder(CreateOrderDTO createOrderDTO);

	/**
	 * 订单确认收货/订单自提
	 *
	 * @author lijx
	 * @date 2022/6/11
	 * @param orderInfo
	 * @return: boolean
	 */
	boolean receiveOrder(OrderInfo orderInfo);

	/**
	 * 预支付(调用统一下单接口)
	 *
	 * @author lijx
	 * @date 2022/6/11
	 * @param prepayDTO
	 * @return: com.huanxing.cloud.mall.common.entity.OrderInfo
	 */
	Result<Object> prepay(PrepayDTO prepayDTO);

	/**
	 * 订单评价
	 * @param id 订单id
	 * @param orderAppraiseList 订单评价列表
	 * @return boolean
	 */
	boolean appraiseOrder(String id, List<OrderAppraiseDTO> orderAppraiseList);

	/**
	 * 订单统计
	 * @return
	 */
	List<Map<String, Object>> statistics();

	/**
	 * 订单结算
	 * @param settlementOrderDTO
	 * @return
	 */
	SettlementOrderVO settlementOrder(SettlementOrderDTO settlementOrderDTO);

}
