package com.huanxing.cloud.order.listener;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.LocalDateTimeUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.huanxing.cloud.common.core.constant.CommonConstants;
import com.huanxing.cloud.common.core.constant.RocketMqConstants;
import com.huanxing.cloud.common.myabtis.tenant.HxTenantContextHolder;
import com.huanxing.cloud.order.api.entity.OrderInfo;
import com.huanxing.cloud.order.api.entity.OrderItem;
import com.huanxing.cloud.order.event.HxOrderPayEvent;
import com.huanxing.cloud.order.service.IOrderInfoService;
import com.huanxing.cloud.order.service.IOrderItemService;
import com.huanxing.cloud.pay.api.constants.PayConstants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
@RocketMQMessageListener(topic = RocketMqConstants.PAY_NOTIFY_TOPIC, consumerGroup = RocketMqConstants.PAY_NOTIFY_TOPIC)
public class HuanxingPayListener implements RocketMQListener<String> {

	private final IOrderInfoService orderInfoService;

	private final IOrderItemService orderItemService;

	private final ApplicationEventPublisher applicationEventPublisher;

	@Override
	public void onMessage(String message) {
		final JSONObject msg = JSONObject.parseObject(message);
		final String tenantId = msg.getString(PayConstants.TENANT_ID);
		if (!StringUtils.hasText(tenantId)) {
			log.warn("tenantId empty! ");
			return;
		}
		HxTenantContextHolder.setTenantId(tenantId);
		final String orderNo = msg.getString(PayConstants.OUT_TRADE_NO);
		if (!StringUtils.hasText(orderNo)) {
			log.warn("orderNo empty! ");
			return;
		}

		final String paySuccessTime = msg.getString(PayConstants.PAY_SUCCESS_TIME);
		if (!StringUtils.hasText(paySuccessTime)) {
			log.warn("paySuccessTime empty! ");
			return;
		}
		final JSONObject extraParams = JSON.parseObject(msg.getString(PayConstants.EXTRA_PARAMS));
		if (CollectionUtils.isEmpty(extraParams)) {
			log.warn("extraParams empty! orderNo: " + orderNo);
			return;
		}
		final String payType = extraParams.getString(PayConstants.EXTRA_PARAMS_PAY_TYPE);
		if (!StringUtils.hasText(payType)) {
			log.warn("payType empty! orderNo: " + orderNo);
			return;
		}

		OrderInfo orderInfo = orderInfoService
			.getOne(Wrappers.<OrderInfo>lambdaQuery().eq(OrderInfo::getOrderNo, orderNo));
		if (null == orderInfo) {
			log.warn("order not found! orderNo: " + orderNo);
			return;
		}

		List<OrderItem> orderItemList = orderItemService
			.list(Wrappers.<OrderItem>lambdaQuery().eq(OrderItem::getOrderId, orderInfo.getId()));
		if (CollectionUtils.isEmpty(orderItemList)) {
			log.warn("order items not found! orderNo: " + orderNo);
			return;
		}

		if (CommonConstants.NO.equals(orderInfo.getPayStatus())) {
			orderInfo.setPaymentTime(LocalDateTimeUtil.parse(paySuccessTime, DatePattern.NORM_DATETIME_PATTERN));
			orderInfo.setPaymentType(payType);
			applicationEventPublisher.publishEvent(new HxOrderPayEvent(this, orderInfo, orderItemList));
		}
	}

}
