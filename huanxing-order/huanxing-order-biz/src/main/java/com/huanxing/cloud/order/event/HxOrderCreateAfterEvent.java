package com.huanxing.cloud.order.event;

import com.huanxing.cloud.order.api.entity.OrderInfo;
import com.huanxing.cloud.order.api.entity.OrderItem;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.util.List;

/**
 * 订单创建后事件
 *
 * @author: lijx
 * @date: 2024/6/01 11:57
 */
public class HxOrderCreateAfterEvent extends ApplicationEvent {

	@Getter
	private final OrderInfo orderInfo;

	@Getter
	private final List<OrderItem> orderItemList;

	/**
	 * 订单创建方式：1.购物车下单；2.普通购买下单
	 */
	@Getter
	private final String createWay;

	public HxOrderCreateAfterEvent(Object source, OrderInfo orderInfo, List<OrderItem> orderItemList,
			String createWay) {
		super(source);
		this.orderInfo = orderInfo;
		this.orderItemList = orderItemList;
		this.createWay = createWay;
	}

}
