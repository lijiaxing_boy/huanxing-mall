package com.huanxing.cloud.order.event;

import com.huanxing.cloud.order.api.entity.OrderInfo;
import com.huanxing.cloud.order.api.entity.OrderItem;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.util.List;

/**
 * 订单创建前事件
 *
 * @author: lijx
 * @date: 2024/6/01 11:57
 */
public class HxOrderCreateBeforeEvent extends ApplicationEvent {

	@Getter
	private final OrderInfo orderInfo;

	@Getter
	private final List<OrderItem> orderItemList;

	public HxOrderCreateBeforeEvent(Object source, OrderInfo orderInfo, List<OrderItem> orderItemList) {
		super(source);
		this.orderInfo = orderInfo;
		this.orderItemList = orderItemList;
	}

}
