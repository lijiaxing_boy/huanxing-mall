package com.huanxing.cloud.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huanxing.cloud.order.api.entity.OrderRefund;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;

/**
 * 商城退款单
 *
 * @author lijx
 * @date 2022/5/31
 */
@Mapper
public interface OrderRefundMapper extends BaseMapper<OrderRefund> {

	/**
	 * 商城退款单列表
	 *
	 * @author lijx
	 * @date 2022/5/31
	 * @param page
	 * @param orderRefund
	 * @return: com.baomidou.mybatisplus.core.metadata.IPage<com.huanxing.cloud.mall.common.entity.OrderRefund>
	 */
	IPage<OrderRefund> selectAdminPage(Page page, @Param("query") OrderRefund orderRefund);

	OrderRefund selectRefundById(Serializable id);

	/**
	 * 通过子订单ID查询退款单
	 *
	 * @author lijx
	 * @date 2022/7/1
	 * @param orderItemId
	 * @return: com.huanxing.cloud.mall.common.entity.OrderRefund
	 */
	OrderRefund selectByOrderItemId(String orderItemId);

}
