package com.huanxing.cloud.order.event.listener;

import com.huanxing.cloud.order.event.HxOrderCreateBeforeEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * @author: lijx
 * @date: 2023/4/24 11:57
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class HxOrderCreateBeforeEventListener {

	/**
	 * TODO 拼团活动订单
	 * @param event
	 */
	@EventListener(HxOrderCreateBeforeEvent.class)
	public void groupEventListener(HxOrderCreateBeforeEvent event) {

	}

	/**
	 * TODO 秒杀活动订单
	 * @param event
	 */
	@EventListener(HxOrderCreateBeforeEvent.class)
	public void seckillEventListener(HxOrderCreateBeforeEvent event) {

	}

}
