package com.huanxing.cloud.user.api.constants;

public interface MallUserConstants {

	/**
	 * 用户等级：0、普通用户；1.会员
	 */
	String USER_GRADE_0 = "0";

	String USER_GRADE_1 = "1";

}
