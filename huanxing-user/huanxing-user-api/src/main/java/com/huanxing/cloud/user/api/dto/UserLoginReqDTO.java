package com.huanxing.cloud.user.api.dto;

import lombok.Data;

@Data
public class UserLoginReqDTO {

	private String appId;

	private String jsCode;

	private String clientType;

	private Integer shareUserNumber;

	private String phone;

	private String password;

	private String code;

	private String wxUserId;

}
