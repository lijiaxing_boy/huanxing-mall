package com.huanxing.cloud.user.api.remote;

import com.huanxing.cloud.user.api.entity.UserInfo;
import com.huanxing.cloud.user.api.vo.UserInfoVO;

public interface RemoteMallUserService {

	/**
	 * 根据手机号获取用户信息
	 * @param phone 手机号
	 * @param clientType 客户端类型
	 * @return 用户信息
	 */
	UserInfo getInfoByPhone(String phone, String clientType);

	UserInfoVO getUserById(String userId);

	UserInfoVO getUserByPhone(String phone);

}
