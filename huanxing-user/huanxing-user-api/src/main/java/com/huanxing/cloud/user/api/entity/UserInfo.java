package com.huanxing.cloud.user.api.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.huanxing.cloud.common.core.annotation.Desensitization;
import com.huanxing.cloud.common.core.desensitization.MobilePhoneDesensitization;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 商城用户
 *
 * @author lijx
 * @since 2022/2/22 14:28
 */
@Data
@Schema(description = "商城用户")
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName(value = "user_info")
public class UserInfo extends Model<UserInfo> {

	@Schema(description = "PK")
	@TableId(type = IdType.ASSIGN_ID)
	private String id;

	@Schema(description = "用户编号")
	private Integer userNumber;

	@Schema(description = "昵称")
	private String nickName;

	@Schema(description = "手机号")
	@Desensitization(MobilePhoneDesensitization.class)
	private String phone;

	@Schema(description = "密码")
	private String password;

	@Schema(description = "用户等级：0、普通用户；1.会员")
	private String userGrade;

	@Schema(description = "性别：1、男；2、女；0、未知；")
	private String sex;

	@Schema(description = "头像")
	private String avatarUrl;

	@Schema(description = "所在城市")
	private String city;

	@Schema(description = "所在国家")
	private String country;

	@Schema(description = "所在省份")
	private String province;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建人")
	private String createBy;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改人")
	private String updateBy;

	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "创建时间")
	private LocalDateTime createTime;

	@TableField(fill = FieldFill.UPDATE)
	@Schema(description = "修改时间")
	private LocalDateTime updateTime;

	@TableLogic
	@TableField(fill = FieldFill.INSERT)
	@Schema(description = "逻辑删除：0.显示；1.隐藏；")
	private String delFlag;

	@Schema(description = "用户来源：微信小程序.WX-MA；普通H5.H5；APP.APP；")
	private String userSource;

	@Schema(description = "上级用户主键")
	private String parentId;

	@Schema(description = "账户余额（元）")
	private BigDecimal accountBalance;

	@Schema(description = "租户id")
	private String tenantId;

	@Schema(description = "积分")
	private Integer point;

}
