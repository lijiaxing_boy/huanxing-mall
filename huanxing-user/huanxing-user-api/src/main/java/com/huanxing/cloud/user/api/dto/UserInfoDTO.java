package com.huanxing.cloud.user.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * 用户DTO
 *
 * @author lijx
 * @date 2022/6/10
 */
@Data
@Schema(description = "用户DTO")
public class UserInfoDTO {

	@Schema(description = "appId")
	private String appId;

	@Schema(description = "用户主键")
	private String id;

	@Schema(description = "手机号")
	private String phone;

	@Schema(description = "密码")
	private String password;

	@Schema(description = "验证码")
	private String code;

	@Schema(description = "分享用户编号")
	private Integer shareUserNumber;

	@Schema(description = "上级用户主键")
	private String parentId;

	@Schema(description = "二级用户主键")
	private String secondParentId;

}
