package com.huanxing.cloud.user.api.vo;

import com.huanxing.cloud.common.core.annotation.Desensitization;
import com.huanxing.cloud.common.core.desensitization.MobilePhoneDesensitization;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 用户信息VO
 *
 * @author lijx
 * @date 2022/7/11
 */
@Data
@Schema(description = "用户信息VO")
public class UserInfoVO implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;

	@Schema(description = "PK")
	private String id;

	@Schema(description = "昵称")
	private String nickName;

	@Schema(description = "头像")
	private String avatarUrl;

	@Schema(description = "手机号")
	@Desensitization(MobilePhoneDesensitization.class)
	private String phone;

	@Schema(description = "上级用户主键")
	private String parentId;

	@Schema(description = "密码")
	private String password;

	@Schema(description = "租户id")
	private String tenantId;

}
