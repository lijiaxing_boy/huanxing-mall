package com.huanxing.cloud.user.api.remote;

import com.huanxing.cloud.user.api.entity.UserAddress;

/**
 * 用户地址远程调用
 *
 * @author lijx
 * @date 2024-11-14
 */
public interface RemoteUserAddressService {

	UserAddress getById(String id);

}
