package com.huanxing.cloud.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.user.api.entity.UserAddress;

/**
 * 用户收货地址
 *
 * @author lijx
 * @since 2022/2/22 15:03
 */
public interface IUserAddressService extends IService<UserAddress> {

}
