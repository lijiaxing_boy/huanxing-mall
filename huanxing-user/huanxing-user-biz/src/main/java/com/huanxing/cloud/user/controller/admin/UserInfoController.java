package com.huanxing.cloud.user.controller.admin;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.date.LocalDateTimeUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.user.api.entity.UserInfo;
import com.huanxing.cloud.user.service.IUserInfoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * 商城用户
 *
 * @author lijx
 * @since 2022/3/1 10:13
 */
@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/userinfo")
@Tag(description = "userinfo", name = "商城用户")
public class UserInfoController {

	private final IUserInfoService userInfoService;

	@Operation(summary = "商城用户分页列表")
	@SaCheckPermission("user:userinfo:page")
	@GetMapping("/page")
	public Result page(Page page, UserInfo userInfo) {
		return Result.success(userInfoService.getPage(page, userInfo));
	}

	@Operation(summary = "商城用户查询")
	@GetMapping("/{id}")
	public Result page(@PathVariable("id") String id) {
		return Result.success(userInfoService.getUserById(id));
	}

	@Operation(summary = "商城用户数量查询")
	@GetMapping("/count")
	public Result count(UserInfo userInfo) {
		return Result.success(userInfoService.count(Wrappers.lambdaQuery(userInfo)));
	}

	@Operation(summary = "商城用户统计数量查询")
	@GetMapping("/statistics")
	public Result statistics() {
		long allCount = userInfoService.count(Wrappers.lambdaQuery());
		LocalDateTime localDateTime = LocalDateTime.now();
		long todayCount = userInfoService.count(Wrappers.<UserInfo>lambdaQuery()
			.ge(UserInfo::getCreateTime, LocalDateTimeUtil.beginOfDay(localDateTime))
			.le(UserInfo::getCreateTime, LocalDateTimeUtil.endOfDay(localDateTime)));
		Map<String, Object> rt = new HashMap<>();
		// 今日数量
		rt.put("todayCount", todayCount);
		// 全部数量
		rt.put("allCount", allCount);
		return Result.success(rt);
	}

}
