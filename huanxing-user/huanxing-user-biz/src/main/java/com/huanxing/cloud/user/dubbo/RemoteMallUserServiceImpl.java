package com.huanxing.cloud.user.dubbo;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.huanxing.cloud.user.api.remote.RemoteMallUserService;
import com.huanxing.cloud.user.api.entity.UserInfo;
import com.huanxing.cloud.user.api.vo.UserInfoVO;
import com.huanxing.cloud.user.service.IUserInfoService;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * @author lijia
 * @description
 * @date 2024/11/23
 */
@Service
@DubboService
@RequiredArgsConstructor
public class RemoteMallUserServiceImpl implements RemoteMallUserService {

	private final IUserInfoService userInfoService;

	@Override
	public UserInfo getInfoByPhone(String phone, String clientType) {
		UserInfo userInfo = userInfoService.getOne(Wrappers.<UserInfo>lambdaQuery().eq(UserInfo::getPhone, phone));
		if (ObjectUtil.isNull(userInfo)) {
			userInfo = userInfoService.createUser(phone, clientType);

		}
		return userInfo;
	}

	@Override
	public UserInfoVO getUserById(String userId) {
		UserInfo userInfo = userInfoService.getById(userId);
		if (Objects.isNull(userInfo)) {
			return null;
		}
		return BeanUtil.copyProperties(userInfo, UserInfoVO.class);
	}

	@Override
	public UserInfoVO getUserByPhone(String phone) {
		UserInfo userInfo = userInfoService.getOne(Wrappers.<UserInfo>lambdaQuery().eq(UserInfo::getPhone, phone));
		if (Objects.isNull(userInfo)) {
			return null;
		}
		return BeanUtil.copyProperties(userInfo, UserInfoVO.class);
	}

}
