package com.huanxing.cloud.user.controller.app;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.huanxing.cloud.common.core.constant.CommonConstants;
import com.huanxing.cloud.common.core.util.Result;
import com.huanxing.cloud.common.security.util.SecurityUtils;
import com.huanxing.cloud.user.api.entity.UserAddress;
import com.huanxing.cloud.user.service.IUserAddressService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * 用户收货地址
 *
 * @author lijx
 * @since 2022/2/23 13:11
 */
@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/app/address")
@Tag(description = "address", name = "用户收货地址-API")
public class AppUserAddressController {

	private final IUserAddressService userAddressService;

	@Operation(summary = "收货地址分页列表")
	@GetMapping("/page")
	public Result page(Page page, UserAddress userAddress) {
		userAddress.setUserId(SecurityUtils.getUser().getUserId());
		return Result.success(userAddressService.page(page, Wrappers.lambdaQuery(userAddress)));
	}

	@Operation(summary = "查询用户默认收货地址")
	@GetMapping("/default")
	public Result getDefault() {
		UserAddress userAddress = new UserAddress();
		userAddress.setUserId(SecurityUtils.getUser().getUserId());
		userAddress.setIsDefault(CommonConstants.YES);
		return Result.success(userAddressService.getOne(Wrappers.lambdaQuery(userAddress)));
	}

	@Operation(summary = "新增/编辑用户收货地址")
	@PostMapping
	public Result saveOrUpdate(@RequestBody UserAddress userAddress) {
		userAddress.setUserId(SecurityUtils.getUser().getUserId());
		return Result.success(userAddressService.saveOrUpdate(userAddress));
	}

	/**
	 * 通过id查询
	 * @param id PK
	 * @return UserAddress
	 */
	@GetMapping("/{id}")
	public Result<UserAddress> getById(@PathVariable("id") String id) {
		return Result.success(userAddressService.getById(id));

	}

	/**
	 * 通过id查询
	 * @param id PK
	 * @return UserAddress
	 */
	@DeleteMapping("/{id}")
	public Result<Boolean> deleteById(@PathVariable("id") String id) {
		return Result.success(userAddressService.removeById(id));

	}

}
