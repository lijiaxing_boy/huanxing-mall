package com.huanxing.cloud.user.dubbo;

import com.huanxing.cloud.user.api.remote.RemoteUserAddressService;
import com.huanxing.cloud.user.api.entity.UserAddress;
import com.huanxing.cloud.user.service.IUserAddressService;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.stereotype.Service;

/**
 * @author lijia
 * @description
 * @date 2024/11/23
 */
@Service
@DubboService
@RequiredArgsConstructor
public class RemoteUserAddressServiceImpl implements RemoteUserAddressService {

	private final IUserAddressService userAddressService;

	@Override
	public UserAddress getById(String id) {
		return userAddressService.getById(id);
	}

}
