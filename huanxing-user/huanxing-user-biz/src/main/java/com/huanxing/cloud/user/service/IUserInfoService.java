package com.huanxing.cloud.user.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huanxing.cloud.user.api.entity.UserInfo;
import com.huanxing.cloud.user.api.vo.UserRespVO;

import java.math.BigDecimal;

/**
 * 商城用户
 *
 * @author lijx
 * @since 2022/2/26 16:36
 */
public interface IUserInfoService extends IService<UserInfo> {

	/**
	 * 验证手机号是否存在
	 * @param phone 手机号
	 * @author lijx
	 * @date 2022/3/10 10:07
	 * @return: boolean
	 */
	boolean checkPhone(String phone);

	/**
	 * 分页查询商城用户列表
	 * @param page 分页对象
	 * @param userInfo 查询条件
	 * @author lijx
	 * @date 2022/7/12
	 * @return: com.baomidou.mybatisplus.core.metadata.IPage<com.huanxing.cloud.mall.common.entity.UserInfo>
	 */
	IPage<UserRespVO> getPage(Page page, UserInfo userInfo);

	/**
	 * 更新账户余额
	 * @param userId 用户ID
	 * @param billAmount 账单金额
	 * @author lijx
	 * @date 2022/9/26
	 * @return: boolean
	 */
	boolean updateAccountBalance(String userId, BigDecimal billAmount);

	/**
	 * 根据用户ID获取用户信息
	 * @param id 用户ID
	 * @return
	 */
	UserRespVO getUserById(String id);

	/**
	 * 根据手机号和来源类型创建用户
	 * @param phone 手机号
	 * @param clientType 来源类型
	 * @return
	 */
	UserInfo createUser(String phone, String clientType);

	/**
	 * 更新用户积分
	 * @param userId 用户ID
	 * @param point 积分
	 */
	boolean updatePoint(String userId, int point);

}
